# European Data Portal Licensing Assistant

 * Application is subject to updates....

## Requirements

The requirements have to match exactly!

* JDK 1.8
* Node 5.12.0
* npm 3.8.6
* jspm 0.17.0-beta.16 (npm -g install jspm@0.17.0-beta.16)

## Build

* Clone Repository
```
$ git clone https://gitlab.com/european-data-portal/licensing-assistant.git
$ cd licensing-assistant

```
* Build Frontend
```
$ cd src/main/web/src
$ jspm build lib.js ../../webapp/static/lib.js
```
* Build Backend
```
$ mvn clean package -Dbase.url=https://www.europeandataportal.eu
$ mvn exec:java
```