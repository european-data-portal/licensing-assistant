/**
 * Created by aos on 3/31/15.
 */
/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../api.ts" />

import './licensing_wizard.scss!';
import {Model} from '../model.ts!';

import _ from 'lodash';
import i18next from 'i18next';
import {API} from '../api.ts!';

import _tplTemplate = require('./template.jade!');
import _tplIntro = require('./intro.jade!');
import _tplDerivative = require('./derivative.jade!');
import _tplPublisher = require('./publisher.jade!');

  export class LicensingWizard {
    private license:License;
      private tplOpts = {
          'imports': {
              'jq': jQuery,
              'i18n' : (p) => i18next.t(p)
          }
      };
      private template:_.TemplateExecutor = _.template(_tplTemplate.default(), this.tplOpts);
      private intro:_.TemplateExecutor = _.template(_tplIntro.default(), this.tplOpts);

      private tplDerivative:_.TemplateExecutor = _.template(_tplDerivative.default(), this.tplOpts);
      private tplPublisher:_.TemplateExecutor = _.template(_tplPublisher.default(), this.tplOpts);

    private derivative = {
      items: [
        {
          id: "q1",
          text: "Are the incorporated datasets protected under copyright law or database rights?",
          options: [
            {
              label: "Yes",
              goto: "q2"
            },
            {
              label: "No",
              goto: "n1"
            }
          ]
        },
        {
          id: "q2",
          text: "Did you modify the original work?",
          options: [
            {
              label: "Yes",
              goto: "end"
            },
            {
              label: "No",
              goto: "n1"
            }
          ]
        },
        {
          id: "n1",
          text: "Your work does not constitute a derivative work. However, contractual restrictions might still apply.<br /><br /> EXAMPLE"
        },
        {
          id: "end",
          text: "Your work most likely constitutes a derivative work. Please have a look at compatible licenses here: XXXX"
        }
      ],
      start: "q1",
      title: "Is my work that incorporates other datasets considered a derivative work of theirs?"
    };
    private publisher = {
      items: [
        {
          id: "q1",
          text: "Did you author the dataset?",
          options: [
            {
              label: "Yes",
              goto: "q2"
            }, {
              label: "No",
              goto: "n1"
            }
          ]
        },
        {
          id: "n1",
          text: "Please contact the original dataset author for coordinating publication."
        },
        {
          id: "q2",
          text: "Is the dataset your intellectual creation by selection or arrangement?<br /><br /><br />" +
          "For example, a list of phone numbers of all the people working in an organisation will nto be covered by copyright. The author cannot show any creativity in the selection of the data" +
          "(which is simply all the people working there) nor in the choice of data (simple names and phone numbers). Even if the collection of data" +
          "were very large or if a lot of effort was involved, no creativity was required and therefore no copyright rules apply.<br />",
          options: [
            {
              label: "Yes",
              goto: "y1"
            },
            {
              label: "No",
              goto: "q3"
            }
          ]
        },
        {
          id: "y1",
          text: "The dataset is likely protected under copyright law and you may choose an applicable license."
        },
        {
          id: "q3",
          text: "Was there a qualitatively and/or quantitatively substantial investment in either the obtaining, verification or presentation of the contents?<br /><br />" +
          "For example, a database containing exclusive geographic information related to an entire country will likely be covered by this. This is because such a database will normally require a significant effort to create in terms of time, money, effort etc.",
          options: [
            {
              label: "Yes",
              goto: "y2"
            }, {
              label: "No",
              goto: "n2"
            }
          ]
        },
        {
          id: "y2",
          text: "Your database is likely protected under database rights (also known as 'sui generis rights'). You can find a list of licenses covering these rights here: XXX"
        }, {
          id: "n2",
          text: "Unfortunately, your dataset most likely does not fall under copyright and/or database rights. Please consult a lawyer for further advice on this topic."
        }

      ],
      start: "q1",
      title: "I wish to publish a new dataset. Will it be protected by law?"
    }

    constructor(private el:JQuery) {
      this.showIntro();
    }

    private showIntro() {
      this.el = this.el.html(this.intro());
      this.el.find("#la-wizard-btn-publish").unbind("click").click((e) => {
        e.preventDefault();
        this.el = this.el.html(this.tplPublisher());
      });
      this.el.find("#la-wizard-btn-derivative").unbind("click").click((e) => {
        e.preventDefault();
        this.el = this.el.html(this.tplDerivative());
      });
    }

    private tmp() {
      this.el = this.el.html(this.template());
      var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

      allWells.hide();

      navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
          $item = $(this);

        if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
        }
      });

      allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

        if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
      });

      $('div.setup-panel div a.btn-primary').trigger('click');
    }
  }
