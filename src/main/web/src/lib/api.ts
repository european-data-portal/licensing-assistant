/**
 * Created by aos on 3/31/15.
 */
/// <reference path="typings/tsd.d.ts" />
/// <reference path="config.ts" />

import _ from 'lodash';
import Promise from 'bluebird';
import * as Config from './config.ts!';
import {License,LicenseTerm,Error} from './model.ts!';

export module API {
  var _licenses:License[] = null;
  var _licenseTerms:LicenseTerm[] = null;

  function path(method:string):string {
    return Config.PREFIX + "/" + method;
  }

  function get<T>(method:string):Promise<T> {
    return Promise.resolve($.getJSON(path(method))).then((data) => {
      if (data.code) return Promise.reject(data);
      else return data;
    });
  }

  export function licenses():Promise<License[]> {
    if (_licenses) return Promise.resolve(_licenses);
    else {
      return get<License[]>("licenses").then((lic) => {
        _licenses = lic;
        return lic;
      });
    }
  }

  export function terms():Promise<LicenseTerm[]> {
    if (_licenseTerms) return Promise.resolve(_licenseTerms);
    else return licenses().then((lic:License[]) => {
      var terms: LicenseTerm[] = _.flatMap(lic, "terms");
      _licenseTerms = _.uniqBy<LicenseTerm>(_.flatten(terms), "id");
      return _licenseTerms;
    });
  }

  export function license(shortName:string):Promise<License> {
    if (_licenses) {
      var found:License = _.find(_licenses, (val) => val.shortName === shortName);
      if (found) return Promise.resolve(found);
      else return Promise.reject<License>(new Error(42, "license not found"));
    } else return get<License>("licenses/" + shortName);
  }
}
