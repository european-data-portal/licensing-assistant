$(function() {
    function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }

    window.showLicense = function(name) {
        document.location.href = "?license_id=" + encodeURIComponent(name);
    };

    new LicenseAssistant.ShowLicense($("#la_target"), getParameterByName("license_id"));
});
