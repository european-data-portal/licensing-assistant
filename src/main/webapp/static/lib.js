!function(e){function r(e,r,o){return 4===arguments.length?t.apply(this,arguments):void n(e,{declarative:!0,deps:r,declare:o})}function t(e,r,t,o){n(e,{declarative:!1,deps:r,executingRequire:t,execute:o})}function n(e,r){r.name=e,e in v||(v[e]=r),r.normalizedDeps=r.deps}function o(e,r){if(r[e.groupIndex]=r[e.groupIndex]||[],-1==g.call(r[e.groupIndex],e)){r[e.groupIndex].push(e);for(var t=0,n=e.normalizedDeps.length;n>t;t++){var a=e.normalizedDeps[t],u=v[a];if(u&&!u.evaluated){var d=e.groupIndex+(u.declarative!=e.declarative);if(void 0===u.groupIndex||u.groupIndex<d){if(void 0!==u.groupIndex&&(r[u.groupIndex].splice(g.call(r[u.groupIndex],u),1),0==r[u.groupIndex].length))throw new TypeError("Mixed dependency cycle detected");u.groupIndex=d}o(u,r)}}}}function a(e){var r=v[e];r.groupIndex=0;var t=[];o(r,t);for(var n=!!r.declarative==t.length%2,a=t.length-1;a>=0;a--){for(var u=t[a],i=0;i<u.length;i++){var s=u[i];n?d(s):l(s)}n=!n}}function u(e){return y[e]||(y[e]={name:e,dependencies:[],exports:{},importers:[]})}function d(r){if(!r.module){var t=r.module=u(r.name),n=r.module.exports,o=r.declare.call(e,function(e,r){if(t.locked=!0,"object"==typeof e)for(var o in e)n[o]=e[o];else n[e]=r;for(var a=0,u=t.importers.length;u>a;a++){var d=t.importers[a];if(!d.locked)for(var i=0;i<d.dependencies.length;++i)d.dependencies[i]===t&&d.setters[i](n)}return t.locked=!1,r},r.name);t.setters=o.setters,t.execute=o.execute;for(var a=0,i=r.normalizedDeps.length;i>a;a++){var l,s=r.normalizedDeps[a],c=v[s],f=y[s];f?l=f.exports:c&&!c.declarative?l=c.esModule:c?(d(c),f=c.module,l=f.exports):l=p(s),f&&f.importers?(f.importers.push(t),t.dependencies.push(f)):t.dependencies.push(null),t.setters[a]&&t.setters[a](l)}}}function i(e){var r,t=v[e];if(t)t.declarative?f(e,[]):t.evaluated||l(t),r=t.module.exports;else if(r=p(e),!r)throw new Error("Unable to load dependency "+e+".");return(!t||t.declarative)&&r&&r.__useDefault?r["default"]:r}function l(r){if(!r.module){var t={},n=r.module={exports:t,id:r.name};if(!r.executingRequire)for(var o=0,a=r.normalizedDeps.length;a>o;o++){var u=r.normalizedDeps[o],d=v[u];d&&l(d)}r.evaluated=!0;var c=r.execute.call(e,function(e){for(var t=0,n=r.deps.length;n>t;t++)if(r.deps[t]==e)return i(r.normalizedDeps[t]);throw new TypeError("Module "+e+" not declared as a dependency.")},t,n);c&&(n.exports=c),t=n.exports,t&&t.__esModule?r.esModule=t:r.esModule=s(t)}}function s(r){var t={};if(("object"==typeof r||"function"==typeof r)&&r!==e)if(m)for(var n in r)"default"!==n&&c(t,r,n);else{var o=r&&r.hasOwnProperty;for(var n in r)"default"===n||o&&!r.hasOwnProperty(n)||(t[n]=r[n])}return t["default"]=r,x(t,"__useDefault",{value:!0}),t}function c(e,r,t){try{var n;(n=Object.getOwnPropertyDescriptor(r,t))&&x(e,t,n)}catch(o){return e[t]=r[t],!1}}function f(r,t){var n=v[r];if(n&&!n.evaluated&&n.declarative){t.push(r);for(var o=0,a=n.normalizedDeps.length;a>o;o++){var u=n.normalizedDeps[o];-1==g.call(t,u)&&(v[u]?f(u,t):p(u))}n.evaluated||(n.evaluated=!0,n.module.execute.call(e))}}function p(e){if(I[e])return I[e];if("@node/"==e.substr(0,6))return D(e.substr(6));var r=v[e];if(!r)throw"Module "+e+" not present.";return a(e),f(e,[]),v[e]=void 0,r.declarative&&x(r.module.exports,"__esModule",{value:!0}),I[e]=r.declarative?r.module.exports:r.esModule}var v={},g=Array.prototype.indexOf||function(e){for(var r=0,t=this.length;t>r;r++)if(this[r]===e)return r;return-1},m=!0;try{Object.getOwnPropertyDescriptor({a:0},"a")}catch(h){m=!1}var x;!function(){try{Object.defineProperty({},"a",{})&&(x=Object.defineProperty)}catch(e){x=function(e,r,t){try{e[r]=t.value||t.get.call(e)}catch(n){}}}}();var y={},D="undefined"!=typeof System&&System._nodeRequire||"undefined"!=typeof require&&require.resolve&&"undefined"!=typeof process&&require,I={"@empty":{}};return function(e,n,o,a){return function(u){u(function(u){for(var d={_nodeRequire:D,register:r,registerDynamic:t,get:p,set:function(e,r){I[e]=r},newModule:function(e){return e}},i=0;i<n.length;i++)(function(e,r){r&&r.__esModule?I[e]=r:I[e]=s(r)})(n[i],arguments[i]);a(d);var l=p(e[0]);if(e.length>1)for(var i=1;i<e.length;i++)p(e[i]);return o?l["default"]:l})}}}("undefined"!=typeof self?self:global)

(["1"], [], false, function($__System) {
var require = this.require, exports = this.exports, module = this.module;
!function(e){function r(e,r){for(var n=e.split(".");n.length;)r=r[n.shift()];return r}function n(n){if("string"==typeof n)return r(n,e);if(!(n instanceof Array))throw new Error("Global exports must be a string or array.");for(var t={},o=!0,f=0;f<n.length;f++){var i=r(n[f],e);o&&(t["default"]=i,o=!1),t[n[f].split(".").pop()]=i}return t}function t(r){if(Object.keys)Object.keys(e).forEach(r);else for(var n in e)a.call(e,n)&&r(n)}function o(r){t(function(n){if(-1==l.call(s,n)){try{var t=e[n]}catch(o){s.push(n)}r(n,t)}})}var f,i=$__System,a=Object.prototype.hasOwnProperty,l=Array.prototype.indexOf||function(e){for(var r=0,n=this.length;n>r;r++)if(this[r]===e)return r;return-1},s=["_g","sessionStorage","localStorage","clipboardData","frames","frameElement","external","mozAnimationStartTime","webkitStorageInfo","webkitIndexedDB","mozInnerScreenY","mozInnerScreenX"];i.set("@@global-helpers",i.newModule({prepareGlobal:function(r,t,i){var a=e.define;e.define=void 0;var l;if(i){l={};for(var s in i)l[s]=e[s],e[s]=i[s]}return t||(f={},o(function(e,r){f[e]=r})),function(){var r;if(t)r=n(t);else{r={};var i,s;o(function(e,n){f[e]!==n&&"undefined"!=typeof n&&(r[e]=n,"undefined"!=typeof i?s||i===n||(s=!0):i=n)}),r=s?r:i}if(l)for(var u in l)e[u]=l[u];return e.define=a,r}}}))}("undefined"!=typeof self?self:global);
!function(e){function n(e,n){e=e.replace(l,"");var r=e.match(u),t=(r[1].split(",")[n]||"require").replace(s,""),i=p[t]||(p[t]=new RegExp(a+t+f,"g"));i.lastIndex=0;for(var o,c=[];o=i.exec(e);)c.push(o[2]||o[3]);return c}function r(e,n,t,o){if("object"==typeof e&&!(e instanceof Array))return r.apply(null,Array.prototype.splice.call(arguments,1,arguments.length-1));if("string"==typeof e&&"function"==typeof n&&(e=[e]),!(e instanceof Array)){if("string"==typeof e){var l=i.get(e);return l.__useDefault?l["default"]:l}throw new TypeError("Invalid require")}for(var a=[],f=0;f<e.length;f++)a.push(i["import"](e[f],o));Promise.all(a).then(function(e){n&&n.apply(null,e)},t)}function t(t,l,a){"string"!=typeof t&&(a=l,l=t,t=null),l instanceof Array||(a=l,l=["require","exports","module"].splice(0,a.length)),"function"!=typeof a&&(a=function(e){return function(){return e}}(a)),void 0===l[l.length-1]&&l.pop();var f,u,s;-1!=(f=o.call(l,"require"))&&(l.splice(f,1),t||(l=l.concat(n(a.toString(),f)))),-1!=(u=o.call(l,"exports"))&&l.splice(u,1),-1!=(s=o.call(l,"module"))&&l.splice(s,1);var p={name:t,deps:l,execute:function(n,t,o){for(var p=[],c=0;c<l.length;c++)p.push(n(l[c]));o.uri=o.id,o.config=function(){},-1!=s&&p.splice(s,0,o),-1!=u&&p.splice(u,0,t),-1!=f&&p.splice(f,0,function(e,t,l){return"string"==typeof e&&"function"!=typeof t?n(e):r.call(i,e,t,l,o.id)});var d=a.apply(-1==u?e:t,p);return"undefined"==typeof d&&o&&(d=o.exports),"undefined"!=typeof d?d:void 0}};if(t)c.anonDefine||c.isBundle?c.anonDefine&&c.anonDefine.name&&(c.anonDefine=null):c.anonDefine=p,c.isBundle=!0,i.registerDynamic(p.name,p.deps,!1,p.execute);else{if(c.anonDefine&&!c.anonDefine.name)throw new Error("Multiple anonymous defines in module "+t);c.anonDefine=p}}var i=$__System,o=Array.prototype.indexOf||function(e){for(var n=0,r=this.length;r>n;n++)if(this[n]===e)return n;return-1},l=/(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm,a="(?:^|[^$_a-zA-Z\\xA0-\\uFFFF.])",f="\\s*\\(\\s*(\"([^\"]+)\"|'([^']+)')\\s*\\)",u=/\(([^\)]*)\)/,s=/^\s+|\s+$/g,p={};t.amd={};var c={isBundle:!1,anonDefine:null};i.amdDefine=t,i.amdRequire=r}("undefined"!=typeof self?self:global);
(function() {
var define = $__System.amdDefine;
define("2", [], function() {
  return "law.sublicensing.desc=Grant or extend a licence to the software\nlaw.distribution=Distribution\nwizard.menu.question1=Is my work that incorporates other datasets considered a derivative work of theirs?\nlaw.derivativeworks.desc=Create derivative works of the data\nwizard.menu.question0=I wish to publish a new dataset. Will it be protected by law?\npage.show.compatible=Comparable licences\nwizard.q1.end1.here=here\nlaw.sublicensing=Sublicensing\nstartagain=Start again\nlaw.commercialuse=Commercial use\nwizard.q0.step1.explanation=For example, a list of phone numbers of all the people working in an organisation will not be covered by copyright. The author cannot show any creativity in the selection of the data (which is simply all the people working there) nor in the choice of data (simple names and phone numbers). Even if the collection of data were very large or if a lot of effort was involved, no creativity was required and therefore no copyright rules apply\nlaw.obligation=Obligation\nlaw.prohibition=Prohibition\nwizard.q1.step0.question=Are the incorporated datasets protected under copyright law or database rights?\nwizard.q0.step0.question=Did you author the dataset?\nlaw.reproduction.desc=\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium.\nname=Name\nwizard.menu.header=I am facing the following problem:\nlaw.notice.desc=Keep copyright and licence notices intact\nwizard.q0.end1=Your database is likely protected under database rights (also known as 'sui generis rights'). You can find a list of licences covering these rights\nwizard.q0.end0=Please contact the original dataset author for coordinating publication.\nsettings.weighted=Weighted filtering\nwizard.q1.step1.question=Did you modify the original work?\nlaw.obligation.desc=You are obligated to:\nsettings.advanced=Advanced settings\nlaw.sharealike.desc=Distribute derivative works under the same licence as the original work.\nwizard.q0.step1.question=Is the dataset your intellectual creation by selection or arrangement?\npage.show.title=Show licence\nlaw.derivativeworks=Derivative Works\npage.show.open=Open licence homepage\nlaw.attribution.desc=Give proper credit to the copyright holder and/or author\nlaw.prohibition.desc=You are prohibited from:\nlaw.distribution.desc=Redistribute the data\npage.filter.title=Filter licences\nlaw.commercialuse.desc=Using the original work for commercial purposes.\nlaw.notice=Notice\nlaw.statechanges=State Changes\nlaw.lessercopyleft=Lesser Copyleft\nwizard.q0.step2.question=Was there a qualitatively and/or quantitatively substantial investment in either the obtaining, verification or presentation of the contents?\nlaw.attribution=Attribution\nlaw.copyleft=Copyleft\nterms=Terms\nlaw.lessercopyleft.desc=Licence derivative works under terms specified in the licence which are similar to the original work's.\nlaw.patentgrant=Use patent claims\nlaw.patentgrant.desc=Use any patents of the licensor applicable to the work insofar as needed to use the work in accordance with the copyright usage rights\nlaw.permission.desc=You are free to:\nlaw.reproduction=Reproduction\nyes=Yes\nlaw.sharealike=Sharealike\nlaw.permission=Permission\nwizard.q0.step2.explanation=For example, a database containing exclusive geographic information related to an entire country will likely be covered by this. This is because such a database will normally require a significant effort to create in terms of time, money, effort etc.\nwizard.q1.end1=Your work most likely constitutes a derivative work. Please have a look at compatible licences\nwizard.q1.end0=Your work does not constitute a derivative work. However, contractual restrictions might still apply.\nno=No\nlaw.statechanges.desc=Indicate which changes have been made to the original licenced work in a manner that permits attribution.\npage.filter.introduction=Data which is shared with a licence becomes Open Data. There are many licences available. The licence assistant provides a description of the available licences. It also gives an overview of how to apply licences as re-publisher/distributor of Open Data and how to combine multiple licences.\npage.filter.description=Please find a licence by selecting the preferred licence terms below:\nwizard.q0.end2=The dataset is likely protected under copyright law and you may choose an applicable licence.\nwizard.q0.end3=Unfortunately, your dataset most likely does not fall under copyright and/or database rights. Please consult a lawyer for further advice on this topic.\nlaw.copyleft.desc=";
});

})();
(function() {
var define = $__System.amdDefine;
define("3", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.distribution=Verteilung\nlaw.sublicensing.desc=Eine Lizenz für die Software erweitern oder gewähren\nwizard.menu.question1=Sind andere Datensätze oder ein abgeleitetes Werk von Ihnen in Betracht gezogen worden?\nlaw.derivativeworks.desc=Erstellen Sie abgeleitete Werke der Daten\nwizard.menu.question0=Ich möchte einen neuen Datensatz veröffentlichen. Wird dieser durch das Gesetz geschützt werden?\npage.show.compatible=Vergleichbare Lizenzen\nwizard.q1.end1.here=Hier\nlaw.sublicensing=Unterlizensierung \nstartagain=Bitte beginnen Sie noch einmal\nwizard.q0.step1.explanation=Zum Beispiel wird eine Liste mit Telefonnummern aller Menschen welche in einer Organisation arbeiten, nicht durch das Urheberrecht geschützt. Der Autor kann  keine Kreativität bei der Auswahl der Daten zeigen(die dort arbeitenden Menschen einfach alles), noch bei der Auswahl der Daten (einfache Namen und Telefonnummern ). Auch wenn die Erhebung von Daten sehr groß ist oder wenn eine Menge Aufwand beteiligt war, wurde keine Kreativität erforderlich und daher auch keine Copyright-Regeln geltend gemacht.\nlaw.commercialuse=Kommerzielle Benutzung\nlaw.obligation=Verpflichtung\nlaw.prohibition=Verbot\nwizard.q1.step0.question=Sind die eingebauten Datensätze nach dem Urheberrecht oder dem Datenbankrecht geschützt?\nwizard.q0.step0.question=Haben Sie den Datensatz autorisiert?\nlaw.reproduction.desc=\"Reproduzieren\" bedeutet Kopien des Werks mit irgendwelchen Mitteln ohne Einschränkung durch Ton- oder Bildaufnahmen machen und das Recht der Fixierung und Wiedergabe von Fixierungen der Arbeit, einschließlich der Speicherung einer geschützten Darbietung oder eines Tonträgers in digitaler Form oder ein anderes elektronisches Medium zu machen.\nname=Name\nwizard.menu.header=Ich habe folgendes Problem:\nlaw.notice.desc=Halten Sie das Urheberrecht und Lizenzhinweise intakt\nwizard.q0.end1=Ihre Datenbank wird wahrscheinlich durch Datenbankrechte geschützt (auch als \"sui generis Rechte\" bekannt). Sie können eine Liste der Lizenzen finden welche diese hier abdeckt.\nwizard.q0.end0=Bitte kontaktieren Sie den Original-Datensatz Autor für die Koordination der Veröffentlichung.\nsettings.weighted=Die gewichteten Filter\nwizard.q1.step1.question=Haben Sie das Originalwerk verändert?\nlaw.obligation.desc=Sie sind verpflichtet zu:\nlaw.sharealike.desc=Die Verteilung abgeleiteter Werke wird unter derselben Lizenz wie das ursprüngliche Werk erstellt.\nsettings.advanced=Erweiterte Einstellungen\nwizard.q0.step1.question=Ist der Datensatz Ihre geistige Schöpfung durch Auswahl und Anordnung?\npage.show.title=Lizenz anzeigen\nlaw.derivativeworks=Derivative Works\npage.show.open=Open License Homepage\nlaw.attribution.desc=Geben Sie die richtige Kreditvergabe an den Copyright-Inhaber und / oder Autor\nlaw.prohibition.desc=Ihnen ist folgendes untersagt:\nlaw.distribution.desc=Verteilen Sie die Daten\npage.filter.title=Lizenzen filtern\nlaw.commercialuse.desc=Die ursprüngliche Arbeit für kommerzielle Zwecke benutzen\nlaw.statechanges=Statusänderungen \nlaw.copyleft.lesser=Lesser Copyleft \nlaw.notice=Beachten\nwizard.q0.step2.question=Gab es einen qualitativen Umfang wesentlicher Investition in entweder der Beschaffung, der Überprüfung oder der Darstellung des Inhalts?\nlaw.copyleft=Copyleft \nlaw.attribution=Zuschreibung\nterms=Bedingungen\nlaw.copyleft.lesser.desc=Die Lizenz abgeleiteter Werke werden unter Bedingungen in der Lizenz festgelegt , die dem Originalwerk ähnlich sind.\nlaw.patentgrant.desc=Verwenden Sie keine Patente der Lizenzgeber für die Arbeit, soweit notwendig, um die Arbeit in Übereinstimmung mit den Urhebernutzungsrechte zu verwenden,\nlaw.permission.desc=Sie sind frei zu:\nyes=Ja\nlaw.reproduction=Reproduktion\nlaw.sharealike=Austauschen\nlaw.permission=Erlaubnis\nwizard.q0.step2.explanation=Zum Beispiel kann eine entsprechende Datenbank geographische Information eines ganzes Landes enthalten, welche wahrscheinlich durch diese abgedeckt wird. Dies liegt daran, dass eine solche Datenbank in der Regel einen erheblichen Aufwand an Zeit mit sich bringt, (Geld, Aufwand usw. ) um diese zu erstellen\nwizard.q1.end1=Ihre Arbeit stellt wahrscheinlich meistens ein abgeleitetes Werk. Bitte haben Sie einen Blick auf die kompatible Lizenzen\nwizard.q1.end0=Ihre Arbeit stellt kein abgeleitetes Werk dar. Allerdings könnten vertragliche Beschränkungen  weiterhin gelten.\nno=Nein\nlaw.statechanges.desc=Geben Sie die Änderungen an der ursprünglichen lizenzierten Arbeit in einer Weise an, die Zurechnung ermöglicht.\npage.filter.description=Hier finden Sie eine Lizenz, wo Sie die bevorzugten Lizenzbedingungen auswählen können:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("4", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.sublicensing.desc=Conceder o ampliar una licencia para el software\nlaw.distribution=Distribución\nlaw.derivativeworks.desc=Crear trabajos derivados de los datos\nwizard.menu.question1=¿Es mi trabajo, que incorpora otros conjuntos de datos considerado una obra derivada de los suyos?\nwizard.menu.question0=Deseo publicar un nuevo conjunto de datos. ¿Esta protegido por la ley?\npage.show.compatible=Licencias comparables\nwizard.q1.end1.here=Aquí\nlaw.sublicensing=Sublicencia\nstartagain=Empezar de nuevo\nwizard.q0.step1.explanation=Por ejemplo, una lista de números de teléfono de todas las personas que trabajan en una organización no estará cubierta por los derechos de autor. El autor no puede mostrar ninguna creatividad en la selección de los datos (simplemente cubre todas las personas que trabajan allí) ni en la elección de los datos (nombres simples y números de teléfono). Incluso si la recogida de datos fuera muy extensa o si una gran cantidad de esfuerzo fuera necesario, no incluiría creatividad y por lo tanto no se aplicarían las reglas de derechos de autor\nlaw.commercialuse=Uso comercial\nlaw.obligation=Obligación\nlaw.prohibition=Prohibición\nwizard.q1.step0.question=¿Están los conjuntos de datos incorporados protegidos por derechos de autor o derechos de base de datos?\nwizard.q0.step0.question=¿Ha creado usted el conjunto de datos?\nlaw.reproduction.desc=\"Reproducir\" significa hacer copias del trabajo por cualquier medio incluyendo, sin limitaciones dela reproducción de sonido o visual y el derecho de fijación y reproducción de fijaciones del trabajo, incluyendo el almacenamiento de una interpretación o ejecución protegida o de un fonograma en formato digital u otro medio electrónico.\nname=Nombre\nwizard.menu.header=Tengo el siguiente problema\nlaw.notice.desc=Mantener licencias y copyright avisos intactos\nwizard.q0.end1=Su base de datos está probablemente protegida por derechos de bases de datos (también conocidos como 'derechos sui generis'). Puede encontrar aquí una lista de las licencias que cubren estos derechos.\nwizard.q0.end0=Por favor, póngase en contacto con el autor original de los datos para la coordinación de la publicación.\nsettings.weighted=Filtro ponderado\nwizard.q1.step1.question=¿Ha modificado la obra original?\nlaw.obligation.desc=Usted está obligado a:\nlaw.sharealike.desc=Distribuir los trabajos derivados bajo la misma licencia que el trabajo original.\nsettings.advanced=Ajustes avanzados\nwizard.q0.step1.question=¿Es el conjunto de datos su creación intelectual por selección o acuerdo?\npage.show.title=Mostrar licencia\nlaw.derivativeworks=Trabajos derivados\npage.show.open=Página principal de licencia abierta\nlaw.attribution.desc=Dar crédito al titular de los derechos de autor y / o autor\nlaw.prohibition.desc=Se le prohíbe:\nlaw.distribution.desc=Redistribuir los datos\npage.filter.title=Filtre licencias\nlaw.commercialuse.desc=El uso del trabajo original con fines comerciales.\nlaw.notice=Aviso\nlaw.statechanges=Cambios de estado\nlaw.copyleft.lesser=Menor copyleft\nwizard.q0.step2.question=¿Hubo una inversión cualitativa y / o cuantitativamente sustancial, ya sea en la obtención, la verificación o la presentación de los contenidos?\nlaw.attribution=Atribución\nlaw.copyleft=Copyleft\nterms=Condiciones\nlaw.copyleft.lesser.desc=La licencia derivada se emplea bajo los términos especificados en la licencia los cuales son similares a la obra original.\nlaw.patentgrant=Concesión de la patente\nlaw.patentgrant.desc=Utilice cualquier patente del licenciatario aplicables al trabajo en la medida en que sea necesario para utilizar el trabajo de acuerdo con los derechos  de autor\nlaw.permission.desc=Usted es libre de:\nlaw.sharealike=Compartir por igual\nlaw.reproduction=Reproducción\nyes=Sí\nlaw.permission=Permiso\nwizard.q0.step2.explanation=Por ejemplo, una base de datos que contiene información geográfica exclusivamente relacionada con todo un país probablemente será cubierto por el presente. Esto se debe a que dicha base de datos normalmente collevará un esfuerzo importante para su elaboració en términos de tiempo, dinero, esfuerzo, etc.\nwizard.q1.end1=Su trabajo probablemente constituye una obra derivada. Por favor, eche un vistazo a licencias compatibles\nwizard.q1.end0=Su trabajo no constituye una obra derivada. Sin embargo, todavía pueden existir restricciones contractuales.\nno=No\nlaw.statechanges.desc=Indique qué cambios se han hecho al trabajo con licencia original de una manera que permita la atribución.\npage.filter.description=Por favor, encuentre a continuación una licencia seleccionando los términos de la licencia preferidas:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("5", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.sublicensing.desc=Accorder ou prolonger une licence pour le logiciel\nlaw.distribution=Distribution\nwizard.menu.question1=Mes travaux intégrants d'autres ensembles de données sont-ils considérés comme dérivés des leurs?\nlaw.derivativeworks.desc=Créer des œuvres dérivées des données\nwizard.menu.question0=Je souhaite publier un nouvel ensemble de données. Sera-t-il protégé par la loi? \npage.show.compatible=Licences comparables\nwizard.q1.end1.here=Ici\nlaw.sublicensing=Sous-licence\nstartagain=Recommencer \nwizard.q0.step1.explanation=Par exemple, une liste de numéros de téléphone de toutes les personnes travaillant dans une organisation ne sera pas couverte par le droit d'auteur. L'auteur ne doit pas se montrer créatif dans le choix des données (simplement toutes les personnes travaillant ici), ni dans le choix des données (simplement les noms et numéros de téléphone). Même si la collecte de données était très importante ou si beaucoup d'efforts ont été consentis, aucune créativité n'était nécessaire et donc aucune règle de droit d'auteur n'a été appliquée\nlaw.commercialuse=Usage commercial\nlaw.obligation=Obligation\nlaw.prohibition=Interdiction\nwizard.q1.step0.question=Les jeux de données intégrés sont-ils protégés par des droits d'auteurs ou les droits de bases de données?\nwizard.q0.step0.question=Êtes-vous l'auteur du jeu de données\nlaw.reproduction.desc=«Reproduire» signifie faire des copies des travaux par tous les moyens, sans limitation visuelle ou auditive des enregistrement et droit de fixation et de reproduction des travaux, y compris le stockage d'une performance ou d'un phonogramme protégé sous forme numérique ou autre support électronique.\nname=Nom\nwizard.menu.header=Je suis confronté au problème suivant\nlaw.notice.desc=Gardez les droits d'auteur et les avis de licence intacts. \nwizard.q0.end1=Votre base de données est probablement protégée par les droits de base de données (également appelés «droits sui generis»). Vous pouvez trouver une liste des licences couvrant ces droits ici.\nwizard.q0.end0=Veuillez contacter l'auteur du jeu de données original pour coordonner la publication\nsettings.weighted=Filtrage pondéré\nwizard.q1.step1.question=Avez-vous modifier l'œuvre originale?\nlaw.obligation.desc=Vous êtes obligé de:\nlaw.sharealike.desc=Distribuer les travaux dérivés sous la même licence que l'originale\nsettings.advanced=Paramètres avancés\nwizard.q0.step1.question=Le jeu de données est-il votre création intellectuelle par sélection ou arrangement?\npage.show.title=Montrer la licence\nlaw.derivativeworks=travaux dérivés\npage.show.open=page d'accueil de la licence ouverte\nlaw.attribution.desc=Donner du mérite au titulaire/auteur du droit d'auteur\nlaw.prohibition.desc=Il est interdit de: \nlaw.distribution.desc=Redistribuer les données\npage.filter.title=Filtrage de licence\nlaw.commercialuse.desc=Utilisation de l'œuvre originale à des fins commerciales\nlaw.copyleft.lesser=Copyleft moindre\nlaw.notice=Avis\nlaw.statechanges=changement de l'état\nwizard.q0.step2.question=Y avait-il un investissement substantiellement qualitatif et/ou quantitatif dans l'obtention, la vérification ou la présentation du contenu?\nlaw.attribution=Attribution\nlaw.copyleft=Copyleft \nterms=Termes\nlaw.copyleft.lesser.desc=Travaux de licence dérivés sous les conditions spécifiques stipulées dans la licence qui sont similaires à l'œuvre originale \nlaw.patentgrant=#Mon May 09 12:21:02 CEST 2016\nlaw.sublicensing.desc=Accorder ou prolonger une licence pour le logiciel\nlaw.distribution=Distribution\nwizard.menu.question1=Mes travaux intégrants d'autres ensembles de données sont-ils considérés comme dérivés des leurs?\nlaw.derivativeworks.desc=Créer des œuvres dérivées des données\nwizard.menu.question0=Je souhaite publier un nouvel ensemble de données. Sera-t-il protégé par la loi? \npage.show.compatible=Licences compatibles\nwizard.q1.end1.here=Ici\nlaw.sublicensing=Sous-licence\nstartagain=Recommencer \nwizard.q0.step1.explanation=Par exemple, une liste de numéros de téléphone de toutes les personnes travaillant dans une organisation ne sera pas couverte par le droit d'auteur. L'auteur ne doit pas se montrer créatif dans le choix des données (simplement toutes les personnes travaillant ici), ni dans le choix des données (simplement les noms et numéros de téléphone). Même si la collecte de données était très importante ou si beaucoup d'efforts ont été consentis, aucune créativité n'était nécessaire et donc aucune règle de droit d'auteur n'a été appliquée\nlaw.commercialuse=Usage commercial\nlaw.obligation=Obligation\nlaw.prohibition=Interdiction\nwizard.q1.step0.question=Les jeux de données intégrés sont-ils protégés par des droits d'auteurs ou les droits de bases de données?\nwizard.q0.step0.question=Êtes-vous l'auteur du jeu de données\nlaw.reproduction.desc=«Reproduire» signifie faire des copies des travaux par tous les moyens, sans limitation visuelle ou auditive des enregistrement et droit de fixation et de reproduction des travaux, y compris le stockage d'une performance ou d'un phonogramme protégé sous forme numérique ou autre support électronique.\nname=Nom\nwizard.menu.header=Je suis confronté au problème suivant\nlaw.notice.desc=Gardez les droits d'auteur et les avis de licence intacts. \nwizard.q0.end1=Votre base de données est probablement protégée par les droits de base de données (également appelés «droits sui generis»). Vous pouvez trouver une liste des licences couvrant ces droits ici.\nwizard.q0.end0=Veuillez contacter l'auteur du jeu de données original pour coordonner la publication\nsettings.weighted=Filtrage pondéré\nwizard.q1.step1.question=Avez-vous modifier l'œuvre originale?\nlaw.obligation.desc=Vous êtes obligé de:\nlaw.sharealike.desc=Distribuer les travaux dérivés sous la même licence que l'originale\nsettings.advanced=Paramètres avancés\nwizard.q0.step1.question=Le jeu de données est-il votre création intellectuelle par sélection ou arrangement?\npage.show.title=Montrer la licence\nlaw.derivativeworks=travaux dérivés\npage.show.open=page d'accueil de la licence ouverte\nlaw.attribution.desc=Donner du mérite au titulaire/auteur du droit d'auteur\nlaw.prohibition.desc=Il est interdit de: \nlaw.distribution.desc=Redistribuer les données\npage.filter.title=Filtrage de licence\nlaw.commercialuse.desc=Utilisation de l'œuvre originale à des fins commerciales\nlaw.copyleft.lesser=Copyleft moindre\nlaw.notice=Avis\nlaw.statechanges=changement de l'état\nwizard.q0.step2.question=Y avait-il un investissement substantiellement qualitatif et/ou quantitatif dans l'obtention, la vérification ou la présentation du contenu?\nlaw.attribution=Attribution\nlaw.copyleft=Copyleft \nterms=Termes\nlaw.copyleft.lesser.desc=Travaux de licence dérivés sous les conditions spécifiques stipulées dans la licence qui sont similaires à l'œuvre originale \nlaw.patentgrant=Délivrance des brevets\nlaw.patentgrant.desc=Utilisez n'importe quel brevêt du concédant applicable au travaux lorsque c'est nécessaire pour utiliser les dits travaux en conformité avec les droits d'utilisation\nlaw.permission.desc=Vous êtes libre de : \nyes=Oui\nlaw.reproduction=Reproduction\nlaw.sharealike=ShareAlike\nlaw.permission=Autorisation\nwizard.q0.step2.explanation=Par exemple, une base de données contenant l'information géographique exclusive liée à un pays tout entier sera probablement couvert par la présente. En effet, créer une telle base de données exige normalement un effort important en termes de temps, d'argent, d'effort, etc.\nwizard.q1.end1=Votre travail constitue très probablement un travail dérivé. Merci de jeter un oeil aux licences compatibles\nwizard.q1.end0=Votre travail ne constitue pas un travail dérivé. Toutefois, les restrictions contractuelles pourraient toujours être valables\nno=Non\nlaw.statechanges.desc=Indiquez quels changements ont été apportés au travail d'origine sous licence d'une manière permettant une attribution.\npage.filter.description=Veuillez trouver une licence en sélectionnant les conditions préférentielles de licence ci-dessous\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("6", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.distribution=Distribuzione\nlaw.sublicensing.desc=Concedere o prorogare una licenza per il software\nlaw.derivativeworks.desc=Creare opera derivata dei dati\nwizard.menu.question1=Mio lavoro, che incorpora altri set di dati, è considerato come un'opera derivata di loro?\nwizard.menu.question0=Vorrei pubblicare un nuovo set di dati. Sarà protetto dalla legge?\npage.show.compatible=Licenze comparabili\nwizard.q1.end1.here=Qui\nlaw.sublicensing=Sottolicenza \nstartagain=Ricominciare\nwizard.q0.step1.explanation=Ad esempio, un elenco di numeri di telefono di tutte le persone che lavorano in una organizzazione non sarà coperto dal diritto d'autore. L'autore non può dimostrare nessuna creatività nella selezione dei dati (semplicemente sono tutte le persone che vi lavorano), né nella scelta dei dati (nomi semplici e numeri di telefono). Anche se la raccolta dei dati fosse molto grande o se un grande sforzo fosse stato coinvolto, nessuna creatività sarebbe stata richiesta e quindi non si applicano le regole sul diritto d'autore.\nlaw.commercialuse=Uso commerciale \nlaw.obligation=Obbligo\nlaw.prohibition=Divieto\nwizard.q1.step0.question=I set di dati incorporati sono protteti dal diritto d'autore o  dai diritti relativi alle basi di dati?\nwizard.q0.step0.question=È Lei l'autore del set di dati?\nlaw.reproduction.desc=«Riprodurre» significa fare copie dell'opera con qualsiasi mezzo, compresa, senza limitazioni da registrazioni sonore o visive e il diritto di fissazione e che riproducono fissazioni del lavoro, compreso lo stoccaggio di esecuzioni o fonogrammi protetti in formato digitale o altro supporto elettronico.\nname=Nome\nwizard.menu.header=Sto affrontando il seguente problema :\nlaw.notice.desc=Mantenere intatti i copyright e gli avvisi di licenza \nwizard.q0.end1=La banca dati è probabilmente protetta dai diritti di database (il cosiddetto  «il diritto sui generis»). Qui si può trovare un elenco di licenze che coprono questi diritti.\nwizard.q0.end0=Si prega di contattare l'autore del set di dati originale per coordinare la pubblicazione.\nsettings.weighted=Filtro de ponderazione\nwizard.q1.step1.question=Ha Lei modificato l'opera originaria?\nlaw.obligation.desc=Lei è obbligato a:\nsettings.advanced=Impostazion avanzate\nlaw.sharealike.desc=Distribuire le opere derivate sotto la stessa licenza dell'opera originaria.\nwizard.q0.step1.question=È il set di dati una sua creazione intellettuale per scelta o disposizione?\npage.show.title=Mostrare licenza\nlaw.derivativeworks=Opera derivata\npage.show.open=Pagina iniziale della licenza aperta\nlaw.attribution.desc=Riconoscere il giusto merito dal titolare/autore del diritto d'autore \nlaw.prohibition.desc=È vietato di:\nlaw.distribution.desc=Ridistribuire i dati\npage.filter.title=Filtraggio de licenze\nlaw.commercialuse.desc=Uso  a fini commerciali de l'oppera originaria\nlaw.statechanges=Cambiamenti di stato\nlaw.copyleft.lesser=Copyleft minore\nlaw.notice=Avviso\nwizard.q0.step2.question=C'è stato un investimento sostanziale in termini qualitativi o quantitativi  sia nell'ottenere, la verifica o la presentazione dei contenuti?\nlaw.copyleft=Copyleft \nlaw.attribution=Attribuzione\nterms=Condizioni\nlaw.copyleft.lesser.desc=La licenza derivata si usa nelle condizioni specificate nella licenza le quali sono simili all'opera originaria.\nlaw.patentgrant=Concessione del brevetto\nlaw.patentgrant.desc=Utilizzi qualsiasi brevetto detenuto dal licenziante applicabile all’opera nella misura in cui  è necessario per l'utilizzo de l'opera in conformità ai diritti di utilizzo\nlaw.permission.desc=Lei è libero di: \nyes=Sì\nlaw.sharealike=Condivi allo stesso modo\nlaw.reproduction=Riproduzione\nlaw.permission=Autorizzazione\nwizard.q0.step2.explanation=Ad esempio, una banca dati contenente esclusivamente informazioni geografiche relative ad un intero paese sarà probabilmente coperto da questa. Di fatto, una tale banca dati, di norma, esigerà un notevole sforzo per la sua elaborazione in termini di tempo, denaro, impegno, ecc.\nwizard.q1.end1=Il Suo lavoro molto probabilmente costituisce un'opera derivata. Vi preghiamo di dare un'occhiata alle licenze compatibili.\nwizard.q1.end0=Il suo lavoro non costituisce un'oppera derivata. Tuttavia, alcune restrizioni contrattuali potrebbero ancora trovare applicazione.\nno=Non\nlaw.statechanges.desc=Indicare quali modifiche sono state apportate all'opera originaria su licenza in modo da consentire un'attribuzione.\npage.filter.description=Si prega di trovare una licenza qui sotto selezionando le condizioni di licenza preferite:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("7", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.distribution=Verdeling\nlaw.sublicensing.desc=Licentie aan de software verlenen of uitbreiden\nwizard.menu.question1=Word mijn werk, dat andere datasets bevat, als afgeleid werk van hen beschouwd?\nlaw.derivativeworks.desc=Creëer afgeleide elementen van de gegevens\nwizard.menu.question0=Il wil een nieuwe dataset uitgeven. Is het wettelijk beschermd?\npage.show.compatible=Vergelijkbare licenties\nwizard.q1.end1.here=hier\nlaw.sublicensing=Sublicentieering\nstartagain=Begin opnieuw\nlaw.commercialuse=Commercieel gebruik\nwizard.q0.step1.explanation=Bijvoorbeeld: een lijst met telefoonnummers van alle personen die bij een bedrijf werken is niet met auteursrechten voorzien. De auteur kan geen creativiteit laten blijken in de keus van het type gegevens (simpelweg alle mensen die bij het bedrijf werken), noch in de keus van de gegevens zel (namen en telefoonnummers). Ook al is het aantal gegevens groot en werd er veel moeite in gestoken, was hier geen creativiteit gevraagd en daarom kunnen geen auteursrechten geldig gemaakt worden.    \nlaw.obligation=Verplichting\nlaw.prohibition=Verbod\nwizard.q1.step0.question=Zijn de geïntegreerde datasets door auteursrechten of database rechten beschermd?\nlaw.reproduction.desc=\"Repoduceren\" betekent het maken van kopieën van het werk in welke vorm dan ook; inclusief opnames in geluid en beeld en het recht op fixatie van het werk, en diens reproductie; inclusief het opslaan van een bescherme uitvoering of geluidsopname in gigitaal vorm of door een ander electronisch medium. \nwizard.q0.step0.question=Bent u de auteur van deze dataset?\nname=Naam\nwizard.menu.header=Ik heb het volgende probleem:\nlaw.notice.desc=Laat auteursrechten en licentie notificaties onveranderd\nwizard.q0.end1=Uw database is waarschijnlijk door database rechten beschermd (ook bekend onder 'sui generis' rechten). Hier is een lijst van licenties die deze rechten afdekken.\nwizard.q0.end0=Neem contact op met de auteur van de oorspronkelijke dataset om publikatie te regelen.\nsettings.weighted=Gewogen filteren\nwizard.q1.step1.question=Hebt u het oorspronkelijk werk veranderd?\nlaw.obligation.desc=U bent verplicht om:\nlaw.sharealike.desc=Afgeleid werk doorgeven onder dezelfde licentie als het oorspronkelijke werk.\nsettings.advanced=Geavanceerde settings\nwizard.q0.step1.question=Is de dataset uw eigen creatie door selectie of ordening? \npage.show.title=Toon licentie\nlaw.derivativeworks=Afgeleid werk\npage.show.open=Open license homepage\nlaw.attribution.desc=Geef adequate referentie m.b.t. de copyright houder en/of auteur \nlaw.prohibition.desc=Het is u niet toegestaan om:\nlaw.distribution.desc=Gegevens verder doorgeven\npage.filter.title=Filter licenties\nlaw.commercialuse.desc=Gebruik van oorspronkelijk werk voor commerciële doeleinden.\nlaw.statechanges=Toestand veranderingen\nlaw.copyleft.lesser=Mindere Copyleft\nlaw.notice=Mededeling\nwizard.q0.step2.question=Was er een kwalitatief en/of kwantitatief belangrijke investering nodig om de inhoud te verkrijgen, te controleren of te presenteren?\nlaw.copyleft=Copyleft\nlaw.attribution=Attributie\nterms=Voorwaarden\nlaw.copyleft.lesser.desc=Licentie-afgeleid werk volgens de in de licentie bepaalde voorwaarden, die vergelijkbaar zijn met het oorspronkelijk werk. \nlaw.patentgrant=Patent subsidie\nlaw.patentgrant.desc=Gebruik octrooien van de licentiehouder voor het werk, binnen de copyright gebruikersrechten. \nlaw.permission.desc=U kunt:\nyes=Ja\nlaw.sharealike=Sharealike\nlaw.reproduction=Reproduktie\nlaw.permission=Toestemming\nwizard.q0.step2.explanation=Bijvoorbeeld: een database met exclusieve geografische informatie met betrekking tot een heel land zal waarschijnlijk hierdoor gedekt zijn. De rede is dat de ontwikkeling van een dergelijke database normalerwijs een aanzienlijke moeite vergt m.b.t. tijd, financiële middelen, enz.  \nwizard.q1.end1=Uw werk is waarschijnlijk afgeleid. Kijk a.u.b. naar verenigbare licenties\nwizard.q1.end0=Uw werk is niet afgeleid. Toch zouden contractuele restricties van toepassing kunnen zijn.\nno=Nee\nlaw.statechanges.desc=Geef aan welke veranderingen u aan het oorspronkelijke geoctrooieerde werk heeft aangebracht, op een manier die een attributie mogelijk maakt. \npage.filter.description=Kies hieronder een licentie uit de voorkeur licentie voorwaarden:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("8", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.distribution=Dystrybucja\nlaw.sublicensing.desc=Przyznanie lub poszerzenie licencji na oprogramowanie\nlaw.derivativeworks.desc=Stwόrz pochodne prace na podstawie danych\nwizard.menu.question1=Czy moja praca, która zawiera inne zestawy danych może być  traktowana  jako dzieło od nich pochodne?\nwizard.menu.question0=Chciałbym opublikować bazę danych. Czy będzie chroniona przez prawo?\npage.show.compatible=Porównywalne licencje\nwizard.q1.end1.here=Tutaj\nlaw.sublicensing=Udzielanie sublicencji\nstartagain=Zacznij jeszcze raz\nwizard.q0.step1.explanation=Na przykład, lista numerόw telefonόw osόb pracujących w organizacji nie będzie chroniona przez prawa autorskie. Autor nie może się wykazać żadną kreatywnością w selekcji danych (wszystkie osoby tam pracujące) ani w wyborze danych (nazwiska i  numery telefonów). Nawet jeśli zbiorka danych była prowadzona na szeroką skalę lub wymagała dużego nakładu pracy,  kreatywność (oryginalność) nie była wymagana i z tego powodu prawa autorskie nie stosują się \nlaw.commercialuse=Użytek handlowy\nlaw.obligation=Obowiązek \nlaw.prohibition=Zakaz\nwizard.q1.step0.question=Czy włączone zestawy danych są chronione prawami autorskimi lub prawami baz danych?\nwizard.q0.step0.question=Czy jest Pan/Pani autorem zbioru danych?\nlaw.reproduction.desc='Reprodukować ' znaczy robić kopie pracy/dzieła za pomocą wszystkich środków bez wyłączności : nagranie dźwiękowe lub obrazowe, prawo do utrwalania i odtwarzania pracy/dzieła w tym przechowywanie chronionego dzieła artystycznego lub fonogramu w formie cyfrowej lub za pomocą innego nośnika elektronicznegp\nname=Nazwa/nazwisko\nwizard.menu.header=Mam następujacy problem\nlaw.notice.desc=Przechowywaj notki o prawa autorskich i licencjach nienaruszone\nwizard.q0.end1=Twoja baza danych jest prawdopodobnie chroniona prawem odnosnie baz danych (zwanym 'sui generis rights'). Lista odpowiadających licencji znajduje się tutaj:\nwizard.q0.end0=Proszę skontaktować się z autorem oryginalnego zestawu danych w celach koordynacji publikacji.\nsettings.weighted=Filter ważony\nwizard.q1.step1.question=Czy dokonałeś zmian w oryginalnym dziele/pracy?\nlaw.obligation.desc=Jesteś zobowiazany do:\nwizard.q0.step1.question=Czy zbiór danych jest twoją kreacja intelektualna dokonana poprzez wybór i selekcje czy dobieranie?\nlaw.sharealike.desc=Rozmieść pochodne prace pod tą samą licencją co oryginalna praca\nsettings.advanced=Zaawansowane ustawienia\npage.show.title=Pokaż licencję\nlaw.derivativeworks=Prace/dzieła pochodne\npage.show.open=Otwόrz stronę głόwną licencji\nlaw.attribution.desc=Przyznaj odpowiedni kredyt właścicielowi praw autorskich \nlaw.prohibition.desc=Zabrania się:\nlaw.distribution.desc=Rozmieść dane\npage.filter.title=Filtruj licencje/licencje filtrujące \nlaw.commercialuse.desc=Używanie oryginalnego dzieła w celach handlowych\nlaw.copyleft.lesser=Lesser Copyleft\nlaw.statechanges=Wykaż zmiany\nlaw.notice=Uwaga/ogłoszenie \nwizard.q0.step2.question=Czy nakład inwestycyjny w sensie jakościowym i ilościowym w otrzymanie, sprawdzenie lub prezentacje zawartości był znaczny?\nlaw.copyleft=Copyleft\nlaw.attribution=Przydział\nterms=Terminy\nlaw.copyleft.lesser.desc=Zalicencjuj prace pochodne na zasadach określonych w oryginalnej licencji\nlaw.patentgrant=Przyznanie patentu\nlaw.patentgrant.desc=Użyj jakikolwiek patent licencjodawcy stosujący się do dotychczasowych prac, o ile potrzebny do użycia, zgodnie z prawami użytkownika praw autorskich \nlaw.permission.desc=Można :\nlaw.sharealike=Sharealike\nlaw.reproduction=Reprodukcja\nyes=Tak \nlaw.permission=Pozwolenie\nwizard.q0.step2.explanation=Na przykład baza danych zawierająca wyjątkowe informacje geograficzne dotyczące całego kraju będzie prawdopodobnie tym objęta. To dlatego że ta  baza danych będzie wymagać zazwyczaj  znacznego nakładu pracy, czasu lub finansowego. \nwizard.q1.end1=Twoja praca ma polegać głownie na pracy odtwórczej. Proszę o konsultowanie podobnych licencji. \nwizard.q1.end0=Twoja praca nie jest praca odtwórcza. Jednak, ograniczenia kontraktowe mogą nadal obowiązywać \nno=Nie\nlaw.statechanges.desc=Proszę wskazać zmiany jakie zostały naniesione to oryginalnych licencji w sposób aby umożliwić atrybucję \npage.filter.description=Proszę o znalezienie licencji poprzez wybranie preferowanych terminów  licencyjnych poniżej:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("9", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.sublicensing.desc=Udeliť alebo rozšíriť licenciu na softvér\nlaw.distribution=Distribúcia\nwizard.menu.question1=Je moje dielo, ktoré zahŕňa iné datasety považované za odvodené dielo ich autorov?\nlaw.derivativeworks.desc=Vytvoriť z údajov odvodené dielo\nwizard.menu.question0=Chcel by som uverejniť nový dataset. Bude právne chránený?\npage.show.compatible=Porovnateľné licencie\nwizard.q1.end1.here=tu\nlaw.sublicensing=Poskytovanie licencie\nstartagain=Začať znovu\nwizard.q0.step1.explanation=Napríklad, telefónny zoznam všetkých ľudí pracujúcich v organizácii nebude chránený autorskými právami. Autor nevyužíva tvorivosť pri výbere údajov (jednoducho len všetci ľudia, ktorí tam pracujú) ani pri formátovaní údajov (jednoducho mená a telefónne čísla) Aj keby bola sada údajov veľmi veľká, alebo si vyžadovala veľa úsilia na skompletizovanie, nebola využitá tvorivosť a preto nie sú uplatnené žiadne predpisy ochrany autorských práv.\nlaw.commercialuse=Komerčné použitie\nlaw.obligation=Povinnosť\nlaw.prohibition=Zákaz\nwizard.q1.step0.question=Sú zahrnuté datasety chránené autorským zákonom alebo databázovými právami?\nwizard.q0.step0.question=Vytvorili ste dataset?\nlaw.reproduction.desc=\"Reprodukovať\" znamená vytvárať kópie predmetného diela akýmikoľvek prostriedkami vrátane vytvorenia neobmedzeného zvukového či vizuálneho záznamu a právo na opravu a reprodukciu opravy diela, vrátane jeho zabezpečenej formy skladovania alebo zvukového záznamu v digitálnej forme alebo inom elektronickom médiu.\nname=Názov\nwizard.menu.header=Čelím nasledovnému problému:\nlaw.notice.desc=Zachovať autorské práva a licenčné oznámenia nedotknuté\nwizard.q0.end1=Vaša databáza je pravdepodobne chránená databázovými právami (taktiež známe ako \"sui generis\" práva). Tu nájdete zoznam licencií, ktoré pokrývajú takéto práva.\nwizard.q0.end0=Kontaktujte, prosím, autora pôvodného datasetu kvôli skoordinovaniu uverejnenia.\nsettings.weighted=Vyvážené filtrovanie\nwizard.q1.step1.question=Zmenili ste pôvodné dielo?\nlaw.obligation.desc=Ste povinný:\nwizard.q0.step1.question=Je dataset Vašim duševným výtvorom čo sa týka selekcie a usporiadania údajov?\nsettings.advanced=Pokročilé nastavenia\nlaw.sharealike.desc=Šíriť odvodené diela pod rovnakou licenciou aká je uplatnená pri pôvodnom diele.\npage.show.title=Ukázať licenciu\nlaw.derivativeworks=Odvodené diela\npage.show.open=Domovská stránka otvorenej licencie\nlaw.attribution.desc=Uveďte korektný odkaz na autora a/alebo na vlastníka autorských práv\nlaw.prohibition.desc=Je zakázané:\nlaw.distribution.desc=Ďalšia distribúcia údajov\npage.filter.title=Filtrovať licencie\nlaw.commercialuse.desc=Použitie pôvodného diela na komerčné účely.\nlaw.statechanges=Zmeny podmienok\nlaw.copyleft.lesser=Voľnejšia licencia typu Copyleft\nlaw.notice=Oznam\nwizard.q0.step2.question=Boli pre získanie, overenie či prezentáciu obsahu vykonané kvalitatívne a/alebo kvantitatívne významné investície?\nlaw.copyleft=Licencia typu Copyleft\nlaw.attribution=Uvedenie autora (Attribution)\nterms=Podmienky\nlaw.copyleft.lesser.desc=Licencovať odvodené diela pod podmienkami podobnými tým, ktoré sú uvedené v licencii pôvodného diela.\nlaw.patentgrant=Patent grant\nlaw.patentgrant.desc=Použitie akýchkoľvek patentov poskytovateľa licencie, ktoré sa vzťahujú na využívanie diela, musí byť v súlade s autorskými právami.\nlaw.permission.desc=Môžete bez obmedzenia:\nlaw.sharealike=Rovnaké šírenie (Sharealike)\nlaw.reproduction=Reprodukcia\nyes=Áno\nlaw.permission=Povolenie\nwizard.q0.step2.explanation=Napríklad databáza, ktorá obsahuje exkluzívne geografické informácie vzťahujúce sa ku štátu ako celku, bude pokrytá týmito podmienkami. Dôvodom je to, že vytvorenie takej databázy si vyžaduje značné úsilie v zmysle času, peňazí, práce atď.\nwizard.q1.end1=Vaše dielo je s najväčšou pravdepodobnosťou odvodeným dielom. Nájdite si, prosím, vhodné licencie.\nwizard.q1.end0=Vaše dielo nie je odvodeným dielom. Napriek tomu môžu byť stále uplatniteľné zmluvné obmedzenia.\nno=Nie\nlaw.statechanges.desc=Uveďte zmeny, ktoré boli vykonané na pôvodnom licencovanom diele tak, aby bolo možné uviesť zdroj.\npage.filter.description=Nájdite príslušnú licenciu výberom vyhovujúcich licenčných podmienok uvedených nižšie:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("a", [], function() {
  return "#Generated from la_en.properties\n#Mon May 09 12:21:02 CEST 2016\nlaw.distribution=Distribution\nlaw.sublicensing.desc=Bevilja eller förläng en licens till programvaran\nlaw.derivativeworks.desc=Skapa bearbetningar av data\nwizard.menu.question1=Är mitt arbete som innehåller andra datamängder betraktat som en bearbetning?\nwizard.menu.question0=Jag önskar att publicera en ny datauppsättning. Kommer det att skyddas av lagen?\npage.show.compatible=Jämförbara licenser\nwizard.q1.end1.here=här\nlaw.sublicensing=Underlicensiering\nstartagain=Börja om\nlaw.commercialuse=Kommersiell användning\nwizard.q0.step1.explanation=Till exempel kommer en lista med telefonnummer för alla personer som arbetar i en organisation inte omfattas av upphovsrätten. Författaren kan varken visa någon kreativitet i urvalet av data (som är helt enkelt alla de människor som arbetar där) eller i valet av data (enkla namn och telefonnummer). Även om insamlingen av data vore mycket stort eller om en stor insats var inbegripen, krävs ingen kreativitet och därför inga upphovsrättsliga regler\nlaw.obligation=Skyldighet\nlaw.prohibition=Förbud\nwizard.q1.step0.question=Är de inbegripna datamängderna skyddade enligt lagen om upphovsrätt eller databasrättigheter?\nwizard.q0.step0.question=Har du redigerat datamängden?\nlaw.reproduction.desc=Kopiera innebär att göra kopior av arbetet på alla sätt inklusive, utan begränsning av ljud eller bildupptagningar och rätten till fixering och kopierade upptagningar av arbetet, inklusive lagring av en skyddad prestanda eller fonogram i digital form eller annat elektroniskt medium.\nname=Namn\nwizard.menu.header=Jag står inför följande problem:\nlaw.notice.desc=Håll upphovsrätt och licens meddelanden intakt\nwizard.q0.end1=Din databas är sannolikt skyddad under databasrättigheterna (även känd som \"sui generis rättigheter\"). Du kan hitta en lista av licenser som täcker dessa rättigheter.\nwizard.q0.end0=Vänligen kontakta den ursprungliga författaren till datamängden för att samordna publicering.\nsettings.weighted=Viktad filtrering\nwizard.q1.step1.question=Har du ändrat det ursprungliga arbetet?\nlaw.obligation.desc=Du är skyldig att:\nsettings.advanced=Avancerade inställningar\nlaw.sharealike.desc=Fördela bearbetningar under samma licens som det ursprungliga verket.\nwizard.q0.step1.question=Är datamängden din intellektuella skapelse genom val eller arrangemang?\npage.show.title=Visa licens\nlaw.derivativeworks=Bearbetningar\npage.show.open=Öppen licens hemsida\nlaw.attribution.desc=Ge korrekt kredit till upphovsrättsinnehavaren och / eller författare\nlaw.prohibition.desc=Det är förbjudet att:\nlaw.distribution.desc=Omfördela data\npage.filter.title=filterlicenser\nlaw.commercialuse.desc=Använda det ursprungliga arbetet för kommersiella ändamål.\nlaw.statechanges=Ange förändringar\nlaw.copyleft.lesser=Lesser Copyleft\nlaw.notice=Lägga märke till\nwizard.q0.step2.question=Fanns det någon kvalitativ och / eller kvantitativ väsentlig investering vid anskaffning, granskning eller presentation av innehållet?\nlaw.attribution=Erkännande\nlaw.copyleft=Copyleft\nterms=Villkor\nlaw.copyleft.lesser.desc=Licensbearbetningar fungerar under villkor som anges i licensen som liknar det ursprungliga arbetets.\nlaw.patentgrant=Patentsöknig\nlaw.patentgrant.desc=Använd de licensgivarpatent som gäller för arbetet i den mån som behövs för att använda det i enlighet med rättigheterna i upphovsrättsanvändningen\nlaw.permission.desc=Du är fri att:\nlaw.reproduction=Kopiering\nlaw.sharealike=Dela lika\nyes=Ja\nlaw.permission=Tillåtelse\nwizard.q0.step2.explanation=Till exempel kommer en databas som innehåller exklusiv geografisk information relaterad till ett helt land sannolikt omfattas av detta. Detta beror på att en sådan databas normalt kommer att kräva en betydande insats för att skapas vad gäller tid, pengar, ansträngning etc.\nwizard.q1.end1=Ditt arbete utgör troligen en bearbetning. Ta en titt på kompatibla licenser\nwizard.q1.end0=Ditt arbete utgör inte en bearbetning. Däremot kan avtalsmässiga begränsningar fortfarande gälla.\nno=Nej\nlaw.statechanges.desc=Ange vilka ändringar har gjorts till det ursprungliga licensierade arbetat på ett sätt som tillåter tillskrivning.\npage.filter.description=Du hittar en licens genom att välja föredragna licensvillkoren nedan:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("b", [], function() {
  return "#Generated from la_en.properties\n#Mon Aug 01 11:31:44 CEST 2016\nlaw.distribution=Distribuição\nlaw.sublicensing.desc=Conceder ou prolongar uma licença de software\nlaw.derivativeworks.desc=Criar trabalhos derivados dos dados\nwizard.menu.question1=A minha obra, que incorpora outros conjuntos de dados, é considerada trabalho derivado?\nwizard.menu.question0=Quero publicar um novo conjunto de dados. Estará protegido por lei?\npage.show.compatible=Licenças comparáveis\nwizard.q1.end1.here=Aqui\nlaw.sublicensing=Sublicenciamento\nstartagain=Recomeçar\nwizard.q0.step1.explanation=Por exemplo, uma lista dos números de telefone de todas as pessoas que trabalham na organização não estará coberta pelos direitos de autor. O autor não deve mostrar-se criativo na seleção dos dados (simplesmente todas as pessoas que lá trabalham) nem na escolha dos dados (simplesmente os nomes e os números de telefone). Ainda que a recolha de dados seja muito extensa e requeira grande esforço, não há exteriorização da criatividade e, assim sendo, não há lugar a direitos de autor.\nlaw.commercialuse=Utilização comercial\nlaw.obligation=Obrigação\nlaw.prohibition=Proibição\nwizard.q1.step0.question=Os conjuntos de dados incorporados estão protegidos por direitos de autor ou direitos de base de dados?\nwizard.q0.step0.question=É o autor do conjunto de dados?\nlaw.reproduction.desc=\"Reproduzir\" significa fazer cópias da obra por qualquer meio, incluindo, sem qualquer limitação, por gravação sonora ou visual, e o direito de fixar e reproduzir fixações da obra, incluindo o armazenamento de uma execução ou fonograma protegidos, em formato digital ou qualquer outro meio eletrónico. \nname=Nome\nwizard.menu.header=Tenho o problema seguinte\nlaw.notice.desc=Conserve intactas as declarações de direitos de autor e as licenças.\nwizard.q0.end1=A sua base de dados está provavelmente protegida por direitos de base de dados (também conhecidos como \"direitos sui generis\"). Pode encontrar aqui uma lista das licenças que cobrem estes direitos.\nwizard.q0.end0=Contacte o autor do conjunto de dados original para coordenar a publicação.\nsettings.weighted=Filtragem ponderada\nwizard.q1.step1.question=Modificou a obra original?\nlaw.obligation.desc=Está obrigado a\nsettings.advanced=Parâmetros avançados\nlaw.sharealike.desc=Distribuir trabalhos derivados sob a mesma licença que a obra original.\nwizard.q0.step1.question=O conjunto de dados é uma criação intelectual sua por seleção ou acordo?\npage.show.title=Mostrar a licença\nlaw.derivativeworks=Trabalhos derivados\npage.show.open=Página de acolhimento da licença aberta\nlaw.attribution.desc=Reconhecer o direito que assiste ao titular dos direitos de autor e/ou ao autor\nlaw.prohibition.desc=Está proibido de\nlaw.distribution.desc=Redistribuir os dados\npage.filter.title=Filtrar licenças\nlaw.commercialuse.desc=Utilizar a obra original para fins comerciais.\nlaw.notice=Aviso\nlaw.copyleft.lesser=Menor Copyleft\nlaw.statechanges=Mudança de estado\nwizard.q0.step2.question=Houve um investimento qualitativo e/ou quantivativamente substancial na obtenção, na verificação ou na apresentação dos conteúdos?\nlaw.copyleft=Cláusula copyleft\nlaw.attribution=Atribuição\nterms=Termos\nlaw.copyleft.lesser.desc=Trabalhos derivados sob uma licença igual ou com termos equivalentes à licença original.\nlaw.patentgrant.desc=Direito de utilização de quaisquer patentes de que o licenciante seja titular, na medida do necessário para a utilização dos direitos concedidos sobre a obra.\nlaw.permission.desc=É livre de\nyes=Sim\nlaw.sharealike=Compartilhar\nlaw.reproduction=Reprodução\nlaw.permission=Autorização\nwizard.q0.step2.explanation=Por exemplo, uma base de dados contendo a informação geográfica exclusiva associada a um país inteiro estará provavelmente coberta assim. Isto deve-se ao facto de a criação de uma tal base de dados requerer normalmente um esforço significativo em termos de tempo, dinheiro, trabalho, etc.\nwizard.q1.end1=A sua obra constitui muito provavelmente um trabalho derivado. Verifique as licenças compatíveis\nwizard.q1.end0=A sua obra não constitui um trabalho derivado. No entanto, podem ser aplicáveis restrições contratuais.\nno=Não\nlaw.statechanges.desc=Indique que alterações foram introduzidas no trabalho original sob licença de modo a permitir a atribuição.\npage.filter.description=Encontre uma licença selecionando a seguir os seus termos preferenciais\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("c", [], function() {
  return "#Generated from la_en.properties\n#Mon Aug 01 11:31:44 CEST 2016\nlaw.distribution=Disztribúció\nlaw.sublicensing.desc=Licencadás vagy a licenc meghosszabbítása a szoftver számára\nlaw.derivativeworks.desc=Hozzon létre származtatott tartalmakat az adatokból\nwizard.menu.question1=Más adatkészleteket magába foglaló alkotásom az adatkészletek származtatott tartalmának minősül?\nwizard.menu.question0=Új adatkészletet szeretnék közzétenni. Vonatkozik-e rá jogszabályi védelem?\npage.show.compatible=Hasonló licencek\nwizard.q1.end1.here=itt\nlaw.sublicensing=Allicencek\nstartagain=Újrakezdés\nlaw.commercialuse=Kereskedelmi felhasználás\nwizard.q0.step1.explanation=Egy szervezet dolgozóinak telefonszámára például nem vonatkozik a szerzői jog. A szerző nem lehet kreatív sem az adatok kijelölése (amely pusztán az ott dolgozókat jelenti), sem pedig az adatok kiválasztása (nevek és telefonszámok) tekintetében. Abban az esetben is, ha az adatok gyűjteménye nagy vagy ha nagy erőfeszítésbe került azok összegyűjtése, semmilyen kreativitásra nem volt szükség, ezért semmilyen szerzői jog nem alkalmazható\nlaw.obligation=Kötelezettségek\nlaw.prohibition=Tilalom\nwizard.q1.step0.question=A tartalomba beépített adatkészleteket védi-e a szerzői jog vagy az adatbázisjog?\nwizard.q0.step0.question=Ön az adatkészlet szerzője?\nlaw.reproduction.desc=A „másolás” azt jelenti, hogy másolatokat készít az alkotásról, ideértve többek között a hang- és képfelvételeket, valamint az alkotás rögzítésének és a rögzítések másolásának jogát, ideértve a védett előadások vagy hangfelvételek digitális formában vagy más elektronikus adathordozón történő tárolását.\nname=Név\nwizard.menu.header=A következő problémám van\nlaw.notice.desc=Hagyja érintetlenül a szerzői jogot és a licencmegjegyzéseket\nwizard.q0.end1=Adatbázisa valószínűleg az adatbázisjog védelme alá esik (más néven „sui generis” jogok). Az ezen jogokra érvényes licencek listáját itt találhatja.\nwizard.q0.end0=Lépjen kapcsolatba az eredeti adatkészlet szerzőjével a közzététel koordinálása tekintetében.\nsettings.weighted=Súlyozott szűrés\nwizard.q1.step1.question=Módosította az eredeti alkotást?\nlaw.obligation.desc=Kötelező\nlaw.sharealike.desc=Ossza meg a származtatott alkotásokat az eredeti alkotással megegyező licenc szerint.\nsettings.advanced=Speciális beállítások\nwizard.q0.step1.question=Az adatkészlet az ön kijelölésen vagy elrendezésen alapuló szellemi alkotása?\npage.show.title=Licenc megjelenítése\nlaw.derivativeworks=Származtatott tartalmak\npage.show.open=Nyílt licenc kezdőlap\nlaw.attribution.desc=Nevezze meg az előírásoknak megfelelően a szerzői jog tulajdonosát és/vagy a szerzőt\nlaw.prohibition.desc=Tilos\nlaw.distribution.desc=Az adatok továbbosztása\npage.filter.title=Licencek szűrése\nlaw.commercialuse.desc=az eredeti alkotás kereskedelmi célú felhasználása.\nlaw.statechanges=Állapotváltozás\nlaw.copyleft.lesser=Kevesebb copyleft\nlaw.notice=Tájékoztatás\nwizard.q0.step2.question=Jelentős mennyiségi és/vagy minőségi beruházásra volt-e szükség a tartalom megszerzéséhez, ellenőrzéséhez vagy bemutatásához?\nlaw.copyleft=Copyleft\nlaw.attribution=Szerző megnevezése\nterms=Feltételek\nlaw.copyleft.lesser.desc=A származtatott művek licencelése az eredeti alkotás licencében meghatározott feltételekhez hasonló feltételek szerint működik.\nlaw.patentgrant.desc=Használja a licencadónak az alkotásra vonatkozó bármelyik szabadalmát, amennyiben arra szükség van az alkotás szerzői jogoknak megfelelő felhasználásához\nlaw.permission.desc=Joga van\nlaw.sharealike=azonos továbbadási feltételek (Sharealike)\nlaw.reproduction=Másolás\nyes=Igen\nlaw.permission=Hozzájárulás\nwizard.q0.step2.explanation=Például egy országra vonatkozó kizárólagos földrajzi információkat tartalmazó adatbázisra valószínűleg ez a licenc vonatkozik. Ezért azért van így, mert egy ilyen adatbázis általában jelentős időbeli, anyagi stb. erőfeszítést igényel.\nwizard.q1.end1=Alkotása valószínűleg származtatott tartalomnak minősül. Tekintse meg a kompatibilis licenceket\nwizard.q1.end0=Alkotása nem minősül származtatott tartalomnak. Ugyanakkor azonban megtörténhet, hogy szerződéses korlátozások vonatkoznak rá.\nno=Nem\nlaw.statechanges.desc=Jelezze, hogy milyen módosításokat végzett az eredeti licencelt tartalmon olyan módon, amely lehetővé teszi a szerző megnevezését.\npage.filter.description=Válasszon ki egy licencet az előnyben részesített licencfeltételek kijelölésével\\:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("d", [], function() {
  return "#Generated from la_en.properties\n#Mon Aug 01 11:31:44 CEST 2016\nlaw.distribution=Distribucija\nlaw.sublicensing.desc=Dodijeli ili produži dozvolu softveru\nlaw.derivativeworks.desc=Napravite izvedene radove podataka\nwizard.menu.question1=Smatra li se moj rad koji uključuje druge skupove podataka njihovim izvedenim radom?\nwizard.menu.question0=Želim objaviti novi set podataka. Hoće li biti zakonom zaštićen?\npage.show.compatible=Usporedive licence\nwizard.q1.end1.here=ovdje\nlaw.sublicensing=Podlicenciranje\nstartagain=Ispočetka\nwizard.q0.step1.explanation=Primjerice, popis brojeva telefona svih ljudi koji rade u organizaciji neće biti obuhvaćen autorskim pravima. Autor ne može pokazati kreativnost u odabiru podataka (koji su jednostavno svi ljudi koji tamo rade) niti u odabiru podataka (jednostavna imena i brojevi telefona). Čak i da je skupina podataka vrlo velika ili da je uključeno puno truda, ne zahtijeva se kreativnost i stoga se ne primjenjuju pravila o autorskim pravima.\nlaw.commercialuse=Komercijalna uporaba\nlaw.obligation=Obveza\nlaw.prohibition=Zabrana\nwizard.q1.step0.question=Jesu li inkorporirani setovi podataka zaštićeni sukladno zakonu o autorskim pravima ili zakonu o pravima na baze podataka?\nwizard.q0.step0.question=Jeste li vi autor skupa podataka?\nlaw.reproduction.desc=„Reproducirati” znači kopirati rad bilo kojim putem uključujući bez ograničenja zvučnim ili vizualnim snimkama i pravo fiksacija i reproduciranja fiksacija rada, uključujući pohranjivanje zaštićene izvedbe ili fonograma u digitalnom obliku ili drugih elektroničkih medija.\nname=Ime\nwizard.menu.header=Suočen sam sa sljedećim problemom\nlaw.notice.desc=Obavijesti o autorskim pravima i dozvolama trebaju ostati nedirnutima\nwizard.q0.end1=Vaša baza podataka vjerojatno je zaštićena pravima o bazama podataka (također poznata kao „sui generis prava”). Možete pronaći popis dozvola koje pokrivaju ova prava ovdje.\nwizard.q0.end0=Molimo kontaktirajte autora originalnog skupa podataka za koordiniranje publikacije.\nsettings.weighted=Ponderirano filtriranje\nwizard.q1.step1.question=Jeste li modificirali originalni rad?\nlaw.obligation.desc=Obvezni ste\nsettings.advanced=Napredne postavke\nlaw.sharealike.desc=Distribuiraj izvedene radove s istom dozvolom kao i izvorni rad.\nwizard.q0.step1.question=Je li skup podataka vaše intelektualno ostvarenje odabirom ili slaganjem?\npage.show.title=Prikaži dozvolu\nlaw.derivativeworks=Izvedeni radovi\npage.show.open=Početna stranica otvorene dozvole\nlaw.attribution.desc=Dajte potrebno priznanje nositelju i/ili autoru zaštite autorskih prava\nlaw.prohibition.desc=Zabranjeno je\nlaw.distribution.desc=Redistribuiraj podatke\npage.filter.title=Filtriraj dozvole\nlaw.commercialuse.desc=Korištenje izvornog rada u komercijalne svrhe.\nlaw.statechanges=Promjena države\nlaw.copyleft.lesser=Manje Copylefta\nlaw.notice=Obavijest\nwizard.q0.step2.question=Je li bilo kvalitativnog i/ili kvantitativnog naknadnog ulaganja u dobivanje, verifikaciju ili prezentiranje sadržaja?\nlaw.attribution=Dodjeljivanje\nlaw.copyleft=Copyleft\nterms=Uvjeti\nlaw.copyleft.lesser.desc=Licencirajte izvedene radove prema uvjetima navedenima u dozvoli, a koji su slični uvjetima za originalni rad.\nlaw.patentgrant.desc=Koristite bilo koji patent davatelja dozvole primjenjiv na rad u mjeri u kojoj je potrebno kako biste rad koristili radu skladu s korištenjem autorskih prava\nlaw.permission.desc=Slobodno možete\nlaw.reproduction=Reprodukcija\nyes=Da\nlaw.sharealike=Dijeli pod istim uvjetima\nlaw.permission=Dopuštenje\nwizard.q0.step2.explanation=Primjerice, ovo će najvjerojatnije obuhvatiti bazu podataka koja sadrži ekskluzivne geografske informacije vezane uz cijelu državu. Ovo je zbog toga što će takva baza podataka obično zahtijevati značajan trud po pitanju vremena, novca, truda, itd.\nwizard.q1.end1=Vaš rad vrlo vjerojatno sadrži izvedeni rad. Molimo pogledajte kompatibilne dozvole\nwizard.q1.end0=Vaš rad ne sadržava izvedeni rad. Međutim, ugovorna ograničenja i dalje se mogu primjenjivati.\nno=Ne\nlaw.statechanges.desc=Navedite koje su promjene napravljene u izvornom radu s dozvolom na način koji dozvoljava dodjeljivanje.\npage.filter.description=Molimo potražite dozvolu odabirom preferiranih uvjeta dodjele dozvola u nastavku\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("e", [], function() {
  return "#Generated from la_en.properties\n#Mon Sep 05 17:59:54 CEST 2016\nlaw.distribution=Distribuire\nlaw.sublicensing.desc=Acordați sau extindeți o licență pentru software\nlaw.derivativeworks.desc=Creați lucrări derivate din date\nwizard.menu.question1=Este lucrarea mea, care încorporează alte seturi de date, considerată o lucrare derivată a acestora?\nwizard.menu.question0=Vreau să public un nou set de date. Va fi protejat prin lege?\npage.show.compatible=Licențe comparabile\nwizard.q1.end1.here=aici\nlaw.sublicensing=Sublicențiere\nstartagain=Reîncepeți\nwizard.q0.step1.explanation=De exemplu, o listă cu numerele de telefon ale tuturor persoanelor care lucrează în cadrul unei organizații nu va intra sub incidența dreptului de autor. Autorul nu poate demonstra nicio creativitate în selectarea datelor (este vorba, pur și simplu, de persoanele care lucrează acolo) și nici în alegerea datelor (sunt, pur și simplu, nume și numere de telefon). Chiar dacă colectarea datelor a fost de anvergură sau a presupus multe eforturi, nu a fost necesară nicio creativitate, astfel că nu se aplică regulile dreptului de autor.\nlaw.commercialuse=Utilizare comercială\nlaw.obligation=Obligație\nlaw.prohibition=Interdicție\nwizard.q1.step0.question=Sunt seturile de date încorporate protejate în temeiul legislației privind drepturile de autor sau drepturile asupra bazelor de date?\nlaw.reproduction.desc=„A reproduce” înseamnă a face copii ale lucrării prin orice mijloace, inclusiv, dar fără a se limita la înregistrări audio sau video și dreptul de fixare și de reproducere a fixărilor lucrării, inclusiv stocarea unei reprezentații sau fonograme protejate sub formă digitală sau prin orice alt mijloc electronic.\nwizard.q0.step0.question=Sunteți autorul setului de date?\nname=Nume\nwizard.menu.header=Mă confrunt cu următoarea problemă:\nlaw.notice.desc=Păstrați intacte înștiințările referitoare la dreptul de autor și licență\nwizard.q0.end1=Baza dumneavoastră de date este probabil protejată în temeiul drepturilor asupra bazelor de date (denumite și „drepturi sui generis”). Puteți găsi o listă a licențelor care acoperă aceste drepturi aici.\nwizard.q0.end0=Vă rugăm să contactați autorul inițial al setului de date pentru coordonarea publicării.\nsettings.weighted=Filtrare ponderată\nwizard.q1.step1.question=Ați modificat lucrarea originală?\nlaw.obligation.desc=Sunteți obligat să\\:\nwizard.q0.step1.question=Este setul de date creația dumneavoastră intelectuală prin selecție sau aranjare?\nlaw.sharealike.desc=Distribuiți lucrări derivate sub aceeași licență ca lucrarea originală.\nsettings.advanced=Setări avansate\npage.show.title=Afișare licență\nlaw.derivativeworks=Lucrări derivate\npage.show.open=Deschidere pagină principală licență\nlaw.attribution.desc=Creditați în mod corespunzător deținătorul dreptului de autor și/sau autorul\nlaw.prohibition.desc=Vi se interzice să\\:\nlaw.distribution.desc=Redistribuiți datele\npage.filter.title=Filtrare licențe\nlaw.commercialuse.desc=Folosiți lucrarea originală în scopuri comerciale.\nlaw.statechanges=Modificări ale stării\nlaw.copyleft.lesser=Copyleft limitat\nlaw.notice=Înștiințare\nwizard.q0.step2.question=A existat o investiție calitativă și/sau cantitativă substanțială pentru obținerea, verificarea sau prezentarea conținutului?\nlaw.copyleft=Copyleft\nlaw.attribution=Atribuire\nterms=Termeni\nlaw.copyleft.lesser.desc=Licențiați lucrările derivate în temeiul termenilor specificați în licență, similari termenilor lucrării originale.\nlaw.patentgrant.desc=Utilizați orice brevete ale licențiatorului aplicabile lucrării în măsura necesară pentru a folosi lucrarea în conformitate cu drepturile de utilizare supuse dreptului de autor\nlaw.permission.desc=Sunteți liber să\\:\nlaw.sharealike=Partajarea similară\nlaw.reproduction=Reproducere\nyes=Da\nlaw.permission=Permisiune\nwizard.q0.step2.explanation=De exemplu, o bază de date care conține informații geografice exclusive referitoare la o țară întreagă vor intra, probabil, aici, deoarece o astfel de bază de date necesită, în mod normal, eforturi substanțiale în materie de timp alocat, bani, demersuri etc.\nwizard.q1.end1=Lucrarea dumneavoastră constituie, cel mai probabil, o lucrare derivată. Vă rugăm să consultați licențele compatibile.\nwizard.q1.end0=Lucrarea dumneavoastră nu constituie o lucrare derivată. Totuși, s-ar putea aplica unele restricții contractuale.\nno=Nu\nlaw.statechanges.desc=Indicați ce modificări au fost aduse lucrării originale licențiate într-o manieră care permite atribuirea.\npage.filter.description=Vă rugăm să găsiți o licență selectând termenii de licențiere preferați de mai jos:\npage.filter.introduction=Datele partajate cu o licență devin date deschise. Există multe tipuri de licențe disponibile. Asistentul pentru licențe vă oferă o descriere a licențelor disponibile. De asemenea, vă oferă o imagine de ansamblu a modului în care se pot aplica licențe ca entitate care re-publică/distribuie date deschise și a modului în care pot fi combinate mai multe licențe.";
});

})();
(function() {
var define = $__System.amdDefine;
define("f", [], function() {
  return "#Generated from la_en.properties\n#Mon Sep 05 17:49:48 CEST 2016\nlaw.distribution=Distribuce\nlaw.sublicensing.desc=Udělit nebo rozšířit licenci na software\nwizard.menu.question1=Lze moji práci, jejíž součástí jsou jiné soubory dat, považovat za práci z nich odvozenou?\nlaw.derivativeworks.desc=Vytvořit z dat odvozené práce\nwizard.menu.question0=Chci zveřejnit nový soubor dat. Bude chráněn zákonem?\npage.show.compatible=Srovnatelné licence\nwizard.q1.end1.here=zde\nlaw.sublicensing=Sublicencování\nstartagain=Začít znovu\nlaw.commercialuse=Obchodní použití\nwizard.q0.step1.explanation=Například na seznam telefonních čísel všech lidí pracujících v organizaci se nevztahují autorská práva. Autor nemůže k výběru dat (což znamená jednoduše všechny lidi, kteří zde pracují) ani k výběru z nich (jednoduchá jména a telefonní čísla) přistupovat kreativně. I kdyby byl soubor dat opravdu velký nebo to znamenalo značné úsilí, nebylo třeba kreativity, a tudíž se na tento případ nevztahují pravidla pro autorská práva\nlaw.obligation=Povinnost\nlaw.prohibition=Zákaz\nwizard.q1.step0.question=Jsou obsažené soubory dat chráněny zákonem o autorských právech nebo databázovými právy?\nlaw.reproduction.desc=„Reprodukovat“ znamená jakýmkoli způsobem pořizovat kopie práce včetně mimo jiné zvukového nebo vizuálního záznamu a práva na fixaci a reprodukci fixace práce včetně ukládání chráněného výkonu nebo záznamu v digitální podobě nebo na jiném elektronickém médiu.\nwizard.q0.step0.question=Jste autorem souboru dat?\nname=Jméno\nwizard.menu.header=Mám následující problém:\nlaw.notice.desc=Nechat autorská práva a licenci beze změn\nwizard.q0.end1=Vaše databáze je pravděpodobně chráněna databázovými právy (označovanými též jako „práva sui generis“). Seznam licencí vztahujících se na tato práva je k dispozici zde.\nwizard.q0.end0=Pro koordinaci zveřejnění kontaktujte autora původních souborů dat.\nsettings.weighted=Vážené filtrování\nwizard.q1.step1.question=Upravil(a) jste původní práci?\nlaw.obligation.desc=Máte povinnost:\nlaw.sharealike.desc=Distribuovat odvozené práce na základě stejné licence jako původní práce.\nsettings.advanced=Pokročilé nastavení\nwizard.q0.step1.question=Je soubor dat vaší duševní tvorbou na základě výběru nebo uspořádání?\npage.show.title=Zobrazit licenci\nlaw.derivativeworks=Odvozené práce\npage.show.open=Domovská stránka otevřené licence\nlaw.attribution.desc=Náležitě uznat vlastníka autorských práv nebo autora\nlaw.prohibition.desc=Máte zakázáno:\nlaw.distribution.desc=Redistribuovat data\npage.filter.title=Filtrovat licence\nlaw.commercialuse.desc=Používat původní práci pro obchodní účely.\nlaw.copyleft.lesser=Lesser Copyleft\nlaw.statechanges=Státní změny\nlaw.notice=Upozornění\nwizard.q0.step2.question=Vyžádalo si získání, ověření nebo prezentace obsahu výraznější kvalitativní nebo kvantitativní investici?\nlaw.copyleft=Copyleft\nlaw.attribution=Uvedení zdroje\nterms=Pojmy\nlaw.copyleft.lesser.desc=Licencování odvozených prací na základě podmínek specifikovaných v licenci, které jsou podobné podmínkám původní práce.\nlaw.patentgrant.desc=Používat jakékoli patenty poskytovatele licence vhodné pro práci vzhledem k tomu, že je práci potřeba používat v souladu s právy na užívání autorských práv\nlaw.permission.desc=Můžete volně:\nlaw.reproduction=Reprodukce\nyes=Ano\nlaw.sharealike=Sharealike\nlaw.permission=Povolení\nwizard.q0.step2.explanation=Jedná se pravděpodobně například o databáze obsahující exkluzivní zeměpisné informace týkající se celé země. Důvodem je, že vytvoření takové databáze obvykle vyžaduje značné úsilí, pokud jde o čas, peníze, práci aj.\nwizard.q1.end1=Vaše práce je s největší pravděpodobností odvozenou prací. Podívejte se na kompatibilní licence\nwizard.q1.end0=Vaše práce není odvozenou prací. Přesto se na tento případ mohou vztahovat smluvní omezení.\nno=Ne\nlaw.statechanges.desc=Uveďte, jaké změny byly s původní licencovanou prací provedeny v tom směru, že by to umožňovalo uvést zdroj.\npage.filter.description=Vyberte licenci dle preferovaných licenčních podmínek:\npage.filter.introduction=Data, která se sdílejí s licencí, jsou otevřenými daty. K dispozici je celá řada licencí. Průvodce licencemi vám dá popis dostupných licencí. Součástí je také přehled, jak licence uplatňovat v případě opakovaného zveřejnění či distribuce otevřených dat a jak kombinovat více licencí.";
});

})();
(function() {
var define = $__System.amdDefine;
define("10", [], function() {
  return "#Generated from la_en.properties\n#Mon Sep 05 18:00:39 CEST 2016\nlaw.distribution=Jakelu\nlaw.sublicensing.desc=Myönnä tai pidennä ohjelmistolisenssi\nlaw.derivativeworks.desc=Luo datasta johdannaisteoksia.\nwizard.menu.question1=Onko muita tietoaineistoja sisällyttävä työni niiden johdannaisteos?\nwizard.menu.question0=Haluan julkaista uuden tietoaineiston. Onko se suojattu lailla?\npage.show.compatible=Vastaavat lisenssit\nwizard.q1.end1.here=tästä\nlaw.sublicensing=Alilisensointi\nstartagain=Aloita uudestaan\nwizard.q0.step1.explanation=Esimerkiksi luettelo kaikkien organisaatiossa työskentelevien henkilöiden puhelinnumeroista ei kuulu tekijänoikeuksien alaisuuteen. Sisällön laatija ei voi osoittaa minkäänlaista luovuutta datan valinnassa (joka on yksinkertaisesti kaikki paikan työntekijät) tai valita dataa (vain nimet ja puhelinnumerot). Vaikka kerätty data-aineisto olisi hyvin laaja tai vaatinut paljon vaivaa, luovuutta ei tarvittu ja siksi tekijänoikeussäännökset eivät päde\nlaw.commercialuse=Kaupallinen käyttö\nlaw.obligation=Velvollisuus\nlaw.prohibition=Kielto\nwizard.q1.step0.question=Onko yhdistetyt tietoaineistot suojattu tekijänoikeuslailla tai tietokantaoikeuksilla?\nwizard.q0.step0.question=Oletko tietoaineiston laatija?\nlaw.reproduction.desc=\"Jäljentää\" tarkoittaa kopioiden ottamista teoksesta millä tahansa tavoin, mukaan lukien ilman rajoitusta ääni- ja visuaaliset tallennukset ja oikeus tallentaa ja jäljentää teoksen tallennuksia, mukaan lukien suojatun esityksen tai äänitteen tallennus digitaaliseen muotoon tai sähköiselle medialle.\nname=Nimi\nwizard.menu.header=Minulla on seuraava ongelma:\nlaw.notice.desc=Pidä tekijänoikeus- ja lisenssimaininnat kunnossa\nwizard.q0.end1=Tietokantasi on todennäköisesti suojattu tietokantaoikeuksilla (ns. \"sui generis\" eli erityinen suoja). Löydät luettelon nämä oikeudet kattavista lisensseistä täältä.\nwizard.q0.end0=Ota yhteyttä alkuperäisen tietoaineiston laatijaan saadaksesi koordinoidun julkaisun.\nsettings.weighted=Painotettu suodatus\nwizard.q1.step1.question=Muokkasitko alkuperäistä teosta?\nlaw.obligation.desc=Sinulla on velvollisuus:\nlaw.sharealike.desc=Jaa johdannaisteos samalla lisenssillä kuin alkuperäinen teos.\nsettings.advanced=Lisäasetukset\nwizard.q0.step1.question=Onko tietoaineisto henkinen luomistyösi valinnan tai järjestelyn pohjalta?\npage.show.title=Näytä lisenssi\nlaw.derivativeworks=Johdannaisteokset\npage.show.open=Avaa lisenssin kotisivu\nlaw.attribution.desc=Nimeä tekijänoikeuksien haltija ja/tai kirjoittaja asianmukaisesti.\nlaw.prohibition.desc=Sinun ei ole lupa:\nlaw.distribution.desc=Jakele data uudestaan\npage.filter.title=Suodata lisenssit\nlaw.commercialuse.desc=Käyttää alkuperäistä teosta kaupallisiin tarkoituksiin.\nlaw.notice=Huomautus\nlaw.statechanges=Tilamuutokset\nlaw.copyleft.lesser=Heikko käyttäjänoikeus\nwizard.q0.step2.question=Liittyikö sisällön hankintaan, tarkistukseen tai esittämiseen laadullisesti ja/tai määrällisesti merkittävä sijoitus?\nlaw.attribution=Nimeäminen\nlaw.copyleft=Käyttäjänoikeus\nterms=Ehdot\nlaw.copyleft.lesser.desc=Lisenssin johdannaistyöt lisenssissä määritellyin ehdoin, jotka ovat samanlaiset kuin alkuperäisen teoksen.\nlaw.patentgrant.desc=Käytä lisenssinantajan teokseen kohdistuvia patentteja tarpeen mukaan, jotta käyttäisit teosta tekijänoikeuden käyttöoikeuksien mukaisesti.\nlaw.permission.desc=Voit vapaasti\\:\nlaw.reproduction=Kopiointi\nlaw.sharealike=Sama lisenssi\nyes=Kyllä\nlaw.permission=Lupa\nwizard.q0.step2.explanation=Esimerkiksi tietokanta, joka sisältää koko maahan liittyvää eksklusiivista maantieteellistä tietoa on todennäköisesti tämän alainen. Syynä on se, että tällaisen tietokannan luonti yleensä vaatii merkittävää vaivannäköä ajallisesti, rahallisesti jne.\nwizard.q1.end1=Työsi muodostaa mitä todennäköisimmin johdannaisteoksen. Tarkista yhteensopivat lisenssit\nwizard.q1.end0=Työsi ei muodosta johdannaisteosta. Sopimukselliset rajoitukset saattavat kuitenkin silti olla voimassa.\nno=Ei\nlaw.statechanges.desc=Osoita, mitä muutoksia alkuperäiseen lisensoituun teokseen on tehty tavalla, joka sallii nimeämisen.\npage.filter.description=Hae lisenssi valitsemalla halutut lisenssiehdot alla:\npage.filter.introduction=Lisenssillä jaetusta datasta tulee avointa dataa. Lisenssejä on paljon saataville. Lisenssiassistentti tarjoaa kuvauksen saatavilla olevista lisensseistä. Se antaa myös yleisohjeet siitä, miten haet lisenssejä avoimen datan uudelleenjulkaisijana/jakelijana, ja miten yhdistät useita lisenssejä.";
});

})();
(function() {
var define = $__System.amdDefine;
define("11", [], function() {
  return "#language converter\n#Mon Nov 07 13:09:32 CET 2016\nlaw.distribution=Разпространение\nlaw.sublicensing.desc=Предоставяне или удължаване на лиценз за софтуера\nwizard.menu.question1=Работата ми, която включва други набори от данни, счита ли се за тяхна производна работа?\nlaw.derivativeworks.desc=Създаване на производни работи на данните\nwizard.menu.question0=Искам да публикувам нов набор от данни. Ще бъде ли защитен от закона?\npage.show.compatible=Сравними лицензи\nwizard.q1.end1.here=тук\nlaw.sublicensing=Сублицензиране\npage.filter.introduction=Данни, които са споделени чрез лиценз, стават свободно достъпни данни. Предлагат се много лицензи. Асистентът за лиценз предоставя описание на наличните лицензи. Той също така представя начини за прилагане на лицензите като следващ издател/дистрибутор на свободно достъпни данни и начини за съчетаване на няколко лиценза.\nstartagain=Стартирай отново\nlaw.copyleft.desc=\nwizard.q0.step1.explanation=Например списък с телефонни номера на всички лица, които работят в една организация, няма да бъде обхванат от авторско право. Авторът не показва творчество при подбора на данните (които просто са всички хора, които работят там), нито в избора на данни (просто имена и телефонни номера). Дори събирането на данни да е било много обширно или да е отнело много усилия, не се е изисквало творчество и поради това не се прилагат правилата за авторско право\nlaw.commercialuse=Търговско приложение\nlaw.obligation=Задължение\nlaw.prohibition=Забрана\nwizard.q1.step0.question=Защитени ли са включените набори от данни съгласно закона за авторското право или правата върху базите данни?\nwizard.q0.step0.question=Вие ли сте автор на набора от данни?\nlaw.reproduction.desc=„Възпроизвеждане“ означава да се направят копия на Работата посредством всякакви средства, включително, без ограничение до, аудио или видео записи, и правото на фиксиране и възпроизвеждане на записите на Работата, включително съхранение на защитено изпълнение или звукозапис в цифров вид или на друг електронен носител.\nname=Име\nwizard.menu.header=Срещнах следния проблем\\:\nlaw.notice.desc=Запазете известяванията за авторско право и лиценз непроменени\nwizard.q0.end3=За съжаление вашият набор от данни най-вероятно не попада в обхвата на авторското право и/или правата върху базите данни. Моля, консултирайте се с адвокат за допълнителен съвет по тази тема.\nwizard.q0.end2=Наборът от данни вероятно е защитен от закона за авторското право и вие можете да изберете приложим лиценз.\nwizard.q0.end1=Вашата база данни вероятно е защитена от правата върху базите данни (известни още като „единствени по рода си права“). Можете да намерите списък на лицензи, които обхващат тези права\nwizard.q0.end0=Свържете се с автора на оригиналния набор от данни, за да координирате публикуването му.\nlaw.lessercopyleft=Второстепенен Copyleft\nsettings.weighted=Претеглено филтриране\nwizard.q1.step1.question=Променили ли сте оригиналната работа?\nlaw.obligation.desc=Задължени сте да\\:\nlaw.sharealike.desc=Разпространението на производните работи е със същия лиценз, като този на оригиналната работа.\nsettings.advanced=Разширени настройки\\:\nwizard.q0.step1.question=Наборът от данни ваше интелектуално творение ли е, ако е създаден чрез подбор или систематизиране?\npage.show.title=Показване на лиценз\nlaw.patentgrant.desc=Използвайте всички патенти на лицензодателя, приложими за работата, доколкото е необходимо, за да използвате работата в съответствие с правата за ползване на авторско право\nlaw.derivativeworks=Производни работи\npage.show.open=Открит лиценз начална страница\nlaw.attribution.desc=Отдайте подобаващо признание на притежателя на авторското право и/или на автора\nlaw.prohibition.desc=Забранено ви е да\\:\nlaw.distribution.desc=Повторна дистрибуция на данните\npage.filter.title=Лицензи за филтриране\nlaw.lessercopyleft.desc=Лицензионните производни произведения отговарят на условията, посочени в лиценза, които са подобни на тези на оригиналната работа.\nlaw.commercialuse.desc=Използване на оригиналната работа за търговски цели.\nlaw.notice=Известие\nlaw.statechanges=Състоянието се променя\nwizard.q0.step2.question=Имало ли е качествено и/или количествено съществени инвестиции за придобиването, проверката или представянето на съдържанието?\nlaw.attribution=Приписване\nlaw.copyleft=Copyleft\nterms=Условия\nlaw.patentgrant=Използване на патентни претенции\nlaw.permission.desc=Свободни сте да\\:\nyes=Да\nlaw.reproduction=Възпроизвеждане\nlaw.sharealike=Споделяй споделеното \nlaw.permission=Разрешение\nwizard.q0.step2.explanation=Например една база данни, съдържаща изключителна географска информация, отнасяща се за цяла държава, вероятно ще бъде обхваната от този Copyleft. Това е така, защото създаването на една такава база данни обикновено изисква значителни усилия по отношение на време, пари, старание и т.н.\nwizard.q1.end1=Вашата работа най-вероятно е производна работа. Моля, запознайте се със съвместимите лицензи.\nwizard.q1.end0=Вашата работа не е производна работа. Обаче може да се прилагат договорни ограничения.\nno=Няма\nlaw.statechanges.desc=Посочете какви промени са направени в оригиналната лицензирана работа по начин, който да позволява приписване.\npage.filter.description=Намерете лиценз, като изберете предпочитаните условия за лиценза по-долу\\:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("12", [], function() {
  return "#language converter\n#Mon Nov 07 13:09:33 CET 2016\nlaw.distribution=Προώθηση\nlaw.sublicensing.desc=Παραχώρηση ή επέκταση άδειας στο λογισμικό\nwizard.menu.question1=Το έργο μου, στο οποίο ενσωματώνονται άλλα σύνολα δεδομένων, θεωρείται παράγωγο των πρωτότυτων έργων;\nlaw.derivativeworks.desc=Δημιουργήστε παράγωγα έργα των δεδομένων\nwizard.menu.question0=Επιθυμώ να δημοσιεύσω ένα νέο σύνολο δεδομένων. Θα προστατεύεται από το νόμο;\npage.show.compatible=Συγκρίσιμες άδειες\nwizard.q1.end1.here=εδώ\nlaw.sublicensing=Υποαδειοδότηση\npage.filter.introduction=Τα δεδομένα που δίνονται προς κοινή χρήση με άδεια, καθίστανται Ανοιχτά Δεδομένα. Διατίθενται ποικίλες άδειες. Στον «Βοηθό ανοιχτών αδειών» παρουσιάζονται οι διαθέσιμες άδειες, γίνεται επισκόπηση της εφαρμογής των αδειών από τον επανεκδότη/διανομέα Ανοιχτών Δεδομένων και δίνονται οδηγίες για τον συνδυασμό πολλαπλών αδειών.\nstartagain=Επανεκκίνηση\nwizard.q0.step1.explanation=Για παράδειγμα, ο κατάλογος τηλεφώνων του προσωπικού που εργάζεται σε κάποιον οργανισμό δεν καλύπτεται από πνευματικά δικαιώματα. Ο συντάκτης του δεν είναι δημιουργός όταν επιλέγει τα δεδομένα (τα οποία είναι, απλούστατα, όλοι οι εκεί εργαζόμενοι) και τις αντιστοιχίες τους (απλά ονόματα και τηλεφωνικοί αριθμοί). Ακόμα και αν συλλέχθηκε πολύ μεγάλος όγκος δεδομένων, ακόμα και αν καταβλήθηκε πολύς κόπος, δεν χρειάστηκε να δημιουργήσει κάτι, οπότε δεν ισχύουν οι κανόνες περί πνευματικών δικαιωμάτων. \nlaw.commercialuse=Εμπορική χρήση\nlaw.obligation=Υποχρέωση\nlaw.prohibition=Απαγόρευση\nwizard.q1.step0.question=Τα ενσωματωθέντα σύνολα δεδομένων προστατεύονται από τη νομοθεσία περί πνευματικών δικαιωμάτων ή/και από δικαιώματα βάσης δεδομένων;\nwizard.q0.step0.question=Είστε ο δημιουργός του συνόλου δεδομένων;\nlaw.reproduction.desc=Αναπαραγωγή σημαίνει παραγωγή αντιτύπων του Έργου με οιαδήποτε μέσα, όπως, μεταξύ άλλων, με ηχογράφηση ή βιντεοσκόπηση, καθώς και δικαίωμα υλικής ενσωμάτωσης και αναπαραγωγής των υλικών ενσωματώσεων του Έργου, συμπεριλαμβανομένης της αποθήκευσης κάποιας προστατευμένης ερμηνείας/εκτέλεσης ή φωνογραφήματος σε ψηφιακή μορφή ή σε άλλο ηλεκτρονικό μέσο.\nname=Όνομα\nwizard.menu.header=Αντιμετωπίζω το εξής πρόβλημα\\:\nlaw.notice.desc=Τηρείτε άθικτες τις ανακοινώσεις περί πνευματικών δικαιωμάτων και αδειών.\nwizard.q0.end3=Δυστυχώς, το σύνολο δεδομένων σας πιθανότατα δεν προστατεύεται από δικαιώματα πνευματικής ιδιοκτησίας ή/και από δικαιώματα βάσης δεδομένων. Συμβουλευθείτε δικηγόρο σχετικά με το θέμα. \nwizard.q0.end2=Το σύνολο δεδομένων πιθανότατα προστατεύεται από πνευματικά δικαιώματα και μπορείτε να επιλέξετε την κατάλληλη άδεια.\nwizard.q0.end1=Η βάση δεδομένων σας πιθανότατα προστατεύεται από δικαιώματα βάσης δεδομένων, γνωστά και ως συγγενικά δικαιώματα ειδικής φύσεως (sui generis). Σας διατίθεται κατάλογος αδειών που καλύπτουν τα εν λόγω δικαιώματα\nwizard.q0.end0=Επικοινωνήστε με τον δημιουργό του πρωτοτύπου συνόλου δεδομένων για να συντονίσετε τη δημοσίευση.\nlaw.lessercopyleft=Περιορισμένη άδεια copyleft (Lesser Copyleft)\nsettings.weighted=Σταθμισμένο φιλτράρισμα\nwizard.q1.step1.question=Επιφέρατε μεταβολές στο πρωτότυπο έργο;\nlaw.obligation.desc=Υποχρεούστε να\\:\nlaw.sharealike.desc=Προωθήστε παράγωγα έργα δυνάμει της άδειας που ισχύει και για το πρωτότυπο έργο.\nsettings.advanced=Ρυθμίσεις για προχωρημένους\nwizard.q0.step1.question=Αποτελεί το σύνολο δεδομένων δική σας διανοητική δημιουργία βάσει επιλογής/ή συμφωνίας;\npage.show.title=Προβολή αδειών\nlaw.patentgrant.desc=Κάντε χρήση των ευρεσιτεχνιών του αδειοδότη οι οποίες ισχύουν για το έργο, στον βαθμό που χρειάζεται για να χρησιμοποιήσετε το έργο σύμφωνα με τα δικαιώματα χρήσης πνευματικών δικαιωμάτων\nlaw.derivativeworks=Παράγωγα έργα\npage.show.open=Αρχική σελίδα σχετικά με τις Ανοιχτές άδειες\nlaw.attribution.desc=Αναφερθείτε δεόντως στον κάτοχο των πνευματικών δικαιωμάτων και στον δημιουργό.\nlaw.prohibition.desc=Σας απαγορεύονται τα εξής\\:\nlaw.distribution.desc=Προωθήστε περαιτέρω τα δεδομένα\npage.filter.title=Φιλτράρισμα αδειών\nlaw.lessercopyleft.desc=Αδειοδοτήστε παράγωγα έργα δυνάμει των όρων που ορίζονται στην άδεια και είναι παρόμοιοι με του όρους του πρωτοτύπου έργου.\nlaw.commercialuse.desc=Χρήση του πρωτότυπου έργου για εμπορικούς σκοπούς.\nlaw.notice=Ανακοίνωση\nlaw.statechanges=Δηλώστε τις μετατροπές\nwizard.q0.step2.question=Έγινε σημαντική ποιοτική ή/και ποσοτική επένδυση στην απόκτηση, την επαλήθευση ή την παρουσίαση των περιεχομένων;\nlaw.attribution=Απόδοση\nlaw.copyleft=Άδεια copyleft\nterms=Όροι\nlaw.patentgrant=Κάντε χρήση αξιώσεων ευρεσιτεχνίας\nlaw.permission.desc=Είστε ελεύθεροι να πράξετε τα εξής\\:\nyes=Ναι\nlaw.reproduction=Αναπαραγωγή\nlaw.sharealike=Δικαιώματα share-alike\nlaw.permission=Άδεια\nwizard.q0.step2.explanation=Για παράδειγμα, μια βάση δεδομένων που περιέχει αποκλειστικές γεωγραφικές πληροφορίες σχετικά με ολόκληρη χώρα, πιθανότατα καλύπτεται από την εν λόγω άδεια, δεδομένου ότι η δημιουργία τέτοιας βάσης δεδομένων απαιτεί συνήθως σημαντική προετοιμασία, δηλαδή χρόνο, χρήματα, κόπο, κτλ.\nwizard.q1.end1=Το έργο σας είναι πιθανότατα παράγωγο έργο. Δείτε τις συμβατές άδειες.\nwizard.q1.end0=Το έργο σας δεν είναι παράγωγο έργο. Ενδέχεται ωστόσο να ισχύουν περιορισμοί.\nno=Όχι\nlaw.statechanges.desc=Υποδείξτε τις μετατροπές που έγιναν στο πρωτότυπο αδειοδοτημένο έργο, με τρόπο ώστε να είναι εφικτή η απόδοσή τους στον μετατροπέα.\npage.filter.description=Παρακαλούμε βρείτε άδεια επιλέγοντας παρακάτω τους προτιμώμενους όρους της άδειας\\:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("13", [], function() {
  return "#language converter\n#Mon Nov 07 13:09:32 CET 2016\nlaw.distribution=Distribuzzjoni\nlaw.sublicensing.desc=Agħti jew estendi liċenzja għas-softwer\nwizard.menu.question1=Ix-xogħol tiegħi li jinkorpora settijiet oħra tad-data huwa meqjus xogħol derivattiv tagħhom?\nlaw.derivativeworks.desc=Oħloq xogħlijiet derivattivi tad-data\nwizard.menu.question0=Nixtieq  nippubblika s-sett il-ġdid tad-data. Dan se jkun protett mil-liġi?\npage.show.compatible=Liċenzji komparabbli\nwizard.q1.end1.here=hawnhekk\nlaw.sublicensing=Is-Subliċenzjar\npage.filter.introduction=Data li hija kondiviża b'liċenzja ssir Data Miftuħa. Hemm ħafna liċenzji disponibbli. L-assistent tal-liċenzja jipprovdi deskrizzjoni tal-liċenzji disponibbli. Din tagħti wkoll ħarsa ġenerali ta' kif jiġu applikati l-liċenzji bħal pubblikatur/distributur mill-ġdid tad-Data Miftuħa u kif jiġu kkombinati l-liċenzji multipli.\nstartagain=Ibda mill-ġdid\nwizard.q0.step1.explanation=Pereżempju, lista ta' numri tat-telefown tan-nies kollha li jaħdmu f'organizzazzjoni mhux se tkun koperta mid-drittijiet tal-awtur. L-awtur ma jista' juri l-ebda kreattività fl-għażla tad-data (li hija sempliċiment il-persuni kollha jaħdmu hemm) u lanqas fl-għażla tad-data (ismijiet sempliċi u numri tat-telefown). Anke jekk il-ġabra tad-data kienet kbira ħafna jew jekk kien involut ħafna sforz, l-ebda kreattivtà ma kienet meħtieġa u għalhekk l-ebda regoli tad-drittijiet tal-awtur ma japplikaw\nlaw.commercialuse=Użu kummerċjali\nlaw.obligation=Obbligu\nlaw.prohibition=Projbizzjoni\nwizard.q1.step0.question=Is-settijiet tad-data inkorporati huma koperti taħt il-liġi tad-drittijiet tal-awtur jew id-drittijiet tal-bażi tad-data?\nwizard.q0.step0.question=Inti kont l-awtur ta' dan is-sett ta' data?\nlaw.reproduction.desc=\"Irriproduċi\" tfisser li tagħmel kopji tax-xogħol permezz tal-mezzi kollha inkluż mingħajr limitazzjoni permezz ta' rekordings bil-ħoss u viżwali u d-dritt ta' fissazzjoni u r-riproduzzjoni ta' fissazzjonijet tax-xogħol, inkluża ħażna ta' prestazzjoni protetta jew fonogramm fil-forma diġitali jew medium elettroniku ieħor.\nname=Isem\nwizard.menu.header=Qed naffaċċja l-problema li ġejja\\:\nlaw.notice.desc=Żomm l-avviżi tad-drittijiet tal-awtur u tal-liċenzji intatti\nwizard.q0.end3=Sfortunatament, is-sett tiegħek tad-data x'aktarx ma jaqax taħt id-drittijiet tal-awtur u/jew id-drittijiet tal-bażi tad-data. Jekk jogħġbok ikkonsulta avukat għal aktar avviż dwar dan is-suġġett.\nwizard.q0.end2=Is-sett tad-data huwa x'aktarx protett taħt il-liġi tad-drittijiet tal-awtur u inti tista' tagħżel liċenzja applikabbli.\nwizard.q0.end1=Il-bażi tad-data tiegħek hija x'aktarx protetta taħt id-drittijiet tal-bażi tad-data (magħrufa wkoll bħala \"drittijiet sui generis). Inti tista' ssib lista ta' liċenzji li jkopru dawn id-drittijiet\nwizard.q0.end0=Jekk jogħġbok ikkuntattja s-sett tad-data oriġinali għall-koordinazzjoni tal-pubblikazzjoni.\nlaw.lessercopyleft=Lesser Copyleft\nsettings.weighted=Iffiltrar ippeżat\nwizard.q1.step1.question=Inti mmodifikajt ix-xogħol oriġinali?\nlaw.obligation.desc=Inti obbligat li\\:\nlaw.sharealike.desc=Iddistribwixxi x-xogħlijiet derivattivi taħt l-istess liċenzja bħax-xogħol oriġinali.\nsettings.advanced=Settings avvanzati\nwizard.q0.step1.question=Is-sett tad-data huwa l-ħolqien intellettwali tiegħek b'għażla jew b'arranġament?\npage.show.title=Uri l-liċenzja\nlaw.patentgrant.desc=Uża kwalunkwe privattiv tal-liċenzjatur applikabbli għax-xogħol sa fejn meħtieġ biex tuża x-xogħol skont id-drittijiet ta' użu tad-drittijiet tal-awtur\nlaw.derivativeworks=Xogħlijiet derivattivi\npage.show.open=Il-paġna ewlenija tal-liċenzja miftuħa\nlaw.attribution.desc=Agħti kreditu xieraq lid-detentur tad-drittijiet tal-awtur u/jew l-awtur\nlaw.prohibition.desc=Inti pprojbit minn\\:\nlaw.distribution.desc=Iddistribwixxi mill-ġdid id-data\npage.filter.title=Liċenzji ta' filtru\nlaw.lessercopyleft.desc=Agħti liċenzja lix-xogħlijiet derivattivi taħt it-termini speċifikati fil-liċenzja li huma simili għax-xogħlijiet oriġinali.\nlaw.commercialuse.desc=L-użu tax-xogħol oriġinali għal skopijiet kummerċjali.\nlaw.notice=Avviż\nwizard.q0.step2.question=Kien hemm investiment sostanzjali kwalitattiv u/jew kwantitattiv jew fil-kisba, fil-verifika jew fil-preżentazzjoni tal-kontenuti?\nlaw.attribution=Attribuzzjoni\nlaw.copyleft=Copyleft\nterms=Termini\nlaw.patentgrant=Uża pretensjonijiet għall-privattivi\nlaw.permission.desc=Inti liberu/a li\\:\nyes=Iva\nlaw.reproduction=Riproduzzjoni\nlaw.sharealike=Sharealike\nlaw.permission=Permess\nwizard.q0.step2.explanation=Pereżempju, bażi  tad-data li fiha informazzjoni ġeografika esklussiva relatata ma' pajjiż kollu x'aktarx se tkun koperta minn din. Dan għaliex tali bażi tad-data normalment se tirrikjedi sforz sinifikanti biex tinħoloq f'termini ta' ħin, flus, sforz, eċċ.\nwizard.q1.end1=Ix-xogħol tiegħek x'aktarx jikkostitwixxi xogħol derivattiv. Jekk jogħġbok agħti ħarsa lejn il-liċenzji kompatibbli.\nwizard.q1.end0=Ix-xogħol tiegħek ma jikkostitwixxix xogħol derivattiv. Madankollu, xorta waħda jistgħu japplikaw ir-restrizzjonijiet kuntrattwali.\nno=Le\nlaw.statechanges.desc=Indika liema bidliet saru fix-xogħol oriġinali liċenzjat b'mod li jippermetti l-attribuzzjoni.\npage.filter.description=Jekk jogħġbok sib liċenzja billi tagħżel it-termini ta' liċenzja ppreferuti hawn taħt\\:\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("14", [], function() {
  return "#language converter\n#Tue Feb 21 14:49:44 CET 2017\nlaw.distribution=Levitamine\nlaw.sublicensing.desc=Tarkvaralitsentsi andmine või laiendamine\nwizard.menu.question1=Kas minu teost loetakse teistest tuletatud teoseks, kui minu teos hõlmab teisi andmehulki?\nlaw.derivativeworks.desc=Andmetest tuletatud teoste koostamine\nwizard.menu.question0=Soovin avaldada uue andmehulga. Kas see on õigusnormidega kaitstud?\npage.show.compatible=Võrreldavad litsentsid\nwizard.q1.end1.here=siin\nlaw.sublicensing=Edasi litsentsimine\npage.filter.introduction=Litsentsi alusel jagatavad andmed muutuvad avaandmeteks. Võimalikke litsentse on mitmeid. Litsentsiabis on kasutatavate litsentside kirjeldused. Selles on samuti ülevaade litsentside kohaldamisest, kui olete avaandmete taasavaldaja/-levitaja, ning mitme litsentsi ühendamisest.\nstartagain=Alusta uuesti\nwizard.q0.step1.explanation=Näiteks ei ole autoriõigustega kaitstud mingi organisatsiooni kõigi töötajate telefoninumbrite loetelu. Autor ei saa näidata mingit loomingulist panust nende andmete valikul, sest selles on lihtsalt kõik seal töötavad inimesed ning nende nimed ja telefoninumbrid. Ehkki see andmehulk võib olla väga suur ja töömahukas, ei olnud selleks vaja mingit loomingulisust ja seetõttu ei kohaldata sellele autoriõiguste põhimõtteid.\nlaw.commercialuse=Kaubanduslik kasutus\nlaw.obligation=Kohustus\nlaw.prohibition=Keeld\nwizard.q1.step0.question=Kas kaasatud andmehulgad on kaitstud autoriõigustega või andmebaasi õigustega?\nwizard.q0.step0.question=Kas te olete andmehulga autor?\nlaw.reproduction.desc=Reprodutseerimine tähendab teosest koopiate tegemist piiranguteta mis tahes meetodil heli või pildi salvestamisega ning teose salvestamise ja salvestuste reprodutseerimise õigust, sealhulgas kaitstud esituse või fonogrammi talletamist digitaalsel kujul või muu elektroonse vahendi kaudu.\nname=Nimi\nwizard.menu.header=Mul on järgmine probleem\nlaw.notice.desc=Hoidke puutumatult alles kõik autori- ja omandiõiguste märked\nwizard.q0.end3=Kahjuks ei kohaldata teie andmehulgale tõenäoliselt autori- ja/või andmebaasi õigusi. Täpsemate nõuannete saamiseks sellel teemal pöörduge juristi poole.\nwizard.q0.end2=See andmehulk on tõenäoliselt autoriõigustega kaitstud ja te võite valida asjaomase litsentsi.\nwizard.q0.end1=Teie andmebaas on tõenäoliselt kaitstud andmebaasi õigustega (nn sui generis õigused). Vaadake nende õigustega seotud litsentside loetelu.\nwizard.q0.end0=Pöörduge avaldamise koordineerimiseks algse andmehulga autori poole.\nlaw.lessercopyleft=Väiksem Copyleft\nsettings.weighted=Kaalutud filter\nwizard.q1.step1.question=Kas te muutsite algset teost?\nlaw.obligation.desc=Teie kohustused\nlaw.sharealike.desc=Levitage tuletatud teoseid algse teosega sama litsentsi alusel.\nsettings.advanced=Täpsemad sätted\nwizard.q0.step1.question=Kas andmehulk on teie intellektuaalne looming valiku või paigutuse alusel?\npage.show.title=Kuva litsents\nlaw.patentgrant.desc=Sellele teosele kohaldatavate litsentsiandja patentide kasutamine niivõrd, kui on vaja kõnealuse teose kasutamiseks kooskõlas autoriõiguste kasutusõigustega.\nlaw.derivativeworks=Tuletatud teosed\npage.show.open=Ava litsentsi avaleht\nlaw.attribution.desc=Viidake nõuetekohaselt autoriõiguste valdajale ja/või autorile\nlaw.prohibition.desc=Teile on keelatud järgmised tegevused.\nlaw.distribution.desc=Andmete levitamine\npage.filter.title=Filtreeri litsentse\nlaw.lessercopyleft.desc=Tuletatud teoste litsentseerimine algse teosega sarnastel tingimustel, nagu oli algses litsentsis kindlaks määratud.\nlaw.commercialuse.desc=Algse teose kasutamine kaubanduslikul otstarbel.\nlaw.notice=Teade\nlaw.statechanges=Oleku muutused\nwizard.q0.step2.question=Kas sisu loomiseks, kontrollimiseks ja kuvamiseks on tehtud kvalitatiivselt ja/või kvantitatiivselt olulisi investeeringuid?\nlaw.attribution=Viitamine\nlaw.copyleft=Copyleft\nterms=Tingimused\nlaw.patentgrant=Kasuta patendinõudeid\nlaw.permission.desc=Te võite\\:\nyes=Jah\nlaw.reproduction=Reprodutseerimine\nlaw.sharealike=Sharealike\nlaw.permission=Luba\nwizard.q0.step2.explanation=Näiteks kasutatakse seda tõenäoliselt andmebaasi kohta, mis sisaldab ainuõigustega geograafilist teavet kogu riigi kohta, sest sellise andmebaasi loomiseks tuleb kulutada märkimisväärselt palju aega, raha, jõudu jne.\nwizard.q1.end1=Tõenäoliselt kujutab teie töö endast tuletatud teost. Tutvuge asjaomaste litsentsidega.\nwizard.q1.end0=Teie töö ei kujuta endast tuletatud teost. Siiski võidakse kohaldada lepingulisi piiranguid.\nno=Ei.\nlaw.statechanges.desc=Näidake, mis muudatusi on tehtud algses litsentseeritud töös nii, et see lubaks viitamist.\npage.filter.description=Leidke litsents, valides allpool sobivad litsentsi tingimused\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("15", [], function() {
  return "#language converter\n#Tue Feb 21 14:49:44 CET 2017\nlaw.distribution=Dáileadh\nlaw.sublicensing.desc=Ceadúnas bogearra a dheonú nó síneadh a chur leis\nwizard.menu.question1=Mo chuid oibre ina bhfuil tacair shonraí eile comhshnaidhmithe, an meastar gur obair í atá díorthaithe ó na tacair shonraí sin?\nlaw.derivativeworks.desc=Cruthaigh oibreacha díorthacha de na sonraí\nwizard.menu.question0=Ba mhaith liom tacar sonraí nua a fhoilsiú. An mbeidh sé faoi chosaint dlí?\npage.show.compatible=Ceadúnais inchomparáide\nwizard.q1.end1.here=anseo\nlaw.sublicensing=Focheadúnú\npage.filter.introduction=Déantar Sonraí Oscailte de shonraí a chomhroinntear le ceadúnas. Tá a lán ceadúnas ar fáil. Tugann an cúntóir ceadúnais cur síos ar na ceadúnais atá ar fáil. Lena chois sin tugann sé forléargas ar an tslí inar féidir ceadúnais a úsáid mar athfhoilsitheoir/dáiltheoir Sonraí Oscailte agus conas mar a chomhcheanglaítear ceadúnais iolracha.\nstartagain=Tosaigh arís\nlaw.copyleft.desc=\nwizard.q0.step1.explanation=Mar shampla, níl liosta uimhreacha fóin na ndaoine go léir atá ag obair in eagraíocht clúdaithe ag cóipcheart. Ní féidir leis an údar aon chruthaitheacht a léiriú maidir leis na sonraí a roghnú (mar níl iontu ach na daoine uile atá ag obair ansin) nó maidir le rogha na sonraí (mar níl ann ach ainmneacha agus uimhreacha fóin). Fiú amháin dá mba rud é gur bailiúchán sonraí mór fada é nó go raibh mórobair i gceist ní raibh gá le cruthaitheacht agus dá bhrí sin níl feidhm ag cóipcheart\nlaw.commercialuse=Úsáid thráchtála\nlaw.obligation=Oibleagáid\nlaw.prohibition=Cosc\nwizard.q1.step0.question=An bhfuil na tacair shonraí chomhshnaidhmithe faoi chosaint faoi dhlí cóipchirt nó faoi chearta bunachar sonraí?\nwizard.q0.step0.question=An tusa údar an tacair shonraí?\nlaw.reproduction.desc=Is ea a chiallaíonn “atáirgeadh” cóipeanna a dhéanamh den Obair ar mhodh ar bith lena n-áirítear meáin gan teorainn, trí thaifid fhuaime nó amhairc agus an ceart iad a fhosú, agus fosuithe den Obair a atáirgeadh, rud lena n-áirítear léiriú nó fónagram faoi chosaint a stóráil i bhfoirm dhigiteach nó mar mheán leictreonach eile.\nname=Ainm\nwizard.menu.header=Tá an fhadhb seo a leanas agam\nlaw.notice.desc=Coinnigh cóipcheart agus fógraí ceadúnais i bhfeidhm\nwizard.q0.end3=Ar an drochuair is ea is dóichí nach n-áirítear do thacar sonraí faoi chearta cóipchirt ná / nó faoi chearta bunachar sonraí. Téigh i gcomhairle le dlíodóir chun tuilleadh comhairle ar an ábhar seo a fháil.\nwizard.q0.end2=Is ea is dóichí go bhfuil an tacar sonraí faoi chosaint ag dlí cóipchirt agus is féidir leat ceadúnas iomchuí a roghnú.\nwizard.q0.end1=Is ea is dóichí go bhfuil do bhunachar sonraí faoi chosaint ag cearta bunachar sonraí (dá nglaoitear ‘sui generis rights’ leis). Tá liosta ceadúnas a chlúdaíonn na cearta seo ar fáil\nwizard.q0.end0=Déan teagmháil le húdar bunaidh an tacair shonraí maidir lena bhfoilsiú a chomhordú.\nlaw.lessercopyleft=Lesser Copyleft\nsettings.weighted=Scagadh ualaithe\nwizard.q1.step1.question=An ndearna tú mionathrú ar an obair bhunaidh?\nlaw.obligation.desc=Tá d’oibleagáid ort\nlaw.sharealike.desc=Dáil oibreacha díorthacha faoin cheadúnas céanna leis an obair bhunaidh.\nsettings.advanced=Ardshocruithe\nwizard.q0.step1.question=An é do chruthú intleachtúil ó thaobh rogha nó leagain amach é an tacar sonraí?\npage.show.title=Taispeáin ceadúnas\nlaw.patentgrant.desc=Bain úsáid as paitinní ar bith de chuid an cheadúnaí a bhaineann leis an obair sa mhéid agus is gá chun an obair a úsáid de réir na gcearta um úsáid cóipchirt\nlaw.derivativeworks=Oibreacha Díorthacha\npage.show.open=Leathanach baile an cheadúnais oscailte\nlaw.attribution.desc=Tabhair creidiúint cheart don sealbhóir cóipchirt agus/nó don údar\nlaw.prohibition.desc=Tá cosc ort\nlaw.distribution.desc=Athdháil na sonraí\npage.filter.title=Ceadúnais scagaire\nlaw.lessercopyleft.desc=Oibreacha díorthacha a cheadúnú faoi théarmaí arna sainiú sa cheadúnas atá cosúil leis na téarmaí atá i bhfeidhm maidir leis an obair bhunaidh.\nlaw.commercialuse.desc=An obair bhunaidh a úsáid chun críocha tráchtála.\nlaw.notice=Fógra\nlaw.statechanges=Luaigh Athruithe\nwizard.q0.step2.question=An raibh infheistiú suntasach ó thaobh cáilíochta agus/nó cainníochta i gceist maidir leis an t-inneachar a fháil, a fhíorú nó a chur i láthair?\nlaw.attribution=Aitreabúideacht\nlaw.copyleft=Copyleft\nterms=Téarmaí\nlaw.patentgrant=Bain úsáid as éilimh paitinne\nlaw.permission.desc=Tá cead agat\nyes=Is féidir\nlaw.reproduction=Atáirgeadh\nlaw.sharealike=Sharealike\nlaw.permission=Cead\nwizard.q0.step2.explanation=Mar shampla, is dócha go bhfuil bunachar sonraí ina bhfuil faisnéis gheografach eisiach a bhaineann le tír iomlán clúdaithe aige seo. Tá sin amhlaidh toisc go mbíonn gá le hiarracht mhór, de ghnáth, le bunachar sonraí a chruthú,ó thaobh ama, airgid, dua srl.\nwizard.q1.end1=Is dóichí gur obair dhíorthach í do chuid oibre. Féach ar cheadúnais chomhoiriúnacha le do thoil\nwizard.q1.end0=Ní hobair dhíorthach í do chuid oibre. Mar sin féin d’fhéadfadh srianta maidir le conradh a bheith i bhfeidhm i gcónaí.\nno=Níl\nlaw.statechanges.desc=Cuir in iúl cén athruithe a rinneadh ar an obair bhunaidh cheadúnaithe ar shlí a cheadaíonn aitreabúideacht.\npage.filter.description=Tá an ceadúnas ar fáil tríd na téarmaí ceadúnais is fearr a roghnú anseo thíos\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("16", [], function() {
  return "#language converter\n#Tue Feb 21 14:49:44 CET 2017\nlaw.distribution=Distribution\nlaw.sublicensing.desc=Tildel eller udvid licensen for softwaren\nwizard.menu.question1=Er mit værk, der inkorporerer andre datasæt, at betragte som et afledt værk af disse datasæt?\nlaw.derivativeworks.desc=Skab afledte værker af dataene\nwizard.menu.question0=Jeg ønsker at offentliggøre et nyt datasæt. Er det lovbeskyttet?\npage.show.compatible=Sammenlignelige licenser\nwizard.q1.end1.here=her\nlaw.sublicensing=Tildeling af underlicens\npage.filter.introduction=Data, som deles under en licens, bliver til åbne data. Der er mange tilgængelige data. Licensassistenten beskriver de tilgængelige licenser. Den giver også et overblik over, hvordan du som genudgiver/videresender af åbne data kan anvende licenser, samt hvordan du kan kombinere flere forskellige licenser.\nstartagain=Start igen\nwizard.q0.step1.explanation=F.eks. er en liste over telefonnumre på alle de ansatte i en organisation ikke omfattet af ophavsretten. Forfatteren kan ikke udvise kreativitet i udvælgelsen af data (hvilket vil sige alle de personer, der arbejder i organisationen), eller i valget af data (navne og telefonnumre). Selvom datasamlingen er meget stor, eller det har krævet en stor indsats, har det ikke krævet kreativitet, og der gælder derfor ingen ophavsretsregler.\nlaw.commercialuse=Kommerciel anvendelse\nlaw.obligation=Forpligtelse\nlaw.prohibition=Forbud\nwizard.q1.step0.question=Er de inkorporerede datasæt beskyttet af ophavsret eller databaserettigheder?\nwizard.q0.step0.question=Er du forfatter til datasættet?\nlaw.reproduction.desc=\"Reproducere\" betyder at tage kopier af værket på en hvilken som helst måde uden begrænsninger i form af lydmæssige eller visuelle optagelser, samt retten til at optage og reproducere optagelser af værket, herunder lagring af en beskyttet forevisning eller fonogram i digital form eller som andet elektronisk medium.\nname=Navn\nwizard.menu.header=Jeg har følgende problem\nlaw.notice.desc=Bevar ophavsret og licensmeddelser intakte\nwizard.q0.end3=Desværre er dine datasæt sandsynligvis ikke omfattet af ophavsrettigheder eller databaserettigheder. Rådfør dig med en advokat for at få yderligere rådgivning om dette emne.\nwizard.q0.end2=Datasættet er sandsynligvis omfattet af ophavsrettigheder, og du kan vælge en relevant licens.\nwizard.q0.end1=Din database er sandsynligvis omfattet af ophavsrettigehder (også kaldet \"sui generis-rettigheder\"). Du kan se en liste over licenser, som omfatter disse rettigheder. \nwizard.q0.end0=Kontakt forfatteren til det originale datasæt for at koordinere offentliggørelsen.\nlaw.lessercopyleft=Lesser Copyleft\nsettings.weighted=Vægtet filtrering\nwizard.q1.step1.question=Har du ændret i det originale værk?\nlaw.obligation.desc=Du er forpligtet til\nlaw.sharealike.desc=Distribuer afledte værker under den samme licens som det originale værk.\nsettings.advanced=Avancerede indstillinger\nwizard.q0.step1.question=Er datasættet dit intellektuelle værk enten ved udvælgelse eller arrangement?\npage.show.title=Vis licens\nlaw.patentgrant.desc=Anvend et hvilket som helst patent fra licensgiveren, som er gældende for værket, hvis du har brug for det for at anvende værket i overensstemmelse med brugsrettighederne\nlaw.derivativeworks=Afledte værker\npage.show.open=Hjemmeside for åben licens\nlaw.attribution.desc=Giv en passende erkendelse til ophavsrettighedshaveren og/eller forfatteren\nlaw.prohibition.desc=Du har ikke lov til at\nlaw.distribution.desc=Videresend dataene\npage.filter.title=Filterlicenser\nlaw.lessercopyleft.desc=Tag licens på afledte værker på de vilkår, der er angivet i den licens, som svarer til licensen for det originale værk.\nlaw.commercialuse.desc=Anvendelse af det originale værk til kommercielle formål.\nlaw.notice=Meddelelse\nlaw.statechanges=Angiv ændringer\nwizard.q0.step2.question=Er der foretaget en kvalitativ og/eller kvantitativ stor investering i enten fremskaffelsen, verificeringen eller præsentationen af indholdet?\nlaw.attribution=Attribution\nlaw.copyleft=Copyleft\nterms=Vilkår\nlaw.patentgrant=Anvend patenter\nlaw.permission.desc=Du har lov til at\nyes=Ja\nlaw.reproduction=Reproduktion\nlaw.sharealike=Sharealike\nlaw.permission=Godkendelse\nwizard.q0.step2.explanation=En database, der f.eks. indeholder eksklusive geografiske informationer vedrørende et helt land, vil sandsynligvis være omfattet af dette. Det skyldes, at det normalt kræver en særlig indsats at oprette en sådan database, hvad angår tid, penge, indsats etc.\nwizard.q1.end1=Dit arbejde er sandsynligvis et afledt værk. Kig nærmere på kompatible licenser.\nwizard.q1.end0=Dit værk er ikke et afledt værk. Det kan dog stadig være underlagt kontraktlige begrænsninger\nno=Nej\nlaw.statechanges.desc=Angiv, hvilke ændringer der er foretaget i det originale godkendte arbejde, på en måde der muliggør attribution.\npage.filter.description=Find en licens ved at vælge mellem de foretrukne licensvilkår nedenfor\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("17", [], function() {
  return "#language converter\n#Tue Feb 21 14:49:44 CET 2017\nlaw.distribution=Izplatīšana\nlaw.sublicensing.desc=Piešķiriet vai pagariniet licenci programmatūrai\nwizard.menu.question1=Vai mans darbs, kas ietver citas datu kopas, ir uzskatāms par to atvasinātu darbu?\nlaw.derivativeworks.desc=Izveidojiet no datiem atvasinātus darbus\nwizard.menu.question0=Es vēlos publicēt jaunu datu kopu. Vai to aizsargās tiesību akti?\npage.show.compatible=Salīdzināmās atļaujas\nwizard.q1.end1.here=šeit\nlaw.sublicensing=Apakšlicenču izsniegšana\npage.filter.introduction=Dati, kurus kopīgo līdz ar licenci, kļūst par atklātajiem datiem. Ir pieejamas daudzas licences. Licences palīgs sniedz pieejamo licenču aprakstus. Tas arī sniedz pārskatu par to, kā piemērot licences, kad jūs pārpublicējat vai izplatāt tālāk atklātos datus, un kā kombinēt vairākas licences.\nstartagain=Sākt no sākuma\nlaw.copyleft.desc=\nwizard.q0.step1.explanation=Piemēram, autortiesības nav attiecināmas uz visu kādas organizācijas darbinieku telefona numuru sarakstu. Autors nevar pierādīt ne radošumu datu atlasē (sarakstā vienkārši ir visi organizācijā strādājošie cilvēki), ne datu izvēlē (tikai vārdi un telefona numuri). Neskatoties uz to, vai savākto datu apjoms ir ļoti liels vai to vākšanā ir ieguldīts liels darbs, šajā procesā nav nepieciešams radošums, un tāpēc autortiesību noteikumi nav piemērojami.\nlaw.commercialuse=Komerciāla izmantošana\nlaw.obligation=Pienākums\nlaw.prohibition=Aizliegums\nwizard.q1.step0.question=Vai iekļautās datu kopas aizsargā autortiesības vai datu bāzes veidotāja tiesības?\nwizard.q0.step0.question=Vai jūs esat datu kopas autors?\nlaw.reproduction.desc=“Pavairot” nozīmē izgatavot darba kopijas ar jebkādiem līdzekļiem, t. sk. ar skaņas vai attēla ierakstiem, un tiesības fiksēt un pavairot darba fiksācijas, ieskaitot aizsargāta izpildījuma vai fonogrammas glabāšanu digitālā formā vai citā elektroniskā datu nesējā.\nname=Nosaukums\nwizard.menu.header=Es saskaros ar šādu problēmu\nlaw.notice.desc=Saglabājiet nemainītus paziņojumus par autortiesībām un licencēm\nwizard.q0.end3=Diemžēl, uz jūsu datu kopu, visticamāk, nav attiecināmas autortiesības un/vai datu bāzes veidotāja tiesības. Lūdzu, konsultējieties ar juristu, lai uzzinātu vairāk par šo tēmu.\nwizard.q0.end2=Datu kopa, visticamāk, ir aizsargāta ar autortiesību aktiem, un jūs varat izvēlēties piemērojamu licenci.\nwizard.q0.end1=Jūsu datu bāze, visticamāk, ir aizsargāta ar datu bāzes veidotāja tiesībām (pazīstamas arī kā “sui generis tiesības”). Jūs varat atrast sarakstu ar licencēm, kas ietver šīs tiesības\nwizard.q0.end0=Lūdzu, sazinieties ar oriģinālās datu kopas autoru, lai saskaņotu publikāciju.\nlaw.lessercopyleft=Lesser Copyleft licence\nsettings.weighted=Svērtā filtrēšana\nwizard.q1.step1.question=Vai jūs pārveidojāt oriģinālo darbu?\nlaw.obligation.desc=Jums ir pienākums\nlaw.sharealike.desc=Izplatiet atvasinātus darbus saskaņā ar tādu pašu licenci, kā oriģinālo darbu.\nsettings.advanced=Papildu iestatījumi\nwizard.q0.step1.question=Vai datu kopa ir jūsu intelektuāla jaunrade, pateicoties atlasei vai sakārtojumam?\npage.show.title=Parādīt licenci\nlaw.patentgrant.desc=Izmantojiet jebkādus licences devēja patentus, kas piemērojami darbam, ciktāl tas ir nepieciešams, lai izmantotu darbu atbilstoši autortiesību izmantošanas tiesībām\nlaw.derivativeworks=Atvasināti darbi\npage.show.open=Atvērt licences mājaslapu\nlaw.attribution.desc=Sniedziet pienācīgas atsauces uz autortiesību īpašnieku un/vai autoru\nlaw.prohibition.desc=Jums ir aizliegts\nlaw.distribution.desc=Izplatiet datus tālāk\npage.filter.title=Filtrēt licences\nlaw.lessercopyleft.desc=Licencējiet atvasinātus darbus atbilstoši licencē norādītajiem noteikumiem, kas ir līdzīgi oriģinālā darba noteikumiem.\nlaw.commercialuse.desc=Oriģinālā darba izmantošana komerciāliem mērķiem.\nlaw.notice=Paziņojums\nlaw.statechanges=Norādiet izmaiņas\nwizard.q0.step2.question=Vai satura iegūšanā, pārbaudē vai prezentēšanā bija būtiski kvalitatīvi un/vai kvantitatīvi ieguldījumi?\nlaw.attribution=Attiecinājums\nlaw.copyleft=Copyleft\nterms=Noteikumi\nlaw.patentgrant=Izmantojiet patentu pieprasījumus\nlaw.permission.desc=Jūs varat brīvi\nyes=Jā\nlaw.reproduction=Pavairošana\nlaw.sharealike=Sharealike\nlaw.permission=Atļauja\nwizard.q0.step2.explanation=Piemēram, ir ļoti iespējams, ka šāda licence attieksies uz datu bāzi, kurā ir ietverta ģeogrāfiska informācija par visu valsti. Tas ir tāpēc, ka šādas datu bāzes izveide parasti prasa lielus ieguldījumus laika, naudas, darba u. c. ziņā.\nwizard.q1.end1=Jūsu darbs, visticamāk, ir uzskatāms par atvasinātu darbu. Lūdzu, apskatiet savietojamas licences\nwizard.q1.end0=Jūsu darbs nav uzskatāms par atvasinātu darbu. Tomēr var būt piemērojami līgumiski ierobežojumi.\nno=Nē\nlaw.statechanges.desc=Norādiet, kādas izmaiņas ir izdarītas oriģinālajā licencētajā darbā tādā veidā, kas pieļauj attiecinājumu.\npage.filter.description=Sameklējiet licenci, atlasot vēlamos licences noteikumus tālāk\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("18", [], function() {
  return "#language converter\n#Tue Feb 21 14:49:44 CET 2017\nlaw.distribution=Paskirstymas\nlaw.sublicensing.desc=Suteikite arba pratęskite programinės įrangos licenciją\nwizard.menu.question1=Ar mano darbas, kuris apima kitus duomenų rinkinius, laikomas jų išvestiniu darbu?\nlaw.derivativeworks.desc=Sukurti duomenų išvestinius darbus\nwizard.menu.question0=Aš noriu paskelbti naują duomenų rinkinį. Ar tai bus saugoma įstatymo?\npage.show.compatible=Lyginamosios licencijos\nwizard.q1.end1.here=čia\nlaw.sublicensing=Sublicencijos suteikimas\npage.filter.introduction=Duomenys, kurie pateikiami su licencija, tampa atviraisiais duomenimis. Yra daugybė licencijų. Licencijų pagalbos programoje pateikiami esamų licencijų aprašymai. Joje taip pat aiškinama, kaip naudoti licencijas, jei esate atvirųjų duomenų perleidėjas / platintojas ir kaip suderinti keletą licencijų.\nstartagain=Pradėti iš naujo\nwizard.q0.step1.explanation=Pavyzdžiui, visų organizacijoje dirbančių žmonių telefonų numerių sąrašui nebus taikomos autorių teisės. Autorius neatlieka kūrybinio darbo nei atrinkdamas duomenis (tai yra tiesiog ten dirbantys žmonės), nei pasirinkdamas duomenis (tai yra tiesiog vardai ir telefonų numeriai). Net jeigu duomenų yra labai daug ir įdėta daug pastangų juos renkant, tai nereikalauja jokio kūrybiškumo, todėl autorių teisių taisyklės netaikomos\nlaw.commercialuse=Komercinis naudojimas\nlaw.obligation=Įpareigojimas\nlaw.prohibition=Draudimas\nwizard.q1.step0.question=Ar įtrauktiems duomenų rinkiniams taikomi teisės aktai dėl autorių teisių arba duomenų bazių teisių?\nwizard.q0.step0.question=Ar jūs esate duomenų rinkinio autorius?\nlaw.reproduction.desc=„Atgaminti“ reiškia padaryti darbo kopijas bet kokiu būdu, įskaitant, be apribojimų, garso arba vaizdo įrašus, ir teisę įrašyti bei atgaminti darbo įrašą, įskaitant saugomo kūrinio arba fonogramos skaitmeniniame formate ar kitoje elektroninėje laikmenoje, saugojimą.\nname=Pavadinimas\nwizard.menu.header=Aš susiduriu su šia problema\nlaw.notice.desc=Nepažeiskite autorių teisių ir licencijų pranešimų\nwizard.q0.end3=Deja, jūsų duomenų rinkiniui tikriausiai netaikomos autorių ir (arba) duomenų bazės teisės. Teisininkas suteiks jums daugiau informacijos šia tema.\nwizard.q0.end2=Duomenų rinkiniui tikriausiai yra taikomi teisės aktai dėl autorių teisių, ir jūs galite pasirinkti taikytiną licenciją.\nwizard.q0.end1=Jūsų duomenų bazei tikriausiai yra taikomos duomenų bazės teisės („sui generis“ teisės). Jūs galite susirasti licencijų, susijusių su šiomis teisėmis, sąrašą\nwizard.q0.end0=Susisiekite su originalaus duomenų rinkinio autoriumi dėl paskelbimo suderinimo.\nlaw.lessercopyleft=Mažesnės autorių laisvės (angl. „copyleft“)\nsettings.weighted=Svertinis filtravimas\nwizard.q1.step1.question=Ar jūs pakeitėte originalų darbą?\nlaw.obligation.desc=Jūs privalote\nlaw.sharealike.desc=Išvestinius darbus skirstykite pagal tą pačią licenciją kaip ir originalų darbą.\nsettings.advanced=Papildomos nuostatos\nwizard.q0.step1.question=Ar duomenų rinkinys yra jūsų intelektinis kūrinys pagal pasirinkimą ar susitarimą?\npage.show.title=Rodyti licenciją\nlaw.patentgrant.desc=Naudokite bet kokius licencijos išdavėjo patentus, taikomus darbui tiek, kiek reikia naudoti darbą pagal autorių teisių naudojimo teises\nlaw.derivativeworks=Išvestiniai darbai\npage.show.open=Atviros licencijos pradžios tinklalapis\nlaw.attribution.desc=Tinkamai nurodykite autorių teisių subjektą ir (arba) autorių\nlaw.prohibition.desc=Jums draudžiama\nlaw.distribution.desc=Perskirstyti duomenis\npage.filter.title=Filtruoti licencijas\nlaw.lessercopyleft.desc=Išvestiniams darbams yra taikomos licencijoje nurodytos sąlygos, kurios panašios į originalaus darbo sąlygas.\nlaw.commercialuse.desc=Originalaus darbo naudojimas komerciniams tikslams.\nlaw.notice=Pranešimas\nlaw.statechanges=Nurodyti pakeitimus\nwizard.q0.step2.question=Ar gaunant, patvirtinant arba pateikiant turinį buvo kokybiškai ir (arba) kiekybiškai svarbių investicijų?\nlaw.attribution=Priskyrimas\nlaw.copyleft=Autorių laisvės (angl. „copyleft“)\nterms=Sąlygos\nlaw.patentgrant=Naudoti patentų apibrėžtį\nlaw.permission.desc=Jums leidžiama\nyes=Taip\nlaw.reproduction=Atgaminimas\nlaw.sharealike=Platinimas nurodytomis sąlygomis\nlaw.permission=Leidimas\nwizard.q0.step2.explanation=Tai gali apimti duomenų bazę, kurioje yra išskirtinė geografinė informacija, susijusi su visa šalimi. Taip yra todėl, kad tokiai duomenų bazei sukurti paprastai reikia daug laiko, pinigų, pastangų ir pan.\nwizard.q1.end1=Jūsų darbas tikriausiai yra išvestinis. Prašome peržiūrėti suderinamas licencijas\nwizard.q1.end0=Jūsų darbas nėra išvestinis. Nepaisant to, gali būti taikomi sutartiniai apribojimai.\nno=Ne\nlaw.statechanges.desc=Nurodykite, kokie pakeitimai buvo atlikti originaliam licencijuotam darbui taip, kad būtų galima atlikti priskyrimą.\npage.filter.description=Suraskite licenciją pasirinkdami žemiau esančias licencijos sąlygas\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("19", [], function() {
  return "#language converter\n#Tue Feb 21 14:49:44 CET 2017\nlaw.distribution=Distribucija\nlaw.sublicensing.desc=Dodeli ali podaljšaj licenco za program\nwizard.menu.question1=Ali moje delo, ki vključuje druge zbirke podatkov, velja za derivativno delo drugih?\nlaw.derivativeworks.desc=Ustvari derivativna dela iz podatkov\nwizard.menu.question0=Želim objaviti novo zbirko podatkov. Ali bo zaščitena z zakonom?\npage.show.compatible=Primerljive licence\nwizard.q1.end1.here=tukaj\nlaw.sublicensing=Podlicenciranje\npage.filter.introduction=Podatki, ki se delijo z licenco, postanejo odprti podatki. Na voljo je veliko licenc. Pomočnik za licence vsebuje opise razpoložljivih licenc. Vsebuje tudi navodila za uporabo licenc za ponovne izdajatelje/izvajalce distribucije odprtih podatkov in združevanje več licenc.\nstartagain=Začni znova\nlaw.copyleft.desc=\nwizard.q0.step1.explanation=Na primer, avtorske pravice ne veljajo za seznam telefonskih številk vseh oseb, zaposlenih v organizaciji. Avtor ne more pokazati nikakršne ustvarjalnosti pri izbiranju podatkov (ki se nanašajo samo na vse osebe, ki tam delajo) niti pri njihovem izboru (samo imena in telefonske številke). Tudi če je bila zbirka podatkov zelo velika ali če je bilo v delo vloženega veliko napora, pa to delo ni zahtevalo ustvarjalnosti, zato pravila v zvezi z avtorskimi pravicami ne veljajo.\nlaw.commercialuse=Poslovna uporaba\nlaw.obligation=Obveznost\nlaw.prohibition=Prepoved\nwizard.q1.step0.question=Ali so vključene zbirke podatkov zaščite z zakonom o avtorskih pravicah ali pravicah, ki se nanašajo na zbirke podatkov?\nwizard.q0.step0.question=Ali ste vi avtor zbirke podatkov?\nlaw.reproduction.desc=„Reproducirati“ pomeni ustvariti kopije dela na kakršen koli način, med drugim tudi z zvočnim ali vizualnim zapisom in pravico do snemanja in reprodukcije posnetkov dela, vključno s shranjevanjem zaščitene izvedbe ali fonograma v digitalni obliki ali na drugem elektronskem nosilcu.\nname=Ime\nwizard.menu.header=Imam naslednjo težavo\nlaw.notice.desc=Obvestila o avtorskih pravicah in licenci naj ostanejo nespremenjena\nwizard.q0.end3=Žal avtorske pravice in/ali pravice, ki se nanašajo na zbirke podatkov, ne veljajo za vašo zbirko podatkov. Za dodatne nasvete v zvezi s tem vprašanjem se obrnite na odvetnika.\nwizard.q0.end2=Zbirka podatkov je verjetno zaščitena z zakonom o avtorskih pravicah, zato lahko izberete ustrezno licenco.\nwizard.q0.end1=Vaša zbirka podatkov je verjetno zaščitena z zakonom, ki se nanaša na zbirke podatkov (i.e. „pravice sui generis“). Na voljo je seznam licenc, ki zajemajo te pravice\nwizard.q0.end0=Obrnite se na avtorja izvirne zbirke podatkov za usklajeno objavo.\nlaw.lessercopyleft=Lesser Copyleft\nsettings.weighted=Tehtano filtriranje\nwizard.q1.step1.question=Ali ste spremenili izvirno delo?\nlaw.obligation.desc=Vaše obveznosti so\nlaw.sharealike.desc=Izvedi distribucijo derivativnih del na podlagi iste licence kot izvirno delo.\nsettings.advanced=Napredne nastavitve\nwizard.q0.step1.question=Ali je zbirka podatkov vaša intelektualna stvaritev na podlagi izbora ali ureditve?\npage.show.title=Pokaži licenco\nlaw.patentgrant.desc=Po potrebi uporabite kateri koli patent dajalca licence, ki je do zdaj veljal za delo, da bi uporabili delo v skladu s pravicami intelektualne lastnine.\nlaw.derivativeworks=Derivativna dela\npage.show.open=Domača stran odprte licence\nlaw.attribution.desc=Ustrezno navedite imetnika avtorskih pravic in/ali avtorja\nlaw.prohibition.desc=Prepovedano je\nlaw.distribution.desc=Ponovno deli podatke\npage.filter.title=Licence za filtre\nlaw.lessercopyleft.desc=Licenciraj derivativna dela pod pogoji, določenimi v licenci, ki so podobna pogojem za izvirno delo.\nlaw.commercialuse.desc=Uporaba izvirnega dela za poslovne namene.\nlaw.notice=Obvestilo\nlaw.statechanges=Navedi spremembe\nwizard.q0.step2.question=Ali je bil v pridobivanje, preverjanje ali predstavitev vsebin vložen kvalitativno in/ali kvantitativno velik vložek?\nlaw.attribution=Atribucija\nlaw.copyleft=Copyleft\nterms=Pogoji\nlaw.patentgrant=Uporabi patentne zahtevke.\nlaw.permission.desc=Dovoljeno je\nyes=Da\nlaw.reproduction=Reprodukcija\nlaw.sharealike=Sharealike (deljenje pod istimi pogoji)\nlaw.permission=Dovoljenje\nwizard.q0.step2.explanation=Na primer, ta bo verjetno veljala za bazo podatkov z ekskluzivnimi geografskimi informacijami, ki se nanašajo na celotno državo. V ustvarjanje takšne baze podatkov je namreč treba vložiti precej časa, denarja, truda itd.\nwizard.q1.end1=Vaše delo je najverjetneje derivativno delo. Oglejte si združljive licence.\nwizard.q1.end0=Vaše delo ni derivativno delo. Kljub temu lahko še vedno veljajo pogodbene omejitve.\nno=Ne\nlaw.statechanges.desc=Navedite, katere spremembe izvirnega licenciranega dela so bile opravljene na način, ki dovoljuje atribucijo.\npage.filter.description=Poiščite licenco, tako da izberete želene licenčne pogoje spodaj\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("1a", [], function() {
  return "#language converter\n#Sun Oct 21 13:50:41 CEST 2018\nlaw.patentgrant=Bruk patentkrav\nwizard.q1.end0=Arbeid ditt utgjør ikke avledet arbeid. Men, kontraktsmessige restriksjoner kan fortsatt gjelde.\nwizard.q1.end1=Arbeidet ditt utgjør trolig et avledet arbeid. Vennligst se nærmere på kompatible lisenser.\nlaw.prohibition=Forbud\nlaw.lessercopyleft=Lesser Copyleft\nwizard.q0.step2.explanation=For eksempel, en database som inneholder eksklusiv geografisk informasjon relatert til et helt land, vil sannsynligvis bli dekket av dette.  Det er fordi en slik database normalt krever en betydelig innsats å skape, med tanke på tid, penger, innsats osv.\npage.filter.title=Filterlisenser\nwizard.q0.step1.explanation=For eksempel, en liste med telefonnummer til alle som arbeider i en organisasjon, vil ikke bli dekket av opphavsretten. Forfatteren kan ikke vise til kreativitet i utvelgelsen av dataen (som  rett og slett bare er alle som jobber der), heller ikke valg av data (bare navn og telefonnummer). Selv om samlingen av data var veldig stor eller krevde stor innsats, så krevdes det ingen kreativitet, og derfor gjelder ikke opphavsretten.\npage.show.open=Hjemmeside for åpne lisenser\nlaw.permission=Tillatelse\nlaw.commercialuse=Kommersielt bruk\npage.show.title=Vis lisens\nterms=Vilkår\nlaw.statechanges.desc=Angi hvilke endringer som har blitt gjort på det opphavlige lisensierte arbeidet, på en slik måte at det muliggjør attribusjon.\nwizard.q1.step0.question=Er de innlemmede datasettene beskyttet av lov om opphavsrett eller databaserettigheter?\nlaw.notice.desc=Behold opphavsrett og lisensmerknader intakte\nlaw.derivativeworks=Deriverte verk\nlaw.distribution.desc=Redistribuer dataen\nlaw.attribution.desc=Krediter rettighetshaveren og/eller forfatteren\nwizard.q0.step1.question=Er datasettet ditt intellektuelle skaperverk gjennom utvalg eller bearbeiding?\nlaw.copyleft=Copyleft\npage.filter.introduction=Data som er delt med lisens, regnes som åpne data. Det er mange tilgjengelige lisenser. Lisensassistenten beskriver de tilgjengelige lisensene. Den gir også en oversikt over hvordan du kan bruke lisenser som gjenutgiver/distributør av åpne data, og hvordan du kan kombinere flere lisenser.\nwizard.q0.end3=Dessverre, datasettene dine er sannsynligvis ikke omfattet av opphavsrett eller databaserettigheter. Vennligst rådfør med advokat for ytterligere rådgivning om dette emnet.\nlaw.statechanges=Angi endringer\nwizard.q0.end0=Vennligst kontakt den opphavlige forfatteren av datasettet for å koordinere publiseringen.\nlaw.sharealike=Dele på like vilkår\nwizard.q0.end2=Datasettet er sannsynligvis beskyttet under opphavsrett, og du kan velge en passende lisens.\nwizard.q0.end1=Datasettet er sannsynligvis beskyttet under databaserettigheter (også kalt \"sui-generis retten\"). Du kan finne en liste med lisenser som dekker disse rettighetene.\npage.show.compatible=Kompatible lisenser\nlaw.reproduction.desc=Å \"reprodusere\" innebærer å lage kopier av arbeidet på en hvilken som helst måte,  uten begrensinger i form av lyd eller visuelle opptak. Det gir også rett til å bearbeide og reprodusere bearbeidelser av arbeidet, inkludert lagring av en beskyttet presentasjon eller fonogram i en digital form eller andre elektroniske medium.\nlaw.sublicensing.desc=Innvilg eller utvid lisensen til programvaren\nwizard.q1.end1.here=her\nsettings.weighted=Vektet filtrering\nwizard.q1.step1.question=Har du endrer det originale arbeidet?\nname=Navn\nlaw.derivativeworks.desc=Lag avledete verk av dataen\nwizard.q0.step2.question=Var det noe kvalitativ eller kvantitativ vesentlig investering ved innhenting, verifisering eller presentering av innholdet?\nlaw.lessercopyleft.desc=Lisensier avledet arbeid under vilkår som spesifisert i lisensen, som ligner lisensen til det originale arbeidet.\nno=Nei\nlaw.obligation.desc=Du er forpliktet til å\nsettings.advanced=Avanserte innstillinger\nwizard.menu.header=Jeg har følgende problem\nlaw.attribution=Attribusjon\nlaw.distribution=Distribusjon\nlaw.sharealike.desc=Distribuer avledet arbeid under den samme lisensen som det originale arbeidet.\nwizard.q0.step0.question=Er du forfatter av datasettet?\npage.filter.description=Vennligst finn en lisens ved å velge mellom de foretrukne lisensvilkårene nedenfor\\:\nlaw.reproduction=Reproduksjon\nlaw.prohibition.desc=Du har ikke lov å\nlaw.notice=Melding\nlaw.obligation=Forpliktelse\nlaw.patentgrant.desc=Bruk patenter fra lisensgiveren passende til arbeidet, i den grad det er nødvendig for å kunne bruke arbeidet i henhold til bruk av opphavsretten\nyes=Ja\nlaw.commercialuse.desc=Bruke det originale arbeidet for kommersielle formål.\nlaw.sublicensing=Tildele underlisens\nwizard.menu.question1=Er mitt arbeid, som inneholder andre datasett, betraktet som et avledet arbeid?\nwizard.menu.question0=Jeg ønsker å publisere et nytt datasett. Vil det være lovbeskyttet?\nstartagain=Start på nytt\nlaw.permission.desc=Du har lov til å\n";
});

})();
$__System.register("1b", ["1c", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a"], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var i18next_1,
      en_properties_text_1,
      de_properties_text_1,
      es_properties_text_1,
      fr_properties_text_1,
      it_properties_text_1,
      nl_properties_text_1,
      pl_properties_text_1,
      sk_properties_text_1,
      sv_properties_text_1,
      pt_properties_text_1,
      hu_properties_text_1,
      hr_properties_text_1,
      ro_properties_text_1,
      cs_properties_text_1,
      fi_properties_text_1,
      bg_properties_text_1,
      el_properties_text_1,
      mt_properties_text_1,
      et_properties_text_1,
      ga_properties_text_1,
      da_properties_text_1,
      lv_properties_text_1,
      lt_properties_text_1,
      sl_properties_text_1,
      no_properties_text_1;
  var lang,
      i18opts;
  function getTranslations(lang) {
    var props = {};
    switch (lang) {
      case 'en':
        props = en_properties_text_1["default"];
        break;
      case 'de':
        props = de_properties_text_1["default"];
        break;
      case 'es':
        props = es_properties_text_1["default"];
        break;
      case 'fr':
        props = fr_properties_text_1["default"];
        break;
      case 'it':
        props = it_properties_text_1["default"];
        break;
      case 'nl':
        props = nl_properties_text_1["default"];
        break;
      case 'pl':
        props = pl_properties_text_1["default"];
        break;
      case 'sk':
        props = sk_properties_text_1["default"];
        break;
      case 'sv':
        props = sv_properties_text_1["default"];
        break;
      case 'pt':
        props = pt_properties_text_1["default"];
        break;
      case 'hu':
        props = hu_properties_text_1["default"];
        break;
      case 'hr':
        props = hr_properties_text_1["default"];
        break;
      case 'ro':
        props = ro_properties_text_1["default"];
        break;
      case 'cs':
        props = cs_properties_text_1["default"];
        break;
      case 'fi':
        props = fi_properties_text_1["default"];
        break;
      case 'bg':
        props = bg_properties_text_1["default"];
        break;
      case 'el':
        props = el_properties_text_1["default"];
        break;
      case 'mt':
        props = mt_properties_text_1["default"];
        break;
      case 'et':
        props = et_properties_text_1["default"];
        break;
      case 'ga':
        props = ga_properties_text_1["default"];
        break;
      case 'da':
        props = da_properties_text_1["default"];
        break;
      case 'lv':
        props = lv_properties_text_1["default"];
        break;
      case 'lt':
        props = lt_properties_text_1["default"];
        break;
      case 'sl':
        props = sl_properties_text_1["default"];
        break;
      case 'no':
        props = no_properties_text_1["default"];
        break;
    }
    var lines = props.match(/[^\r\n]+/g);
    var translations = {};
    for (var _i = 0,
        _a = _.filter(lines, function(l) {
          return !_.startsWith(l, "#");
        }); _i < _a.length; _i++) {
      var line = _a[_i];
      var split = line.split(/=/);
      translations[split.shift()] = split.join('=');
    }
    return translations;
  }
  return {
    setters: [function(i18next_1_1) {
      i18next_1 = i18next_1_1;
    }, function(en_properties_text_1_1) {
      en_properties_text_1 = en_properties_text_1_1;
    }, function(de_properties_text_1_1) {
      de_properties_text_1 = de_properties_text_1_1;
    }, function(es_properties_text_1_1) {
      es_properties_text_1 = es_properties_text_1_1;
    }, function(fr_properties_text_1_1) {
      fr_properties_text_1 = fr_properties_text_1_1;
    }, function(it_properties_text_1_1) {
      it_properties_text_1 = it_properties_text_1_1;
    }, function(nl_properties_text_1_1) {
      nl_properties_text_1 = nl_properties_text_1_1;
    }, function(pl_properties_text_1_1) {
      pl_properties_text_1 = pl_properties_text_1_1;
    }, function(sk_properties_text_1_1) {
      sk_properties_text_1 = sk_properties_text_1_1;
    }, function(sv_properties_text_1_1) {
      sv_properties_text_1 = sv_properties_text_1_1;
    }, function(pt_properties_text_1_1) {
      pt_properties_text_1 = pt_properties_text_1_1;
    }, function(hu_properties_text_1_1) {
      hu_properties_text_1 = hu_properties_text_1_1;
    }, function(hr_properties_text_1_1) {
      hr_properties_text_1 = hr_properties_text_1_1;
    }, function(ro_properties_text_1_1) {
      ro_properties_text_1 = ro_properties_text_1_1;
    }, function(cs_properties_text_1_1) {
      cs_properties_text_1 = cs_properties_text_1_1;
    }, function(fi_properties_text_1_1) {
      fi_properties_text_1 = fi_properties_text_1_1;
    }, function(bg_properties_text_1_1) {
      bg_properties_text_1 = bg_properties_text_1_1;
    }, function(el_properties_text_1_1) {
      el_properties_text_1 = el_properties_text_1_1;
    }, function(mt_properties_text_1_1) {
      mt_properties_text_1 = mt_properties_text_1_1;
    }, function(et_properties_text_1_1) {
      et_properties_text_1 = et_properties_text_1_1;
    }, function(ga_properties_text_1_1) {
      ga_properties_text_1 = ga_properties_text_1_1;
    }, function(da_properties_text_1_1) {
      da_properties_text_1 = da_properties_text_1_1;
    }, function(lv_properties_text_1_1) {
      lv_properties_text_1 = lv_properties_text_1_1;
    }, function(lt_properties_text_1_1) {
      lt_properties_text_1 = lt_properties_text_1_1;
    }, function(sl_properties_text_1_1) {
      sl_properties_text_1 = sl_properties_text_1_1;
    }, function(no_properties_text_1_1) {
      no_properties_text_1 = no_properties_text_1_1;
    }],
    execute: function() {
      lang = $("html").attr("lang") || "en";
      ;
      i18opts = {
        keySeparator: '::',
        lng: lang,
        fallbackLng: 'en',
        resources: {
          'en': {translation: getTranslations('en')},
          'de': {translation: getTranslations('de')},
          'es': {translation: getTranslations('es')},
          'fr': {translation: getTranslations('fr')},
          'it': {translation: getTranslations('it')},
          'nl': {translation: getTranslations('nl')},
          'pl': {translation: getTranslations('pl')},
          'sk': {translation: getTranslations('sk')},
          'sv': {translation: getTranslations('sv')},
          'pt': {translation: getTranslations('pt')},
          'hu': {translation: getTranslations('hu')},
          'hr': {translation: getTranslations('hr')},
          'ro': {translation: getTranslations('ro')},
          'cs': {translation: getTranslations('cs')},
          'fi': {translation: getTranslations('fi')},
          'bg': {translation: getTranslations('bg')},
          'el': {translation: getTranslations('el')},
          'mt': {translation: getTranslations('mt')},
          'et': {translation: getTranslations('et')},
          'ga': {translation: getTranslations('ga')},
          'da': {translation: getTranslations('da')},
          'lv': {translation: getTranslations('lv')},
          'lt': {translation: getTranslations('lt')},
          'sl': {translation: getTranslations('sl')},
          'no': {translation: getTranslations('no')}
        }
      };
      i18next_1["default"].init(i18opts);
      exports_1("default", i18next_1["default"]);
    }
  };
});

$__System.register("1d", [], function() { return { setters: [], execute: function() {} } });

$__System.registerDynamic("1e", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<section class=\"la-filter\"><h2><%= i18n(\"page.filter.introduction\") %></h2><h3><%= i18n(\"page.filter.description\") %></h3><div class=\"search-box\"><div id=\"filter-panel\" class=\"collapse filter-panel\"><div class=\"panel panel-default\"><div class=\"panel-body\"><form role=\"form\" class=\"form-inline\"><div class=\"form-group\"><div style=\"margin-left:10px; margin-right:10px;\" class=\"checkbox\"><label><input id=\"cb_flag_weighted\" type=\"checkbox\"/><% window.i11 = i18n %>\n<%= i18n(\"settings.weighted\") %></label></div></div></form></div></div></div><button type=\"button\" data-toggle=\"collapse\" data-target=\"#filter-panel\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-cog\"></span> <%= i18n(\"settings.advanced\") %></button></div><nav class=\"row\"><div class=\"col-md-4\"><button id=\"law-derivativeworks\" class=\"la-permission\"><%= i18n(\"law.derivativeworks\") %></button><button id=\"law-reproduction\" class=\"la-permission\"><%= i18n(\"law.reproduction\") %></button><button id=\"law-sublicensing\" class=\"la-permission\"><%= i18n(\"law.sublicensing\") %></button><button id=\"law-distribution\" class=\"la-permission\"><%= i18n(\"law.distribution\") %></button><button id=\"law-patentgrant\" class=\"la-permission\"><%= i18n(\"law.patentgrant\") %></button></div><div class=\"col-md-4\"><button id=\"law-commercialuse\" class=\"la-prohibition\"><%= i18n(\"law.commercialuse\") %></button></div><div class=\"col-md-4\"><button id=\"law-notice\" class=\"la-obligation\"><%= i18n(\"law.notice\") %></button><button id=\"law-attribution\" class=\"la-obligation\"><%= i18n('law.attribution') %></button><button id=\"law-statechanges\" class=\"la-obligation\"><%= i18n('law.statechanges') %></button><button id=\"law-sharealike\" class=\"la-obligation\"><%= i18n('law.sharealike') %></button><button id=\"law-copyleft\" class=\"la-obligation\"><%= i18n('law.copyleft') %></button><button id=\"law-copyleft-lesser\" class=\"la-obligation\"><%= i18n('law.copyleft.lesser') %></button></div></nav><section><table class=\"table table-striped\"><thead><tr><th class=\"col-md-3\"><%= i18n(\"name\") %></th><th class=\"col-md-9\"><%= i18n(\"terms\") %></th></tr></thead><tbody><tr><td>Row 1 Cell 1</td><td>Row 1 Cell 2</td></tr></tbody></table></section></section>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.registerDynamic("20", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<tr><td scope=\"row\" class=\"col-md-3\"><a onclick=\"showLicense('<%= lic.shortName %>')\"><%= lic.name %></a></td><td style=\"margin-bottom: 1em\" class=\"col-md-9\"><% _.forEach(lic.terms, function(t) { %><button style=\"pointer-events: none\" class=\"la-button btn <%= styles[t.type] %>\"><%= i18n('law.' + t.type.toLowerCase()) %>: <%= i18n('law.' + t.title.toLowerCase()) %></button><% }); %></td></tr>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.register("21", ["1d", "22", "1c", "23", "1e", "20"], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var lodash_1,
      i18next_1,
      api_ts_1,
      _tplTemplate,
      _tplLicenseRow;
  var FilterLicenses;
  return {
    setters: [function(_1) {}, function(lodash_1_1) {
      lodash_1 = lodash_1_1;
    }, function(i18next_1_1) {
      i18next_1 = i18next_1_1;
    }, function(api_ts_1_1) {
      api_ts_1 = api_ts_1_1;
    }, function(_tplTemplate_1) {
      _tplTemplate = _tplTemplate_1;
    }, function(_tplLicenseRow_1) {
      _tplLicenseRow = _tplLicenseRow_1;
    }],
    execute: function() {
      FilterLicenses = (function() {
        function FilterLicenses(el, weighted) {
          var _this = this;
          this.el = el;
          this.template = lodash_1["default"].template(_tplTemplate.default(), {'imports': {
              'jq': jQuery,
              'i18n': function(p) {
                return i18next_1["default"].t(p);
              }
            }});
          this.licenseRowTmpl = lodash_1["default"].template(_tplLicenseRow.default(), {'imports': {
              'jq': jQuery,
              'i18n': function(p) {
                return i18next_1["default"].t(p);
              }
            }});
          this.flag_weighted = false;
          this.filterTerms = [];
          this.styleMap = {
            "Permission": "btn-success",
            "Obligation": "btn-warning",
            "Prohibition": "btn-danger"
          };
          this.el = el.html(this.template());
          this.flag_weighted = weighted || false;
          this.tableBody = this.el.find("tbody");
          this.nav = this.el.find("nav");
          this.setupSearch();
          api_ts_1.API.terms().then(function(terms) {
            var categories = lodash_1["default"].groupBy(terms, function(t) {
              return t.type;
            });
            var newNav = $("<nav class='row'/>");
            for (var category in categories) {
              var col = $("<div class='col-md-4'><h3>" + i18next_1["default"].t("law." + category.toLowerCase()) + "</h3></div>");
              categories[category].forEach(function(term) {
                var cat = category;
                var catClass = 'la-' + category.toLowerCase();
                var button = $('<button type="button" class="la-button ' + catClass + ' btn btn-primary' + '">' + i18next_1["default"].t('law.' + term.title.toLowerCase()) + '</button>');
                button.click(function(e) {
                  e.preventDefault();
                  if (!lodash_1["default"].includes(_this.filterTerms, term)) {
                    button.removeClass("btn-primary");
                    button.addClass(_this.styleMap[cat]);
                    _this.filterTerms.push(term);
                  } else {
                    button.blur();
                    button.addClass("btn-primary");
                    button.removeClass(_this.styleMap[cat]);
                    _this.filterTerms = lodash_1["default"].filter(_this.filterTerms, function(t) {
                      return t.id !== term.id;
                    });
                  }
                  _this.filter();
                });
                col.append(button);
              });
              newNav.append(col);
            }
            _this.nav.replaceWith(newNav);
            _this.nav = newNav;
          });
          this.filter();
        }
        FilterLicenses.prototype.setupSearch = function() {
          var _this = this;
          this.el.find("#cb_flag_weighted").change(function() {
            return _this.filter();
          });
        };
        FilterLicenses.prototype.filter = function() {
          var _this = this;
          api_ts_1.API.licenses().then(function(promised) {
            var body = $("<tbody />");
            var filterTerms = _this.filterTerms.map(function(t) {
              return t.id;
            });
            var licenses = lodash_1["default"].sortBy(promised, 'name');
            if ($("#cb_flag_weighted").is(':checked')) {
              console.log("weighted");
              licenses = lodash_1["default"].sortBy(licenses, function(lic) {
                var licTerms = lic.terms.map(function(t) {
                  return t.id;
                });
                return -lodash_1["default"].intersection(licTerms, filterTerms).length;
              });
            } else {
              licenses = lodash_1["default"].filter(licenses, function(lic) {
                var licTerms = lic.terms.map(function(t) {
                  return t.id;
                });
                return lodash_1["default"].intersection(licTerms, filterTerms).length == filterTerms.length;
              });
            }
            licenses.forEach(function(lic) {
              body.append(_this.licenseRowTmpl({
                'lic': lic,
                'styles': _this.styleMap
              }));
            });
            _this.tableBody.replaceWith(body);
            _this.tableBody = body;
          });
        };
        return FilterLicenses;
      }());
      exports_1("FilterLicenses", FilterLicenses);
    }
  };
});

$__System.register("24", [], function() { return { setters: [], execute: function() {} } });

$__System.registerDynamic("25", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<section class=\"la-wizard\"><div class=\"stepwizard\"><div class=\"stepwizard-row setup-panel\"><div class=\"stepwizard-step\"><a href=\"#step-1\" type=\"button\" class=\"btn btn-primary btn-circle\">1</a><p>Step 1</p></div><div class=\"stepwizard-step\"><a href=\"#step-2\" type=\"button\" disabled=\"disabled\" class=\"btn btn-default btn-circle\">2</a><p>Step 2</p></div><div class=\"stepwizard-step\"><a href=\"#step-3\" type=\"button\" disabled=\"disabled\" class=\"btn btn-default btn-circle\">3</a><p>Step 3</p></div></div></div><form role=\"form\"><div id=\"step-1\" class=\"row setup-content\"><div class=\"col-xs-12\"><div class=\"col-md-12\"><h3> Step 1</h3><div class=\"form-group\"><label class=\"control-label\">First Name</label><input maxlength=\"100\" type=\"text\" required=\"required\" placeholder=\"Enter First Name\" class=\"form-control\"/></div><div class=\"form-group\"><label class=\"control-label\">Last Name</label><input maxlength=\"100\" type=\"text\" required=\"required\" placeholder=\"Enter Last Name\" class=\"form-control\"/></div><button type=\"button\" class=\"btn btn-primary nextBtn btn-lg pull-right\">Next</button></div></div></div><div id=\"step-2\" class=\"row setup-content\"><div class=\"col-xs-12\"><div class=\"col-md-12\"><h3> Step 2</h3><div class=\"form-group\"><label class=\"control-label\">Company Name</label><input maxlength=\"200\" type=\"text\" required=\"required\" placeholder=\"Enter Company Name\" class=\"form-control\"/></div><div class=\"form-group\"><label class=\"control-label\">Company Address</label><input maxlength=\"200\" type=\"text\" required=\"required\" placeholder=\"Enter Company Address\" class=\"form-control\"/></div><button type=\"button\" class=\"btn btn-primary nextBtn btn-lg pull-right\">Next</button></div></div></div><div id=\"step-3\" class=\"row setup-content\"><div class=\"col-xs-12\"><div class=\"col-md-12\"><h3> Step 3</h3><button type=\"submit\" class=\"btn btn-success btn-lg pull-right\">Finish!</button></div></div></div></form></section>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.registerDynamic("26", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<section class=\"la-wizard\"><row><div><h2><%= i18n(\"wizard.menu.header\") %></h2><p><button id=\"la-wizard-btn-publish\" type=\"button\" class=\"btn btn-default\"><%= i18n(\"wizard.menu.question0\") %></button></p><p><button id=\"la-wizard-btn-derivative\" type=\"button\" class=\"btn btn-default\"><%= i18n(\"wizard.menu.question1\") %></button></p></div></row></section>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.registerDynamic("27", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<div class=\"licensing-wizard\"><h2><%= i18n(\"wizard.menu.question1\") %></h2><ul id=\"la-wizard-tabs\" role=\"tablist\" style=\"display: none\" class=\"nav nav-tabs\"><li role=\"presentation\" class=\"active\"><a href=\"#la-q1\" role=\"tab\" data-toggle=\"tab\">la-q1</a></li><li role=\"presentation\"><a href=\"#la-q2\" role=\"tab\" data-toggle=\"tab\">la-q2</a></li><li role=\"presentation\"><a href=\"#la-n1\" role=\"tab\" data-toggle=\"tab\">la-n1</a></li><li role=\"presentation\"><a href=\"#la-end\" role=\"tab\" data-toggle=\"tab\">la-end</a></li></ul><div class=\"tab-content\"><div id=\"la-q1\" role=\"tabpanel\" class=\"tab-pane active\"><h3><%= i18n(\"wizard.q1.step0.question\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-n1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-danger\"><%= i18n(\"no\") %></button>&nbsp;<button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q2']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-success\"><%= i18n(\"yes\") %></button></div><div id=\"la-q2\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q1.step1.question\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-n1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-danger\"><%=  i18n(\"no\") %></button>&nbsp;<button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-end']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-success\"><%= i18n(\"yes\") %></button></div><div id=\"la-end\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q1.end1\") %><a href=\"http://www.paneuropeandataportal.eu/basic-page/57/filter-license\"><%= i18n(\"wizard.q1.end1.here\") %></a></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-primary\"><%= i18n(\"startagain\") %></button></div><div id=\"la-n1\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q1.end0\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-primary\"><%= i18n(\"startagain\") %></button></div></div></div>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.registerDynamic("28", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<div class=\"licensing-wizard\"><h2><%= i18n(\"wizard.menu.question0\") %></h2><ul id=\"la-wizard-tabs\" role=\"tablist\" style=\"display: none\" class=\"nav nav-tabs\"><li role=\"presentation\" class=\"active\"><a href=\"#la-q1\" role=\"tab\" data-toggle=\"tab\">la-q1</a></li><li role=\"presentation\"><a href=\"#la-q2\" role=\"tab\" data-toggle=\"tab\">la-q2</a></li><li role=\"presentation\"><a href=\"#la-q3\" role=\"tab\" data-toggle=\"tab\">la-q3</a></li><li role=\"presentation\"><a href=\"#la-n1\" role=\"tab\" data-toggle=\"tab\">la-n1</a></li><li role=\"presentation\"><a href=\"#la-n2\" role=\"tab\" data-toggle=\"tab\">la-n2</a></li><li role=\"presentation\"><a href=\"#la-y1\" role=\"tab\" data-toggle=\"tab\">la-y1</a></li><li role=\"presentation\"><a href=\"#la-y2\" role=\"tab\" data-toggle=\"tab\">la-y2</a></li></ul><div class=\"tab-content\"><div id=\"la-q1\" role=\"tabpanel\" class=\"tab-pane active\"><h3><%= i18n(\"wizard.q0.step0.question\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-n1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-danger\"><%= i18n(\"no\") %></button>&nbsp;<button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q2']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-success\"><%= i18n(\"yes\") %></button></div><div id=\"la-n1\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q0.end0\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-primary\"><%= i18n(\"startagain\") %></button></div><div id=\"la-q2\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q0.step1.question\") %><br/><br/><br/><%= i18n(\"wizard.q0.step1.explanation\") %><br/></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q3']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-danger\"> <%= i18n(\"no\") %></button>&nbsp;<button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-y1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-success\"><%= i18n(\"yes\") %></button></div><div id=\"la-y1\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q0.end2\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-primary\"><%= i18n(\"startagain\") %></button></div><div id=\"la-q3\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q0.step2.question\") %><br/><br/><%= i18n(\"wizard.q0.step2.explanation\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-n2']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-danger\"><%= i18n(\"no\") %></button>&nbsp;<button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-y2']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-success\"><%= i18n(\"yes\") %></button></div><div id=\"la-y2\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q0.end1\") %> <a href=\"/basic-page/57/filter-license\"><%= i18n(\"wizard.q1.end1.here\") %>.</a></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-primary\"><%= i18n(\"startagain\") %></button></div><div id=\"la-n2\" role=\"tabpanel\" class=\"tab-pane\"><h3><%= i18n(\"wizard.q0.end3\") %></h3><button onclick=\"$(&quot;#la-wizard-tabs a[href='#la-q1']&quot;).tab(&quot;show&quot;)\" class=\"btn btn-primary\"><%= i18n(\"startagain\") %></button></div></div></div>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.register("29", ["24", "22", "1c", "25", "26", "27", "28"], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var lodash_1,
      i18next_1,
      _tplTemplate,
      _tplIntro,
      _tplDerivative,
      _tplPublisher;
  var LicensingWizard;
  return {
    setters: [function(_1) {}, function(lodash_1_1) {
      lodash_1 = lodash_1_1;
    }, function(i18next_1_1) {
      i18next_1 = i18next_1_1;
    }, function(_tplTemplate_1) {
      _tplTemplate = _tplTemplate_1;
    }, function(_tplIntro_1) {
      _tplIntro = _tplIntro_1;
    }, function(_tplDerivative_1) {
      _tplDerivative = _tplDerivative_1;
    }, function(_tplPublisher_1) {
      _tplPublisher = _tplPublisher_1;
    }],
    execute: function() {
      LicensingWizard = (function() {
        function LicensingWizard(el) {
          this.el = el;
          this.tplOpts = {'imports': {
              'jq': jQuery,
              'i18n': function(p) {
                return i18next_1["default"].t(p);
              }
            }};
          this.template = lodash_1["default"].template(_tplTemplate.default(), this.tplOpts);
          this.intro = lodash_1["default"].template(_tplIntro.default(), this.tplOpts);
          this.tplDerivative = lodash_1["default"].template(_tplDerivative.default(), this.tplOpts);
          this.tplPublisher = lodash_1["default"].template(_tplPublisher.default(), this.tplOpts);
          this.derivative = {
            items: [{
              id: "q1",
              text: "Are the incorporated datasets protected under copyright law or database rights?",
              options: [{
                label: "Yes",
                goto: "q2"
              }, {
                label: "No",
                goto: "n1"
              }]
            }, {
              id: "q2",
              text: "Did you modify the original work?",
              options: [{
                label: "Yes",
                goto: "end"
              }, {
                label: "No",
                goto: "n1"
              }]
            }, {
              id: "n1",
              text: "Your work does not constitute a derivative work. However, contractual restrictions might still apply.<br /><br /> EXAMPLE"
            }, {
              id: "end",
              text: "Your work most likely constitutes a derivative work. Please have a look at compatible licenses here: XXXX"
            }],
            start: "q1",
            title: "Is my work that incorporates other datasets considered a derivative work of theirs?"
          };
          this.publisher = {
            items: [{
              id: "q1",
              text: "Did you author the dataset?",
              options: [{
                label: "Yes",
                goto: "q2"
              }, {
                label: "No",
                goto: "n1"
              }]
            }, {
              id: "n1",
              text: "Please contact the original dataset author for coordinating publication."
            }, {
              id: "q2",
              text: "Is the dataset your intellectual creation by selection or arrangement?<br /><br /><br />" + "For example, a list of phone numbers of all the people working in an organisation will nto be covered by copyright. The author cannot show any creativity in the selection of the data" + "(which is simply all the people working there) nor in the choice of data (simple names and phone numbers). Even if the collection of data" + "were very large or if a lot of effort was involved, no creativity was required and therefore no copyright rules apply.<br />",
              options: [{
                label: "Yes",
                goto: "y1"
              }, {
                label: "No",
                goto: "q3"
              }]
            }, {
              id: "y1",
              text: "The dataset is likely protected under copyright law and you may choose an applicable license."
            }, {
              id: "q3",
              text: "Was there a qualitatively and/or quantitatively substantial investment in either the obtaining, verification or presentation of the contents?<br /><br />" + "For example, a database containing exclusive geographic information related to an entire country will likely be covered by this. This is because such a database will normally require a significant effort to create in terms of time, money, effort etc.",
              options: [{
                label: "Yes",
                goto: "y2"
              }, {
                label: "No",
                goto: "n2"
              }]
            }, {
              id: "y2",
              text: "Your database is likely protected under database rights (also known as 'sui generis rights'). You can find a list of licenses covering these rights here: XXX"
            }, {
              id: "n2",
              text: "Unfortunately, your dataset most likely does not fall under copyright and/or database rights. Please consult a lawyer for further advice on this topic."
            }],
            start: "q1",
            title: "I wish to publish a new dataset. Will it be protected by law?"
          };
          this.showIntro();
        }
        LicensingWizard.prototype.showIntro = function() {
          var _this = this;
          this.el = this.el.html(this.intro());
          this.el.find("#la-wizard-btn-publish").unbind("click").click(function(e) {
            e.preventDefault();
            _this.el = _this.el.html(_this.tplPublisher());
          });
          this.el.find("#la-wizard-btn-derivative").unbind("click").click(function(e) {
            e.preventDefault();
            _this.el = _this.el.html(_this.tplDerivative());
          });
        };
        LicensingWizard.prototype.tmp = function() {
          this.el = this.el.html(this.template());
          var navListItems = $('div.setup-panel div a'),
              allWells = $('.setup-content'),
              allNextBtn = $('.nextBtn');
          allWells.hide();
          navListItems.click(function(e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);
            if (!$item.hasClass('disabled')) {
              navListItems.removeClass('btn-primary').addClass('btn-default');
              $item.addClass('btn-primary');
              allWells.hide();
              $target.show();
              $target.find('input:eq(0)').focus();
            }
          });
          allNextBtn.click(function() {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;
            if (isValid)
              nextStepWizard.removeAttr('disabled').trigger('click');
          });
          $('div.setup-panel div a.btn-primary').trigger('click');
        };
        return LicensingWizard;
      }());
      exports_1("LicensingWizard", LicensingWizard);
    }
  };
});

$__System.register("2a", [], function() { return { setters: [], execute: function() {} } });

$__System.registerDynamic("@system-env", [], false, function() {
  return {
    "browser": true,
    "node": false,
    "default": true
  };
});

$__System.registerDynamic("2b", ["@system-env"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var productionEnv = $__require('@system-env').production;
  var process = module.exports = {};
  var queue = [];
  var draining = false;
  var currentQueue;
  var queueIndex = -1;
  function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
      queue = currentQueue.concat(queue);
    } else {
      queueIndex = -1;
    }
    if (queue.length) {
      drainQueue();
    }
  }
  function drainQueue() {
    if (draining) {
      return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;
    var len = queue.length;
    while (len) {
      currentQueue = queue;
      queue = [];
      while (++queueIndex < len) {
        if (currentQueue) {
          currentQueue[queueIndex].run();
        }
      }
      queueIndex = -1;
      len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
  }
  process.nextTick = function(fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
      for (var i = 1; i < arguments.length; i++) {
        args[i - 1] = arguments[i];
      }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
      setTimeout(drainQueue, 0);
    }
  };
  function Item(fun, array) {
    this.fun = fun;
    this.array = array;
  }
  Item.prototype.run = function() {
    this.fun.apply(null, this.array);
  };
  process.title = 'browser';
  process.browser = true;
  process.env = {NODE_ENV: productionEnv ? 'production' : 'development'};
  process.argv = [];
  process.version = '';
  process.versions = {};
  function noop() {}
  process.on = noop;
  process.addListener = noop;
  process.once = noop;
  process.off = noop;
  process.removeListener = noop;
  process.removeAllListeners = noop;
  process.emit = noop;
  process.binding = function(name) {
    throw new Error('process.binding is not supported');
  };
  process.cwd = function() {
    return '/';
  };
  process.chdir = function(dir) {
    throw new Error('process.chdir is not supported');
  };
  process.umask = function() {
    return 0;
  };
  return module.exports;
});

$__System.registerDynamic("1f", ["@empty", "2b"], true, function($__require, exports, module) {
  "use strict";
  var process = $__require("2b");
  var define,
      global = this,
      GLOBAL = this;
  exports.merge = function merge(a, b) {
    if (arguments.length === 1) {
      var attrs = a[0];
      for (var i = 1; i < a.length; i++) {
        attrs = merge(attrs, a[i]);
      }
      return attrs;
    }
    var ac = a['class'];
    var bc = b['class'];
    if (ac || bc) {
      ac = ac || [];
      bc = bc || [];
      if (!Array.isArray(ac))
        ac = [ac];
      if (!Array.isArray(bc))
        bc = [bc];
      a['class'] = ac.concat(bc).filter(nulls);
    }
    for (var key in b) {
      if (key != 'class') {
        a[key] = b[key];
      }
    }
    return a;
  };
  function nulls(val) {
    return val != null && val !== '';
  }
  exports.joinClasses = joinClasses;
  function joinClasses(val) {
    return (Array.isArray(val) ? val.map(joinClasses) : (val && typeof val === 'object') ? Object.keys(val).filter(function(key) {
      return val[key];
    }) : [val]).filter(nulls).join(' ');
  }
  exports.cls = function cls(classes, escaped) {
    var buf = [];
    for (var i = 0; i < classes.length; i++) {
      if (escaped && escaped[i]) {
        buf.push(exports.escape(joinClasses([classes[i]])));
      } else {
        buf.push(joinClasses(classes[i]));
      }
    }
    var text = joinClasses(buf);
    if (text.length) {
      return ' class="' + text + '"';
    } else {
      return '';
    }
  };
  exports.style = function(val) {
    if (val && typeof val === 'object') {
      return Object.keys(val).map(function(style) {
        return style + ':' + val[style];
      }).join(';');
    } else {
      return val;
    }
  };
  exports.attr = function attr(key, val, escaped, terse) {
    if (key === 'style') {
      val = exports.style(val);
    }
    if ('boolean' == typeof val || null == val) {
      if (val) {
        return ' ' + (terse ? key : key + '="' + key + '"');
      } else {
        return '';
      }
    } else if (0 == key.indexOf('data') && 'string' != typeof val) {
      if (JSON.stringify(val).indexOf('&') !== -1) {
        console.warn('Since Jade 2.0.0, ampersands (`&`) in data attributes ' + 'will be escaped to `&amp;`');
      }
      ;
      if (val && typeof val.toISOString === 'function') {
        console.warn('Jade will eliminate the double quotes around dates in ' + 'ISO form after 2.0.0');
      }
      return ' ' + key + "='" + JSON.stringify(val).replace(/'/g, '&apos;') + "'";
    } else if (escaped) {
      if (val && typeof val.toISOString === 'function') {
        console.warn('Jade will stringify dates in ISO form after 2.0.0');
      }
      return ' ' + key + '="' + exports.escape(val) + '"';
    } else {
      if (val && typeof val.toISOString === 'function') {
        console.warn('Jade will stringify dates in ISO form after 2.0.0');
      }
      return ' ' + key + '="' + val + '"';
    }
  };
  exports.attrs = function attrs(obj, terse) {
    var buf = [];
    var keys = Object.keys(obj);
    if (keys.length) {
      for (var i = 0; i < keys.length; ++i) {
        var key = keys[i],
            val = obj[key];
        if ('class' == key) {
          if (val = joinClasses(val)) {
            buf.push(' ' + key + '="' + val + '"');
          }
        } else {
          buf.push(exports.attr(key, val, false, terse));
        }
      }
    }
    return buf.join('');
  };
  var jade_encode_html_rules = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;'
  };
  var jade_match_html = /[&<>"]/g;
  function jade_encode_char(c) {
    return jade_encode_html_rules[c] || c;
  }
  exports.escape = jade_escape;
  function jade_escape(html) {
    var result = String(html).replace(jade_match_html, jade_encode_char);
    if (result === '' + html)
      return html;
    else
      return result;
  }
  ;
  exports.rethrow = function rethrow(err, filename, lineno, str) {
    if (!(err instanceof Error))
      throw err;
    if ((typeof window != 'undefined' || !filename) && !str) {
      err.message += ' on line ' + lineno;
      throw err;
    }
    try {
      str = str || $__require('@empty').readFileSync(filename, 'utf8');
    } catch (ex) {
      rethrow(err, null, lineno);
    }
    var context = 3,
        lines = str.split('\n'),
        start = Math.max(lineno - context, 0),
        end = Math.min(lines.length, lineno + context);
    var context = lines.slice(start, end).map(function(line, i) {
      var curr = i + start + 1;
      return (curr == lineno ? '  > ' : '    ') + curr + '| ' + line;
    }).join('\n');
    err.path = filename;
    err.message = (filename || 'Jade') + ':' + lineno + '\n' + context + '\n\n' + err.message;
    throw err;
  };
  exports.DebugItem = function DebugItem(lineno, filename) {
    this.lineno = lineno;
    this.filename = filename;
  };
  return module.exports;
});

$__System.registerDynamic("2c", ["1f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var jade = $__require('1f');
  module.exports = function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;
    buf.push("<section class=\"show_license license\"><h1 class=\"title\"><%= name %></h1><p class=\"description\"><%= description %></p><p class=\"url\"><h4><a href=\"<%= url %>\"><%= i18n(\"page.show.open\") %></a></h4></p><div class=\"row\"><div class=\"col-md-9\"><% if(permissions.length > 0) { %><div class=\"panel panel-success\"><div class=\"panel-heading\"><h3 class=\"panel-title\"><%= i18n(\"law.permission.desc\") %> </h3></div><div class=\"panel-body\"><ul class=\"terms list-group\"><% jq.each(permissions, function(i, e) { %><li class=\"list-group-item term\"><span style=\"font-weight: bold\"><%= i18n('law.' + e.title.toLowerCase()) %></span><br/><%= i18n('law.' + e.title.toLowerCase() + '.desc') %></li><% }); %></ul></div></div><% } %>\n<% if(obligations.length > 0) { %><div class=\"panel panel-warning\"><div class=\"panel-heading\"><h3 class=\"panel-title\"><%= i18n(\"law.obligation.desc\") %> </h3></div><div class=\"panel-body\"><ul class=\"terms list-group\"><% jq.each(obligations, function(i, e) { %><li class=\"list-group-item term\"><span style=\"font-weight: bold\"><%= i18n('law.' + e.title.toLowerCase()) %></span><br/><%= i18n('law.' + e.title.toLowerCase() + '.desc') %></li><% }); %></ul></div></div><% } if(prohibitions.length > 0) { %><div class=\"panel panel-danger\"><div class=\"panel-heading\"><h3 class=\"panel-title\"><%= i18n(\"law.prohibition.desc\") %></h3></div><div class=\"panel-body\"><ul class=\"terms list-group\"><% jq.each(prohibitions, function(i, e) { %><li class=\"list-group-item term\"><span style=\"font-weight: bold\"><%= i18n('law.' + e.title.toLowerCase()) %></span><br/><%= i18n('law.' + e.title.toLowerCase() + '.desc') %></li><% }); %></ul></div></div><% } %></div><div class=\"col-md-3\"><div class=\"compa-heading\"><%= i18n(\"page.show.compatible\") %></div><ul class=\"compatible\"><% jq.each(compatibleLicenses, function(i, e) { %><li><a onclick=\"showLicense('<%= e %>');\"><%= e %></a></li><% }); %></ul></div></div></section>");
    ;
    return buf.join("");
  };
  return module.exports;
});

$__System.registerDynamic("2d", ["2e", "2f"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  var _EventEmitter2 = $__require('2e');
  var _EventEmitter3 = _interopRequireDefault(_EventEmitter2);
  var _utils = $__require('2f');
  var utils = _interopRequireWildcard(_utils);
  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    } else {
      var newObj = {};
      if (obj != null) {
        for (var key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key))
            newObj[key] = obj[key];
        }
      }
      newObj.default = obj;
      return newObj;
    }
  }
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _defaults(obj, defaults) {
    var keys = Object.getOwnPropertyNames(defaults);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var value = Object.getOwnPropertyDescriptor(defaults, key);
      if (value && value.configurable && obj[key] === undefined) {
        Object.defineProperty(obj, key, value);
      }
    }
    return obj;
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }
  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass);
  }
  var ResourceStore = function(_EventEmitter) {
    _inherits(ResourceStore, _EventEmitter);
    function ResourceStore() {
      var data = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      var options = arguments.length <= 1 || arguments[1] === undefined ? {
        ns: ['translation'],
        defaultNS: 'translation'
      } : arguments[1];
      _classCallCheck(this, ResourceStore);
      var _this = _possibleConstructorReturn(this, _EventEmitter.call(this));
      _this.data = data;
      _this.options = options;
      return _this;
    }
    ResourceStore.prototype.addNamespaces = function addNamespaces(ns) {
      if (this.options.ns.indexOf(ns) < 0) {
        this.options.ns.push(ns);
      }
    };
    ResourceStore.prototype.removeNamespaces = function removeNamespaces(ns) {
      var index = this.options.ns.indexOf(ns);
      if (index > -1) {
        this.options.ns.splice(index, 1);
      }
    };
    ResourceStore.prototype.getResource = function getResource(lng, ns, key) {
      var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
      var keySeparator = options.keySeparator || this.options.keySeparator;
      if (keySeparator === undefined)
        keySeparator = '.';
      var path = [lng, ns];
      if (key && typeof key !== 'string')
        path = path.concat(key);
      if (key && typeof key === 'string')
        path = path.concat(keySeparator ? key.split(keySeparator) : key);
      if (lng.indexOf('.') > -1) {
        path = lng.split('.');
      }
      return utils.getPath(this.data, path);
    };
    ResourceStore.prototype.addResource = function addResource(lng, ns, key, value) {
      var options = arguments.length <= 4 || arguments[4] === undefined ? {silent: false} : arguments[4];
      var keySeparator = this.options.keySeparator;
      if (keySeparator === undefined)
        keySeparator = '.';
      var path = [lng, ns];
      if (key)
        path = path.concat(keySeparator ? key.split(keySeparator) : key);
      if (lng.indexOf('.') > -1) {
        path = lng.split('.');
        value = ns;
        ns = path[1];
      }
      this.addNamespaces(ns);
      utils.setPath(this.data, path, value);
      if (!options.silent)
        this.emit('added', lng, ns, key, value);
    };
    ResourceStore.prototype.addResources = function addResources(lng, ns, resources) {
      for (var m in resources) {
        if (typeof resources[m] === 'string')
          this.addResource(lng, ns, m, resources[m], {silent: true});
      }
      this.emit('added', lng, ns, resources);
    };
    ResourceStore.prototype.addResourceBundle = function addResourceBundle(lng, ns, resources, deep, overwrite) {
      var path = [lng, ns];
      if (lng.indexOf('.') > -1) {
        path = lng.split('.');
        deep = resources;
        resources = ns;
        ns = path[1];
      }
      this.addNamespaces(ns);
      var pack = utils.getPath(this.data, path) || {};
      if (deep) {
        utils.deepExtend(pack, resources, overwrite);
      } else {
        pack = _extends({}, pack, resources);
      }
      utils.setPath(this.data, path, pack);
      this.emit('added', lng, ns, resources);
    };
    ResourceStore.prototype.removeResourceBundle = function removeResourceBundle(lng, ns) {
      if (this.hasResourceBundle(lng, ns)) {
        delete this.data[lng][ns];
      }
      this.removeNamespaces(ns);
      this.emit('removed', lng, ns);
    };
    ResourceStore.prototype.hasResourceBundle = function hasResourceBundle(lng, ns) {
      return this.getResource(lng, ns) !== undefined;
    };
    ResourceStore.prototype.getResourceBundle = function getResourceBundle(lng, ns) {
      if (!ns)
        ns = this.options.defaultNS;
      if (this.options.compatibilityAPI === 'v1')
        return _extends({}, this.getResource(lng, ns));
      return this.getResource(lng, ns);
    };
    ResourceStore.prototype.toJSON = function toJSON() {
      return this.data;
    };
    return ResourceStore;
  }(_EventEmitter3.default);
  exports.default = ResourceStore;
  return module.exports;
});

$__System.registerDynamic("30", ["31", "2e", "32", "33", "2f"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) {
    return typeof obj;
  } : function(obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
  };
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  var _EventEmitter2 = $__require('2e');
  var _EventEmitter3 = _interopRequireDefault(_EventEmitter2);
  var _postProcessor = $__require('32');
  var _postProcessor2 = _interopRequireDefault(_postProcessor);
  var _v = $__require('33');
  var compat = _interopRequireWildcard(_v);
  var _utils = $__require('2f');
  var utils = _interopRequireWildcard(_utils);
  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    } else {
      var newObj = {};
      if (obj != null) {
        for (var key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key))
            newObj[key] = obj[key];
        }
      }
      newObj.default = obj;
      return newObj;
    }
  }
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _defaults(obj, defaults) {
    var keys = Object.getOwnPropertyNames(defaults);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var value = Object.getOwnPropertyDescriptor(defaults, key);
      if (value && value.configurable && obj[key] === undefined) {
        Object.defineProperty(obj, key, value);
      }
    }
    return obj;
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }
  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass);
  }
  var Translator = function(_EventEmitter) {
    _inherits(Translator, _EventEmitter);
    function Translator(services) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      _classCallCheck(this, Translator);
      var _this = _possibleConstructorReturn(this, _EventEmitter.call(this));
      utils.copy(['resourceStore', 'languageUtils', 'pluralResolver', 'interpolator', 'backendConnector'], services, _this);
      _this.options = options;
      _this.logger = _logger2.default.create('translator');
      return _this;
    }
    Translator.prototype.changeLanguage = function changeLanguage(lng) {
      if (lng)
        this.language = lng;
    };
    Translator.prototype.exists = function exists(key) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {interpolation: {}} : arguments[1];
      if (this.options.compatibilityAPI === 'v1') {
        options = compat.convertTOptions(options);
      }
      return this.resolve(key, options) !== undefined;
    };
    Translator.prototype.extractFromKey = function extractFromKey(key, options) {
      var nsSeparator = options.nsSeparator || this.options.nsSeparator;
      if (nsSeparator === undefined)
        nsSeparator = ':';
      var namespaces = options.ns || this.options.defaultNS;
      if (nsSeparator && key.indexOf(nsSeparator) > -1) {
        var parts = key.split(nsSeparator);
        namespaces = parts[0];
        key = parts[1];
      }
      if (typeof namespaces === 'string')
        namespaces = [namespaces];
      return {
        key: key,
        namespaces: namespaces
      };
    };
    Translator.prototype.translate = function translate(keys) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) !== 'object') {
        options = this.options.overloadTranslationOptionHandler(arguments);
      } else if (this.options.compatibilityAPI === 'v1') {
        options = compat.convertTOptions(options);
      }
      if (keys === undefined || keys === null || keys === '')
        return '';
      if (typeof keys === 'number')
        keys = String(keys);
      if (typeof keys === 'string')
        keys = [keys];
      var lng = options.lng || this.language;
      if (lng && lng.toLowerCase() === 'cimode')
        return keys[keys.length - 1];
      var keySeparator = options.keySeparator || this.options.keySeparator || '.';
      var _extractFromKey = this.extractFromKey(keys[keys.length - 1], options);
      var key = _extractFromKey.key;
      var namespaces = _extractFromKey.namespaces;
      var namespace = namespaces[namespaces.length - 1];
      var res = this.resolve(keys, options);
      var resType = Object.prototype.toString.apply(res);
      var noObject = ['[object Number]', '[object Function]', '[object RegExp]'];
      var joinArrays = options.joinArrays !== undefined ? options.joinArrays : this.options.joinArrays;
      if (res && typeof res !== 'string' && noObject.indexOf(resType) < 0 && !(joinArrays && resType === '[object Array]')) {
        if (!options.returnObjects && !this.options.returnObjects) {
          this.logger.warn('accessing an object - but returnObjects options is not enabled!');
          return this.options.returnedObjectHandler ? this.options.returnedObjectHandler(key, res, options) : 'key \'' + key + ' (' + this.language + ')\' returned an object instead of string.';
        }
        var copy = resType === '[object Array]' ? [] : {};
        for (var m in res) {
          copy[m] = this.translate('' + key + keySeparator + m, _extends({
            joinArrays: false,
            ns: namespaces
          }, options));
        }
        res = copy;
      } else if (joinArrays && resType === '[object Array]') {
        res = res.join(joinArrays);
        if (res)
          res = this.extendTranslation(res, key, options);
      } else {
        var usedDefault = false,
            usedKey = false;
        if (!this.isValidLookup(res) && options.defaultValue !== undefined) {
          usedDefault = true;
          res = options.defaultValue;
        }
        if (!this.isValidLookup(res)) {
          usedKey = true;
          res = key;
        }
        if (usedKey || usedDefault) {
          this.logger.log('missingKey', lng, namespace, key, res);
          if (this.options.saveMissing) {
            var lngs = [];
            if (this.options.saveMissingTo === 'fallback' && this.options.fallbackLng && this.options.fallbackLng[0]) {
              for (var i = 0; i < this.options.fallbackLng.length; i++) {
                lngs.push(this.options.fallbackLng[i]);
              }
            } else if (this.options.saveMissingTo === 'all') {
              lngs = this.languageUtils.toResolveHierarchy(options.lng || this.language);
            } else {
              lngs.push(options.lng || this.language);
            }
            if (this.options.missingKeyHandler) {
              this.options.missingKeyHandler(lngs, namespace, key, res);
            } else if (this.backendConnector && this.backendConnector.saveMissing) {
              this.backendConnector.saveMissing(lngs, namespace, key, res);
            }
            this.emit('missingKey', lngs, namespace, key, res);
          }
        }
        res = this.extendTranslation(res, key, options);
        if (usedKey && res === key && this.options.appendNamespaceToMissingKey)
          res = namespace + ':' + key;
        if (usedKey && this.options.parseMissingKeyHandler)
          res = this.options.parseMissingKeyHandler(res);
      }
      return res;
    };
    Translator.prototype.extendTranslation = function extendTranslation(res, key, options) {
      var _this2 = this;
      if (options.interpolation)
        this.interpolator.init(options);
      var data = options.replace && typeof options.replace !== 'string' ? options.replace : options;
      if (this.options.interpolation.defaultVariables)
        data = _extends({}, this.options.interpolation.defaultVariables, data);
      res = this.interpolator.interpolate(res, data);
      res = this.interpolator.nest(res, function() {
        for (var _len = arguments.length,
            args = Array(_len),
            _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }
        return _this2.translate.apply(_this2, args);
      }, options);
      if (options.interpolation)
        this.interpolator.reset();
      var postProcess = options.postProcess || this.options.postProcess;
      var postProcessorNames = typeof postProcess === 'string' ? [postProcess] : postProcess;
      if (res !== undefined && postProcessorNames && postProcessorNames.length && options.applyPostProcessor !== false) {
        res = _postProcessor2.default.handle(postProcessorNames, res, key, options, this);
      }
      return res;
    };
    Translator.prototype.resolve = function resolve(keys) {
      var _this3 = this;
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      var found = void 0;
      if (typeof keys === 'string')
        keys = [keys];
      keys.forEach(function(k) {
        if (_this3.isValidLookup(found))
          return;
        var _extractFromKey2 = _this3.extractFromKey(k, options);
        var key = _extractFromKey2.key;
        var namespaces = _extractFromKey2.namespaces;
        if (_this3.options.fallbackNS)
          namespaces = namespaces.concat(_this3.options.fallbackNS);
        var needsPluralHandling = options.count !== undefined && typeof options.count !== 'string';
        var needsContextHandling = options.context !== undefined && typeof options.context === 'string' && options.context !== '';
        var codes = options.lngs ? options.lngs : _this3.languageUtils.toResolveHierarchy(options.lng || _this3.language);
        namespaces.forEach(function(ns) {
          if (_this3.isValidLookup(found))
            return;
          codes.forEach(function(code) {
            if (_this3.isValidLookup(found))
              return;
            var finalKey = key;
            var finalKeys = [finalKey];
            var pluralSuffix = void 0;
            if (needsPluralHandling)
              pluralSuffix = _this3.pluralResolver.getSuffix(code, options.count);
            if (needsPluralHandling && needsContextHandling)
              finalKeys.push(finalKey + pluralSuffix);
            if (needsContextHandling)
              finalKeys.push(finalKey += '' + _this3.options.contextSeparator + options.context);
            if (needsPluralHandling)
              finalKeys.push(finalKey += pluralSuffix);
            var possibleKey = void 0;
            while (possibleKey = finalKeys.pop()) {
              if (_this3.isValidLookup(found))
                continue;
              found = _this3.getResource(code, ns, possibleKey, options);
            }
          });
        });
      });
      return found;
    };
    Translator.prototype.isValidLookup = function isValidLookup(res) {
      return res !== undefined && !(!this.options.returnNull && res === null) && !(!this.options.returnEmptyString && res === '');
    };
    Translator.prototype.getResource = function getResource(code, ns, key) {
      var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
      return this.resourceStore.getResource(code, ns, key, options);
    };
    return Translator;
  }(_EventEmitter3.default);
  exports.default = Translator;
  return module.exports;
});

$__System.registerDynamic("34", ["31"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  var LanguageUtil = function() {
    function LanguageUtil(options) {
      _classCallCheck(this, LanguageUtil);
      this.options = options;
      this.whitelist = this.options.whitelist || false;
      this.logger = _logger2.default.create('languageUtils');
    }
    LanguageUtil.prototype.getLanguagePartFromCode = function getLanguagePartFromCode(code) {
      if (code.indexOf('-') < 0)
        return code;
      var specialCases = ['NB-NO', 'NN-NO', 'nb-NO', 'nn-NO', 'nb-no', 'nn-no'];
      var p = code.split('-');
      return this.formatLanguageCode(specialCases.indexOf(code) > -1 ? p[1].toLowerCase() : p[0]);
    };
    LanguageUtil.prototype.formatLanguageCode = function formatLanguageCode(code) {
      if (typeof code === 'string' && code.indexOf('-') > -1) {
        var specialCases = ['hans', 'hant', 'latn', 'cyrl', 'cans', 'mong', 'arab'];
        var p = code.split('-');
        if (this.options.lowerCaseLng) {
          p = p.map(function(part) {
            return part.toLowerCase();
          });
        } else if (p.length === 2) {
          p[0] = p[0].toLowerCase();
          p[1] = p[1].toUpperCase();
          if (specialCases.indexOf(p[1].toLowerCase()) > -1)
            p[1] = capitalize(p[1].toLowerCase());
        } else if (p.length === 3) {
          p[0] = p[0].toLowerCase();
          if (p[1].length === 2)
            p[1] = p[1].toUpperCase();
          if (p[0] !== 'sgn' && p[2].length === 2)
            p[2] = p[2].toUpperCase();
          if (specialCases.indexOf(p[1].toLowerCase()) > -1)
            p[1] = capitalize(p[1].toLowerCase());
          if (specialCases.indexOf(p[2].toLowerCase()) > -1)
            p[2] = capitalize(p[2].toLowerCase());
        }
        return p.join('-');
      } else {
        return this.options.cleanCode || this.options.lowerCaseLng ? code.toLowerCase() : code;
      }
    };
    LanguageUtil.prototype.isWhitelisted = function isWhitelisted(code) {
      if (this.options.load === 'languageOnly')
        code = this.getLanguagePartFromCode(code);
      return !this.whitelist || !this.whitelist.length || this.whitelist.indexOf(code) > -1 ? true : false;
    };
    LanguageUtil.prototype.toResolveHierarchy = function toResolveHierarchy(code, fallbackCode) {
      var _this = this;
      fallbackCode = fallbackCode || this.options.fallbackLng || [];
      if (typeof fallbackCode === 'string')
        fallbackCode = [fallbackCode];
      var codes = [];
      var addCode = function addCode(code) {
        if (_this.isWhitelisted(code)) {
          codes.push(code);
        } else {
          _this.logger.warn('rejecting non-whitelisted language code: ' + code);
        }
      };
      if (typeof code === 'string' && code.indexOf('-') > -1) {
        if (this.options.load !== 'languageOnly')
          addCode(this.formatLanguageCode(code));
        if (this.options.load !== 'currentOnly')
          addCode(this.getLanguagePartFromCode(code));
      } else if (typeof code === 'string') {
        addCode(this.formatLanguageCode(code));
      }
      fallbackCode.forEach(function(fc) {
        if (codes.indexOf(fc) < 0)
          addCode(_this.formatLanguageCode(fc));
      });
      return codes;
    };
    return LanguageUtil;
  }();
  ;
  exports.default = LanguageUtil;
  return module.exports;
});

$__System.registerDynamic("35", ["31"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  var sets = [{
    lngs: ['ach', 'ak', 'am', 'arn', 'br', 'fil', 'gun', 'ln', 'mfe', 'mg', 'mi', 'oc', 'tg', 'ti', 'tr', 'uz', 'wa'],
    nr: [1, 2],
    fc: 1
  }, {
    lngs: ['af', 'an', 'ast', 'az', 'bg', 'bn', 'ca', 'da', 'de', 'dev', 'el', 'en', 'eo', 'es', 'es_ar', 'et', 'eu', 'fi', 'fo', 'fur', 'fy', 'gl', 'gu', 'ha', 'he', 'hi', 'hu', 'hy', 'ia', 'it', 'kn', 'ku', 'lb', 'mai', 'ml', 'mn', 'mr', 'nah', 'nap', 'nb', 'ne', 'nl', 'nn', 'no', 'nso', 'pa', 'pap', 'pms', 'ps', 'pt', 'pt_br', 'rm', 'sco', 'se', 'si', 'so', 'son', 'sq', 'sv', 'sw', 'ta', 'te', 'tk', 'ur', 'yo'],
    nr: [1, 2],
    fc: 2
  }, {
    lngs: ['ay', 'bo', 'cgg', 'fa', 'id', 'ja', 'jbo', 'ka', 'kk', 'km', 'ko', 'ky', 'lo', 'ms', 'sah', 'su', 'th', 'tt', 'ug', 'vi', 'wo', 'zh'],
    nr: [1],
    fc: 3
  }, {
    lngs: ['be', 'bs', 'dz', 'hr', 'ru', 'sr', 'uk'],
    nr: [1, 2, 5],
    fc: 4
  }, {
    lngs: ['ar'],
    nr: [0, 1, 2, 3, 11, 100],
    fc: 5
  }, {
    lngs: ['cs', 'sk'],
    nr: [1, 2, 5],
    fc: 6
  }, {
    lngs: ['csb', 'pl'],
    nr: [1, 2, 5],
    fc: 7
  }, {
    lngs: ['cy'],
    nr: [1, 2, 3, 8],
    fc: 8
  }, {
    lngs: ['fr'],
    nr: [1, 2],
    fc: 9
  }, {
    lngs: ['ga'],
    nr: [1, 2, 3, 7, 11],
    fc: 10
  }, {
    lngs: ['gd'],
    nr: [1, 2, 3, 20],
    fc: 11
  }, {
    lngs: ['is'],
    nr: [1, 2],
    fc: 12
  }, {
    lngs: ['jv'],
    nr: [0, 1],
    fc: 13
  }, {
    lngs: ['kw'],
    nr: [1, 2, 3, 4],
    fc: 14
  }, {
    lngs: ['lt'],
    nr: [1, 2, 10],
    fc: 15
  }, {
    lngs: ['lv'],
    nr: [1, 2, 0],
    fc: 16
  }, {
    lngs: ['mk'],
    nr: [1, 2],
    fc: 17
  }, {
    lngs: ['mnk'],
    nr: [0, 1, 2],
    fc: 18
  }, {
    lngs: ['mt'],
    nr: [1, 2, 11, 20],
    fc: 19
  }, {
    lngs: ['or'],
    nr: [2, 1],
    fc: 2
  }, {
    lngs: ['ro'],
    nr: [1, 2, 20],
    fc: 20
  }, {
    lngs: ['sl'],
    nr: [5, 1, 2, 3],
    fc: 21
  }];
  var _rulesPluralsTypes = {
    1: function _(n) {
      return Number(n > 1);
    },
    2: function _(n) {
      return Number(n != 1);
    },
    3: function _(n) {
      return 0;
    },
    4: function _(n) {
      return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
    },
    5: function _(n) {
      return Number(n === 0 ? 0 : n == 1 ? 1 : n == 2 ? 2 : n % 100 >= 3 && n % 100 <= 10 ? 3 : n % 100 >= 11 ? 4 : 5);
    },
    6: function _(n) {
      return Number(n == 1 ? 0 : n >= 2 && n <= 4 ? 1 : 2);
    },
    7: function _(n) {
      return Number(n == 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
    },
    8: function _(n) {
      return Number(n == 1 ? 0 : n == 2 ? 1 : n != 8 && n != 11 ? 2 : 3);
    },
    9: function _(n) {
      return Number(n >= 2);
    },
    10: function _(n) {
      return Number(n == 1 ? 0 : n == 2 ? 1 : n < 7 ? 2 : n < 11 ? 3 : 4);
    },
    11: function _(n) {
      return Number(n == 1 || n == 11 ? 0 : n == 2 || n == 12 ? 1 : n > 2 && n < 20 ? 2 : 3);
    },
    12: function _(n) {
      return Number(n % 10 != 1 || n % 100 == 11);
    },
    13: function _(n) {
      return Number(n !== 0);
    },
    14: function _(n) {
      return Number(n == 1 ? 0 : n == 2 ? 1 : n == 3 ? 2 : 3);
    },
    15: function _(n) {
      return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
    },
    16: function _(n) {
      return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n !== 0 ? 1 : 2);
    },
    17: function _(n) {
      return Number(n == 1 || n % 10 == 1 ? 0 : 1);
    },
    18: function _(n) {
      return Number(n == 0 ? 0 : n == 1 ? 1 : 2);
    },
    19: function _(n) {
      return Number(n == 1 ? 0 : n === 0 || n % 100 > 1 && n % 100 < 11 ? 1 : n % 100 > 10 && n % 100 < 20 ? 2 : 3);
    },
    20: function _(n) {
      return Number(n == 1 ? 0 : n === 0 || n % 100 > 0 && n % 100 < 20 ? 1 : 2);
    },
    21: function _(n) {
      return Number(n % 100 == 1 ? 1 : n % 100 == 2 ? 2 : n % 100 == 3 || n % 100 == 4 ? 3 : 0);
    }
  };
  function createRules() {
    var l,
        rules = {};
    sets.forEach(function(set) {
      set.lngs.forEach(function(l) {
        return rules[l] = {
          numbers: set.nr,
          plurals: _rulesPluralsTypes[set.fc]
        };
      });
    });
    return rules;
  }
  var PluralResolver = function() {
    function PluralResolver(languageUtils) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      _classCallCheck(this, PluralResolver);
      this.languageUtils = languageUtils;
      this.options = options;
      this.logger = _logger2.default.create('pluralResolver');
      this.rules = createRules();
    }
    PluralResolver.prototype.addRule = function addRule(lng, obj) {
      this.rules[lng] = obj;
    };
    PluralResolver.prototype.getRule = function getRule(code) {
      return this.rules[this.languageUtils.getLanguagePartFromCode(code)];
    };
    PluralResolver.prototype.needsPlural = function needsPlural(code) {
      var rule = this.getRule(code);
      return rule && rule.numbers.length <= 1 ? false : true;
    };
    PluralResolver.prototype.getSuffix = function getSuffix(code, count) {
      var rule = this.getRule(code);
      if (rule) {
        if (rule.numbers.length === 1)
          return '';
        var idx = rule.noAbs ? rule.plurals(count) : rule.plurals(Math.abs(count));
        var suffix = rule.numbers[idx];
        if (rule.numbers.length === 2 && rule.numbers[0] === 1) {
          if (suffix === 2) {
            suffix = 'plural';
          } else if (suffix === 1) {
            suffix = '';
          }
        }
        if (this.options.compatibilityJSON === 'v1') {
          if (suffix === 1)
            return '';
          if (typeof suffix === 'number')
            return '_plural_' + suffix.toString();
        }
        return this.options.prepend && suffix.toString() ? this.options.prepend + suffix.toString() : suffix.toString();
      } else {
        this.logger.warn('no plural rule found for: ' + code);
        return '';
      }
    };
    return PluralResolver;
  }();
  ;
  exports.default = PluralResolver;
  return module.exports;
});

$__System.registerDynamic("36", ["2f", "31"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _utils = $__require('2f');
  var utils = _interopRequireWildcard(_utils);
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    } else {
      var newObj = {};
      if (obj != null) {
        for (var key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key))
            newObj[key] = obj[key];
        }
      }
      newObj.default = obj;
      return newObj;
    }
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  var Interpolator = function() {
    function Interpolator() {
      var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      _classCallCheck(this, Interpolator);
      this.logger = _logger2.default.create('interpolator');
      this.init(options, true);
    }
    Interpolator.prototype.init = function init() {
      var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      var reset = arguments[1];
      if (reset)
        this.options = options;
      if (!options.interpolation)
        options.interpolation = {escapeValue: true};
      var iOpts = options.interpolation;
      this.escapeValue = iOpts.escapeValue;
      this.prefix = iOpts.prefix ? utils.regexEscape(iOpts.prefix) : iOpts.prefixEscaped || '{{';
      this.suffix = iOpts.suffix ? utils.regexEscape(iOpts.suffix) : iOpts.suffixEscaped || '}}';
      this.unescapePrefix = iOpts.unescapeSuffix ? '' : iOpts.unescapePrefix || '-';
      this.unescapeSuffix = this.unescapePrefix ? '' : iOpts.unescapeSuffix || '';
      this.nestingPrefix = iOpts.nestingPrefix ? utils.regexEscape(iOpts.nestingPrefix) : iOpts.nestingPrefixEscaped || utils.regexEscape('$t(');
      this.nestingSuffix = iOpts.nestingSuffix ? utils.regexEscape(iOpts.nestingSuffix) : iOpts.nestingSuffixEscaped || utils.regexEscape(')');
      var regexpStr = this.prefix + '(.+?)' + this.suffix;
      this.regexp = new RegExp(regexpStr, 'g');
      var regexpUnescapeStr = this.prefix + this.unescapePrefix + '(.+?)' + this.unescapeSuffix + this.suffix;
      this.regexpUnescape = new RegExp(regexpUnescapeStr, 'g');
      var nestingRegexpStr = this.nestingPrefix + '(.+?)' + this.nestingSuffix;
      this.nestingRegexp = new RegExp(nestingRegexpStr, 'g');
    };
    Interpolator.prototype.reset = function reset() {
      if (this.options)
        this.init(this.options);
    };
    Interpolator.prototype.interpolate = function interpolate(str, data) {
      var match = void 0,
          value = void 0;
      function regexSafe(val) {
        return val.replace(/\$/g, '$$$$');
      }
      while (match = this.regexpUnescape.exec(str)) {
        var _value = utils.getPath(data, match[1].trim());
        str = str.replace(match[0], _value);
      }
      while (match = this.regexp.exec(str)) {
        value = utils.getPath(data, match[1].trim());
        if (typeof value !== 'string')
          value = utils.makeString(value);
        if (!value) {
          this.logger.warn('missed to pass in variable ' + match[1] + ' for interpolating ' + str);
          value = '';
        }
        value = this.escapeValue ? regexSafe(utils.escape(value)) : regexSafe(value);
        str = str.replace(match[0], value);
        this.regexp.lastIndex = 0;
      }
      return str;
    };
    Interpolator.prototype.nest = function nest(str, fc) {
      var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];
      var match = void 0,
          value = void 0;
      var clonedOptions = JSON.parse(JSON.stringify(options));
      clonedOptions.applyPostProcessor = false;
      function regexSafe(val) {
        return val.replace(/\$/g, '$$$$');
      }
      function handleHasOptions(key) {
        if (key.indexOf(',') < 0)
          return key;
        var p = key.split(',');
        key = p.shift();
        var optionsString = p.join(',');
        optionsString = this.interpolate(optionsString, clonedOptions);
        try {
          clonedOptions = JSON.parse(optionsString);
        } catch (e) {
          this.logger.error('failed parsing options string in nesting for key ' + key, e);
        }
        return key;
      }
      while (match = this.nestingRegexp.exec(str)) {
        value = fc(handleHasOptions.call(this, match[1].trim()), clonedOptions);
        if (typeof value !== 'string')
          value = utils.makeString(value);
        if (!value) {
          this.logger.warn('missed to pass in variable ' + match[1] + ' for interpolating ' + str);
          value = '';
        }
        value = this.escapeValue ? regexSafe(utils.escape(value)) : regexSafe(value);
        str = str.replace(match[0], value);
        this.regexp.lastIndex = 0;
      }
      return str;
    };
    return Interpolator;
  }();
  exports.default = Interpolator;
  return module.exports;
});

$__System.registerDynamic("37", ["2f", "31", "2e"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  var _slicedToArray = function() {
    function sliceIterator(arr, i) {
      var _arr = [];
      var _n = true;
      var _d = false;
      var _e = undefined;
      try {
        for (var _i = arr[Symbol.iterator](),
            _s; !(_n = (_s = _i.next()).done); _n = true) {
          _arr.push(_s.value);
          if (i && _arr.length === i)
            break;
        }
      } catch (err) {
        _d = true;
        _e = err;
      } finally {
        try {
          if (!_n && _i["return"])
            _i["return"]();
        } finally {
          if (_d)
            throw _e;
        }
      }
      return _arr;
    }
    return function(arr, i) {
      if (Array.isArray(arr)) {
        return arr;
      } else if (Symbol.iterator in Object(arr)) {
        return sliceIterator(arr, i);
      } else {
        throw new TypeError("Invalid attempt to destructure non-iterable instance");
      }
    };
  }();
  var _utils = $__require('2f');
  var utils = _interopRequireWildcard(_utils);
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  var _EventEmitter2 = $__require('2e');
  var _EventEmitter3 = _interopRequireDefault(_EventEmitter2);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    } else {
      var newObj = {};
      if (obj != null) {
        for (var key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key))
            newObj[key] = obj[key];
        }
      }
      newObj.default = obj;
      return newObj;
    }
  }
  function _defaults(obj, defaults) {
    var keys = Object.getOwnPropertyNames(defaults);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var value = Object.getOwnPropertyDescriptor(defaults, key);
      if (value && value.configurable && obj[key] === undefined) {
        Object.defineProperty(obj, key, value);
      }
    }
    return obj;
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }
  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass);
  }
  function remove(arr, what) {
    var found = arr.indexOf(what);
    while (found !== -1) {
      arr.splice(found, 1);
      found = arr.indexOf(what);
    }
  }
  var Connector = function(_EventEmitter) {
    _inherits(Connector, _EventEmitter);
    function Connector(backend, store, services) {
      var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
      _classCallCheck(this, Connector);
      var _this = _possibleConstructorReturn(this, _EventEmitter.call(this));
      _this.backend = backend;
      _this.store = store;
      _this.services = services;
      _this.options = options;
      _this.logger = _logger2.default.create('backendConnector');
      _this.state = {};
      _this.queue = [];
      _this.backend && _this.backend.init && _this.backend.init(services, options.backend, options);
      return _this;
    }
    Connector.prototype.queueLoad = function queueLoad(languages, namespaces, callback) {
      var _this2 = this;
      var toLoad = [],
          pending = [],
          toLoadLanguages = [],
          toLoadNamespaces = [];
      languages.forEach(function(lng) {
        var hasAllNamespaces = true;
        namespaces.forEach(function(ns) {
          var name = lng + '|' + ns;
          if (_this2.store.hasResourceBundle(lng, ns)) {
            _this2.state[name] = 2;
          } else if (_this2.state[name] < 0) {} else if (_this2.state[name] === 1) {
            if (pending.indexOf(name) < 0)
              pending.push(name);
          } else {
            _this2.state[name] = 1;
            hasAllNamespaces = false;
            if (pending.indexOf(name) < 0)
              pending.push(name);
            if (toLoad.indexOf(name) < 0)
              toLoad.push(name);
            if (toLoadNamespaces.indexOf(ns) < 0)
              toLoadNamespaces.push(ns);
          }
        });
        if (!hasAllNamespaces)
          toLoadLanguages.push(lng);
      });
      if (toLoad.length || pending.length) {
        this.queue.push({
          pending: pending,
          loaded: {},
          errors: [],
          callback: callback
        });
      }
      return {
        toLoad: toLoad,
        pending: pending,
        toLoadLanguages: toLoadLanguages,
        toLoadNamespaces: toLoadNamespaces
      };
    };
    Connector.prototype.loaded = function loaded(name, err, data) {
      var _this3 = this;
      var _name$split = name.split('|');
      var _name$split2 = _slicedToArray(_name$split, 2);
      var lng = _name$split2[0];
      var ns = _name$split2[1];
      if (err)
        this.emit('failedLoading', lng, ns, err);
      if (data) {
        this.store.addResourceBundle(lng, ns, data);
      }
      this.state[name] = err ? -1 : 2;
      this.queue.forEach(function(q) {
        utils.pushPath(q.loaded, [lng], ns);
        remove(q.pending, name);
        if (err)
          q.errors.push(err);
        if (q.pending.length === 0 && !q.done) {
          q.errors.length ? q.callback(q.errors) : q.callback();
          _this3.emit('loaded', q.loaded);
          q.done = true;
        }
      });
      this.queue = this.queue.filter(function(q) {
        return !q.done;
      });
    };
    Connector.prototype.read = function read(lng, ns, fcName, tried, wait, callback) {
      var _this4 = this;
      if (!tried)
        tried = 0;
      if (!wait)
        wait = 250;
      if (!lng.length)
        return callback(null, {});
      this.backend[fcName](lng, ns, function(err, data) {
        if (err && data && tried < 5) {
          setTimeout(function() {
            _this4.read.call(_this4, lng, ns, fcName, ++tried, wait * 2, callback);
          }, wait);
          return;
        }
        callback(err, data);
      });
    };
    Connector.prototype.load = function load(languages, namespaces, callback) {
      var _this5 = this;
      if (!this.backend) {
        this.logger.warn('No backend was added via i18next.use. Will not load resources.');
        return callback && callback();
      }
      var options = _extends({}, this.backend.options, this.options.backend);
      if (typeof languages === 'string')
        languages = this.services.languageUtils.toResolveHierarchy(languages);
      if (typeof namespaces === 'string')
        namespaces = [namespaces];
      var toLoad = this.queueLoad(languages, namespaces, callback);
      if (!toLoad.toLoad.length) {
        if (!toLoad.pending.length)
          callback();
        return;
      }
      if (options.allowMultiLoading && this.backend.readMulti) {
        this.read(toLoad.toLoadLanguages, toLoad.toLoadNamespaces, 'readMulti', null, null, function(err, data) {
          if (err)
            _this5.logger.warn('loading namespaces ' + toLoad.toLoadNamespaces.join(', ') + ' for languages ' + toLoad.toLoadLanguages.join(', ') + ' via multiloading failed', err);
          if (!err && data)
            _this5.logger.log('loaded namespaces ' + toLoad.toLoadNamespaces.join(', ') + ' for languages ' + toLoad.toLoadLanguages.join(', ') + ' via multiloading', data);
          toLoad.toLoad.forEach(function(name) {
            var _name$split3 = name.split('|');
            var _name$split4 = _slicedToArray(_name$split3, 2);
            var l = _name$split4[0];
            var n = _name$split4[1];
            var bundle = utils.getPath(data, [l, n]);
            if (bundle) {
              _this5.loaded(name, err, bundle);
            } else {
              var _err = 'loading namespace ' + n + ' for language ' + l + ' via multiloading failed';
              _this5.loaded(name, _err);
              _this5.logger.error(_err);
            }
          });
        });
      } else {
        (function() {
          var read = function read(name) {
            var _this6 = this;
            var _name$split5 = name.split('|');
            var _name$split6 = _slicedToArray(_name$split5, 2);
            var lng = _name$split6[0];
            var ns = _name$split6[1];
            this.read(lng, ns, 'read', null, null, function(err, data) {
              if (err)
                _this6.logger.warn('loading namespace ' + ns + ' for language ' + lng + ' failed', err);
              if (!err && data)
                _this6.logger.log('loaded namespace ' + ns + ' for language ' + lng, data);
              _this6.loaded(name, err, data);
            });
          };
          ;
          toLoad.toLoad.forEach(function(name) {
            read.call(_this5, name);
          });
        })();
      }
    };
    Connector.prototype.saveMissing = function saveMissing(languages, namespace, key, fallbackValue) {
      if (this.backend && this.backend.create)
        this.backend.create(languages, namespace, key, fallbackValue);
      this.store.addResource(languages[0], namespace, key, fallbackValue);
    };
    return Connector;
  }(_EventEmitter3.default);
  exports.default = Connector;
  return module.exports;
});

$__System.registerDynamic("2f", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  exports.makeString = makeString;
  exports.copy = copy;
  exports.setPath = setPath;
  exports.pushPath = pushPath;
  exports.getPath = getPath;
  exports.deepExtend = deepExtend;
  exports.regexEscape = regexEscape;
  exports.escape = escape;
  function makeString(object) {
    if (object == null)
      return '';
    return '' + object;
  }
  function copy(a, s, t) {
    a.forEach(function(m) {
      if (s[m])
        t[m] = s[m];
    });
  }
  function getLastOfPath(object, path, Empty) {
    function cleanKey(key) {
      return key && key.indexOf('###') > -1 ? key.replace(/###/g, '.') : key;
    }
    var stack = typeof path !== 'string' ? [].concat(path) : path.split('.');
    while (stack.length > 1) {
      if (!object)
        return {};
      var key = cleanKey(stack.shift());
      if (!object[key] && Empty)
        object[key] = new Empty();
      object = object[key];
    }
    if (!object)
      return {};
    return {
      obj: object,
      k: cleanKey(stack.shift())
    };
  }
  function setPath(object, path, newValue) {
    var _getLastOfPath = getLastOfPath(object, path, Object);
    var obj = _getLastOfPath.obj;
    var k = _getLastOfPath.k;
    obj[k] = newValue;
  }
  function pushPath(object, path, newValue, concat) {
    var _getLastOfPath2 = getLastOfPath(object, path, Object);
    var obj = _getLastOfPath2.obj;
    var k = _getLastOfPath2.k;
    obj[k] = obj[k] || [];
    if (concat)
      obj[k] = obj[k].concat(newValue);
    if (!concat)
      obj[k].push(newValue);
  }
  function getPath(object, path) {
    var _getLastOfPath3 = getLastOfPath(object, path);
    var obj = _getLastOfPath3.obj;
    var k = _getLastOfPath3.k;
    if (!obj)
      return undefined;
    return obj[k];
  }
  function deepExtend(target, source, overwrite) {
    for (var prop in source) {
      if (prop in target) {
        if (typeof target[prop] === 'string' || target[prop] instanceof String || typeof source[prop] === 'string' || source[prop] instanceof String) {
          if (overwrite)
            target[prop] = source[prop];
        } else {
          deepExtend(target[prop], source[prop], overwrite);
        }
      } else {
        target[prop] = source[prop];
      }
    }
    return target;
  }
  function regexEscape(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
  }
  var _entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };
  function escape(data) {
    if (typeof data === 'string') {
      return data.replace(/[&<>"'\/]/g, function(s) {
        return _entityMap[s];
      });
    } else {
      return data;
    }
  }
  return module.exports;
});

$__System.registerDynamic("2e", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  var EventEmitter = function() {
    function EventEmitter() {
      _classCallCheck(this, EventEmitter);
      this.observers = {};
    }
    EventEmitter.prototype.on = function on(events, listener) {
      var _this = this;
      events.split(' ').forEach(function(event) {
        _this.observers[event] = _this.observers[event] || [];
        _this.observers[event].push(listener);
      });
    };
    EventEmitter.prototype.off = function off(event, listener) {
      var _this2 = this;
      if (!this.observers[event]) {
        return;
      }
      this.observers[event].forEach(function() {
        if (!listener) {
          delete _this2.observers[event];
        } else {
          var index = _this2.observers[event].indexOf(listener);
          if (index > -1) {
            _this2.observers[event].splice(index, 1);
          }
        }
      });
    };
    EventEmitter.prototype.emit = function emit(event) {
      for (var _len = arguments.length,
          args = Array(_len > 1 ? _len - 1 : 0),
          _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      if (this.observers[event]) {
        this.observers[event].forEach(function(observer) {
          observer.apply(undefined, args);
        });
      }
      if (this.observers['*']) {
        this.observers['*'].forEach(function(observer) {
          var _ref;
          observer.apply(observer, (_ref = [event]).concat.apply(_ref, args));
        });
      }
    };
    return EventEmitter;
  }();
  exports.default = EventEmitter;
  return module.exports;
});

$__System.registerDynamic("38", ["2f", "31", "2e"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  var _utils = $__require('2f');
  var utils = _interopRequireWildcard(_utils);
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  var _EventEmitter2 = $__require('2e');
  var _EventEmitter3 = _interopRequireDefault(_EventEmitter2);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    } else {
      var newObj = {};
      if (obj != null) {
        for (var key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key))
            newObj[key] = obj[key];
        }
      }
      newObj.default = obj;
      return newObj;
    }
  }
  function _defaults(obj, defaults) {
    var keys = Object.getOwnPropertyNames(defaults);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var value = Object.getOwnPropertyDescriptor(defaults, key);
      if (value && value.configurable && obj[key] === undefined) {
        Object.defineProperty(obj, key, value);
      }
    }
    return obj;
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }
  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass);
  }
  var Connector = function(_EventEmitter) {
    _inherits(Connector, _EventEmitter);
    function Connector(cache, store, services) {
      var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
      _classCallCheck(this, Connector);
      var _this = _possibleConstructorReturn(this, _EventEmitter.call(this));
      _this.cache = cache;
      _this.store = store;
      _this.services = services;
      _this.options = options;
      _this.logger = _logger2.default.create('cacheConnector');
      _this.cache && _this.cache.init && _this.cache.init(services, options.cache, options);
      return _this;
    }
    Connector.prototype.load = function load(languages, namespaces, callback) {
      var _this2 = this;
      if (!this.cache)
        return callback && callback();
      var options = _extends({}, this.cache.options, this.options.cache);
      if (typeof languages === 'string')
        languages = this.services.languageUtils.toResolveHierarchy(languages);
      if (typeof namespaces === 'string')
        namespaces = [namespaces];
      if (options.enabled) {
        this.cache.load(languages, function(err, data) {
          if (err)
            _this2.logger.error('loading languages ' + languages.join(', ') + ' from cache failed', err);
          if (data) {
            for (var l in data) {
              for (var n in data[l]) {
                if (n === 'i18nStamp')
                  continue;
                var bundle = data[l][n];
                if (bundle)
                  _this2.store.addResourceBundle(l, n, bundle);
              }
            }
          }
          if (callback)
            callback();
        });
      } else {
        if (callback)
          callback();
      }
    };
    Connector.prototype.save = function save() {
      if (this.cache && this.options.cache && this.options.cache.enabled)
        this.cache.save(this.store.data);
    };
    return Connector;
  }(_EventEmitter3.default);
  exports.default = Connector;
  return module.exports;
});

$__System.registerDynamic("39", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  exports.get = get;
  exports.transformOptions = transformOptions;
  function get() {
    return {
      debug: false,
      initImmediate: true,
      ns: ['translation'],
      defaultNS: ['translation'],
      fallbackLng: ['dev'],
      fallbackNS: false,
      whitelist: false,
      load: 'all',
      preload: false,
      keySeparator: '.',
      nsSeparator: ':',
      pluralSeparator: '_',
      contextSeparator: '_',
      saveMissing: false,
      saveMissingTo: 'fallback',
      missingKeyHandler: false,
      postProcess: false,
      returnNull: true,
      returnEmptyString: true,
      returnObjects: false,
      joinArrays: false,
      returnedObjectHandler: function returnedObjectHandler() {},
      parseMissingKeyHandler: false,
      appendNamespaceToMissingKey: false,
      overloadTranslationOptionHandler: function overloadTranslationOptionHandler(args) {
        return {defaultValue: args[1]};
      },
      interpolation: {
        escapeValue: true,
        prefix: '{{',
        suffix: '}}',
        unescapePrefix: '-',
        nestingPrefix: '$t(',
        nestingSuffix: ')',
        defaultVariables: undefined
      }
    };
  }
  function transformOptions(options) {
    if (typeof options.ns === 'string')
      options.ns = [options.ns];
    if (typeof options.fallbackLng === 'string')
      options.fallbackLng = [options.fallbackLng];
    if (typeof options.fallbackNS === 'string')
      options.fallbackNS = [options.fallbackNS];
    if (options.whitelist && options.whitelist.indexOf('cimode') < 0)
      options.whitelist.push('cimode');
    return options;
  }
  return module.exports;
});

$__System.registerDynamic("32", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  exports.default = {
    processors: {},
    addPostProcessor: function addPostProcessor(module) {
      this.processors[module.name] = module;
    },
    handle: function handle(processors, value, key, options, translator) {
      var _this = this;
      processors.forEach(function(processor) {
        if (_this.processors[processor])
          value = _this.processors[processor].process(value, key, options, translator);
      });
      return value;
    }
  };
  return module.exports;
});

$__System.registerDynamic("31", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  var consoleLogger = {
    type: 'logger',
    log: function log(args) {
      this._output('log', args);
    },
    warn: function warn(args) {
      this._output('warn', args);
    },
    error: function error(args) {
      this._output('error', args);
    },
    _output: function _output(type, args) {
      if (console && console[type])
        console[type].apply(console, Array.prototype.slice.call(args));
    }
  };
  var Logger = function() {
    function Logger(concreteLogger) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      _classCallCheck(this, Logger);
      this.subs = [];
      this.init(concreteLogger, options);
    }
    Logger.prototype.init = function init(concreteLogger) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      this.prefix = options.prefix || 'i18next:';
      this.logger = concreteLogger || consoleLogger;
      this.options = options;
      this.debug = options.debug === false ? false : true;
    };
    Logger.prototype.setDebug = function setDebug(bool) {
      this.debug = bool;
      this.subs.forEach(function(sub) {
        sub.setDebug(bool);
      });
    };
    Logger.prototype.log = function log() {
      this.forward(arguments, 'log', '', true);
    };
    Logger.prototype.warn = function warn() {
      this.forward(arguments, 'warn', '', true);
    };
    Logger.prototype.error = function error() {
      this.forward(arguments, 'error', '');
    };
    Logger.prototype.deprecate = function deprecate() {
      this.forward(arguments, 'warn', 'WARNING DEPRECATED: ', true);
    };
    Logger.prototype.forward = function forward(args, lvl, prefix, debugOnly) {
      if (debugOnly && !this.debug)
        return;
      if (typeof args[0] === 'string')
        args[0] = prefix + this.prefix + ' ' + args[0];
      this.logger[lvl](args);
    };
    Logger.prototype.create = function create(moduleName) {
      var sub = new Logger(this.logger, _extends({prefix: this.prefix + ':' + moduleName + ':'}, this.options));
      this.subs.push(sub);
      return sub;
    };
    return Logger;
  }();
  ;
  exports.default = new Logger();
  return module.exports;
});

$__System.registerDynamic("33", ["31"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  exports.convertAPIOptions = convertAPIOptions;
  exports.convertJSONOptions = convertJSONOptions;
  exports.convertTOptions = convertTOptions;
  exports.appendBackwardsAPI = appendBackwardsAPI;
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function convertInterpolation(options) {
    options.interpolation = {unescapeSuffix: 'HTML'};
    options.interpolation.prefix = options.interpolationPrefix || '__';
    options.interpolation.suffix = options.interpolationSuffix || '__';
    options.interpolation.escapeValue = options.escapeInterpolation || false;
    options.interpolation.nestingPrefix = options.reusePrefix || '$t(';
    options.interpolation.nestingSuffix = options.reuseSuffix || ')';
    return options;
  }
  function convertAPIOptions(options) {
    if (options.resStore)
      options.resources = options.resStore;
    if (options.ns && options.ns.defaultNs) {
      options.defaultNS = options.ns.defaultNs;
      options.ns = options.ns.namespaces;
    } else {
      options.defaultNS = options.ns || 'translation';
    }
    if (options.fallbackToDefaultNS && options.defaultNS)
      options.fallbackNS = options.defaultNS;
    options.saveMissing = options.sendMissing;
    options.saveMissingTo = options.sendMissingTo || 'current';
    options.returnNull = options.fallbackOnNull ? false : true;
    options.returnEmptyString = options.fallbackOnEmpty ? false : true;
    options.returnObjects = options.returnObjectTrees;
    options.joinArrays = '\n';
    options.returnedObjectHandler = options.objectTreeKeyHandler;
    options.parseMissingKeyHandler = options.parseMissingKey;
    options.appendNamespaceToMissingKey = true;
    options.nsSeparator = options.nsseparator;
    options.keySeparator = options.keyseparator;
    if (options.shortcutFunction === 'sprintf') {
      options.overloadTranslationOptionHandler = function(args) {
        var values = [];
        for (var i = 1; i < args.length; i++) {
          values.push(args[i]);
        }
        return {
          postProcess: 'sprintf',
          sprintf: values
        };
      };
    }
    options.whitelist = options.lngWhitelist;
    options.preload = options.preload;
    if (options.load === 'current')
      options.load = 'currentOnly';
    if (options.load === 'unspecific')
      options.load = 'languageOnly';
    options.backend = options.backend || {};
    options.backend.loadPath = options.resGetPath || 'locales/__lng__/__ns__.json';
    options.backend.addPath = options.resPostPath || 'locales/add/__lng__/__ns__';
    options.backend.allowMultiLoading = options.dynamicLoad;
    options.cache = options.cache || {};
    options.cache.prefix = 'res_';
    options.cache.expirationTime = 7 * 24 * 60 * 60 * 1000;
    options.cache.enabled = options.useLocalStorage ? true : false;
    options = convertInterpolation(options);
    if (options.defaultVariables)
      options.interpolation.defaultVariables = options.defaultVariables;
    return options;
  }
  function convertJSONOptions(options) {
    options = convertInterpolation(options);
    options.joinArrays = '\n';
    return options;
  }
  function convertTOptions(options) {
    if (options.interpolationPrefix || options.interpolationSuffix || options.escapeInterpolation) {
      options = convertInterpolation(options);
    }
    options.nsSeparator = options.nsseparator;
    options.keySeparator = options.keyseparator;
    options.returnObjects = options.returnObjectTrees;
    return options;
  }
  function appendBackwardsAPI(i18n) {
    i18n.lng = function() {
      _logger2.default.deprecate('i18next.lng() can be replaced by i18next.language for detected language or i18next.languages for languages ordered by translation lookup.');
      return i18n.services.languageUtils.toResolveHierarchy(i18n.language)[0];
    };
    i18n.preload = function(lngs, cb) {
      _logger2.default.deprecate('i18next.preload() can be replaced with i18next.loadLanguages()');
      i18n.loadLanguages(lngs, cb);
    };
    i18n.setLng = function(lng, options, callback) {
      _logger2.default.deprecate('i18next.setLng() can be replaced with i18next.changeLanguage() or i18next.getFixedT() to get a translation function with fixed language or namespace.');
      if (typeof options === 'function') {
        callback = options;
        options = {};
      }
      if (!options)
        options = {};
      if (options.fixLng === true) {
        if (callback)
          return callback(null, i18n.getFixedT(lng));
      }
      i18n.changeLanguage(lng, callback);
    };
    i18n.addPostProcessor = function(name, fc) {
      _logger2.default.deprecate('i18next.addPostProcessor() can be replaced by i18next.use({ type: \'postProcessor\', name: \'name\', process: fc })');
      i18n.use({
        type: 'postProcessor',
        name: name,
        process: fc
      });
    };
  }
  return module.exports;
});

$__System.registerDynamic("3a", ["31", "2e", "2d", "30", "34", "35", "36", "37", "38", "39", "32", "33"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) {
    return typeof obj;
  } : function(obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
  };
  var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  var _logger = $__require('31');
  var _logger2 = _interopRequireDefault(_logger);
  var _EventEmitter2 = $__require('2e');
  var _EventEmitter3 = _interopRequireDefault(_EventEmitter2);
  var _ResourceStore = $__require('2d');
  var _ResourceStore2 = _interopRequireDefault(_ResourceStore);
  var _Translator = $__require('30');
  var _Translator2 = _interopRequireDefault(_Translator);
  var _LanguageUtils = $__require('34');
  var _LanguageUtils2 = _interopRequireDefault(_LanguageUtils);
  var _PluralResolver = $__require('35');
  var _PluralResolver2 = _interopRequireDefault(_PluralResolver);
  var _Interpolator = $__require('36');
  var _Interpolator2 = _interopRequireDefault(_Interpolator);
  var _BackendConnector = $__require('37');
  var _BackendConnector2 = _interopRequireDefault(_BackendConnector);
  var _CacheConnector = $__require('38');
  var _CacheConnector2 = _interopRequireDefault(_CacheConnector);
  var _defaults2 = $__require('39');
  var _postProcessor = $__require('32');
  var _postProcessor2 = _interopRequireDefault(_postProcessor);
  var _v = $__require('33');
  var compat = _interopRequireWildcard(_v);
  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    } else {
      var newObj = {};
      if (obj != null) {
        for (var key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key))
            newObj[key] = obj[key];
        }
      }
      newObj.default = obj;
      return newObj;
    }
  }
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function _defaults(obj, defaults) {
    var keys = Object.getOwnPropertyNames(defaults);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var value = Object.getOwnPropertyDescriptor(defaults, key);
      if (value && value.configurable && obj[key] === undefined) {
        Object.defineProperty(obj, key, value);
      }
    }
    return obj;
  }
  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }
  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass);
  }
  var I18n = function(_EventEmitter) {
    _inherits(I18n, _EventEmitter);
    function I18n() {
      var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      var callback = arguments[1];
      _classCallCheck(this, I18n);
      var _this = _possibleConstructorReturn(this, _EventEmitter.call(this));
      _this.options = (0, _defaults2.transformOptions)(options);
      _this.services = {};
      _this.logger = _logger2.default;
      _this.modules = {};
      if (callback && !_this.isInitialized)
        _this.init(options, callback);
      return _this;
    }
    I18n.prototype.init = function init(options, callback) {
      var _this2 = this;
      if (typeof options === 'function') {
        callback = options;
        options = {};
      }
      if (!options)
        options = {};
      if (options.compatibilityAPI === 'v1') {
        this.options = _extends({}, (0, _defaults2.get)(), (0, _defaults2.transformOptions)(compat.convertAPIOptions(options)), {});
      } else if (options.compatibilityJSON === 'v1') {
        this.options = _extends({}, (0, _defaults2.get)(), (0, _defaults2.transformOptions)(compat.convertJSONOptions(options)), {});
      } else {
        this.options = _extends({}, (0, _defaults2.get)(), this.options, (0, _defaults2.transformOptions)(options));
      }
      if (!callback)
        callback = function callback() {};
      function createClassOnDemand(ClassOrObject) {
        if (!ClassOrObject)
          return;
        if (typeof ClassOrObject === 'function')
          return new ClassOrObject();
        return ClassOrObject;
      }
      if (!this.options.isClone) {
        if (this.modules.logger) {
          _logger2.default.init(createClassOnDemand(this.modules.logger), this.options);
        } else {
          _logger2.default.init(null, this.options);
        }
        var lu = new _LanguageUtils2.default(this.options);
        this.store = new _ResourceStore2.default(this.options.resources, this.options);
        var s = this.services;
        s.logger = _logger2.default;
        s.resourceStore = this.store;
        s.resourceStore.on('added removed', function(lng, ns) {
          s.cacheConnector.save();
        });
        s.languageUtils = lu;
        s.pluralResolver = new _PluralResolver2.default(lu, {
          prepend: this.options.pluralSeparator,
          compatibilityJSON: this.options.compatibilityJSON
        });
        s.interpolator = new _Interpolator2.default(this.options);
        s.backendConnector = new _BackendConnector2.default(createClassOnDemand(this.modules.backend), s.resourceStore, s, this.options);
        s.backendConnector.on('*', function(event) {
          for (var _len = arguments.length,
              args = Array(_len > 1 ? _len - 1 : 0),
              _key = 1; _key < _len; _key++) {
            args[_key - 1] = arguments[_key];
          }
          _this2.emit.apply(_this2, [event].concat(args));
        });
        s.backendConnector.on('loaded', function(loaded) {
          s.cacheConnector.save();
        });
        s.cacheConnector = new _CacheConnector2.default(createClassOnDemand(this.modules.cache), s.resourceStore, s, this.options);
        s.cacheConnector.on('*', function(event) {
          for (var _len2 = arguments.length,
              args = Array(_len2 > 1 ? _len2 - 1 : 0),
              _key2 = 1; _key2 < _len2; _key2++) {
            args[_key2 - 1] = arguments[_key2];
          }
          _this2.emit.apply(_this2, [event].concat(args));
        });
        if (this.modules.languageDetector) {
          s.languageDetector = createClassOnDemand(this.modules.languageDetector);
          s.languageDetector.init(s, this.options.detection, this.options);
        }
        this.translator = new _Translator2.default(this.services, this.options);
        this.translator.on('*', function(event) {
          for (var _len3 = arguments.length,
              args = Array(_len3 > 1 ? _len3 - 1 : 0),
              _key3 = 1; _key3 < _len3; _key3++) {
            args[_key3 - 1] = arguments[_key3];
          }
          _this2.emit.apply(_this2, [event].concat(args));
        });
      }
      var storeApi = ['getResource', 'addResource', 'addResources', 'addResourceBundle', 'removeResourceBundle', 'hasResourceBundle', 'getResourceBundle'];
      storeApi.forEach(function(fcName) {
        _this2[fcName] = function() {
          return this.store[fcName].apply(this.store, arguments);
        };
      });
      if (this.options.compatibilityAPI === 'v1')
        compat.appendBackwardsAPI(this);
      var load = function load() {
        _this2.changeLanguage(_this2.options.lng, function(err, t) {
          _this2.emit('initialized', _this2.options);
          _this2.logger.log('initialized', _this2.options);
          callback(err, t);
        });
      };
      if (this.options.resources || !this.options.initImmediate) {
        load();
      } else {
        setTimeout(load, 0);
      }
      return this;
    };
    I18n.prototype.loadResources = function loadResources(callback) {
      var _this3 = this;
      if (!callback)
        callback = function callback() {};
      if (!this.options.resources) {
        var _ret = function() {
          if (_this3.language && _this3.language.toLowerCase() === 'cimode')
            return {v: callback()};
          var toLoad = [];
          var append = function append(lng) {
            var lngs = _this3.services.languageUtils.toResolveHierarchy(lng);
            lngs.forEach(function(l) {
              if (toLoad.indexOf(l) < 0)
                toLoad.push(l);
            });
          };
          append(_this3.language);
          if (_this3.options.preload) {
            _this3.options.preload.forEach(function(l) {
              append(l);
            });
          }
          _this3.services.cacheConnector.load(toLoad, _this3.options.ns, function() {
            _this3.services.backendConnector.load(toLoad, _this3.options.ns, callback);
          });
        }();
        if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object")
          return _ret.v;
      } else {
        callback(null);
      }
    };
    I18n.prototype.use = function use(module) {
      if (module.type === 'backend') {
        this.modules.backend = module;
      }
      if (module.type === 'cache') {
        this.modules.cache = module;
      }
      if (module.type === 'logger' || module.log && module.warn && module.warn) {
        this.modules.logger = module;
      }
      if (module.type === 'languageDetector') {
        this.modules.languageDetector = module;
      }
      if (module.type === 'postProcessor') {
        _postProcessor2.default.addPostProcessor(module);
      }
      return this;
    };
    I18n.prototype.changeLanguage = function changeLanguage(lng, callback) {
      var _this4 = this;
      var done = function done(err) {
        if (lng) {
          _this4.emit('languageChanged', lng);
          _this4.logger.log('languageChanged', lng);
        }
        if (callback)
          callback(err, function() {
            for (var _len4 = arguments.length,
                args = Array(_len4),
                _key4 = 0; _key4 < _len4; _key4++) {
              args[_key4] = arguments[_key4];
            }
            return _this4.t.apply(_this4, args);
          });
      };
      if (!lng && this.services.languageDetector)
        lng = this.services.languageDetector.detect();
      if (lng) {
        this.language = lng;
        this.languages = this.services.languageUtils.toResolveHierarchy(lng);
        this.translator.changeLanguage(lng);
        if (this.services.languageDetector)
          this.services.languageDetector.cacheUserLanguage(lng);
      }
      this.loadResources(function(err) {
        done(err);
      });
    };
    I18n.prototype.getFixedT = function getFixedT(lng, ns) {
      var _this5 = this;
      var fixedT = function fixedT(key, options) {
        options = options || {};
        options.lng = options.lng || fixedT.lng;
        options.ns = options.ns || fixedT.ns;
        return _this5.t(key, options);
      };
      fixedT.lng = lng;
      fixedT.ns = ns;
      return fixedT;
    };
    I18n.prototype.t = function t() {
      return this.translator && this.translator.translate.apply(this.translator, arguments);
    };
    I18n.prototype.exists = function exists() {
      return this.translator && this.translator.exists.apply(this.translator, arguments);
    };
    I18n.prototype.setDefaultNamespace = function setDefaultNamespace(ns) {
      this.options.defaultNS = ns;
    };
    I18n.prototype.loadNamespaces = function loadNamespaces(ns, callback) {
      var _this6 = this;
      if (!this.options.ns)
        return callback && callback();
      if (typeof ns === 'string')
        ns = [ns];
      ns.forEach(function(n) {
        if (_this6.options.ns.indexOf(n) < 0)
          _this6.options.ns.push(n);
      });
      this.loadResources(callback);
    };
    I18n.prototype.loadLanguages = function loadLanguages(lngs, callback) {
      if (typeof lngs === 'string')
        lngs = [lngs];
      var preloaded = this.options.preload || [];
      var newLngs = lngs.filter(function(lng) {
        return preloaded.indexOf(lng) < 0;
      });
      if (!newLngs.length)
        return callback();
      this.options.preload = preloaded.concat(newLngs);
      this.loadResources(callback);
    };
    I18n.prototype.dir = function dir(lng) {
      if (!lng)
        lng = this.language;
      var rtlLngs = ['ar', 'shu', 'sqr', 'ssh', 'xaa', 'yhd', 'yud', 'aao', 'abh', 'abv', 'acm', 'acq', 'acw', 'acx', 'acy', 'adf', 'ads', 'aeb', 'aec', 'afb', 'ajp', 'apc', 'apd', 'arb', 'arq', 'ars', 'ary', 'arz', 'auz', 'avl', 'ayh', 'ayl', 'ayn', 'ayp', 'bbz', 'pga', 'he', 'iw', 'ps', 'pbt', 'pbu', 'pst', 'prp', 'prd', 'ur', 'ydd', 'yds', 'yih', 'ji', 'yi', 'hbo', 'men', 'xmn', 'fa', 'jpr', 'peo', 'pes', 'prs', 'dv', 'sam'];
      return rtlLngs.indexOf(this.services.languageUtils.getLanguagePartFromCode(lng)) >= 0 ? 'rtl' : 'ltr';
    };
    I18n.prototype.createInstance = function createInstance() {
      var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      var callback = arguments[1];
      return new I18n(options, callback);
    };
    I18n.prototype.cloneInstance = function cloneInstance() {
      var _this7 = this;
      var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      var callback = arguments[1];
      var clone = new I18n(_extends({}, options, this.options, {isClone: true}), callback);
      var membersToCopy = ['store', 'translator', 'services', 'language'];
      membersToCopy.forEach(function(m) {
        clone[m] = _this7[m];
      });
      return clone;
    };
    return I18n;
  }(_EventEmitter3.default);
  exports.default = new I18n();
  return module.exports;
});

$__System.registerDynamic("3b", ["3a"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  Object.defineProperty(exports, "__esModule", {value: true});
  var _i18next = $__require('3a');
  var _i18next2 = _interopRequireDefault(_i18next);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  exports.default = _i18next2.default;
  return module.exports;
});

$__System.registerDynamic("1c", ["3b"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = $__require('3b').default;
  return module.exports;
});

$__System.registerDynamic("22", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  ;
  (function() {
    var undefined;
    var VERSION = '4.11.1';
    var LARGE_ARRAY_SIZE = 200;
    var FUNC_ERROR_TEXT = 'Expected a function';
    var HASH_UNDEFINED = '__lodash_hash_undefined__';
    var PLACEHOLDER = '__lodash_placeholder__';
    var BIND_FLAG = 1,
        BIND_KEY_FLAG = 2,
        CURRY_BOUND_FLAG = 4,
        CURRY_FLAG = 8,
        CURRY_RIGHT_FLAG = 16,
        PARTIAL_FLAG = 32,
        PARTIAL_RIGHT_FLAG = 64,
        ARY_FLAG = 128,
        REARG_FLAG = 256,
        FLIP_FLAG = 512;
    var UNORDERED_COMPARE_FLAG = 1,
        PARTIAL_COMPARE_FLAG = 2;
    var DEFAULT_TRUNC_LENGTH = 30,
        DEFAULT_TRUNC_OMISSION = '...';
    var HOT_COUNT = 150,
        HOT_SPAN = 16;
    var LAZY_FILTER_FLAG = 1,
        LAZY_MAP_FLAG = 2,
        LAZY_WHILE_FLAG = 3;
    var INFINITY = 1 / 0,
        MAX_SAFE_INTEGER = 9007199254740991,
        MAX_INTEGER = 1.7976931348623157e+308,
        NAN = 0 / 0;
    var MAX_ARRAY_LENGTH = 4294967295,
        MAX_ARRAY_INDEX = MAX_ARRAY_LENGTH - 1,
        HALF_MAX_ARRAY_LENGTH = MAX_ARRAY_LENGTH >>> 1;
    var argsTag = '[object Arguments]',
        arrayTag = '[object Array]',
        boolTag = '[object Boolean]',
        dateTag = '[object Date]',
        errorTag = '[object Error]',
        funcTag = '[object Function]',
        genTag = '[object GeneratorFunction]',
        mapTag = '[object Map]',
        numberTag = '[object Number]',
        objectTag = '[object Object]',
        promiseTag = '[object Promise]',
        regexpTag = '[object RegExp]',
        setTag = '[object Set]',
        stringTag = '[object String]',
        symbolTag = '[object Symbol]',
        weakMapTag = '[object WeakMap]',
        weakSetTag = '[object WeakSet]';
    var arrayBufferTag = '[object ArrayBuffer]',
        dataViewTag = '[object DataView]',
        float32Tag = '[object Float32Array]',
        float64Tag = '[object Float64Array]',
        int8Tag = '[object Int8Array]',
        int16Tag = '[object Int16Array]',
        int32Tag = '[object Int32Array]',
        uint8Tag = '[object Uint8Array]',
        uint8ClampedTag = '[object Uint8ClampedArray]',
        uint16Tag = '[object Uint16Array]',
        uint32Tag = '[object Uint32Array]';
    var reEmptyStringLeading = /\b__p \+= '';/g,
        reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
        reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;
    var reEscapedHtml = /&(?:amp|lt|gt|quot|#39|#96);/g,
        reUnescapedHtml = /[&<>"'`]/g,
        reHasEscapedHtml = RegExp(reEscapedHtml.source),
        reHasUnescapedHtml = RegExp(reUnescapedHtml.source);
    var reEscape = /<%-([\s\S]+?)%>/g,
        reEvaluate = /<%([\s\S]+?)%>/g,
        reInterpolate = /<%=([\s\S]+?)%>/g;
    var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
        reIsPlainProp = /^\w*$/,
        rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]/g;
    var reRegExpChar = /[\\^$.*+?()[\]{}|]/g,
        reHasRegExpChar = RegExp(reRegExpChar.source);
    var reTrim = /^\s+|\s+$/g,
        reTrimStart = /^\s+/,
        reTrimEnd = /\s+$/;
    var reBasicWord = /[a-zA-Z0-9]+/g;
    var reEscapeChar = /\\(\\)?/g;
    var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;
    var reFlags = /\w*$/;
    var reHasHexPrefix = /^0x/i;
    var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
    var reIsBinary = /^0b[01]+$/i;
    var reIsHostCtor = /^\[object .+?Constructor\]$/;
    var reIsOctal = /^0o[0-7]+$/i;
    var reIsUint = /^(?:0|[1-9]\d*)$/;
    var reLatin1 = /[\xc0-\xd6\xd8-\xde\xdf-\xf6\xf8-\xff]/g;
    var reNoMatch = /($^)/;
    var reUnescapedString = /['\n\r\u2028\u2029\\]/g;
    var rsAstralRange = '\\ud800-\\udfff',
        rsComboMarksRange = '\\u0300-\\u036f\\ufe20-\\ufe23',
        rsComboSymbolsRange = '\\u20d0-\\u20f0',
        rsDingbatRange = '\\u2700-\\u27bf',
        rsLowerRange = 'a-z\\xdf-\\xf6\\xf8-\\xff',
        rsMathOpRange = '\\xac\\xb1\\xd7\\xf7',
        rsNonCharRange = '\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf',
        rsQuoteRange = '\\u2018\\u2019\\u201c\\u201d',
        rsSpaceRange = ' \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000',
        rsUpperRange = 'A-Z\\xc0-\\xd6\\xd8-\\xde',
        rsVarRange = '\\ufe0e\\ufe0f',
        rsBreakRange = rsMathOpRange + rsNonCharRange + rsQuoteRange + rsSpaceRange;
    var rsApos = "['\u2019]",
        rsAstral = '[' + rsAstralRange + ']',
        rsBreak = '[' + rsBreakRange + ']',
        rsCombo = '[' + rsComboMarksRange + rsComboSymbolsRange + ']',
        rsDigits = '\\d+',
        rsDingbat = '[' + rsDingbatRange + ']',
        rsLower = '[' + rsLowerRange + ']',
        rsMisc = '[^' + rsAstralRange + rsBreakRange + rsDigits + rsDingbatRange + rsLowerRange + rsUpperRange + ']',
        rsFitz = '\\ud83c[\\udffb-\\udfff]',
        rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
        rsNonAstral = '[^' + rsAstralRange + ']',
        rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
        rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
        rsUpper = '[' + rsUpperRange + ']',
        rsZWJ = '\\u200d';
    var rsLowerMisc = '(?:' + rsLower + '|' + rsMisc + ')',
        rsUpperMisc = '(?:' + rsUpper + '|' + rsMisc + ')',
        rsOptLowerContr = '(?:' + rsApos + '(?:d|ll|m|re|s|t|ve))?',
        rsOptUpperContr = '(?:' + rsApos + '(?:D|LL|M|RE|S|T|VE))?',
        reOptMod = rsModifier + '?',
        rsOptVar = '[' + rsVarRange + ']?',
        rsOptJoin = '(?:' + rsZWJ + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
        rsSeq = rsOptVar + reOptMod + rsOptJoin,
        rsEmoji = '(?:' + [rsDingbat, rsRegional, rsSurrPair].join('|') + ')' + rsSeq,
        rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';
    var reApos = RegExp(rsApos, 'g');
    var reComboMark = RegExp(rsCombo, 'g');
    var reComplexSymbol = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');
    var reComplexWord = RegExp([rsUpper + '?' + rsLower + '+' + rsOptLowerContr + '(?=' + [rsBreak, rsUpper, '$'].join('|') + ')', rsUpperMisc + '+' + rsOptUpperContr + '(?=' + [rsBreak, rsUpper + rsLowerMisc, '$'].join('|') + ')', rsUpper + '?' + rsLowerMisc + '+' + rsOptLowerContr, rsUpper + '+' + rsOptUpperContr, rsDigits, rsEmoji].join('|'), 'g');
    var reHasComplexSymbol = RegExp('[' + rsZWJ + rsAstralRange + rsComboMarksRange + rsComboSymbolsRange + rsVarRange + ']');
    var reHasComplexWord = /[a-z][A-Z]|[A-Z]{2,}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/;
    var contextProps = ['Array', 'Buffer', 'DataView', 'Date', 'Error', 'Float32Array', 'Float64Array', 'Function', 'Int8Array', 'Int16Array', 'Int32Array', 'Map', 'Math', 'Object', 'Promise', 'Reflect', 'RegExp', 'Set', 'String', 'Symbol', 'TypeError', 'Uint8Array', 'Uint8ClampedArray', 'Uint16Array', 'Uint32Array', 'WeakMap', '_', 'clearTimeout', 'isFinite', 'parseInt', 'setTimeout'];
    var templateCounter = -1;
    var typedArrayTags = {};
    typedArrayTags[float32Tag] = typedArrayTags[float64Tag] = typedArrayTags[int8Tag] = typedArrayTags[int16Tag] = typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] = typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] = typedArrayTags[uint32Tag] = true;
    typedArrayTags[argsTag] = typedArrayTags[arrayTag] = typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] = typedArrayTags[dataViewTag] = typedArrayTags[dateTag] = typedArrayTags[errorTag] = typedArrayTags[funcTag] = typedArrayTags[mapTag] = typedArrayTags[numberTag] = typedArrayTags[objectTag] = typedArrayTags[regexpTag] = typedArrayTags[setTag] = typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;
    var cloneableTags = {};
    cloneableTags[argsTag] = cloneableTags[arrayTag] = cloneableTags[arrayBufferTag] = cloneableTags[dataViewTag] = cloneableTags[boolTag] = cloneableTags[dateTag] = cloneableTags[float32Tag] = cloneableTags[float64Tag] = cloneableTags[int8Tag] = cloneableTags[int16Tag] = cloneableTags[int32Tag] = cloneableTags[mapTag] = cloneableTags[numberTag] = cloneableTags[objectTag] = cloneableTags[regexpTag] = cloneableTags[setTag] = cloneableTags[stringTag] = cloneableTags[symbolTag] = cloneableTags[uint8Tag] = cloneableTags[uint8ClampedTag] = cloneableTags[uint16Tag] = cloneableTags[uint32Tag] = true;
    cloneableTags[errorTag] = cloneableTags[funcTag] = cloneableTags[weakMapTag] = false;
    var deburredLetters = {
      '\xc0': 'A',
      '\xc1': 'A',
      '\xc2': 'A',
      '\xc3': 'A',
      '\xc4': 'A',
      '\xc5': 'A',
      '\xe0': 'a',
      '\xe1': 'a',
      '\xe2': 'a',
      '\xe3': 'a',
      '\xe4': 'a',
      '\xe5': 'a',
      '\xc7': 'C',
      '\xe7': 'c',
      '\xd0': 'D',
      '\xf0': 'd',
      '\xc8': 'E',
      '\xc9': 'E',
      '\xca': 'E',
      '\xcb': 'E',
      '\xe8': 'e',
      '\xe9': 'e',
      '\xea': 'e',
      '\xeb': 'e',
      '\xcC': 'I',
      '\xcd': 'I',
      '\xce': 'I',
      '\xcf': 'I',
      '\xeC': 'i',
      '\xed': 'i',
      '\xee': 'i',
      '\xef': 'i',
      '\xd1': 'N',
      '\xf1': 'n',
      '\xd2': 'O',
      '\xd3': 'O',
      '\xd4': 'O',
      '\xd5': 'O',
      '\xd6': 'O',
      '\xd8': 'O',
      '\xf2': 'o',
      '\xf3': 'o',
      '\xf4': 'o',
      '\xf5': 'o',
      '\xf6': 'o',
      '\xf8': 'o',
      '\xd9': 'U',
      '\xda': 'U',
      '\xdb': 'U',
      '\xdc': 'U',
      '\xf9': 'u',
      '\xfa': 'u',
      '\xfb': 'u',
      '\xfc': 'u',
      '\xdd': 'Y',
      '\xfd': 'y',
      '\xff': 'y',
      '\xc6': 'Ae',
      '\xe6': 'ae',
      '\xde': 'Th',
      '\xfe': 'th',
      '\xdf': 'ss'
    };
    var htmlEscapes = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#39;',
      '`': '&#96;'
    };
    var htmlUnescapes = {
      '&amp;': '&',
      '&lt;': '<',
      '&gt;': '>',
      '&quot;': '"',
      '&#39;': "'",
      '&#96;': '`'
    };
    var objectTypes = {
      'function': true,
      'object': true
    };
    var stringEscapes = {
      '\\': '\\',
      "'": "'",
      '\n': 'n',
      '\r': 'r',
      '\u2028': 'u2028',
      '\u2029': 'u2029'
    };
    var freeParseFloat = parseFloat,
        freeParseInt = parseInt;
    var freeExports = (objectTypes[typeof exports] && exports && !exports.nodeType) ? exports : undefined;
    var freeModule = (objectTypes[typeof module] && module && !module.nodeType) ? module : undefined;
    var moduleExports = (freeModule && freeModule.exports === freeExports) ? freeExports : undefined;
    var freeGlobal = checkGlobal(freeExports && freeModule && typeof global == 'object' && global);
    var freeSelf = checkGlobal(objectTypes[typeof self] && self);
    var freeWindow = checkGlobal(objectTypes[typeof window] && window);
    var thisGlobal = checkGlobal(objectTypes[typeof this] && this);
    var root = freeGlobal || ((freeWindow !== (thisGlobal && thisGlobal.window)) && freeWindow) || freeSelf || thisGlobal || Function('return this')();
    function addMapEntry(map, pair) {
      map.set(pair[0], pair[1]);
      return map;
    }
    function addSetEntry(set, value) {
      set.add(value);
      return set;
    }
    function apply(func, thisArg, args) {
      var length = args.length;
      switch (length) {
        case 0:
          return func.call(thisArg);
        case 1:
          return func.call(thisArg, args[0]);
        case 2:
          return func.call(thisArg, args[0], args[1]);
        case 3:
          return func.call(thisArg, args[0], args[1], args[2]);
      }
      return func.apply(thisArg, args);
    }
    function arrayAggregator(array, setter, iteratee, accumulator) {
      var index = -1,
          length = array.length;
      while (++index < length) {
        var value = array[index];
        setter(accumulator, value, iteratee(value), array);
      }
      return accumulator;
    }
    function arrayConcat(array, other) {
      var index = -1,
          length = array.length,
          othIndex = -1,
          othLength = other.length,
          result = Array(length + othLength);
      while (++index < length) {
        result[index] = array[index];
      }
      while (++othIndex < othLength) {
        result[index++] = other[othIndex];
      }
      return result;
    }
    function arrayEach(array, iteratee) {
      var index = -1,
          length = array.length;
      while (++index < length) {
        if (iteratee(array[index], index, array) === false) {
          break;
        }
      }
      return array;
    }
    function arrayEachRight(array, iteratee) {
      var length = array.length;
      while (length--) {
        if (iteratee(array[length], length, array) === false) {
          break;
        }
      }
      return array;
    }
    function arrayEvery(array, predicate) {
      var index = -1,
          length = array.length;
      while (++index < length) {
        if (!predicate(array[index], index, array)) {
          return false;
        }
      }
      return true;
    }
    function arrayFilter(array, predicate) {
      var index = -1,
          length = array.length,
          resIndex = 0,
          result = [];
      while (++index < length) {
        var value = array[index];
        if (predicate(value, index, array)) {
          result[resIndex++] = value;
        }
      }
      return result;
    }
    function arrayIncludes(array, value) {
      return !!array.length && baseIndexOf(array, value, 0) > -1;
    }
    function arrayIncludesWith(array, value, comparator) {
      var index = -1,
          length = array.length;
      while (++index < length) {
        if (comparator(value, array[index])) {
          return true;
        }
      }
      return false;
    }
    function arrayMap(array, iteratee) {
      var index = -1,
          length = array.length,
          result = Array(length);
      while (++index < length) {
        result[index] = iteratee(array[index], index, array);
      }
      return result;
    }
    function arrayPush(array, values) {
      var index = -1,
          length = values.length,
          offset = array.length;
      while (++index < length) {
        array[offset + index] = values[index];
      }
      return array;
    }
    function arrayReduce(array, iteratee, accumulator, initAccum) {
      var index = -1,
          length = array.length;
      if (initAccum && length) {
        accumulator = array[++index];
      }
      while (++index < length) {
        accumulator = iteratee(accumulator, array[index], index, array);
      }
      return accumulator;
    }
    function arrayReduceRight(array, iteratee, accumulator, initAccum) {
      var length = array.length;
      if (initAccum && length) {
        accumulator = array[--length];
      }
      while (length--) {
        accumulator = iteratee(accumulator, array[length], length, array);
      }
      return accumulator;
    }
    function arraySome(array, predicate) {
      var index = -1,
          length = array.length;
      while (++index < length) {
        if (predicate(array[index], index, array)) {
          return true;
        }
      }
      return false;
    }
    function baseExtremum(array, iteratee, comparator) {
      var index = -1,
          length = array.length;
      while (++index < length) {
        var value = array[index],
            current = iteratee(value);
        if (current != null && (computed === undefined ? current === current : comparator(current, computed))) {
          var computed = current,
              result = value;
        }
      }
      return result;
    }
    function baseFind(collection, predicate, eachFunc, retKey) {
      var result;
      eachFunc(collection, function(value, key, collection) {
        if (predicate(value, key, collection)) {
          result = retKey ? key : value;
          return false;
        }
      });
      return result;
    }
    function baseFindIndex(array, predicate, fromRight) {
      var length = array.length,
          index = fromRight ? length : -1;
      while ((fromRight ? index-- : ++index < length)) {
        if (predicate(array[index], index, array)) {
          return index;
        }
      }
      return -1;
    }
    function baseIndexOf(array, value, fromIndex) {
      if (value !== value) {
        return indexOfNaN(array, fromIndex);
      }
      var index = fromIndex - 1,
          length = array.length;
      while (++index < length) {
        if (array[index] === value) {
          return index;
        }
      }
      return -1;
    }
    function baseIndexOfWith(array, value, fromIndex, comparator) {
      var index = fromIndex - 1,
          length = array.length;
      while (++index < length) {
        if (comparator(array[index], value)) {
          return index;
        }
      }
      return -1;
    }
    function baseMean(array, iteratee) {
      var length = array ? array.length : 0;
      return length ? (baseSum(array, iteratee) / length) : NAN;
    }
    function baseReduce(collection, iteratee, accumulator, initAccum, eachFunc) {
      eachFunc(collection, function(value, index, collection) {
        accumulator = initAccum ? (initAccum = false, value) : iteratee(accumulator, value, index, collection);
      });
      return accumulator;
    }
    function baseSortBy(array, comparer) {
      var length = array.length;
      array.sort(comparer);
      while (length--) {
        array[length] = array[length].value;
      }
      return array;
    }
    function baseSum(array, iteratee) {
      var result,
          index = -1,
          length = array.length;
      while (++index < length) {
        var current = iteratee(array[index]);
        if (current !== undefined) {
          result = result === undefined ? current : (result + current);
        }
      }
      return result;
    }
    function baseTimes(n, iteratee) {
      var index = -1,
          result = Array(n);
      while (++index < n) {
        result[index] = iteratee(index);
      }
      return result;
    }
    function baseToPairs(object, props) {
      return arrayMap(props, function(key) {
        return [key, object[key]];
      });
    }
    function baseUnary(func) {
      return function(value) {
        return func(value);
      };
    }
    function baseValues(object, props) {
      return arrayMap(props, function(key) {
        return object[key];
      });
    }
    function charsStartIndex(strSymbols, chrSymbols) {
      var index = -1,
          length = strSymbols.length;
      while (++index < length && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
      return index;
    }
    function charsEndIndex(strSymbols, chrSymbols) {
      var index = strSymbols.length;
      while (index-- && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
      return index;
    }
    function checkGlobal(value) {
      return (value && value.Object === Object) ? value : null;
    }
    function compareAscending(value, other) {
      if (value !== other) {
        var valIsNull = value === null,
            valIsUndef = value === undefined,
            valIsReflexive = value === value;
        var othIsNull = other === null,
            othIsUndef = other === undefined,
            othIsReflexive = other === other;
        if ((value > other && !othIsNull) || !valIsReflexive || (valIsNull && !othIsUndef && othIsReflexive) || (valIsUndef && othIsReflexive)) {
          return 1;
        }
        if ((value < other && !valIsNull) || !othIsReflexive || (othIsNull && !valIsUndef && valIsReflexive) || (othIsUndef && valIsReflexive)) {
          return -1;
        }
      }
      return 0;
    }
    function compareMultiple(object, other, orders) {
      var index = -1,
          objCriteria = object.criteria,
          othCriteria = other.criteria,
          length = objCriteria.length,
          ordersLength = orders.length;
      while (++index < length) {
        var result = compareAscending(objCriteria[index], othCriteria[index]);
        if (result) {
          if (index >= ordersLength) {
            return result;
          }
          var order = orders[index];
          return result * (order == 'desc' ? -1 : 1);
        }
      }
      return object.index - other.index;
    }
    function countHolders(array, placeholder) {
      var length = array.length,
          result = 0;
      while (length--) {
        if (array[length] === placeholder) {
          result++;
        }
      }
      return result;
    }
    function createMathOperation(operator) {
      return function(value, other) {
        var result;
        if (value === undefined && other === undefined) {
          return 0;
        }
        if (value !== undefined) {
          result = value;
        }
        if (other !== undefined) {
          result = result === undefined ? other : operator(result, other);
        }
        return result;
      };
    }
    function deburrLetter(letter) {
      return deburredLetters[letter];
    }
    function escapeHtmlChar(chr) {
      return htmlEscapes[chr];
    }
    function escapeStringChar(chr) {
      return '\\' + stringEscapes[chr];
    }
    function indexOfNaN(array, fromIndex, fromRight) {
      var length = array.length,
          index = fromIndex + (fromRight ? 0 : -1);
      while ((fromRight ? index-- : ++index < length)) {
        var other = array[index];
        if (other !== other) {
          return index;
        }
      }
      return -1;
    }
    function isHostObject(value) {
      var result = false;
      if (value != null && typeof value.toString != 'function') {
        try {
          result = !!(value + '');
        } catch (e) {}
      }
      return result;
    }
    function isIndex(value, length) {
      value = (typeof value == 'number' || reIsUint.test(value)) ? +value : -1;
      length = length == null ? MAX_SAFE_INTEGER : length;
      return value > -1 && value % 1 == 0 && value < length;
    }
    function iteratorToArray(iterator) {
      var data,
          result = [];
      while (!(data = iterator.next()).done) {
        result.push(data.value);
      }
      return result;
    }
    function mapToArray(map) {
      var index = -1,
          result = Array(map.size);
      map.forEach(function(value, key) {
        result[++index] = [key, value];
      });
      return result;
    }
    function replaceHolders(array, placeholder) {
      var index = -1,
          length = array.length,
          resIndex = 0,
          result = [];
      while (++index < length) {
        var value = array[index];
        if (value === placeholder || value === PLACEHOLDER) {
          array[index] = PLACEHOLDER;
          result[resIndex++] = index;
        }
      }
      return result;
    }
    function setToArray(set) {
      var index = -1,
          result = Array(set.size);
      set.forEach(function(value) {
        result[++index] = value;
      });
      return result;
    }
    function stringSize(string) {
      if (!(string && reHasComplexSymbol.test(string))) {
        return string.length;
      }
      var result = reComplexSymbol.lastIndex = 0;
      while (reComplexSymbol.test(string)) {
        result++;
      }
      return result;
    }
    function stringToArray(string) {
      return string.match(reComplexSymbol);
    }
    function unescapeHtmlChar(chr) {
      return htmlUnescapes[chr];
    }
    function runInContext(context) {
      context = context ? _.defaults({}, context, _.pick(root, contextProps)) : root;
      var Date = context.Date,
          Error = context.Error,
          Math = context.Math,
          RegExp = context.RegExp,
          TypeError = context.TypeError;
      var arrayProto = context.Array.prototype,
          objectProto = context.Object.prototype,
          stringProto = context.String.prototype;
      var funcToString = context.Function.prototype.toString;
      var hasOwnProperty = objectProto.hasOwnProperty;
      var idCounter = 0;
      var objectCtorString = funcToString.call(Object);
      var objectToString = objectProto.toString;
      var oldDash = root._;
      var reIsNative = RegExp('^' + funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&').replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$');
      var Buffer = moduleExports ? context.Buffer : undefined,
          Reflect = context.Reflect,
          Symbol = context.Symbol,
          Uint8Array = context.Uint8Array,
          clearTimeout = context.clearTimeout,
          enumerate = Reflect ? Reflect.enumerate : undefined,
          getOwnPropertySymbols = Object.getOwnPropertySymbols,
          iteratorSymbol = typeof(iteratorSymbol = Symbol && Symbol.iterator) == 'symbol' ? iteratorSymbol : undefined,
          objectCreate = Object.create,
          propertyIsEnumerable = objectProto.propertyIsEnumerable,
          setTimeout = context.setTimeout,
          splice = arrayProto.splice;
      var nativeCeil = Math.ceil,
          nativeFloor = Math.floor,
          nativeGetPrototype = Object.getPrototypeOf,
          nativeIsFinite = context.isFinite,
          nativeJoin = arrayProto.join,
          nativeKeys = Object.keys,
          nativeMax = Math.max,
          nativeMin = Math.min,
          nativeParseInt = context.parseInt,
          nativeRandom = Math.random,
          nativeReplace = stringProto.replace,
          nativeReverse = arrayProto.reverse,
          nativeSplit = stringProto.split;
      var DataView = getNative(context, 'DataView'),
          Map = getNative(context, 'Map'),
          Promise = getNative(context, 'Promise'),
          Set = getNative(context, 'Set'),
          WeakMap = getNative(context, 'WeakMap'),
          nativeCreate = getNative(Object, 'create');
      var metaMap = WeakMap && new WeakMap;
      var nonEnumShadows = !propertyIsEnumerable.call({'valueOf': 1}, 'valueOf');
      var realNames = {};
      var dataViewCtorString = toSource(DataView),
          mapCtorString = toSource(Map),
          promiseCtorString = toSource(Promise),
          setCtorString = toSource(Set),
          weakMapCtorString = toSource(WeakMap);
      var symbolProto = Symbol ? Symbol.prototype : undefined,
          symbolValueOf = symbolProto ? symbolProto.valueOf : undefined,
          symbolToString = symbolProto ? symbolProto.toString : undefined;
      function lodash(value) {
        if (isObjectLike(value) && !isArray(value) && !(value instanceof LazyWrapper)) {
          if (value instanceof LodashWrapper) {
            return value;
          }
          if (hasOwnProperty.call(value, '__wrapped__')) {
            return wrapperClone(value);
          }
        }
        return new LodashWrapper(value);
      }
      function baseLodash() {}
      function LodashWrapper(value, chainAll) {
        this.__wrapped__ = value;
        this.__actions__ = [];
        this.__chain__ = !!chainAll;
        this.__index__ = 0;
        this.__values__ = undefined;
      }
      lodash.templateSettings = {
        'escape': reEscape,
        'evaluate': reEvaluate,
        'interpolate': reInterpolate,
        'variable': '',
        'imports': {'_': lodash}
      };
      lodash.prototype = baseLodash.prototype;
      lodash.prototype.constructor = lodash;
      LodashWrapper.prototype = baseCreate(baseLodash.prototype);
      LodashWrapper.prototype.constructor = LodashWrapper;
      function LazyWrapper(value) {
        this.__wrapped__ = value;
        this.__actions__ = [];
        this.__dir__ = 1;
        this.__filtered__ = false;
        this.__iteratees__ = [];
        this.__takeCount__ = MAX_ARRAY_LENGTH;
        this.__views__ = [];
      }
      function lazyClone() {
        var result = new LazyWrapper(this.__wrapped__);
        result.__actions__ = copyArray(this.__actions__);
        result.__dir__ = this.__dir__;
        result.__filtered__ = this.__filtered__;
        result.__iteratees__ = copyArray(this.__iteratees__);
        result.__takeCount__ = this.__takeCount__;
        result.__views__ = copyArray(this.__views__);
        return result;
      }
      function lazyReverse() {
        if (this.__filtered__) {
          var result = new LazyWrapper(this);
          result.__dir__ = -1;
          result.__filtered__ = true;
        } else {
          result = this.clone();
          result.__dir__ *= -1;
        }
        return result;
      }
      function lazyValue() {
        var array = this.__wrapped__.value(),
            dir = this.__dir__,
            isArr = isArray(array),
            isRight = dir < 0,
            arrLength = isArr ? array.length : 0,
            view = getView(0, arrLength, this.__views__),
            start = view.start,
            end = view.end,
            length = end - start,
            index = isRight ? end : (start - 1),
            iteratees = this.__iteratees__,
            iterLength = iteratees.length,
            resIndex = 0,
            takeCount = nativeMin(length, this.__takeCount__);
        if (!isArr || arrLength < LARGE_ARRAY_SIZE || (arrLength == length && takeCount == length)) {
          return baseWrapperValue(array, this.__actions__);
        }
        var result = [];
        outer: while (length-- && resIndex < takeCount) {
          index += dir;
          var iterIndex = -1,
              value = array[index];
          while (++iterIndex < iterLength) {
            var data = iteratees[iterIndex],
                iteratee = data.iteratee,
                type = data.type,
                computed = iteratee(value);
            if (type == LAZY_MAP_FLAG) {
              value = computed;
            } else if (!computed) {
              if (type == LAZY_FILTER_FLAG) {
                continue outer;
              } else {
                break outer;
              }
            }
          }
          result[resIndex++] = value;
        }
        return result;
      }
      LazyWrapper.prototype = baseCreate(baseLodash.prototype);
      LazyWrapper.prototype.constructor = LazyWrapper;
      function Hash() {}
      function hashDelete(hash, key) {
        return hashHas(hash, key) && delete hash[key];
      }
      function hashGet(hash, key) {
        if (nativeCreate) {
          var result = hash[key];
          return result === HASH_UNDEFINED ? undefined : result;
        }
        return hasOwnProperty.call(hash, key) ? hash[key] : undefined;
      }
      function hashHas(hash, key) {
        return nativeCreate ? hash[key] !== undefined : hasOwnProperty.call(hash, key);
      }
      function hashSet(hash, key, value) {
        hash[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
      }
      Hash.prototype = nativeCreate ? nativeCreate(null) : objectProto;
      function MapCache(values) {
        var index = -1,
            length = values ? values.length : 0;
        this.clear();
        while (++index < length) {
          var entry = values[index];
          this.set(entry[0], entry[1]);
        }
      }
      function mapClear() {
        this.__data__ = {
          'hash': new Hash,
          'map': Map ? new Map : [],
          'string': new Hash
        };
      }
      function mapDelete(key) {
        var data = this.__data__;
        if (isKeyable(key)) {
          return hashDelete(typeof key == 'string' ? data.string : data.hash, key);
        }
        return Map ? data.map['delete'](key) : assocDelete(data.map, key);
      }
      function mapGet(key) {
        var data = this.__data__;
        if (isKeyable(key)) {
          return hashGet(typeof key == 'string' ? data.string : data.hash, key);
        }
        return Map ? data.map.get(key) : assocGet(data.map, key);
      }
      function mapHas(key) {
        var data = this.__data__;
        if (isKeyable(key)) {
          return hashHas(typeof key == 'string' ? data.string : data.hash, key);
        }
        return Map ? data.map.has(key) : assocHas(data.map, key);
      }
      function mapSet(key, value) {
        var data = this.__data__;
        if (isKeyable(key)) {
          hashSet(typeof key == 'string' ? data.string : data.hash, key, value);
        } else if (Map) {
          data.map.set(key, value);
        } else {
          assocSet(data.map, key, value);
        }
        return this;
      }
      MapCache.prototype.clear = mapClear;
      MapCache.prototype['delete'] = mapDelete;
      MapCache.prototype.get = mapGet;
      MapCache.prototype.has = mapHas;
      MapCache.prototype.set = mapSet;
      function SetCache(values) {
        var index = -1,
            length = values ? values.length : 0;
        this.__data__ = new MapCache;
        while (++index < length) {
          this.push(values[index]);
        }
      }
      function cacheHas(cache, value) {
        var map = cache.__data__;
        if (isKeyable(value)) {
          var data = map.__data__,
              hash = typeof value == 'string' ? data.string : data.hash;
          return hash[value] === HASH_UNDEFINED;
        }
        return map.has(value);
      }
      function cachePush(value) {
        var map = this.__data__;
        if (isKeyable(value)) {
          var data = map.__data__,
              hash = typeof value == 'string' ? data.string : data.hash;
          hash[value] = HASH_UNDEFINED;
        } else {
          map.set(value, HASH_UNDEFINED);
        }
      }
      SetCache.prototype.push = cachePush;
      function Stack(values) {
        var index = -1,
            length = values ? values.length : 0;
        this.clear();
        while (++index < length) {
          var entry = values[index];
          this.set(entry[0], entry[1]);
        }
      }
      function stackClear() {
        this.__data__ = {
          'array': [],
          'map': null
        };
      }
      function stackDelete(key) {
        var data = this.__data__,
            array = data.array;
        return array ? assocDelete(array, key) : data.map['delete'](key);
      }
      function stackGet(key) {
        var data = this.__data__,
            array = data.array;
        return array ? assocGet(array, key) : data.map.get(key);
      }
      function stackHas(key) {
        var data = this.__data__,
            array = data.array;
        return array ? assocHas(array, key) : data.map.has(key);
      }
      function stackSet(key, value) {
        var data = this.__data__,
            array = data.array;
        if (array) {
          if (array.length < (LARGE_ARRAY_SIZE - 1)) {
            assocSet(array, key, value);
          } else {
            data.array = null;
            data.map = new MapCache(array);
          }
        }
        var map = data.map;
        if (map) {
          map.set(key, value);
        }
        return this;
      }
      Stack.prototype.clear = stackClear;
      Stack.prototype['delete'] = stackDelete;
      Stack.prototype.get = stackGet;
      Stack.prototype.has = stackHas;
      Stack.prototype.set = stackSet;
      function assocDelete(array, key) {
        var index = assocIndexOf(array, key);
        if (index < 0) {
          return false;
        }
        var lastIndex = array.length - 1;
        if (index == lastIndex) {
          array.pop();
        } else {
          splice.call(array, index, 1);
        }
        return true;
      }
      function assocGet(array, key) {
        var index = assocIndexOf(array, key);
        return index < 0 ? undefined : array[index][1];
      }
      function assocHas(array, key) {
        return assocIndexOf(array, key) > -1;
      }
      function assocIndexOf(array, key) {
        var length = array.length;
        while (length--) {
          if (eq(array[length][0], key)) {
            return length;
          }
        }
        return -1;
      }
      function assocSet(array, key, value) {
        var index = assocIndexOf(array, key);
        if (index < 0) {
          array.push([key, value]);
        } else {
          array[index][1] = value;
        }
      }
      function assignInDefaults(objValue, srcValue, key, object) {
        if (objValue === undefined || (eq(objValue, objectProto[key]) && !hasOwnProperty.call(object, key))) {
          return srcValue;
        }
        return objValue;
      }
      function assignMergeValue(object, key, value) {
        if ((value !== undefined && !eq(object[key], value)) || (typeof key == 'number' && value === undefined && !(key in object))) {
          object[key] = value;
        }
      }
      function assignValue(object, key, value) {
        var objValue = object[key];
        if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) || (value === undefined && !(key in object))) {
          object[key] = value;
        }
      }
      function baseAggregator(collection, setter, iteratee, accumulator) {
        baseEach(collection, function(value, key, collection) {
          setter(accumulator, value, iteratee(value), collection);
        });
        return accumulator;
      }
      function baseAssign(object, source) {
        return object && copyObject(source, keys(source), object);
      }
      function baseAt(object, paths) {
        var index = -1,
            isNil = object == null,
            length = paths.length,
            result = Array(length);
        while (++index < length) {
          result[index] = isNil ? undefined : get(object, paths[index]);
        }
        return result;
      }
      function baseClamp(number, lower, upper) {
        if (number === number) {
          if (upper !== undefined) {
            number = number <= upper ? number : upper;
          }
          if (lower !== undefined) {
            number = number >= lower ? number : lower;
          }
        }
        return number;
      }
      function baseClone(value, isDeep, isFull, customizer, key, object, stack) {
        var result;
        if (customizer) {
          result = object ? customizer(value, key, object, stack) : customizer(value);
        }
        if (result !== undefined) {
          return result;
        }
        if (!isObject(value)) {
          return value;
        }
        var isArr = isArray(value);
        if (isArr) {
          result = initCloneArray(value);
          if (!isDeep) {
            return copyArray(value, result);
          }
        } else {
          var tag = getTag(value),
              isFunc = tag == funcTag || tag == genTag;
          if (isBuffer(value)) {
            return cloneBuffer(value, isDeep);
          }
          if (tag == objectTag || tag == argsTag || (isFunc && !object)) {
            if (isHostObject(value)) {
              return object ? value : {};
            }
            result = initCloneObject(isFunc ? {} : value);
            if (!isDeep) {
              return copySymbols(value, baseAssign(result, value));
            }
          } else {
            if (!cloneableTags[tag]) {
              return object ? value : {};
            }
            result = initCloneByTag(value, tag, baseClone, isDeep);
          }
        }
        stack || (stack = new Stack);
        var stacked = stack.get(value);
        if (stacked) {
          return stacked;
        }
        stack.set(value, result);
        if (!isArr) {
          var props = isFull ? getAllKeys(value) : keys(value);
        }
        arrayEach(props || value, function(subValue, key) {
          if (props) {
            key = subValue;
            subValue = value[key];
          }
          assignValue(result, key, baseClone(subValue, isDeep, isFull, customizer, key, value, stack));
        });
        return result;
      }
      function baseConforms(source) {
        var props = keys(source),
            length = props.length;
        return function(object) {
          if (object == null) {
            return !length;
          }
          var index = length;
          while (index--) {
            var key = props[index],
                predicate = source[key],
                value = object[key];
            if ((value === undefined && !(key in Object(object))) || !predicate(value)) {
              return false;
            }
          }
          return true;
        };
      }
      function baseCreate(proto) {
        return isObject(proto) ? objectCreate(proto) : {};
      }
      function baseDelay(func, wait, args) {
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        return setTimeout(function() {
          func.apply(undefined, args);
        }, wait);
      }
      function baseDifference(array, values, iteratee, comparator) {
        var index = -1,
            includes = arrayIncludes,
            isCommon = true,
            length = array.length,
            result = [],
            valuesLength = values.length;
        if (!length) {
          return result;
        }
        if (iteratee) {
          values = arrayMap(values, baseUnary(iteratee));
        }
        if (comparator) {
          includes = arrayIncludesWith;
          isCommon = false;
        } else if (values.length >= LARGE_ARRAY_SIZE) {
          includes = cacheHas;
          isCommon = false;
          values = new SetCache(values);
        }
        outer: while (++index < length) {
          var value = array[index],
              computed = iteratee ? iteratee(value) : value;
          if (isCommon && computed === computed) {
            var valuesIndex = valuesLength;
            while (valuesIndex--) {
              if (values[valuesIndex] === computed) {
                continue outer;
              }
            }
            result.push(value);
          } else if (!includes(values, computed, comparator)) {
            result.push(value);
          }
        }
        return result;
      }
      var baseEach = createBaseEach(baseForOwn);
      var baseEachRight = createBaseEach(baseForOwnRight, true);
      function baseEvery(collection, predicate) {
        var result = true;
        baseEach(collection, function(value, index, collection) {
          result = !!predicate(value, index, collection);
          return result;
        });
        return result;
      }
      function baseFill(array, value, start, end) {
        var length = array.length;
        start = toInteger(start);
        if (start < 0) {
          start = -start > length ? 0 : (length + start);
        }
        end = (end === undefined || end > length) ? length : toInteger(end);
        if (end < 0) {
          end += length;
        }
        end = start > end ? 0 : toLength(end);
        while (start < end) {
          array[start++] = value;
        }
        return array;
      }
      function baseFilter(collection, predicate) {
        var result = [];
        baseEach(collection, function(value, index, collection) {
          if (predicate(value, index, collection)) {
            result.push(value);
          }
        });
        return result;
      }
      function baseFlatten(array, depth, predicate, isStrict, result) {
        var index = -1,
            length = array.length;
        predicate || (predicate = isFlattenable);
        result || (result = []);
        while (++index < length) {
          var value = array[index];
          if (depth > 0 && predicate(value)) {
            if (depth > 1) {
              baseFlatten(value, depth - 1, predicate, isStrict, result);
            } else {
              arrayPush(result, value);
            }
          } else if (!isStrict) {
            result[result.length] = value;
          }
        }
        return result;
      }
      var baseFor = createBaseFor();
      var baseForRight = createBaseFor(true);
      function baseForOwn(object, iteratee) {
        return object && baseFor(object, iteratee, keys);
      }
      function baseForOwnRight(object, iteratee) {
        return object && baseForRight(object, iteratee, keys);
      }
      function baseFunctions(object, props) {
        return arrayFilter(props, function(key) {
          return isFunction(object[key]);
        });
      }
      function baseGet(object, path) {
        path = isKey(path, object) ? [path] : castPath(path);
        var index = 0,
            length = path.length;
        while (object != null && index < length) {
          object = object[path[index++]];
        }
        return (index && index == length) ? object : undefined;
      }
      function baseGetAllKeys(object, keysFunc, symbolsFunc) {
        var result = keysFunc(object);
        return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
      }
      function baseHas(object, key) {
        return hasOwnProperty.call(object, key) || (typeof object == 'object' && key in object && getPrototype(object) === null);
      }
      function baseHasIn(object, key) {
        return key in Object(object);
      }
      function baseInRange(number, start, end) {
        return number >= nativeMin(start, end) && number < nativeMax(start, end);
      }
      function baseIntersection(arrays, iteratee, comparator) {
        var includes = comparator ? arrayIncludesWith : arrayIncludes,
            length = arrays[0].length,
            othLength = arrays.length,
            othIndex = othLength,
            caches = Array(othLength),
            maxLength = Infinity,
            result = [];
        while (othIndex--) {
          var array = arrays[othIndex];
          if (othIndex && iteratee) {
            array = arrayMap(array, baseUnary(iteratee));
          }
          maxLength = nativeMin(array.length, maxLength);
          caches[othIndex] = !comparator && (iteratee || (length >= 120 && array.length >= 120)) ? new SetCache(othIndex && array) : undefined;
        }
        array = arrays[0];
        var index = -1,
            seen = caches[0];
        outer: while (++index < length && result.length < maxLength) {
          var value = array[index],
              computed = iteratee ? iteratee(value) : value;
          if (!(seen ? cacheHas(seen, computed) : includes(result, computed, comparator))) {
            othIndex = othLength;
            while (--othIndex) {
              var cache = caches[othIndex];
              if (!(cache ? cacheHas(cache, computed) : includes(arrays[othIndex], computed, comparator))) {
                continue outer;
              }
            }
            if (seen) {
              seen.push(computed);
            }
            result.push(value);
          }
        }
        return result;
      }
      function baseInverter(object, setter, iteratee, accumulator) {
        baseForOwn(object, function(value, key, object) {
          setter(accumulator, iteratee(value), key, object);
        });
        return accumulator;
      }
      function baseInvoke(object, path, args) {
        if (!isKey(path, object)) {
          path = castPath(path);
          object = parent(object, path);
          path = last(path);
        }
        var func = object == null ? object : object[path];
        return func == null ? undefined : apply(func, object, args);
      }
      function baseIsEqual(value, other, customizer, bitmask, stack) {
        if (value === other) {
          return true;
        }
        if (value == null || other == null || (!isObject(value) && !isObjectLike(other))) {
          return value !== value && other !== other;
        }
        return baseIsEqualDeep(value, other, baseIsEqual, customizer, bitmask, stack);
      }
      function baseIsEqualDeep(object, other, equalFunc, customizer, bitmask, stack) {
        var objIsArr = isArray(object),
            othIsArr = isArray(other),
            objTag = arrayTag,
            othTag = arrayTag;
        if (!objIsArr) {
          objTag = getTag(object);
          objTag = objTag == argsTag ? objectTag : objTag;
        }
        if (!othIsArr) {
          othTag = getTag(other);
          othTag = othTag == argsTag ? objectTag : othTag;
        }
        var objIsObj = objTag == objectTag && !isHostObject(object),
            othIsObj = othTag == objectTag && !isHostObject(other),
            isSameTag = objTag == othTag;
        if (isSameTag && !objIsObj) {
          stack || (stack = new Stack);
          return (objIsArr || isTypedArray(object)) ? equalArrays(object, other, equalFunc, customizer, bitmask, stack) : equalByTag(object, other, objTag, equalFunc, customizer, bitmask, stack);
        }
        if (!(bitmask & PARTIAL_COMPARE_FLAG)) {
          var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
              othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');
          if (objIsWrapped || othIsWrapped) {
            var objUnwrapped = objIsWrapped ? object.value() : object,
                othUnwrapped = othIsWrapped ? other.value() : other;
            stack || (stack = new Stack);
            return equalFunc(objUnwrapped, othUnwrapped, customizer, bitmask, stack);
          }
        }
        if (!isSameTag) {
          return false;
        }
        stack || (stack = new Stack);
        return equalObjects(object, other, equalFunc, customizer, bitmask, stack);
      }
      function baseIsMatch(object, source, matchData, customizer) {
        var index = matchData.length,
            length = index,
            noCustomizer = !customizer;
        if (object == null) {
          return !length;
        }
        object = Object(object);
        while (index--) {
          var data = matchData[index];
          if ((noCustomizer && data[2]) ? data[1] !== object[data[0]] : !(data[0] in object)) {
            return false;
          }
        }
        while (++index < length) {
          data = matchData[index];
          var key = data[0],
              objValue = object[key],
              srcValue = data[1];
          if (noCustomizer && data[2]) {
            if (objValue === undefined && !(key in object)) {
              return false;
            }
          } else {
            var stack = new Stack;
            if (customizer) {
              var result = customizer(objValue, srcValue, key, object, source, stack);
            }
            if (!(result === undefined ? baseIsEqual(srcValue, objValue, customizer, UNORDERED_COMPARE_FLAG | PARTIAL_COMPARE_FLAG, stack) : result)) {
              return false;
            }
          }
        }
        return true;
      }
      function baseIteratee(value) {
        if (typeof value == 'function') {
          return value;
        }
        if (value == null) {
          return identity;
        }
        if (typeof value == 'object') {
          return isArray(value) ? baseMatchesProperty(value[0], value[1]) : baseMatches(value);
        }
        return property(value);
      }
      function baseKeys(object) {
        return nativeKeys(Object(object));
      }
      function baseKeysIn(object) {
        object = object == null ? object : Object(object);
        var result = [];
        for (var key in object) {
          result.push(key);
        }
        return result;
      }
      if (enumerate && !propertyIsEnumerable.call({'valueOf': 1}, 'valueOf')) {
        baseKeysIn = function(object) {
          return iteratorToArray(enumerate(object));
        };
      }
      function baseMap(collection, iteratee) {
        var index = -1,
            result = isArrayLike(collection) ? Array(collection.length) : [];
        baseEach(collection, function(value, key, collection) {
          result[++index] = iteratee(value, key, collection);
        });
        return result;
      }
      function baseMatches(source) {
        var matchData = getMatchData(source);
        if (matchData.length == 1 && matchData[0][2]) {
          return matchesStrictComparable(matchData[0][0], matchData[0][1]);
        }
        return function(object) {
          return object === source || baseIsMatch(object, source, matchData);
        };
      }
      function baseMatchesProperty(path, srcValue) {
        if (isKey(path) && isStrictComparable(srcValue)) {
          return matchesStrictComparable(path, srcValue);
        }
        return function(object) {
          var objValue = get(object, path);
          return (objValue === undefined && objValue === srcValue) ? hasIn(object, path) : baseIsEqual(srcValue, objValue, undefined, UNORDERED_COMPARE_FLAG | PARTIAL_COMPARE_FLAG);
        };
      }
      function baseMerge(object, source, srcIndex, customizer, stack) {
        if (object === source) {
          return;
        }
        if (!(isArray(source) || isTypedArray(source))) {
          var props = keysIn(source);
        }
        arrayEach(props || source, function(srcValue, key) {
          if (props) {
            key = srcValue;
            srcValue = source[key];
          }
          if (isObject(srcValue)) {
            stack || (stack = new Stack);
            baseMergeDeep(object, source, key, srcIndex, baseMerge, customizer, stack);
          } else {
            var newValue = customizer ? customizer(object[key], srcValue, (key + ''), object, source, stack) : undefined;
            if (newValue === undefined) {
              newValue = srcValue;
            }
            assignMergeValue(object, key, newValue);
          }
        });
      }
      function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
        var objValue = object[key],
            srcValue = source[key],
            stacked = stack.get(srcValue);
        if (stacked) {
          assignMergeValue(object, key, stacked);
          return;
        }
        var newValue = customizer ? customizer(objValue, srcValue, (key + ''), object, source, stack) : undefined;
        var isCommon = newValue === undefined;
        if (isCommon) {
          newValue = srcValue;
          if (isArray(srcValue) || isTypedArray(srcValue)) {
            if (isArray(objValue)) {
              newValue = objValue;
            } else if (isArrayLikeObject(objValue)) {
              newValue = copyArray(objValue);
            } else {
              isCommon = false;
              newValue = baseClone(srcValue, true);
            }
          } else if (isPlainObject(srcValue) || isArguments(srcValue)) {
            if (isArguments(objValue)) {
              newValue = toPlainObject(objValue);
            } else if (!isObject(objValue) || (srcIndex && isFunction(objValue))) {
              isCommon = false;
              newValue = baseClone(srcValue, true);
            } else {
              newValue = objValue;
            }
          } else {
            isCommon = false;
          }
        }
        stack.set(srcValue, newValue);
        if (isCommon) {
          mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
        }
        stack['delete'](srcValue);
        assignMergeValue(object, key, newValue);
      }
      function baseNth(array, n) {
        var length = array.length;
        if (!length) {
          return;
        }
        n += n < 0 ? length : 0;
        return isIndex(n, length) ? array[n] : undefined;
      }
      function baseOrderBy(collection, iteratees, orders) {
        var index = -1;
        iteratees = arrayMap(iteratees.length ? iteratees : [identity], baseUnary(getIteratee()));
        var result = baseMap(collection, function(value, key, collection) {
          var criteria = arrayMap(iteratees, function(iteratee) {
            return iteratee(value);
          });
          return {
            'criteria': criteria,
            'index': ++index,
            'value': value
          };
        });
        return baseSortBy(result, function(object, other) {
          return compareMultiple(object, other, orders);
        });
      }
      function basePick(object, props) {
        object = Object(object);
        return arrayReduce(props, function(result, key) {
          if (key in object) {
            result[key] = object[key];
          }
          return result;
        }, {});
      }
      function basePickBy(object, predicate) {
        var index = -1,
            props = getAllKeysIn(object),
            length = props.length,
            result = {};
        while (++index < length) {
          var key = props[index],
              value = object[key];
          if (predicate(value, key)) {
            result[key] = value;
          }
        }
        return result;
      }
      function baseProperty(key) {
        return function(object) {
          return object == null ? undefined : object[key];
        };
      }
      function basePropertyDeep(path) {
        return function(object) {
          return baseGet(object, path);
        };
      }
      function basePullAll(array, values, iteratee, comparator) {
        var indexOf = comparator ? baseIndexOfWith : baseIndexOf,
            index = -1,
            length = values.length,
            seen = array;
        if (iteratee) {
          seen = arrayMap(array, baseUnary(iteratee));
        }
        while (++index < length) {
          var fromIndex = 0,
              value = values[index],
              computed = iteratee ? iteratee(value) : value;
          while ((fromIndex = indexOf(seen, computed, fromIndex, comparator)) > -1) {
            if (seen !== array) {
              splice.call(seen, fromIndex, 1);
            }
            splice.call(array, fromIndex, 1);
          }
        }
        return array;
      }
      function basePullAt(array, indexes) {
        var length = array ? indexes.length : 0,
            lastIndex = length - 1;
        while (length--) {
          var index = indexes[length];
          if (lastIndex == length || index != previous) {
            var previous = index;
            if (isIndex(index)) {
              splice.call(array, index, 1);
            } else if (!isKey(index, array)) {
              var path = castPath(index),
                  object = parent(array, path);
              if (object != null) {
                delete object[last(path)];
              }
            } else {
              delete array[index];
            }
          }
        }
        return array;
      }
      function baseRandom(lower, upper) {
        return lower + nativeFloor(nativeRandom() * (upper - lower + 1));
      }
      function baseRange(start, end, step, fromRight) {
        var index = -1,
            length = nativeMax(nativeCeil((end - start) / (step || 1)), 0),
            result = Array(length);
        while (length--) {
          result[fromRight ? length : ++index] = start;
          start += step;
        }
        return result;
      }
      function baseRepeat(string, n) {
        var result = '';
        if (!string || n < 1 || n > MAX_SAFE_INTEGER) {
          return result;
        }
        do {
          if (n % 2) {
            result += string;
          }
          n = nativeFloor(n / 2);
          if (n) {
            string += string;
          }
        } while (n);
        return result;
      }
      function baseSet(object, path, value, customizer) {
        path = isKey(path, object) ? [path] : castPath(path);
        var index = -1,
            length = path.length,
            lastIndex = length - 1,
            nested = object;
        while (nested != null && ++index < length) {
          var key = path[index];
          if (isObject(nested)) {
            var newValue = value;
            if (index != lastIndex) {
              var objValue = nested[key];
              newValue = customizer ? customizer(objValue, key, nested) : undefined;
              if (newValue === undefined) {
                newValue = objValue == null ? (isIndex(path[index + 1]) ? [] : {}) : objValue;
              }
            }
            assignValue(nested, key, newValue);
          }
          nested = nested[key];
        }
        return object;
      }
      var baseSetData = !metaMap ? identity : function(func, data) {
        metaMap.set(func, data);
        return func;
      };
      function baseSlice(array, start, end) {
        var index = -1,
            length = array.length;
        if (start < 0) {
          start = -start > length ? 0 : (length + start);
        }
        end = end > length ? length : end;
        if (end < 0) {
          end += length;
        }
        length = start > end ? 0 : ((end - start) >>> 0);
        start >>>= 0;
        var result = Array(length);
        while (++index < length) {
          result[index] = array[index + start];
        }
        return result;
      }
      function baseSome(collection, predicate) {
        var result;
        baseEach(collection, function(value, index, collection) {
          result = predicate(value, index, collection);
          return !result;
        });
        return !!result;
      }
      function baseSortedIndex(array, value, retHighest) {
        var low = 0,
            high = array ? array.length : low;
        if (typeof value == 'number' && value === value && high <= HALF_MAX_ARRAY_LENGTH) {
          while (low < high) {
            var mid = (low + high) >>> 1,
                computed = array[mid];
            if ((retHighest ? (computed <= value) : (computed < value)) && computed !== null) {
              low = mid + 1;
            } else {
              high = mid;
            }
          }
          return high;
        }
        return baseSortedIndexBy(array, value, identity, retHighest);
      }
      function baseSortedIndexBy(array, value, iteratee, retHighest) {
        value = iteratee(value);
        var low = 0,
            high = array ? array.length : 0,
            valIsNaN = value !== value,
            valIsNull = value === null,
            valIsUndef = value === undefined;
        while (low < high) {
          var mid = nativeFloor((low + high) / 2),
              computed = iteratee(array[mid]),
              isDef = computed !== undefined,
              isReflexive = computed === computed;
          if (valIsNaN) {
            var setLow = isReflexive || retHighest;
          } else if (valIsNull) {
            setLow = isReflexive && isDef && (retHighest || computed != null);
          } else if (valIsUndef) {
            setLow = isReflexive && (retHighest || isDef);
          } else if (computed == null) {
            setLow = false;
          } else {
            setLow = retHighest ? (computed <= value) : (computed < value);
          }
          if (setLow) {
            low = mid + 1;
          } else {
            high = mid;
          }
        }
        return nativeMin(high, MAX_ARRAY_INDEX);
      }
      function baseSortedUniq(array) {
        return baseSortedUniqBy(array);
      }
      function baseSortedUniqBy(array, iteratee) {
        var index = 0,
            length = array.length,
            value = array[0],
            computed = iteratee ? iteratee(value) : value,
            seen = computed,
            resIndex = 1,
            result = [value];
        while (++index < length) {
          value = array[index], computed = iteratee ? iteratee(value) : value;
          if (!eq(computed, seen)) {
            seen = computed;
            result[resIndex++] = value;
          }
        }
        return result;
      }
      function baseUniq(array, iteratee, comparator) {
        var index = -1,
            includes = arrayIncludes,
            length = array.length,
            isCommon = true,
            result = [],
            seen = result;
        if (comparator) {
          isCommon = false;
          includes = arrayIncludesWith;
        } else if (length >= LARGE_ARRAY_SIZE) {
          var set = iteratee ? null : createSet(array);
          if (set) {
            return setToArray(set);
          }
          isCommon = false;
          includes = cacheHas;
          seen = new SetCache;
        } else {
          seen = iteratee ? [] : result;
        }
        outer: while (++index < length) {
          var value = array[index],
              computed = iteratee ? iteratee(value) : value;
          if (isCommon && computed === computed) {
            var seenIndex = seen.length;
            while (seenIndex--) {
              if (seen[seenIndex] === computed) {
                continue outer;
              }
            }
            if (iteratee) {
              seen.push(computed);
            }
            result.push(value);
          } else if (!includes(seen, computed, comparator)) {
            if (seen !== result) {
              seen.push(computed);
            }
            result.push(value);
          }
        }
        return result;
      }
      function baseUnset(object, path) {
        path = isKey(path, object) ? [path] : castPath(path);
        object = parent(object, path);
        var key = last(path);
        return (object != null && has(object, key)) ? delete object[key] : true;
      }
      function baseUpdate(object, path, updater, customizer) {
        return baseSet(object, path, updater(baseGet(object, path)), customizer);
      }
      function baseWhile(array, predicate, isDrop, fromRight) {
        var length = array.length,
            index = fromRight ? length : -1;
        while ((fromRight ? index-- : ++index < length) && predicate(array[index], index, array)) {}
        return isDrop ? baseSlice(array, (fromRight ? 0 : index), (fromRight ? index + 1 : length)) : baseSlice(array, (fromRight ? index + 1 : 0), (fromRight ? length : index));
      }
      function baseWrapperValue(value, actions) {
        var result = value;
        if (result instanceof LazyWrapper) {
          result = result.value();
        }
        return arrayReduce(actions, function(result, action) {
          return action.func.apply(action.thisArg, arrayPush([result], action.args));
        }, result);
      }
      function baseXor(arrays, iteratee, comparator) {
        var index = -1,
            length = arrays.length;
        while (++index < length) {
          var result = result ? arrayPush(baseDifference(result, arrays[index], iteratee, comparator), baseDifference(arrays[index], result, iteratee, comparator)) : arrays[index];
        }
        return (result && result.length) ? baseUniq(result, iteratee, comparator) : [];
      }
      function baseZipObject(props, values, assignFunc) {
        var index = -1,
            length = props.length,
            valsLength = values.length,
            result = {};
        while (++index < length) {
          var value = index < valsLength ? values[index] : undefined;
          assignFunc(result, props[index], value);
        }
        return result;
      }
      function castArrayLikeObject(value) {
        return isArrayLikeObject(value) ? value : [];
      }
      function castFunction(value) {
        return typeof value == 'function' ? value : identity;
      }
      function castPath(value) {
        return isArray(value) ? value : stringToPath(value);
      }
      function castSlice(array, start, end) {
        var length = array.length;
        end = end === undefined ? length : end;
        return (!start && end >= length) ? array : baseSlice(array, start, end);
      }
      function cloneBuffer(buffer, isDeep) {
        if (isDeep) {
          return buffer.slice();
        }
        var result = new buffer.constructor(buffer.length);
        buffer.copy(result);
        return result;
      }
      function cloneArrayBuffer(arrayBuffer) {
        var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
        new Uint8Array(result).set(new Uint8Array(arrayBuffer));
        return result;
      }
      function cloneDataView(dataView, isDeep) {
        var buffer = isDeep ? cloneArrayBuffer(dataView.buffer) : dataView.buffer;
        return new dataView.constructor(buffer, dataView.byteOffset, dataView.byteLength);
      }
      function cloneMap(map, isDeep, cloneFunc) {
        var array = isDeep ? cloneFunc(mapToArray(map), true) : mapToArray(map);
        return arrayReduce(array, addMapEntry, new map.constructor);
      }
      function cloneRegExp(regexp) {
        var result = new regexp.constructor(regexp.source, reFlags.exec(regexp));
        result.lastIndex = regexp.lastIndex;
        return result;
      }
      function cloneSet(set, isDeep, cloneFunc) {
        var array = isDeep ? cloneFunc(setToArray(set), true) : setToArray(set);
        return arrayReduce(array, addSetEntry, new set.constructor);
      }
      function cloneSymbol(symbol) {
        return symbolValueOf ? Object(symbolValueOf.call(symbol)) : {};
      }
      function cloneTypedArray(typedArray, isDeep) {
        var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
        return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
      }
      function composeArgs(args, partials, holders, isCurried) {
        var argsIndex = -1,
            argsLength = args.length,
            holdersLength = holders.length,
            leftIndex = -1,
            leftLength = partials.length,
            rangeLength = nativeMax(argsLength - holdersLength, 0),
            result = Array(leftLength + rangeLength),
            isUncurried = !isCurried;
        while (++leftIndex < leftLength) {
          result[leftIndex] = partials[leftIndex];
        }
        while (++argsIndex < holdersLength) {
          if (isUncurried || argsIndex < argsLength) {
            result[holders[argsIndex]] = args[argsIndex];
          }
        }
        while (rangeLength--) {
          result[leftIndex++] = args[argsIndex++];
        }
        return result;
      }
      function composeArgsRight(args, partials, holders, isCurried) {
        var argsIndex = -1,
            argsLength = args.length,
            holdersIndex = -1,
            holdersLength = holders.length,
            rightIndex = -1,
            rightLength = partials.length,
            rangeLength = nativeMax(argsLength - holdersLength, 0),
            result = Array(rangeLength + rightLength),
            isUncurried = !isCurried;
        while (++argsIndex < rangeLength) {
          result[argsIndex] = args[argsIndex];
        }
        var offset = argsIndex;
        while (++rightIndex < rightLength) {
          result[offset + rightIndex] = partials[rightIndex];
        }
        while (++holdersIndex < holdersLength) {
          if (isUncurried || argsIndex < argsLength) {
            result[offset + holders[holdersIndex]] = args[argsIndex++];
          }
        }
        return result;
      }
      function copyArray(source, array) {
        var index = -1,
            length = source.length;
        array || (array = Array(length));
        while (++index < length) {
          array[index] = source[index];
        }
        return array;
      }
      function copyObject(source, props, object, customizer) {
        object || (object = {});
        var index = -1,
            length = props.length;
        while (++index < length) {
          var key = props[index];
          var newValue = customizer ? customizer(object[key], source[key], key, object, source) : source[key];
          assignValue(object, key, newValue);
        }
        return object;
      }
      function copySymbols(source, object) {
        return copyObject(source, getSymbols(source), object);
      }
      function createAggregator(setter, initializer) {
        return function(collection, iteratee) {
          var func = isArray(collection) ? arrayAggregator : baseAggregator,
              accumulator = initializer ? initializer() : {};
          return func(collection, setter, getIteratee(iteratee), accumulator);
        };
      }
      function createAssigner(assigner) {
        return rest(function(object, sources) {
          var index = -1,
              length = sources.length,
              customizer = length > 1 ? sources[length - 1] : undefined,
              guard = length > 2 ? sources[2] : undefined;
          customizer = typeof customizer == 'function' ? (length--, customizer) : undefined;
          if (guard && isIterateeCall(sources[0], sources[1], guard)) {
            customizer = length < 3 ? undefined : customizer;
            length = 1;
          }
          object = Object(object);
          while (++index < length) {
            var source = sources[index];
            if (source) {
              assigner(object, source, index, customizer);
            }
          }
          return object;
        });
      }
      function createBaseEach(eachFunc, fromRight) {
        return function(collection, iteratee) {
          if (collection == null) {
            return collection;
          }
          if (!isArrayLike(collection)) {
            return eachFunc(collection, iteratee);
          }
          var length = collection.length,
              index = fromRight ? length : -1,
              iterable = Object(collection);
          while ((fromRight ? index-- : ++index < length)) {
            if (iteratee(iterable[index], index, iterable) === false) {
              break;
            }
          }
          return collection;
        };
      }
      function createBaseFor(fromRight) {
        return function(object, iteratee, keysFunc) {
          var index = -1,
              iterable = Object(object),
              props = keysFunc(object),
              length = props.length;
          while (length--) {
            var key = props[fromRight ? length : ++index];
            if (iteratee(iterable[key], key, iterable) === false) {
              break;
            }
          }
          return object;
        };
      }
      function createBaseWrapper(func, bitmask, thisArg) {
        var isBind = bitmask & BIND_FLAG,
            Ctor = createCtorWrapper(func);
        function wrapper() {
          var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
          return fn.apply(isBind ? thisArg : this, arguments);
        }
        return wrapper;
      }
      function createCaseFirst(methodName) {
        return function(string) {
          string = toString(string);
          var strSymbols = reHasComplexSymbol.test(string) ? stringToArray(string) : undefined;
          var chr = strSymbols ? strSymbols[0] : string.charAt(0);
          var trailing = strSymbols ? castSlice(strSymbols, 1).join('') : string.slice(1);
          return chr[methodName]() + trailing;
        };
      }
      function createCompounder(callback) {
        return function(string) {
          return arrayReduce(words(deburr(string).replace(reApos, '')), callback, '');
        };
      }
      function createCtorWrapper(Ctor) {
        return function() {
          var args = arguments;
          switch (args.length) {
            case 0:
              return new Ctor;
            case 1:
              return new Ctor(args[0]);
            case 2:
              return new Ctor(args[0], args[1]);
            case 3:
              return new Ctor(args[0], args[1], args[2]);
            case 4:
              return new Ctor(args[0], args[1], args[2], args[3]);
            case 5:
              return new Ctor(args[0], args[1], args[2], args[3], args[4]);
            case 6:
              return new Ctor(args[0], args[1], args[2], args[3], args[4], args[5]);
            case 7:
              return new Ctor(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
          }
          var thisBinding = baseCreate(Ctor.prototype),
              result = Ctor.apply(thisBinding, args);
          return isObject(result) ? result : thisBinding;
        };
      }
      function createCurryWrapper(func, bitmask, arity) {
        var Ctor = createCtorWrapper(func);
        function wrapper() {
          var length = arguments.length,
              args = Array(length),
              index = length,
              placeholder = getPlaceholder(wrapper);
          while (index--) {
            args[index] = arguments[index];
          }
          var holders = (length < 3 && args[0] !== placeholder && args[length - 1] !== placeholder) ? [] : replaceHolders(args, placeholder);
          length -= holders.length;
          if (length < arity) {
            return createRecurryWrapper(func, bitmask, createHybridWrapper, wrapper.placeholder, undefined, args, holders, undefined, undefined, arity - length);
          }
          var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
          return apply(fn, this, args);
        }
        return wrapper;
      }
      function createFlow(fromRight) {
        return rest(function(funcs) {
          funcs = baseFlatten(funcs, 1);
          var length = funcs.length,
              index = length,
              prereq = LodashWrapper.prototype.thru;
          if (fromRight) {
            funcs.reverse();
          }
          while (index--) {
            var func = funcs[index];
            if (typeof func != 'function') {
              throw new TypeError(FUNC_ERROR_TEXT);
            }
            if (prereq && !wrapper && getFuncName(func) == 'wrapper') {
              var wrapper = new LodashWrapper([], true);
            }
          }
          index = wrapper ? index : length;
          while (++index < length) {
            func = funcs[index];
            var funcName = getFuncName(func),
                data = funcName == 'wrapper' ? getData(func) : undefined;
            if (data && isLaziable(data[0]) && data[1] == (ARY_FLAG | CURRY_FLAG | PARTIAL_FLAG | REARG_FLAG) && !data[4].length && data[9] == 1) {
              wrapper = wrapper[getFuncName(data[0])].apply(wrapper, data[3]);
            } else {
              wrapper = (func.length == 1 && isLaziable(func)) ? wrapper[funcName]() : wrapper.thru(func);
            }
          }
          return function() {
            var args = arguments,
                value = args[0];
            if (wrapper && args.length == 1 && isArray(value) && value.length >= LARGE_ARRAY_SIZE) {
              return wrapper.plant(value).value();
            }
            var index = 0,
                result = length ? funcs[index].apply(this, args) : value;
            while (++index < length) {
              result = funcs[index].call(this, result);
            }
            return result;
          };
        });
      }
      function createHybridWrapper(func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity) {
        var isAry = bitmask & ARY_FLAG,
            isBind = bitmask & BIND_FLAG,
            isBindKey = bitmask & BIND_KEY_FLAG,
            isCurried = bitmask & (CURRY_FLAG | CURRY_RIGHT_FLAG),
            isFlip = bitmask & FLIP_FLAG,
            Ctor = isBindKey ? undefined : createCtorWrapper(func);
        function wrapper() {
          var length = arguments.length,
              index = length,
              args = Array(length);
          while (index--) {
            args[index] = arguments[index];
          }
          if (isCurried) {
            var placeholder = getPlaceholder(wrapper),
                holdersCount = countHolders(args, placeholder);
          }
          if (partials) {
            args = composeArgs(args, partials, holders, isCurried);
          }
          if (partialsRight) {
            args = composeArgsRight(args, partialsRight, holdersRight, isCurried);
          }
          length -= holdersCount;
          if (isCurried && length < arity) {
            var newHolders = replaceHolders(args, placeholder);
            return createRecurryWrapper(func, bitmask, createHybridWrapper, wrapper.placeholder, thisArg, args, newHolders, argPos, ary, arity - length);
          }
          var thisBinding = isBind ? thisArg : this,
              fn = isBindKey ? thisBinding[func] : func;
          length = args.length;
          if (argPos) {
            args = reorder(args, argPos);
          } else if (isFlip && length > 1) {
            args.reverse();
          }
          if (isAry && ary < length) {
            args.length = ary;
          }
          if (this && this !== root && this instanceof wrapper) {
            fn = Ctor || createCtorWrapper(fn);
          }
          return fn.apply(thisBinding, args);
        }
        return wrapper;
      }
      function createInverter(setter, toIteratee) {
        return function(object, iteratee) {
          return baseInverter(object, setter, toIteratee(iteratee), {});
        };
      }
      function createOver(arrayFunc) {
        return rest(function(iteratees) {
          iteratees = (iteratees.length == 1 && isArray(iteratees[0])) ? arrayMap(iteratees[0], baseUnary(getIteratee())) : arrayMap(baseFlatten(iteratees, 1, isFlattenableIteratee), baseUnary(getIteratee()));
          return rest(function(args) {
            var thisArg = this;
            return arrayFunc(iteratees, function(iteratee) {
              return apply(iteratee, thisArg, args);
            });
          });
        });
      }
      function createPadding(length, chars) {
        chars = chars === undefined ? ' ' : (chars + '');
        var charsLength = chars.length;
        if (charsLength < 2) {
          return charsLength ? baseRepeat(chars, length) : chars;
        }
        var result = baseRepeat(chars, nativeCeil(length / stringSize(chars)));
        return reHasComplexSymbol.test(chars) ? castSlice(stringToArray(result), 0, length).join('') : result.slice(0, length);
      }
      function createPartialWrapper(func, bitmask, thisArg, partials) {
        var isBind = bitmask & BIND_FLAG,
            Ctor = createCtorWrapper(func);
        function wrapper() {
          var argsIndex = -1,
              argsLength = arguments.length,
              leftIndex = -1,
              leftLength = partials.length,
              args = Array(leftLength + argsLength),
              fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
          while (++leftIndex < leftLength) {
            args[leftIndex] = partials[leftIndex];
          }
          while (argsLength--) {
            args[leftIndex++] = arguments[++argsIndex];
          }
          return apply(fn, isBind ? thisArg : this, args);
        }
        return wrapper;
      }
      function createRange(fromRight) {
        return function(start, end, step) {
          if (step && typeof step != 'number' && isIterateeCall(start, end, step)) {
            end = step = undefined;
          }
          start = toNumber(start);
          start = start === start ? start : 0;
          if (end === undefined) {
            end = start;
            start = 0;
          } else {
            end = toNumber(end) || 0;
          }
          step = step === undefined ? (start < end ? 1 : -1) : (toNumber(step) || 0);
          return baseRange(start, end, step, fromRight);
        };
      }
      function createRecurryWrapper(func, bitmask, wrapFunc, placeholder, thisArg, partials, holders, argPos, ary, arity) {
        var isCurry = bitmask & CURRY_FLAG,
            newHolders = isCurry ? holders : undefined,
            newHoldersRight = isCurry ? undefined : holders,
            newPartials = isCurry ? partials : undefined,
            newPartialsRight = isCurry ? undefined : partials;
        bitmask |= (isCurry ? PARTIAL_FLAG : PARTIAL_RIGHT_FLAG);
        bitmask &= ~(isCurry ? PARTIAL_RIGHT_FLAG : PARTIAL_FLAG);
        if (!(bitmask & CURRY_BOUND_FLAG)) {
          bitmask &= ~(BIND_FLAG | BIND_KEY_FLAG);
        }
        var newData = [func, bitmask, thisArg, newPartials, newHolders, newPartialsRight, newHoldersRight, argPos, ary, arity];
        var result = wrapFunc.apply(undefined, newData);
        if (isLaziable(func)) {
          setData(result, newData);
        }
        result.placeholder = placeholder;
        return result;
      }
      function createRound(methodName) {
        var func = Math[methodName];
        return function(number, precision) {
          number = toNumber(number);
          precision = toInteger(precision);
          if (precision) {
            var pair = (toString(number) + 'e').split('e'),
                value = func(pair[0] + 'e' + (+pair[1] + precision));
            pair = (toString(value) + 'e').split('e');
            return +(pair[0] + 'e' + (+pair[1] - precision));
          }
          return func(number);
        };
      }
      var createSet = !(Set && new Set([1, 2]).size === 2) ? noop : function(values) {
        return new Set(values);
      };
      function createWrapper(func, bitmask, thisArg, partials, holders, argPos, ary, arity) {
        var isBindKey = bitmask & BIND_KEY_FLAG;
        if (!isBindKey && typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        var length = partials ? partials.length : 0;
        if (!length) {
          bitmask &= ~(PARTIAL_FLAG | PARTIAL_RIGHT_FLAG);
          partials = holders = undefined;
        }
        ary = ary === undefined ? ary : nativeMax(toInteger(ary), 0);
        arity = arity === undefined ? arity : toInteger(arity);
        length -= holders ? holders.length : 0;
        if (bitmask & PARTIAL_RIGHT_FLAG) {
          var partialsRight = partials,
              holdersRight = holders;
          partials = holders = undefined;
        }
        var data = isBindKey ? undefined : getData(func);
        var newData = [func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity];
        if (data) {
          mergeData(newData, data);
        }
        func = newData[0];
        bitmask = newData[1];
        thisArg = newData[2];
        partials = newData[3];
        holders = newData[4];
        arity = newData[9] = newData[9] == null ? (isBindKey ? 0 : func.length) : nativeMax(newData[9] - length, 0);
        if (!arity && bitmask & (CURRY_FLAG | CURRY_RIGHT_FLAG)) {
          bitmask &= ~(CURRY_FLAG | CURRY_RIGHT_FLAG);
        }
        if (!bitmask || bitmask == BIND_FLAG) {
          var result = createBaseWrapper(func, bitmask, thisArg);
        } else if (bitmask == CURRY_FLAG || bitmask == CURRY_RIGHT_FLAG) {
          result = createCurryWrapper(func, bitmask, arity);
        } else if ((bitmask == PARTIAL_FLAG || bitmask == (BIND_FLAG | PARTIAL_FLAG)) && !holders.length) {
          result = createPartialWrapper(func, bitmask, thisArg, partials);
        } else {
          result = createHybridWrapper.apply(undefined, newData);
        }
        var setter = data ? baseSetData : setData;
        return setter(result, newData);
      }
      function equalArrays(array, other, equalFunc, customizer, bitmask, stack) {
        var index = -1,
            isPartial = bitmask & PARTIAL_COMPARE_FLAG,
            isUnordered = bitmask & UNORDERED_COMPARE_FLAG,
            arrLength = array.length,
            othLength = other.length;
        if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
          return false;
        }
        var stacked = stack.get(array);
        if (stacked) {
          return stacked == other;
        }
        var result = true;
        stack.set(array, other);
        while (++index < arrLength) {
          var arrValue = array[index],
              othValue = other[index];
          if (customizer) {
            var compared = isPartial ? customizer(othValue, arrValue, index, other, array, stack) : customizer(arrValue, othValue, index, array, other, stack);
          }
          if (compared !== undefined) {
            if (compared) {
              continue;
            }
            result = false;
            break;
          }
          if (isUnordered) {
            if (!arraySome(other, function(othValue) {
              return arrValue === othValue || equalFunc(arrValue, othValue, customizer, bitmask, stack);
            })) {
              result = false;
              break;
            }
          } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, customizer, bitmask, stack))) {
            result = false;
            break;
          }
        }
        stack['delete'](array);
        return result;
      }
      function equalByTag(object, other, tag, equalFunc, customizer, bitmask, stack) {
        switch (tag) {
          case dataViewTag:
            if ((object.byteLength != other.byteLength) || (object.byteOffset != other.byteOffset)) {
              return false;
            }
            object = object.buffer;
            other = other.buffer;
          case arrayBufferTag:
            if ((object.byteLength != other.byteLength) || !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
              return false;
            }
            return true;
          case boolTag:
          case dateTag:
            return +object == +other;
          case errorTag:
            return object.name == other.name && object.message == other.message;
          case numberTag:
            return (object != +object) ? other != +other : object == +other;
          case regexpTag:
          case stringTag:
            return object == (other + '');
          case mapTag:
            var convert = mapToArray;
          case setTag:
            var isPartial = bitmask & PARTIAL_COMPARE_FLAG;
            convert || (convert = setToArray);
            if (object.size != other.size && !isPartial) {
              return false;
            }
            var stacked = stack.get(object);
            if (stacked) {
              return stacked == other;
            }
            bitmask |= UNORDERED_COMPARE_FLAG;
            stack.set(object, other);
            return equalArrays(convert(object), convert(other), equalFunc, customizer, bitmask, stack);
          case symbolTag:
            if (symbolValueOf) {
              return symbolValueOf.call(object) == symbolValueOf.call(other);
            }
        }
        return false;
      }
      function equalObjects(object, other, equalFunc, customizer, bitmask, stack) {
        var isPartial = bitmask & PARTIAL_COMPARE_FLAG,
            objProps = keys(object),
            objLength = objProps.length,
            othProps = keys(other),
            othLength = othProps.length;
        if (objLength != othLength && !isPartial) {
          return false;
        }
        var index = objLength;
        while (index--) {
          var key = objProps[index];
          if (!(isPartial ? key in other : baseHas(other, key))) {
            return false;
          }
        }
        var stacked = stack.get(object);
        if (stacked) {
          return stacked == other;
        }
        var result = true;
        stack.set(object, other);
        var skipCtor = isPartial;
        while (++index < objLength) {
          key = objProps[index];
          var objValue = object[key],
              othValue = other[key];
          if (customizer) {
            var compared = isPartial ? customizer(othValue, objValue, key, other, object, stack) : customizer(objValue, othValue, key, object, other, stack);
          }
          if (!(compared === undefined ? (objValue === othValue || equalFunc(objValue, othValue, customizer, bitmask, stack)) : compared)) {
            result = false;
            break;
          }
          skipCtor || (skipCtor = key == 'constructor');
        }
        if (result && !skipCtor) {
          var objCtor = object.constructor,
              othCtor = other.constructor;
          if (objCtor != othCtor && ('constructor' in object && 'constructor' in other) && !(typeof objCtor == 'function' && objCtor instanceof objCtor && typeof othCtor == 'function' && othCtor instanceof othCtor)) {
            result = false;
          }
        }
        stack['delete'](object);
        return result;
      }
      function getAllKeys(object) {
        return baseGetAllKeys(object, keys, getSymbols);
      }
      function getAllKeysIn(object) {
        return baseGetAllKeys(object, keysIn, getSymbolsIn);
      }
      var getData = !metaMap ? noop : function(func) {
        return metaMap.get(func);
      };
      function getFuncName(func) {
        var result = (func.name + ''),
            array = realNames[result],
            length = hasOwnProperty.call(realNames, result) ? array.length : 0;
        while (length--) {
          var data = array[length],
              otherFunc = data.func;
          if (otherFunc == null || otherFunc == func) {
            return data.name;
          }
        }
        return result;
      }
      function getIteratee() {
        var result = lodash.iteratee || iteratee;
        result = result === iteratee ? baseIteratee : result;
        return arguments.length ? result(arguments[0], arguments[1]) : result;
      }
      var getLength = baseProperty('length');
      function getMatchData(object) {
        var result = toPairs(object),
            length = result.length;
        while (length--) {
          result[length][2] = isStrictComparable(result[length][1]);
        }
        return result;
      }
      function getNative(object, key) {
        var value = object[key];
        return isNative(value) ? value : undefined;
      }
      function getPlaceholder(func) {
        var object = hasOwnProperty.call(lodash, 'placeholder') ? lodash : func;
        return object.placeholder;
      }
      function getPrototype(value) {
        return nativeGetPrototype(Object(value));
      }
      function getSymbols(object) {
        return getOwnPropertySymbols(Object(object));
      }
      if (!getOwnPropertySymbols) {
        getSymbols = function() {
          return [];
        };
      }
      var getSymbolsIn = !getOwnPropertySymbols ? getSymbols : function(object) {
        var result = [];
        while (object) {
          arrayPush(result, getSymbols(object));
          object = getPrototype(object);
        }
        return result;
      };
      function getTag(value) {
        return objectToString.call(value);
      }
      if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) || (Map && getTag(new Map) != mapTag) || (Promise && getTag(Promise.resolve()) != promiseTag) || (Set && getTag(new Set) != setTag) || (WeakMap && getTag(new WeakMap) != weakMapTag)) {
        getTag = function(value) {
          var result = objectToString.call(value),
              Ctor = result == objectTag ? value.constructor : undefined,
              ctorString = Ctor ? toSource(Ctor) : undefined;
          if (ctorString) {
            switch (ctorString) {
              case dataViewCtorString:
                return dataViewTag;
              case mapCtorString:
                return mapTag;
              case promiseCtorString:
                return promiseTag;
              case setCtorString:
                return setTag;
              case weakMapCtorString:
                return weakMapTag;
            }
          }
          return result;
        };
      }
      function getView(start, end, transforms) {
        var index = -1,
            length = transforms.length;
        while (++index < length) {
          var data = transforms[index],
              size = data.size;
          switch (data.type) {
            case 'drop':
              start += size;
              break;
            case 'dropRight':
              end -= size;
              break;
            case 'take':
              end = nativeMin(end, start + size);
              break;
            case 'takeRight':
              start = nativeMax(start, end - size);
              break;
          }
        }
        return {
          'start': start,
          'end': end
        };
      }
      function hasPath(object, path, hasFunc) {
        path = isKey(path, object) ? [path] : castPath(path);
        var result,
            index = -1,
            length = path.length;
        while (++index < length) {
          var key = path[index];
          if (!(result = object != null && hasFunc(object, key))) {
            break;
          }
          object = object[key];
        }
        if (result) {
          return result;
        }
        var length = object ? object.length : 0;
        return !!length && isLength(length) && isIndex(key, length) && (isArray(object) || isString(object) || isArguments(object));
      }
      function initCloneArray(array) {
        var length = array.length,
            result = array.constructor(length);
        if (length && typeof array[0] == 'string' && hasOwnProperty.call(array, 'index')) {
          result.index = array.index;
          result.input = array.input;
        }
        return result;
      }
      function initCloneObject(object) {
        return (typeof object.constructor == 'function' && !isPrototype(object)) ? baseCreate(getPrototype(object)) : {};
      }
      function initCloneByTag(object, tag, cloneFunc, isDeep) {
        var Ctor = object.constructor;
        switch (tag) {
          case arrayBufferTag:
            return cloneArrayBuffer(object);
          case boolTag:
          case dateTag:
            return new Ctor(+object);
          case dataViewTag:
            return cloneDataView(object, isDeep);
          case float32Tag:
          case float64Tag:
          case int8Tag:
          case int16Tag:
          case int32Tag:
          case uint8Tag:
          case uint8ClampedTag:
          case uint16Tag:
          case uint32Tag:
            return cloneTypedArray(object, isDeep);
          case mapTag:
            return cloneMap(object, isDeep, cloneFunc);
          case numberTag:
          case stringTag:
            return new Ctor(object);
          case regexpTag:
            return cloneRegExp(object);
          case setTag:
            return cloneSet(object, isDeep, cloneFunc);
          case symbolTag:
            return cloneSymbol(object);
        }
      }
      function indexKeys(object) {
        var length = object ? object.length : undefined;
        if (isLength(length) && (isArray(object) || isString(object) || isArguments(object))) {
          return baseTimes(length, String);
        }
        return null;
      }
      function isFlattenable(value) {
        return isArrayLikeObject(value) && (isArray(value) || isArguments(value));
      }
      function isFlattenableIteratee(value) {
        return isArray(value) && !(value.length == 2 && !isFunction(value[0]));
      }
      function isIterateeCall(value, index, object) {
        if (!isObject(object)) {
          return false;
        }
        var type = typeof index;
        if (type == 'number' ? (isArrayLike(object) && isIndex(index, object.length)) : (type == 'string' && index in object)) {
          return eq(object[index], value);
        }
        return false;
      }
      function isKey(value, object) {
        var type = typeof value;
        if (type == 'number' || type == 'symbol') {
          return true;
        }
        return !isArray(value) && (isSymbol(value) || reIsPlainProp.test(value) || !reIsDeepProp.test(value) || (object != null && value in Object(object)));
      }
      function isKeyable(value) {
        var type = typeof value;
        return type == 'number' || type == 'boolean' || (type == 'string' && value != '__proto__') || value == null;
      }
      function isLaziable(func) {
        var funcName = getFuncName(func),
            other = lodash[funcName];
        if (typeof other != 'function' || !(funcName in LazyWrapper.prototype)) {
          return false;
        }
        if (func === other) {
          return true;
        }
        var data = getData(other);
        return !!data && func === data[0];
      }
      function isPrototype(value) {
        var Ctor = value && value.constructor,
            proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;
        return value === proto;
      }
      function isStrictComparable(value) {
        return value === value && !isObject(value);
      }
      function matchesStrictComparable(key, srcValue) {
        return function(object) {
          if (object == null) {
            return false;
          }
          return object[key] === srcValue && (srcValue !== undefined || (key in Object(object)));
        };
      }
      function mergeData(data, source) {
        var bitmask = data[1],
            srcBitmask = source[1],
            newBitmask = bitmask | srcBitmask,
            isCommon = newBitmask < (BIND_FLAG | BIND_KEY_FLAG | ARY_FLAG);
        var isCombo = ((srcBitmask == ARY_FLAG) && (bitmask == CURRY_FLAG)) || ((srcBitmask == ARY_FLAG) && (bitmask == REARG_FLAG) && (data[7].length <= source[8])) || ((srcBitmask == (ARY_FLAG | REARG_FLAG)) && (source[7].length <= source[8]) && (bitmask == CURRY_FLAG));
        if (!(isCommon || isCombo)) {
          return data;
        }
        if (srcBitmask & BIND_FLAG) {
          data[2] = source[2];
          newBitmask |= bitmask & BIND_FLAG ? 0 : CURRY_BOUND_FLAG;
        }
        var value = source[3];
        if (value) {
          var partials = data[3];
          data[3] = partials ? composeArgs(partials, value, source[4]) : value;
          data[4] = partials ? replaceHolders(data[3], PLACEHOLDER) : source[4];
        }
        value = source[5];
        if (value) {
          partials = data[5];
          data[5] = partials ? composeArgsRight(partials, value, source[6]) : value;
          data[6] = partials ? replaceHolders(data[5], PLACEHOLDER) : source[6];
        }
        value = source[7];
        if (value) {
          data[7] = value;
        }
        if (srcBitmask & ARY_FLAG) {
          data[8] = data[8] == null ? source[8] : nativeMin(data[8], source[8]);
        }
        if (data[9] == null) {
          data[9] = source[9];
        }
        data[0] = source[0];
        data[1] = newBitmask;
        return data;
      }
      function mergeDefaults(objValue, srcValue, key, object, source, stack) {
        if (isObject(objValue) && isObject(srcValue)) {
          baseMerge(objValue, srcValue, undefined, mergeDefaults, stack.set(srcValue, objValue));
        }
        return objValue;
      }
      function parent(object, path) {
        return path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
      }
      function reorder(array, indexes) {
        var arrLength = array.length,
            length = nativeMin(indexes.length, arrLength),
            oldArray = copyArray(array);
        while (length--) {
          var index = indexes[length];
          array[length] = isIndex(index, arrLength) ? oldArray[index] : undefined;
        }
        return array;
      }
      var setData = (function() {
        var count = 0,
            lastCalled = 0;
        return function(key, value) {
          var stamp = now(),
              remaining = HOT_SPAN - (stamp - lastCalled);
          lastCalled = stamp;
          if (remaining > 0) {
            if (++count >= HOT_COUNT) {
              return key;
            }
          } else {
            count = 0;
          }
          return baseSetData(key, value);
        };
      }());
      var stringToPath = memoize(function(string) {
        var result = [];
        toString(string).replace(rePropName, function(match, number, quote, string) {
          result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
        });
        return result;
      });
      function toKey(key) {
        return (typeof key == 'string' || isSymbol(key)) ? key : (key + '');
      }
      function toSource(func) {
        if (func != null) {
          try {
            return funcToString.call(func);
          } catch (e) {}
          try {
            return (func + '');
          } catch (e) {}
        }
        return '';
      }
      function wrapperClone(wrapper) {
        if (wrapper instanceof LazyWrapper) {
          return wrapper.clone();
        }
        var result = new LodashWrapper(wrapper.__wrapped__, wrapper.__chain__);
        result.__actions__ = copyArray(wrapper.__actions__);
        result.__index__ = wrapper.__index__;
        result.__values__ = wrapper.__values__;
        return result;
      }
      function chunk(array, size, guard) {
        if ((guard ? isIterateeCall(array, size, guard) : size === undefined)) {
          size = 1;
        } else {
          size = nativeMax(toInteger(size), 0);
        }
        var length = array ? array.length : 0;
        if (!length || size < 1) {
          return [];
        }
        var index = 0,
            resIndex = 0,
            result = Array(nativeCeil(length / size));
        while (index < length) {
          result[resIndex++] = baseSlice(array, index, (index += size));
        }
        return result;
      }
      function compact(array) {
        var index = -1,
            length = array ? array.length : 0,
            resIndex = 0,
            result = [];
        while (++index < length) {
          var value = array[index];
          if (value) {
            result[resIndex++] = value;
          }
        }
        return result;
      }
      function concat() {
        var length = arguments.length,
            array = castArray(arguments[0]);
        if (length < 2) {
          return length ? copyArray(array) : [];
        }
        var args = Array(length - 1);
        while (length--) {
          args[length - 1] = arguments[length];
        }
        return arrayConcat(array, baseFlatten(args, 1));
      }
      var difference = rest(function(array, values) {
        return isArrayLikeObject(array) ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true)) : [];
      });
      var differenceBy = rest(function(array, values) {
        var iteratee = last(values);
        if (isArrayLikeObject(iteratee)) {
          iteratee = undefined;
        }
        return isArrayLikeObject(array) ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true), getIteratee(iteratee)) : [];
      });
      var differenceWith = rest(function(array, values) {
        var comparator = last(values);
        if (isArrayLikeObject(comparator)) {
          comparator = undefined;
        }
        return isArrayLikeObject(array) ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true), undefined, comparator) : [];
      });
      function drop(array, n, guard) {
        var length = array ? array.length : 0;
        if (!length) {
          return [];
        }
        n = (guard || n === undefined) ? 1 : toInteger(n);
        return baseSlice(array, n < 0 ? 0 : n, length);
      }
      function dropRight(array, n, guard) {
        var length = array ? array.length : 0;
        if (!length) {
          return [];
        }
        n = (guard || n === undefined) ? 1 : toInteger(n);
        n = length - n;
        return baseSlice(array, 0, n < 0 ? 0 : n);
      }
      function dropRightWhile(array, predicate) {
        return (array && array.length) ? baseWhile(array, getIteratee(predicate, 3), true, true) : [];
      }
      function dropWhile(array, predicate) {
        return (array && array.length) ? baseWhile(array, getIteratee(predicate, 3), true) : [];
      }
      function fill(array, value, start, end) {
        var length = array ? array.length : 0;
        if (!length) {
          return [];
        }
        if (start && typeof start != 'number' && isIterateeCall(array, value, start)) {
          start = 0;
          end = length;
        }
        return baseFill(array, value, start, end);
      }
      function findIndex(array, predicate) {
        return (array && array.length) ? baseFindIndex(array, getIteratee(predicate, 3)) : -1;
      }
      function findLastIndex(array, predicate) {
        return (array && array.length) ? baseFindIndex(array, getIteratee(predicate, 3), true) : -1;
      }
      function flatten(array) {
        var length = array ? array.length : 0;
        return length ? baseFlatten(array, 1) : [];
      }
      function flattenDeep(array) {
        var length = array ? array.length : 0;
        return length ? baseFlatten(array, INFINITY) : [];
      }
      function flattenDepth(array, depth) {
        var length = array ? array.length : 0;
        if (!length) {
          return [];
        }
        depth = depth === undefined ? 1 : toInteger(depth);
        return baseFlatten(array, depth);
      }
      function fromPairs(pairs) {
        var index = -1,
            length = pairs ? pairs.length : 0,
            result = {};
        while (++index < length) {
          var pair = pairs[index];
          result[pair[0]] = pair[1];
        }
        return result;
      }
      function head(array) {
        return (array && array.length) ? array[0] : undefined;
      }
      function indexOf(array, value, fromIndex) {
        var length = array ? array.length : 0;
        if (!length) {
          return -1;
        }
        fromIndex = toInteger(fromIndex);
        if (fromIndex < 0) {
          fromIndex = nativeMax(length + fromIndex, 0);
        }
        return baseIndexOf(array, value, fromIndex);
      }
      function initial(array) {
        return dropRight(array, 1);
      }
      var intersection = rest(function(arrays) {
        var mapped = arrayMap(arrays, castArrayLikeObject);
        return (mapped.length && mapped[0] === arrays[0]) ? baseIntersection(mapped) : [];
      });
      var intersectionBy = rest(function(arrays) {
        var iteratee = last(arrays),
            mapped = arrayMap(arrays, castArrayLikeObject);
        if (iteratee === last(mapped)) {
          iteratee = undefined;
        } else {
          mapped.pop();
        }
        return (mapped.length && mapped[0] === arrays[0]) ? baseIntersection(mapped, getIteratee(iteratee)) : [];
      });
      var intersectionWith = rest(function(arrays) {
        var comparator = last(arrays),
            mapped = arrayMap(arrays, castArrayLikeObject);
        if (comparator === last(mapped)) {
          comparator = undefined;
        } else {
          mapped.pop();
        }
        return (mapped.length && mapped[0] === arrays[0]) ? baseIntersection(mapped, undefined, comparator) : [];
      });
      function join(array, separator) {
        return array ? nativeJoin.call(array, separator) : '';
      }
      function last(array) {
        var length = array ? array.length : 0;
        return length ? array[length - 1] : undefined;
      }
      function lastIndexOf(array, value, fromIndex) {
        var length = array ? array.length : 0;
        if (!length) {
          return -1;
        }
        var index = length;
        if (fromIndex !== undefined) {
          index = toInteger(fromIndex);
          index = (index < 0 ? nativeMax(length + index, 0) : nativeMin(index, length - 1)) + 1;
        }
        if (value !== value) {
          return indexOfNaN(array, index, true);
        }
        while (index--) {
          if (array[index] === value) {
            return index;
          }
        }
        return -1;
      }
      function nth(array, n) {
        return (array && array.length) ? baseNth(array, toInteger(n)) : undefined;
      }
      var pull = rest(pullAll);
      function pullAll(array, values) {
        return (array && array.length && values && values.length) ? basePullAll(array, values) : array;
      }
      function pullAllBy(array, values, iteratee) {
        return (array && array.length && values && values.length) ? basePullAll(array, values, getIteratee(iteratee)) : array;
      }
      function pullAllWith(array, values, comparator) {
        return (array && array.length && values && values.length) ? basePullAll(array, values, undefined, comparator) : array;
      }
      var pullAt = rest(function(array, indexes) {
        indexes = arrayMap(baseFlatten(indexes, 1), String);
        var result = baseAt(array, indexes);
        basePullAt(array, indexes.sort(compareAscending));
        return result;
      });
      function remove(array, predicate) {
        var result = [];
        if (!(array && array.length)) {
          return result;
        }
        var index = -1,
            indexes = [],
            length = array.length;
        predicate = getIteratee(predicate, 3);
        while (++index < length) {
          var value = array[index];
          if (predicate(value, index, array)) {
            result.push(value);
            indexes.push(index);
          }
        }
        basePullAt(array, indexes);
        return result;
      }
      function reverse(array) {
        return array ? nativeReverse.call(array) : array;
      }
      function slice(array, start, end) {
        var length = array ? array.length : 0;
        if (!length) {
          return [];
        }
        if (end && typeof end != 'number' && isIterateeCall(array, start, end)) {
          start = 0;
          end = length;
        } else {
          start = start == null ? 0 : toInteger(start);
          end = end === undefined ? length : toInteger(end);
        }
        return baseSlice(array, start, end);
      }
      function sortedIndex(array, value) {
        return baseSortedIndex(array, value);
      }
      function sortedIndexBy(array, value, iteratee) {
        return baseSortedIndexBy(array, value, getIteratee(iteratee));
      }
      function sortedIndexOf(array, value) {
        var length = array ? array.length : 0;
        if (length) {
          var index = baseSortedIndex(array, value);
          if (index < length && eq(array[index], value)) {
            return index;
          }
        }
        return -1;
      }
      function sortedLastIndex(array, value) {
        return baseSortedIndex(array, value, true);
      }
      function sortedLastIndexBy(array, value, iteratee) {
        return baseSortedIndexBy(array, value, getIteratee(iteratee), true);
      }
      function sortedLastIndexOf(array, value) {
        var length = array ? array.length : 0;
        if (length) {
          var index = baseSortedIndex(array, value, true) - 1;
          if (eq(array[index], value)) {
            return index;
          }
        }
        return -1;
      }
      function sortedUniq(array) {
        return (array && array.length) ? baseSortedUniq(array) : [];
      }
      function sortedUniqBy(array, iteratee) {
        return (array && array.length) ? baseSortedUniqBy(array, getIteratee(iteratee)) : [];
      }
      function tail(array) {
        return drop(array, 1);
      }
      function take(array, n, guard) {
        if (!(array && array.length)) {
          return [];
        }
        n = (guard || n === undefined) ? 1 : toInteger(n);
        return baseSlice(array, 0, n < 0 ? 0 : n);
      }
      function takeRight(array, n, guard) {
        var length = array ? array.length : 0;
        if (!length) {
          return [];
        }
        n = (guard || n === undefined) ? 1 : toInteger(n);
        n = length - n;
        return baseSlice(array, n < 0 ? 0 : n, length);
      }
      function takeRightWhile(array, predicate) {
        return (array && array.length) ? baseWhile(array, getIteratee(predicate, 3), false, true) : [];
      }
      function takeWhile(array, predicate) {
        return (array && array.length) ? baseWhile(array, getIteratee(predicate, 3)) : [];
      }
      var union = rest(function(arrays) {
        return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true));
      });
      var unionBy = rest(function(arrays) {
        var iteratee = last(arrays);
        if (isArrayLikeObject(iteratee)) {
          iteratee = undefined;
        }
        return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true), getIteratee(iteratee));
      });
      var unionWith = rest(function(arrays) {
        var comparator = last(arrays);
        if (isArrayLikeObject(comparator)) {
          comparator = undefined;
        }
        return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true), undefined, comparator);
      });
      function uniq(array) {
        return (array && array.length) ? baseUniq(array) : [];
      }
      function uniqBy(array, iteratee) {
        return (array && array.length) ? baseUniq(array, getIteratee(iteratee)) : [];
      }
      function uniqWith(array, comparator) {
        return (array && array.length) ? baseUniq(array, undefined, comparator) : [];
      }
      function unzip(array) {
        if (!(array && array.length)) {
          return [];
        }
        var length = 0;
        array = arrayFilter(array, function(group) {
          if (isArrayLikeObject(group)) {
            length = nativeMax(group.length, length);
            return true;
          }
        });
        return baseTimes(length, function(index) {
          return arrayMap(array, baseProperty(index));
        });
      }
      function unzipWith(array, iteratee) {
        if (!(array && array.length)) {
          return [];
        }
        var result = unzip(array);
        if (iteratee == null) {
          return result;
        }
        return arrayMap(result, function(group) {
          return apply(iteratee, undefined, group);
        });
      }
      var without = rest(function(array, values) {
        return isArrayLikeObject(array) ? baseDifference(array, values) : [];
      });
      var xor = rest(function(arrays) {
        return baseXor(arrayFilter(arrays, isArrayLikeObject));
      });
      var xorBy = rest(function(arrays) {
        var iteratee = last(arrays);
        if (isArrayLikeObject(iteratee)) {
          iteratee = undefined;
        }
        return baseXor(arrayFilter(arrays, isArrayLikeObject), getIteratee(iteratee));
      });
      var xorWith = rest(function(arrays) {
        var comparator = last(arrays);
        if (isArrayLikeObject(comparator)) {
          comparator = undefined;
        }
        return baseXor(arrayFilter(arrays, isArrayLikeObject), undefined, comparator);
      });
      var zip = rest(unzip);
      function zipObject(props, values) {
        return baseZipObject(props || [], values || [], assignValue);
      }
      function zipObjectDeep(props, values) {
        return baseZipObject(props || [], values || [], baseSet);
      }
      var zipWith = rest(function(arrays) {
        var length = arrays.length,
            iteratee = length > 1 ? arrays[length - 1] : undefined;
        iteratee = typeof iteratee == 'function' ? (arrays.pop(), iteratee) : undefined;
        return unzipWith(arrays, iteratee);
      });
      function chain(value) {
        var result = lodash(value);
        result.__chain__ = true;
        return result;
      }
      function tap(value, interceptor) {
        interceptor(value);
        return value;
      }
      function thru(value, interceptor) {
        return interceptor(value);
      }
      var wrapperAt = rest(function(paths) {
        paths = baseFlatten(paths, 1);
        var length = paths.length,
            start = length ? paths[0] : 0,
            value = this.__wrapped__,
            interceptor = function(object) {
              return baseAt(object, paths);
            };
        if (length > 1 || this.__actions__.length || !(value instanceof LazyWrapper) || !isIndex(start)) {
          return this.thru(interceptor);
        }
        value = value.slice(start, +start + (length ? 1 : 0));
        value.__actions__.push({
          'func': thru,
          'args': [interceptor],
          'thisArg': undefined
        });
        return new LodashWrapper(value, this.__chain__).thru(function(array) {
          if (length && !array.length) {
            array.push(undefined);
          }
          return array;
        });
      });
      function wrapperChain() {
        return chain(this);
      }
      function wrapperCommit() {
        return new LodashWrapper(this.value(), this.__chain__);
      }
      function wrapperNext() {
        if (this.__values__ === undefined) {
          this.__values__ = toArray(this.value());
        }
        var done = this.__index__ >= this.__values__.length,
            value = done ? undefined : this.__values__[this.__index__++];
        return {
          'done': done,
          'value': value
        };
      }
      function wrapperToIterator() {
        return this;
      }
      function wrapperPlant(value) {
        var result,
            parent = this;
        while (parent instanceof baseLodash) {
          var clone = wrapperClone(parent);
          clone.__index__ = 0;
          clone.__values__ = undefined;
          if (result) {
            previous.__wrapped__ = clone;
          } else {
            result = clone;
          }
          var previous = clone;
          parent = parent.__wrapped__;
        }
        previous.__wrapped__ = value;
        return result;
      }
      function wrapperReverse() {
        var value = this.__wrapped__;
        if (value instanceof LazyWrapper) {
          var wrapped = value;
          if (this.__actions__.length) {
            wrapped = new LazyWrapper(this);
          }
          wrapped = wrapped.reverse();
          wrapped.__actions__.push({
            'func': thru,
            'args': [reverse],
            'thisArg': undefined
          });
          return new LodashWrapper(wrapped, this.__chain__);
        }
        return this.thru(reverse);
      }
      function wrapperValue() {
        return baseWrapperValue(this.__wrapped__, this.__actions__);
      }
      var countBy = createAggregator(function(result, value, key) {
        hasOwnProperty.call(result, key) ? ++result[key] : (result[key] = 1);
      });
      function every(collection, predicate, guard) {
        var func = isArray(collection) ? arrayEvery : baseEvery;
        if (guard && isIterateeCall(collection, predicate, guard)) {
          predicate = undefined;
        }
        return func(collection, getIteratee(predicate, 3));
      }
      function filter(collection, predicate) {
        var func = isArray(collection) ? arrayFilter : baseFilter;
        return func(collection, getIteratee(predicate, 3));
      }
      function find(collection, predicate) {
        predicate = getIteratee(predicate, 3);
        if (isArray(collection)) {
          var index = baseFindIndex(collection, predicate);
          return index > -1 ? collection[index] : undefined;
        }
        return baseFind(collection, predicate, baseEach);
      }
      function findLast(collection, predicate) {
        predicate = getIteratee(predicate, 3);
        if (isArray(collection)) {
          var index = baseFindIndex(collection, predicate, true);
          return index > -1 ? collection[index] : undefined;
        }
        return baseFind(collection, predicate, baseEachRight);
      }
      function flatMap(collection, iteratee) {
        return baseFlatten(map(collection, iteratee), 1);
      }
      function flatMapDeep(collection, iteratee) {
        return baseFlatten(map(collection, iteratee), INFINITY);
      }
      function flatMapDepth(collection, iteratee, depth) {
        depth = depth === undefined ? 1 : toInteger(depth);
        return baseFlatten(map(collection, iteratee), depth);
      }
      function forEach(collection, iteratee) {
        return (typeof iteratee == 'function' && isArray(collection)) ? arrayEach(collection, iteratee) : baseEach(collection, getIteratee(iteratee));
      }
      function forEachRight(collection, iteratee) {
        return (typeof iteratee == 'function' && isArray(collection)) ? arrayEachRight(collection, iteratee) : baseEachRight(collection, getIteratee(iteratee));
      }
      var groupBy = createAggregator(function(result, value, key) {
        if (hasOwnProperty.call(result, key)) {
          result[key].push(value);
        } else {
          result[key] = [value];
        }
      });
      function includes(collection, value, fromIndex, guard) {
        collection = isArrayLike(collection) ? collection : values(collection);
        fromIndex = (fromIndex && !guard) ? toInteger(fromIndex) : 0;
        var length = collection.length;
        if (fromIndex < 0) {
          fromIndex = nativeMax(length + fromIndex, 0);
        }
        return isString(collection) ? (fromIndex <= length && collection.indexOf(value, fromIndex) > -1) : (!!length && baseIndexOf(collection, value, fromIndex) > -1);
      }
      var invokeMap = rest(function(collection, path, args) {
        var index = -1,
            isFunc = typeof path == 'function',
            isProp = isKey(path),
            result = isArrayLike(collection) ? Array(collection.length) : [];
        baseEach(collection, function(value) {
          var func = isFunc ? path : ((isProp && value != null) ? value[path] : undefined);
          result[++index] = func ? apply(func, value, args) : baseInvoke(value, path, args);
        });
        return result;
      });
      var keyBy = createAggregator(function(result, value, key) {
        result[key] = value;
      });
      function map(collection, iteratee) {
        var func = isArray(collection) ? arrayMap : baseMap;
        return func(collection, getIteratee(iteratee, 3));
      }
      function orderBy(collection, iteratees, orders, guard) {
        if (collection == null) {
          return [];
        }
        if (!isArray(iteratees)) {
          iteratees = iteratees == null ? [] : [iteratees];
        }
        orders = guard ? undefined : orders;
        if (!isArray(orders)) {
          orders = orders == null ? [] : [orders];
        }
        return baseOrderBy(collection, iteratees, orders);
      }
      var partition = createAggregator(function(result, value, key) {
        result[key ? 0 : 1].push(value);
      }, function() {
        return [[], []];
      });
      function reduce(collection, iteratee, accumulator) {
        var func = isArray(collection) ? arrayReduce : baseReduce,
            initAccum = arguments.length < 3;
        return func(collection, getIteratee(iteratee, 4), accumulator, initAccum, baseEach);
      }
      function reduceRight(collection, iteratee, accumulator) {
        var func = isArray(collection) ? arrayReduceRight : baseReduce,
            initAccum = arguments.length < 3;
        return func(collection, getIteratee(iteratee, 4), accumulator, initAccum, baseEachRight);
      }
      function reject(collection, predicate) {
        var func = isArray(collection) ? arrayFilter : baseFilter;
        predicate = getIteratee(predicate, 3);
        return func(collection, function(value, index, collection) {
          return !predicate(value, index, collection);
        });
      }
      function sample(collection) {
        var array = isArrayLike(collection) ? collection : values(collection),
            length = array.length;
        return length > 0 ? array[baseRandom(0, length - 1)] : undefined;
      }
      function sampleSize(collection, n, guard) {
        var index = -1,
            result = toArray(collection),
            length = result.length,
            lastIndex = length - 1;
        if ((guard ? isIterateeCall(collection, n, guard) : n === undefined)) {
          n = 1;
        } else {
          n = baseClamp(toInteger(n), 0, length);
        }
        while (++index < n) {
          var rand = baseRandom(index, lastIndex),
              value = result[rand];
          result[rand] = result[index];
          result[index] = value;
        }
        result.length = n;
        return result;
      }
      function shuffle(collection) {
        return sampleSize(collection, MAX_ARRAY_LENGTH);
      }
      function size(collection) {
        if (collection == null) {
          return 0;
        }
        if (isArrayLike(collection)) {
          var result = collection.length;
          return (result && isString(collection)) ? stringSize(collection) : result;
        }
        if (isObjectLike(collection)) {
          var tag = getTag(collection);
          if (tag == mapTag || tag == setTag) {
            return collection.size;
          }
        }
        return keys(collection).length;
      }
      function some(collection, predicate, guard) {
        var func = isArray(collection) ? arraySome : baseSome;
        if (guard && isIterateeCall(collection, predicate, guard)) {
          predicate = undefined;
        }
        return func(collection, getIteratee(predicate, 3));
      }
      var sortBy = rest(function(collection, iteratees) {
        if (collection == null) {
          return [];
        }
        var length = iteratees.length;
        if (length > 1 && isIterateeCall(collection, iteratees[0], iteratees[1])) {
          iteratees = [];
        } else if (length > 2 && isIterateeCall(iteratees[0], iteratees[1], iteratees[2])) {
          iteratees = [iteratees[0]];
        }
        iteratees = (iteratees.length == 1 && isArray(iteratees[0])) ? iteratees[0] : baseFlatten(iteratees, 1, isFlattenableIteratee);
        return baseOrderBy(collection, iteratees, []);
      });
      var now = Date.now;
      function after(n, func) {
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        n = toInteger(n);
        return function() {
          if (--n < 1) {
            return func.apply(this, arguments);
          }
        };
      }
      function ary(func, n, guard) {
        n = guard ? undefined : n;
        n = (func && n == null) ? func.length : n;
        return createWrapper(func, ARY_FLAG, undefined, undefined, undefined, undefined, n);
      }
      function before(n, func) {
        var result;
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        n = toInteger(n);
        return function() {
          if (--n > 0) {
            result = func.apply(this, arguments);
          }
          if (n <= 1) {
            func = undefined;
          }
          return result;
        };
      }
      var bind = rest(function(func, thisArg, partials) {
        var bitmask = BIND_FLAG;
        if (partials.length) {
          var holders = replaceHolders(partials, getPlaceholder(bind));
          bitmask |= PARTIAL_FLAG;
        }
        return createWrapper(func, bitmask, thisArg, partials, holders);
      });
      var bindKey = rest(function(object, key, partials) {
        var bitmask = BIND_FLAG | BIND_KEY_FLAG;
        if (partials.length) {
          var holders = replaceHolders(partials, getPlaceholder(bindKey));
          bitmask |= PARTIAL_FLAG;
        }
        return createWrapper(key, bitmask, object, partials, holders);
      });
      function curry(func, arity, guard) {
        arity = guard ? undefined : arity;
        var result = createWrapper(func, CURRY_FLAG, undefined, undefined, undefined, undefined, undefined, arity);
        result.placeholder = curry.placeholder;
        return result;
      }
      function curryRight(func, arity, guard) {
        arity = guard ? undefined : arity;
        var result = createWrapper(func, CURRY_RIGHT_FLAG, undefined, undefined, undefined, undefined, undefined, arity);
        result.placeholder = curryRight.placeholder;
        return result;
      }
      function debounce(func, wait, options) {
        var lastArgs,
            lastThis,
            maxWait,
            result,
            timerId,
            lastCallTime = 0,
            lastInvokeTime = 0,
            leading = false,
            maxing = false,
            trailing = true;
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        wait = toNumber(wait) || 0;
        if (isObject(options)) {
          leading = !!options.leading;
          maxing = 'maxWait' in options;
          maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
          trailing = 'trailing' in options ? !!options.trailing : trailing;
        }
        function invokeFunc(time) {
          var args = lastArgs,
              thisArg = lastThis;
          lastArgs = lastThis = undefined;
          lastInvokeTime = time;
          result = func.apply(thisArg, args);
          return result;
        }
        function leadingEdge(time) {
          lastInvokeTime = time;
          timerId = setTimeout(timerExpired, wait);
          return leading ? invokeFunc(time) : result;
        }
        function remainingWait(time) {
          var timeSinceLastCall = time - lastCallTime,
              timeSinceLastInvoke = time - lastInvokeTime,
              result = wait - timeSinceLastCall;
          return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
        }
        function shouldInvoke(time) {
          var timeSinceLastCall = time - lastCallTime,
              timeSinceLastInvoke = time - lastInvokeTime;
          return (!lastCallTime || (timeSinceLastCall >= wait) || (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
        }
        function timerExpired() {
          var time = now();
          if (shouldInvoke(time)) {
            return trailingEdge(time);
          }
          timerId = setTimeout(timerExpired, remainingWait(time));
        }
        function trailingEdge(time) {
          clearTimeout(timerId);
          timerId = undefined;
          if (trailing && lastArgs) {
            return invokeFunc(time);
          }
          lastArgs = lastThis = undefined;
          return result;
        }
        function cancel() {
          if (timerId !== undefined) {
            clearTimeout(timerId);
          }
          lastCallTime = lastInvokeTime = 0;
          lastArgs = lastThis = timerId = undefined;
        }
        function flush() {
          return timerId === undefined ? result : trailingEdge(now());
        }
        function debounced() {
          var time = now(),
              isInvoking = shouldInvoke(time);
          lastArgs = arguments;
          lastThis = this;
          lastCallTime = time;
          if (isInvoking) {
            if (timerId === undefined) {
              return leadingEdge(lastCallTime);
            }
            if (maxing) {
              clearTimeout(timerId);
              timerId = setTimeout(timerExpired, wait);
              return invokeFunc(lastCallTime);
            }
          }
          if (timerId === undefined) {
            timerId = setTimeout(timerExpired, wait);
          }
          return result;
        }
        debounced.cancel = cancel;
        debounced.flush = flush;
        return debounced;
      }
      var defer = rest(function(func, args) {
        return baseDelay(func, 1, args);
      });
      var delay = rest(function(func, wait, args) {
        return baseDelay(func, toNumber(wait) || 0, args);
      });
      function flip(func) {
        return createWrapper(func, FLIP_FLAG);
      }
      function memoize(func, resolver) {
        if (typeof func != 'function' || (resolver && typeof resolver != 'function')) {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        var memoized = function() {
          var args = arguments,
              key = resolver ? resolver.apply(this, args) : args[0],
              cache = memoized.cache;
          if (cache.has(key)) {
            return cache.get(key);
          }
          var result = func.apply(this, args);
          memoized.cache = cache.set(key, result);
          return result;
        };
        memoized.cache = new (memoize.Cache || MapCache);
        return memoized;
      }
      memoize.Cache = MapCache;
      function negate(predicate) {
        if (typeof predicate != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        return function() {
          return !predicate.apply(this, arguments);
        };
      }
      function once(func) {
        return before(2, func);
      }
      var overArgs = rest(function(func, transforms) {
        transforms = (transforms.length == 1 && isArray(transforms[0])) ? arrayMap(transforms[0], baseUnary(getIteratee())) : arrayMap(baseFlatten(transforms, 1, isFlattenableIteratee), baseUnary(getIteratee()));
        var funcsLength = transforms.length;
        return rest(function(args) {
          var index = -1,
              length = nativeMin(args.length, funcsLength);
          while (++index < length) {
            args[index] = transforms[index].call(this, args[index]);
          }
          return apply(func, this, args);
        });
      });
      var partial = rest(function(func, partials) {
        var holders = replaceHolders(partials, getPlaceholder(partial));
        return createWrapper(func, PARTIAL_FLAG, undefined, partials, holders);
      });
      var partialRight = rest(function(func, partials) {
        var holders = replaceHolders(partials, getPlaceholder(partialRight));
        return createWrapper(func, PARTIAL_RIGHT_FLAG, undefined, partials, holders);
      });
      var rearg = rest(function(func, indexes) {
        return createWrapper(func, REARG_FLAG, undefined, undefined, undefined, baseFlatten(indexes, 1));
      });
      function rest(func, start) {
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        start = nativeMax(start === undefined ? (func.length - 1) : toInteger(start), 0);
        return function() {
          var args = arguments,
              index = -1,
              length = nativeMax(args.length - start, 0),
              array = Array(length);
          while (++index < length) {
            array[index] = args[start + index];
          }
          switch (start) {
            case 0:
              return func.call(this, array);
            case 1:
              return func.call(this, args[0], array);
            case 2:
              return func.call(this, args[0], args[1], array);
          }
          var otherArgs = Array(start + 1);
          index = -1;
          while (++index < start) {
            otherArgs[index] = args[index];
          }
          otherArgs[start] = array;
          return apply(func, this, otherArgs);
        };
      }
      function spread(func, start) {
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        start = start === undefined ? 0 : nativeMax(toInteger(start), 0);
        return rest(function(args) {
          var array = args[start],
              otherArgs = castSlice(args, 0, start);
          if (array) {
            arrayPush(otherArgs, array);
          }
          return apply(func, this, otherArgs);
        });
      }
      function throttle(func, wait, options) {
        var leading = true,
            trailing = true;
        if (typeof func != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        if (isObject(options)) {
          leading = 'leading' in options ? !!options.leading : leading;
          trailing = 'trailing' in options ? !!options.trailing : trailing;
        }
        return debounce(func, wait, {
          'leading': leading,
          'maxWait': wait,
          'trailing': trailing
        });
      }
      function unary(func) {
        return ary(func, 1);
      }
      function wrap(value, wrapper) {
        wrapper = wrapper == null ? identity : wrapper;
        return partial(wrapper, value);
      }
      function castArray() {
        if (!arguments.length) {
          return [];
        }
        var value = arguments[0];
        return isArray(value) ? value : [value];
      }
      function clone(value) {
        return baseClone(value, false, true);
      }
      function cloneWith(value, customizer) {
        return baseClone(value, false, true, customizer);
      }
      function cloneDeep(value) {
        return baseClone(value, true, true);
      }
      function cloneDeepWith(value, customizer) {
        return baseClone(value, true, true, customizer);
      }
      function eq(value, other) {
        return value === other || (value !== value && other !== other);
      }
      function gt(value, other) {
        return value > other;
      }
      function gte(value, other) {
        return value >= other;
      }
      function isArguments(value) {
        return isArrayLikeObject(value) && hasOwnProperty.call(value, 'callee') && (!propertyIsEnumerable.call(value, 'callee') || objectToString.call(value) == argsTag);
      }
      var isArray = Array.isArray;
      function isArrayBuffer(value) {
        return isObjectLike(value) && objectToString.call(value) == arrayBufferTag;
      }
      function isArrayLike(value) {
        return value != null && isLength(getLength(value)) && !isFunction(value);
      }
      function isArrayLikeObject(value) {
        return isObjectLike(value) && isArrayLike(value);
      }
      function isBoolean(value) {
        return value === true || value === false || (isObjectLike(value) && objectToString.call(value) == boolTag);
      }
      var isBuffer = !Buffer ? constant(false) : function(value) {
        return value instanceof Buffer;
      };
      function isDate(value) {
        return isObjectLike(value) && objectToString.call(value) == dateTag;
      }
      function isElement(value) {
        return !!value && value.nodeType === 1 && isObjectLike(value) && !isPlainObject(value);
      }
      function isEmpty(value) {
        if (isArrayLike(value) && (isArray(value) || isString(value) || isFunction(value.splice) || isArguments(value) || isBuffer(value))) {
          return !value.length;
        }
        if (isObjectLike(value)) {
          var tag = getTag(value);
          if (tag == mapTag || tag == setTag) {
            return !value.size;
          }
        }
        for (var key in value) {
          if (hasOwnProperty.call(value, key)) {
            return false;
          }
        }
        return !(nonEnumShadows && keys(value).length);
      }
      function isEqual(value, other) {
        return baseIsEqual(value, other);
      }
      function isEqualWith(value, other, customizer) {
        customizer = typeof customizer == 'function' ? customizer : undefined;
        var result = customizer ? customizer(value, other) : undefined;
        return result === undefined ? baseIsEqual(value, other, customizer) : !!result;
      }
      function isError(value) {
        if (!isObjectLike(value)) {
          return false;
        }
        return (objectToString.call(value) == errorTag) || (typeof value.message == 'string' && typeof value.name == 'string');
      }
      function isFinite(value) {
        return typeof value == 'number' && nativeIsFinite(value);
      }
      function isFunction(value) {
        var tag = isObject(value) ? objectToString.call(value) : '';
        return tag == funcTag || tag == genTag;
      }
      function isInteger(value) {
        return typeof value == 'number' && value == toInteger(value);
      }
      function isLength(value) {
        return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
      }
      function isObject(value) {
        var type = typeof value;
        return !!value && (type == 'object' || type == 'function');
      }
      function isObjectLike(value) {
        return !!value && typeof value == 'object';
      }
      function isMap(value) {
        return isObjectLike(value) && getTag(value) == mapTag;
      }
      function isMatch(object, source) {
        return object === source || baseIsMatch(object, source, getMatchData(source));
      }
      function isMatchWith(object, source, customizer) {
        customizer = typeof customizer == 'function' ? customizer : undefined;
        return baseIsMatch(object, source, getMatchData(source), customizer);
      }
      function isNaN(value) {
        return isNumber(value) && value != +value;
      }
      function isNative(value) {
        if (!isObject(value)) {
          return false;
        }
        var pattern = (isFunction(value) || isHostObject(value)) ? reIsNative : reIsHostCtor;
        return pattern.test(toSource(value));
      }
      function isNull(value) {
        return value === null;
      }
      function isNil(value) {
        return value == null;
      }
      function isNumber(value) {
        return typeof value == 'number' || (isObjectLike(value) && objectToString.call(value) == numberTag);
      }
      function isPlainObject(value) {
        if (!isObjectLike(value) || objectToString.call(value) != objectTag || isHostObject(value)) {
          return false;
        }
        var proto = getPrototype(value);
        if (proto === null) {
          return true;
        }
        var Ctor = hasOwnProperty.call(proto, 'constructor') && proto.constructor;
        return (typeof Ctor == 'function' && Ctor instanceof Ctor && funcToString.call(Ctor) == objectCtorString);
      }
      function isRegExp(value) {
        return isObject(value) && objectToString.call(value) == regexpTag;
      }
      function isSafeInteger(value) {
        return isInteger(value) && value >= -MAX_SAFE_INTEGER && value <= MAX_SAFE_INTEGER;
      }
      function isSet(value) {
        return isObjectLike(value) && getTag(value) == setTag;
      }
      function isString(value) {
        return typeof value == 'string' || (!isArray(value) && isObjectLike(value) && objectToString.call(value) == stringTag);
      }
      function isSymbol(value) {
        return typeof value == 'symbol' || (isObjectLike(value) && objectToString.call(value) == symbolTag);
      }
      function isTypedArray(value) {
        return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objectToString.call(value)];
      }
      function isUndefined(value) {
        return value === undefined;
      }
      function isWeakMap(value) {
        return isObjectLike(value) && getTag(value) == weakMapTag;
      }
      function isWeakSet(value) {
        return isObjectLike(value) && objectToString.call(value) == weakSetTag;
      }
      function lt(value, other) {
        return value < other;
      }
      function lte(value, other) {
        return value <= other;
      }
      function toArray(value) {
        if (!value) {
          return [];
        }
        if (isArrayLike(value)) {
          return isString(value) ? stringToArray(value) : copyArray(value);
        }
        if (iteratorSymbol && value[iteratorSymbol]) {
          return iteratorToArray(value[iteratorSymbol]());
        }
        var tag = getTag(value),
            func = tag == mapTag ? mapToArray : (tag == setTag ? setToArray : values);
        return func(value);
      }
      function toInteger(value) {
        if (!value) {
          return value === 0 ? value : 0;
        }
        value = toNumber(value);
        if (value === INFINITY || value === -INFINITY) {
          var sign = (value < 0 ? -1 : 1);
          return sign * MAX_INTEGER;
        }
        var remainder = value % 1;
        return value === value ? (remainder ? value - remainder : value) : 0;
      }
      function toLength(value) {
        return value ? baseClamp(toInteger(value), 0, MAX_ARRAY_LENGTH) : 0;
      }
      function toNumber(value) {
        if (typeof value == 'number') {
          return value;
        }
        if (isSymbol(value)) {
          return NAN;
        }
        if (isObject(value)) {
          var other = isFunction(value.valueOf) ? value.valueOf() : value;
          value = isObject(other) ? (other + '') : other;
        }
        if (typeof value != 'string') {
          return value === 0 ? value : +value;
        }
        value = value.replace(reTrim, '');
        var isBinary = reIsBinary.test(value);
        return (isBinary || reIsOctal.test(value)) ? freeParseInt(value.slice(2), isBinary ? 2 : 8) : (reIsBadHex.test(value) ? NAN : +value);
      }
      function toPlainObject(value) {
        return copyObject(value, keysIn(value));
      }
      function toSafeInteger(value) {
        return baseClamp(toInteger(value), -MAX_SAFE_INTEGER, MAX_SAFE_INTEGER);
      }
      function toString(value) {
        if (typeof value == 'string') {
          return value;
        }
        if (value == null) {
          return '';
        }
        if (isSymbol(value)) {
          return symbolToString ? symbolToString.call(value) : '';
        }
        var result = (value + '');
        return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
      }
      var assign = createAssigner(function(object, source) {
        if (nonEnumShadows || isPrototype(source) || isArrayLike(source)) {
          copyObject(source, keys(source), object);
          return;
        }
        for (var key in source) {
          if (hasOwnProperty.call(source, key)) {
            assignValue(object, key, source[key]);
          }
        }
      });
      var assignIn = createAssigner(function(object, source) {
        if (nonEnumShadows || isPrototype(source) || isArrayLike(source)) {
          copyObject(source, keysIn(source), object);
          return;
        }
        for (var key in source) {
          assignValue(object, key, source[key]);
        }
      });
      var assignInWith = createAssigner(function(object, source, srcIndex, customizer) {
        copyObject(source, keysIn(source), object, customizer);
      });
      var assignWith = createAssigner(function(object, source, srcIndex, customizer) {
        copyObject(source, keys(source), object, customizer);
      });
      var at = rest(function(object, paths) {
        return baseAt(object, baseFlatten(paths, 1));
      });
      function create(prototype, properties) {
        var result = baseCreate(prototype);
        return properties ? baseAssign(result, properties) : result;
      }
      var defaults = rest(function(args) {
        args.push(undefined, assignInDefaults);
        return apply(assignInWith, undefined, args);
      });
      var defaultsDeep = rest(function(args) {
        args.push(undefined, mergeDefaults);
        return apply(mergeWith, undefined, args);
      });
      function findKey(object, predicate) {
        return baseFind(object, getIteratee(predicate, 3), baseForOwn, true);
      }
      function findLastKey(object, predicate) {
        return baseFind(object, getIteratee(predicate, 3), baseForOwnRight, true);
      }
      function forIn(object, iteratee) {
        return object == null ? object : baseFor(object, getIteratee(iteratee), keysIn);
      }
      function forInRight(object, iteratee) {
        return object == null ? object : baseForRight(object, getIteratee(iteratee), keysIn);
      }
      function forOwn(object, iteratee) {
        return object && baseForOwn(object, getIteratee(iteratee));
      }
      function forOwnRight(object, iteratee) {
        return object && baseForOwnRight(object, getIteratee(iteratee));
      }
      function functions(object) {
        return object == null ? [] : baseFunctions(object, keys(object));
      }
      function functionsIn(object) {
        return object == null ? [] : baseFunctions(object, keysIn(object));
      }
      function get(object, path, defaultValue) {
        var result = object == null ? undefined : baseGet(object, path);
        return result === undefined ? defaultValue : result;
      }
      function has(object, path) {
        return object != null && hasPath(object, path, baseHas);
      }
      function hasIn(object, path) {
        return object != null && hasPath(object, path, baseHasIn);
      }
      var invert = createInverter(function(result, value, key) {
        result[value] = key;
      }, constant(identity));
      var invertBy = createInverter(function(result, value, key) {
        if (hasOwnProperty.call(result, value)) {
          result[value].push(key);
        } else {
          result[value] = [key];
        }
      }, getIteratee);
      var invoke = rest(baseInvoke);
      function keys(object) {
        var isProto = isPrototype(object);
        if (!(isProto || isArrayLike(object))) {
          return baseKeys(object);
        }
        var indexes = indexKeys(object),
            skipIndexes = !!indexes,
            result = indexes || [],
            length = result.length;
        for (var key in object) {
          if (baseHas(object, key) && !(skipIndexes && (key == 'length' || isIndex(key, length))) && !(isProto && key == 'constructor')) {
            result.push(key);
          }
        }
        return result;
      }
      function keysIn(object) {
        var index = -1,
            isProto = isPrototype(object),
            props = baseKeysIn(object),
            propsLength = props.length,
            indexes = indexKeys(object),
            skipIndexes = !!indexes,
            result = indexes || [],
            length = result.length;
        while (++index < propsLength) {
          var key = props[index];
          if (!(skipIndexes && (key == 'length' || isIndex(key, length))) && !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
            result.push(key);
          }
        }
        return result;
      }
      function mapKeys(object, iteratee) {
        var result = {};
        iteratee = getIteratee(iteratee, 3);
        baseForOwn(object, function(value, key, object) {
          result[iteratee(value, key, object)] = value;
        });
        return result;
      }
      function mapValues(object, iteratee) {
        var result = {};
        iteratee = getIteratee(iteratee, 3);
        baseForOwn(object, function(value, key, object) {
          result[key] = iteratee(value, key, object);
        });
        return result;
      }
      var merge = createAssigner(function(object, source, srcIndex) {
        baseMerge(object, source, srcIndex);
      });
      var mergeWith = createAssigner(function(object, source, srcIndex, customizer) {
        baseMerge(object, source, srcIndex, customizer);
      });
      var omit = rest(function(object, props) {
        if (object == null) {
          return {};
        }
        props = arrayMap(baseFlatten(props, 1), toKey);
        return basePick(object, baseDifference(getAllKeysIn(object), props));
      });
      function omitBy(object, predicate) {
        predicate = getIteratee(predicate);
        return basePickBy(object, function(value, key) {
          return !predicate(value, key);
        });
      }
      var pick = rest(function(object, props) {
        return object == null ? {} : basePick(object, baseFlatten(props, 1));
      });
      function pickBy(object, predicate) {
        return object == null ? {} : basePickBy(object, getIteratee(predicate));
      }
      function result(object, path, defaultValue) {
        path = isKey(path, object) ? [path] : castPath(path);
        var index = -1,
            length = path.length;
        if (!length) {
          object = undefined;
          length = 1;
        }
        while (++index < length) {
          var value = object == null ? undefined : object[path[index]];
          if (value === undefined) {
            index = length;
            value = defaultValue;
          }
          object = isFunction(value) ? value.call(object) : value;
        }
        return object;
      }
      function set(object, path, value) {
        return object == null ? object : baseSet(object, path, value);
      }
      function setWith(object, path, value, customizer) {
        customizer = typeof customizer == 'function' ? customizer : undefined;
        return object == null ? object : baseSet(object, path, value, customizer);
      }
      function toPairs(object) {
        return baseToPairs(object, keys(object));
      }
      function toPairsIn(object) {
        return baseToPairs(object, keysIn(object));
      }
      function transform(object, iteratee, accumulator) {
        var isArr = isArray(object) || isTypedArray(object);
        iteratee = getIteratee(iteratee, 4);
        if (accumulator == null) {
          if (isArr || isObject(object)) {
            var Ctor = object.constructor;
            if (isArr) {
              accumulator = isArray(object) ? new Ctor : [];
            } else {
              accumulator = isFunction(Ctor) ? baseCreate(getPrototype(object)) : {};
            }
          } else {
            accumulator = {};
          }
        }
        (isArr ? arrayEach : baseForOwn)(object, function(value, index, object) {
          return iteratee(accumulator, value, index, object);
        });
        return accumulator;
      }
      function unset(object, path) {
        return object == null ? true : baseUnset(object, path);
      }
      function update(object, path, updater) {
        return object == null ? object : baseUpdate(object, path, castFunction(updater));
      }
      function updateWith(object, path, updater, customizer) {
        customizer = typeof customizer == 'function' ? customizer : undefined;
        return object == null ? object : baseUpdate(object, path, castFunction(updater), customizer);
      }
      function values(object) {
        return object ? baseValues(object, keys(object)) : [];
      }
      function valuesIn(object) {
        return object == null ? [] : baseValues(object, keysIn(object));
      }
      function clamp(number, lower, upper) {
        if (upper === undefined) {
          upper = lower;
          lower = undefined;
        }
        if (upper !== undefined) {
          upper = toNumber(upper);
          upper = upper === upper ? upper : 0;
        }
        if (lower !== undefined) {
          lower = toNumber(lower);
          lower = lower === lower ? lower : 0;
        }
        return baseClamp(toNumber(number), lower, upper);
      }
      function inRange(number, start, end) {
        start = toNumber(start) || 0;
        if (end === undefined) {
          end = start;
          start = 0;
        } else {
          end = toNumber(end) || 0;
        }
        number = toNumber(number);
        return baseInRange(number, start, end);
      }
      function random(lower, upper, floating) {
        if (floating && typeof floating != 'boolean' && isIterateeCall(lower, upper, floating)) {
          upper = floating = undefined;
        }
        if (floating === undefined) {
          if (typeof upper == 'boolean') {
            floating = upper;
            upper = undefined;
          } else if (typeof lower == 'boolean') {
            floating = lower;
            lower = undefined;
          }
        }
        if (lower === undefined && upper === undefined) {
          lower = 0;
          upper = 1;
        } else {
          lower = toNumber(lower) || 0;
          if (upper === undefined) {
            upper = lower;
            lower = 0;
          } else {
            upper = toNumber(upper) || 0;
          }
        }
        if (lower > upper) {
          var temp = lower;
          lower = upper;
          upper = temp;
        }
        if (floating || lower % 1 || upper % 1) {
          var rand = nativeRandom();
          return nativeMin(lower + (rand * (upper - lower + freeParseFloat('1e-' + ((rand + '').length - 1)))), upper);
        }
        return baseRandom(lower, upper);
      }
      var camelCase = createCompounder(function(result, word, index) {
        word = word.toLowerCase();
        return result + (index ? capitalize(word) : word);
      });
      function capitalize(string) {
        return upperFirst(toString(string).toLowerCase());
      }
      function deburr(string) {
        string = toString(string);
        return string && string.replace(reLatin1, deburrLetter).replace(reComboMark, '');
      }
      function endsWith(string, target, position) {
        string = toString(string);
        target = typeof target == 'string' ? target : (target + '');
        var length = string.length;
        position = position === undefined ? length : baseClamp(toInteger(position), 0, length);
        position -= target.length;
        return position >= 0 && string.indexOf(target, position) == position;
      }
      function escape(string) {
        string = toString(string);
        return (string && reHasUnescapedHtml.test(string)) ? string.replace(reUnescapedHtml, escapeHtmlChar) : string;
      }
      function escapeRegExp(string) {
        string = toString(string);
        return (string && reHasRegExpChar.test(string)) ? string.replace(reRegExpChar, '\\$&') : string;
      }
      var kebabCase = createCompounder(function(result, word, index) {
        return result + (index ? '-' : '') + word.toLowerCase();
      });
      var lowerCase = createCompounder(function(result, word, index) {
        return result + (index ? ' ' : '') + word.toLowerCase();
      });
      var lowerFirst = createCaseFirst('toLowerCase');
      function pad(string, length, chars) {
        string = toString(string);
        length = toInteger(length);
        var strLength = length ? stringSize(string) : 0;
        if (!length || strLength >= length) {
          return string;
        }
        var mid = (length - strLength) / 2;
        return (createPadding(nativeFloor(mid), chars) + string + createPadding(nativeCeil(mid), chars));
      }
      function padEnd(string, length, chars) {
        string = toString(string);
        length = toInteger(length);
        var strLength = length ? stringSize(string) : 0;
        return (length && strLength < length) ? (string + createPadding(length - strLength, chars)) : string;
      }
      function padStart(string, length, chars) {
        string = toString(string);
        length = toInteger(length);
        var strLength = length ? stringSize(string) : 0;
        return (length && strLength < length) ? (createPadding(length - strLength, chars) + string) : string;
      }
      function parseInt(string, radix, guard) {
        if (guard || radix == null) {
          radix = 0;
        } else if (radix) {
          radix = +radix;
        }
        string = toString(string).replace(reTrim, '');
        return nativeParseInt(string, radix || (reHasHexPrefix.test(string) ? 16 : 10));
      }
      function repeat(string, n, guard) {
        if ((guard ? isIterateeCall(string, n, guard) : n === undefined)) {
          n = 1;
        } else {
          n = toInteger(n);
        }
        return baseRepeat(toString(string), n);
      }
      function replace() {
        var args = arguments,
            string = toString(args[0]);
        return args.length < 3 ? string : nativeReplace.call(string, args[1], args[2]);
      }
      var snakeCase = createCompounder(function(result, word, index) {
        return result + (index ? '_' : '') + word.toLowerCase();
      });
      function split(string, separator, limit) {
        if (limit && typeof limit != 'number' && isIterateeCall(string, separator, limit)) {
          separator = limit = undefined;
        }
        limit = limit === undefined ? MAX_ARRAY_LENGTH : limit >>> 0;
        if (!limit) {
          return [];
        }
        string = toString(string);
        if (string && (typeof separator == 'string' || (separator != null && !isRegExp(separator)))) {
          separator += '';
          if (separator == '' && reHasComplexSymbol.test(string)) {
            return castSlice(stringToArray(string), 0, limit);
          }
        }
        return nativeSplit.call(string, separator, limit);
      }
      var startCase = createCompounder(function(result, word, index) {
        return result + (index ? ' ' : '') + upperFirst(word);
      });
      function startsWith(string, target, position) {
        string = toString(string);
        position = baseClamp(toInteger(position), 0, string.length);
        return string.lastIndexOf(target, position) == position;
      }
      function template(string, options, guard) {
        var settings = lodash.templateSettings;
        if (guard && isIterateeCall(string, options, guard)) {
          options = undefined;
        }
        string = toString(string);
        options = assignInWith({}, options, settings, assignInDefaults);
        var imports = assignInWith({}, options.imports, settings.imports, assignInDefaults),
            importsKeys = keys(imports),
            importsValues = baseValues(imports, importsKeys);
        var isEscaping,
            isEvaluating,
            index = 0,
            interpolate = options.interpolate || reNoMatch,
            source = "__p += '";
        var reDelimiters = RegExp((options.escape || reNoMatch).source + '|' + interpolate.source + '|' + (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' + (options.evaluate || reNoMatch).source + '|$', 'g');
        var sourceURL = '//# sourceURL=' + ('sourceURL' in options ? options.sourceURL : ('lodash.templateSources[' + (++templateCounter) + ']')) + '\n';
        string.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
          interpolateValue || (interpolateValue = esTemplateValue);
          source += string.slice(index, offset).replace(reUnescapedString, escapeStringChar);
          if (escapeValue) {
            isEscaping = true;
            source += "' +\n__e(" + escapeValue + ") +\n'";
          }
          if (evaluateValue) {
            isEvaluating = true;
            source += "';\n" + evaluateValue + ";\n__p += '";
          }
          if (interpolateValue) {
            source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
          }
          index = offset + match.length;
          return match;
        });
        source += "';\n";
        var variable = options.variable;
        if (!variable) {
          source = 'with (obj) {\n' + source + '\n}\n';
        }
        source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source).replace(reEmptyStringMiddle, '$1').replace(reEmptyStringTrailing, '$1;');
        source = 'function(' + (variable || 'obj') + ') {\n' + (variable ? '' : 'obj || (obj = {});\n') + "var __t, __p = ''" + (isEscaping ? ', __e = _.escape' : '') + (isEvaluating ? ', __j = Array.prototype.join;\n' + "function print() { __p += __j.call(arguments, '') }\n" : ';\n') + source + 'return __p\n}';
        var result = attempt(function() {
          return Function(importsKeys, sourceURL + 'return ' + source).apply(undefined, importsValues);
        });
        result.source = source;
        if (isError(result)) {
          throw result;
        }
        return result;
      }
      function toLower(value) {
        return toString(value).toLowerCase();
      }
      function toUpper(value) {
        return toString(value).toUpperCase();
      }
      function trim(string, chars, guard) {
        string = toString(string);
        if (!string) {
          return string;
        }
        if (guard || chars === undefined) {
          return string.replace(reTrim, '');
        }
        if (!(chars += '')) {
          return string;
        }
        var strSymbols = stringToArray(string),
            chrSymbols = stringToArray(chars),
            start = charsStartIndex(strSymbols, chrSymbols),
            end = charsEndIndex(strSymbols, chrSymbols) + 1;
        return castSlice(strSymbols, start, end).join('');
      }
      function trimEnd(string, chars, guard) {
        string = toString(string);
        if (!string) {
          return string;
        }
        if (guard || chars === undefined) {
          return string.replace(reTrimEnd, '');
        }
        if (!(chars += '')) {
          return string;
        }
        var strSymbols = stringToArray(string),
            end = charsEndIndex(strSymbols, stringToArray(chars)) + 1;
        return castSlice(strSymbols, 0, end).join('');
      }
      function trimStart(string, chars, guard) {
        string = toString(string);
        if (!string) {
          return string;
        }
        if (guard || chars === undefined) {
          return string.replace(reTrimStart, '');
        }
        if (!(chars += '')) {
          return string;
        }
        var strSymbols = stringToArray(string),
            start = charsStartIndex(strSymbols, stringToArray(chars));
        return castSlice(strSymbols, start).join('');
      }
      function truncate(string, options) {
        var length = DEFAULT_TRUNC_LENGTH,
            omission = DEFAULT_TRUNC_OMISSION;
        if (isObject(options)) {
          var separator = 'separator' in options ? options.separator : separator;
          length = 'length' in options ? toInteger(options.length) : length;
          omission = 'omission' in options ? toString(options.omission) : omission;
        }
        string = toString(string);
        var strLength = string.length;
        if (reHasComplexSymbol.test(string)) {
          var strSymbols = stringToArray(string);
          strLength = strSymbols.length;
        }
        if (length >= strLength) {
          return string;
        }
        var end = length - stringSize(omission);
        if (end < 1) {
          return omission;
        }
        var result = strSymbols ? castSlice(strSymbols, 0, end).join('') : string.slice(0, end);
        if (separator === undefined) {
          return result + omission;
        }
        if (strSymbols) {
          end += (result.length - end);
        }
        if (isRegExp(separator)) {
          if (string.slice(end).search(separator)) {
            var match,
                substring = result;
            if (!separator.global) {
              separator = RegExp(separator.source, toString(reFlags.exec(separator)) + 'g');
            }
            separator.lastIndex = 0;
            while ((match = separator.exec(substring))) {
              var newEnd = match.index;
            }
            result = result.slice(0, newEnd === undefined ? end : newEnd);
          }
        } else if (string.indexOf(separator, end) != end) {
          var index = result.lastIndexOf(separator);
          if (index > -1) {
            result = result.slice(0, index);
          }
        }
        return result + omission;
      }
      function unescape(string) {
        string = toString(string);
        return (string && reHasEscapedHtml.test(string)) ? string.replace(reEscapedHtml, unescapeHtmlChar) : string;
      }
      var upperCase = createCompounder(function(result, word, index) {
        return result + (index ? ' ' : '') + word.toUpperCase();
      });
      var upperFirst = createCaseFirst('toUpperCase');
      function words(string, pattern, guard) {
        string = toString(string);
        pattern = guard ? undefined : pattern;
        if (pattern === undefined) {
          pattern = reHasComplexWord.test(string) ? reComplexWord : reBasicWord;
        }
        return string.match(pattern) || [];
      }
      var attempt = rest(function(func, args) {
        try {
          return apply(func, undefined, args);
        } catch (e) {
          return isError(e) ? e : new Error(e);
        }
      });
      var bindAll = rest(function(object, methodNames) {
        arrayEach(baseFlatten(methodNames, 1), function(key) {
          object[key] = bind(object[key], object);
        });
        return object;
      });
      function cond(pairs) {
        var length = pairs ? pairs.length : 0,
            toIteratee = getIteratee();
        pairs = !length ? [] : arrayMap(pairs, function(pair) {
          if (typeof pair[1] != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          return [toIteratee(pair[0]), pair[1]];
        });
        return rest(function(args) {
          var index = -1;
          while (++index < length) {
            var pair = pairs[index];
            if (apply(pair[0], this, args)) {
              return apply(pair[1], this, args);
            }
          }
        });
      }
      function conforms(source) {
        return baseConforms(baseClone(source, true));
      }
      function constant(value) {
        return function() {
          return value;
        };
      }
      var flow = createFlow();
      var flowRight = createFlow(true);
      function identity(value) {
        return value;
      }
      function iteratee(func) {
        return baseIteratee(typeof func == 'function' ? func : baseClone(func, true));
      }
      function matches(source) {
        return baseMatches(baseClone(source, true));
      }
      function matchesProperty(path, srcValue) {
        return baseMatchesProperty(path, baseClone(srcValue, true));
      }
      var method = rest(function(path, args) {
        return function(object) {
          return baseInvoke(object, path, args);
        };
      });
      var methodOf = rest(function(object, args) {
        return function(path) {
          return baseInvoke(object, path, args);
        };
      });
      function mixin(object, source, options) {
        var props = keys(source),
            methodNames = baseFunctions(source, props);
        if (options == null && !(isObject(source) && (methodNames.length || !props.length))) {
          options = source;
          source = object;
          object = this;
          methodNames = baseFunctions(source, keys(source));
        }
        var chain = !(isObject(options) && 'chain' in options) || !!options.chain,
            isFunc = isFunction(object);
        arrayEach(methodNames, function(methodName) {
          var func = source[methodName];
          object[methodName] = func;
          if (isFunc) {
            object.prototype[methodName] = function() {
              var chainAll = this.__chain__;
              if (chain || chainAll) {
                var result = object(this.__wrapped__),
                    actions = result.__actions__ = copyArray(this.__actions__);
                actions.push({
                  'func': func,
                  'args': arguments,
                  'thisArg': object
                });
                result.__chain__ = chainAll;
                return result;
              }
              return func.apply(object, arrayPush([this.value()], arguments));
            };
          }
        });
        return object;
      }
      function noConflict() {
        if (root._ === this) {
          root._ = oldDash;
        }
        return this;
      }
      function noop() {}
      function nthArg(n) {
        n = toInteger(n);
        return rest(function(args) {
          return baseNth(args, n);
        });
      }
      var over = createOver(arrayMap);
      var overEvery = createOver(arrayEvery);
      var overSome = createOver(arraySome);
      function property(path) {
        return isKey(path) ? baseProperty(path) : basePropertyDeep(path);
      }
      function propertyOf(object) {
        return function(path) {
          return object == null ? undefined : baseGet(object, path);
        };
      }
      var range = createRange();
      var rangeRight = createRange(true);
      function times(n, iteratee) {
        n = toInteger(n);
        if (n < 1 || n > MAX_SAFE_INTEGER) {
          return [];
        }
        var index = MAX_ARRAY_LENGTH,
            length = nativeMin(n, MAX_ARRAY_LENGTH);
        iteratee = getIteratee(iteratee);
        n -= MAX_ARRAY_LENGTH;
        var result = baseTimes(length, iteratee);
        while (++index < n) {
          iteratee(index);
        }
        return result;
      }
      function toPath(value) {
        if (isArray(value)) {
          return arrayMap(value, toKey);
        }
        return isSymbol(value) ? [value] : copyArray(stringToPath(value));
      }
      function uniqueId(prefix) {
        var id = ++idCounter;
        return toString(prefix) + id;
      }
      var add = createMathOperation(function(augend, addend) {
        return augend + addend;
      });
      var ceil = createRound('ceil');
      var divide = createMathOperation(function(dividend, divisor) {
        return dividend / divisor;
      });
      var floor = createRound('floor');
      function max(array) {
        return (array && array.length) ? baseExtremum(array, identity, gt) : undefined;
      }
      function maxBy(array, iteratee) {
        return (array && array.length) ? baseExtremum(array, getIteratee(iteratee), gt) : undefined;
      }
      function mean(array) {
        return baseMean(array, identity);
      }
      function meanBy(array, iteratee) {
        return baseMean(array, getIteratee(iteratee));
      }
      function min(array) {
        return (array && array.length) ? baseExtremum(array, identity, lt) : undefined;
      }
      function minBy(array, iteratee) {
        return (array && array.length) ? baseExtremum(array, getIteratee(iteratee), lt) : undefined;
      }
      var multiply = createMathOperation(function(multiplier, multiplicand) {
        return multiplier * multiplicand;
      });
      var round = createRound('round');
      var subtract = createMathOperation(function(minuend, subtrahend) {
        return minuend - subtrahend;
      });
      function sum(array) {
        return (array && array.length) ? baseSum(array, identity) : 0;
      }
      function sumBy(array, iteratee) {
        return (array && array.length) ? baseSum(array, getIteratee(iteratee)) : 0;
      }
      lodash.after = after;
      lodash.ary = ary;
      lodash.assign = assign;
      lodash.assignIn = assignIn;
      lodash.assignInWith = assignInWith;
      lodash.assignWith = assignWith;
      lodash.at = at;
      lodash.before = before;
      lodash.bind = bind;
      lodash.bindAll = bindAll;
      lodash.bindKey = bindKey;
      lodash.castArray = castArray;
      lodash.chain = chain;
      lodash.chunk = chunk;
      lodash.compact = compact;
      lodash.concat = concat;
      lodash.cond = cond;
      lodash.conforms = conforms;
      lodash.constant = constant;
      lodash.countBy = countBy;
      lodash.create = create;
      lodash.curry = curry;
      lodash.curryRight = curryRight;
      lodash.debounce = debounce;
      lodash.defaults = defaults;
      lodash.defaultsDeep = defaultsDeep;
      lodash.defer = defer;
      lodash.delay = delay;
      lodash.difference = difference;
      lodash.differenceBy = differenceBy;
      lodash.differenceWith = differenceWith;
      lodash.drop = drop;
      lodash.dropRight = dropRight;
      lodash.dropRightWhile = dropRightWhile;
      lodash.dropWhile = dropWhile;
      lodash.fill = fill;
      lodash.filter = filter;
      lodash.flatMap = flatMap;
      lodash.flatMapDeep = flatMapDeep;
      lodash.flatMapDepth = flatMapDepth;
      lodash.flatten = flatten;
      lodash.flattenDeep = flattenDeep;
      lodash.flattenDepth = flattenDepth;
      lodash.flip = flip;
      lodash.flow = flow;
      lodash.flowRight = flowRight;
      lodash.fromPairs = fromPairs;
      lodash.functions = functions;
      lodash.functionsIn = functionsIn;
      lodash.groupBy = groupBy;
      lodash.initial = initial;
      lodash.intersection = intersection;
      lodash.intersectionBy = intersectionBy;
      lodash.intersectionWith = intersectionWith;
      lodash.invert = invert;
      lodash.invertBy = invertBy;
      lodash.invokeMap = invokeMap;
      lodash.iteratee = iteratee;
      lodash.keyBy = keyBy;
      lodash.keys = keys;
      lodash.keysIn = keysIn;
      lodash.map = map;
      lodash.mapKeys = mapKeys;
      lodash.mapValues = mapValues;
      lodash.matches = matches;
      lodash.matchesProperty = matchesProperty;
      lodash.memoize = memoize;
      lodash.merge = merge;
      lodash.mergeWith = mergeWith;
      lodash.method = method;
      lodash.methodOf = methodOf;
      lodash.mixin = mixin;
      lodash.negate = negate;
      lodash.nthArg = nthArg;
      lodash.omit = omit;
      lodash.omitBy = omitBy;
      lodash.once = once;
      lodash.orderBy = orderBy;
      lodash.over = over;
      lodash.overArgs = overArgs;
      lodash.overEvery = overEvery;
      lodash.overSome = overSome;
      lodash.partial = partial;
      lodash.partialRight = partialRight;
      lodash.partition = partition;
      lodash.pick = pick;
      lodash.pickBy = pickBy;
      lodash.property = property;
      lodash.propertyOf = propertyOf;
      lodash.pull = pull;
      lodash.pullAll = pullAll;
      lodash.pullAllBy = pullAllBy;
      lodash.pullAllWith = pullAllWith;
      lodash.pullAt = pullAt;
      lodash.range = range;
      lodash.rangeRight = rangeRight;
      lodash.rearg = rearg;
      lodash.reject = reject;
      lodash.remove = remove;
      lodash.rest = rest;
      lodash.reverse = reverse;
      lodash.sampleSize = sampleSize;
      lodash.set = set;
      lodash.setWith = setWith;
      lodash.shuffle = shuffle;
      lodash.slice = slice;
      lodash.sortBy = sortBy;
      lodash.sortedUniq = sortedUniq;
      lodash.sortedUniqBy = sortedUniqBy;
      lodash.split = split;
      lodash.spread = spread;
      lodash.tail = tail;
      lodash.take = take;
      lodash.takeRight = takeRight;
      lodash.takeRightWhile = takeRightWhile;
      lodash.takeWhile = takeWhile;
      lodash.tap = tap;
      lodash.throttle = throttle;
      lodash.thru = thru;
      lodash.toArray = toArray;
      lodash.toPairs = toPairs;
      lodash.toPairsIn = toPairsIn;
      lodash.toPath = toPath;
      lodash.toPlainObject = toPlainObject;
      lodash.transform = transform;
      lodash.unary = unary;
      lodash.union = union;
      lodash.unionBy = unionBy;
      lodash.unionWith = unionWith;
      lodash.uniq = uniq;
      lodash.uniqBy = uniqBy;
      lodash.uniqWith = uniqWith;
      lodash.unset = unset;
      lodash.unzip = unzip;
      lodash.unzipWith = unzipWith;
      lodash.update = update;
      lodash.updateWith = updateWith;
      lodash.values = values;
      lodash.valuesIn = valuesIn;
      lodash.without = without;
      lodash.words = words;
      lodash.wrap = wrap;
      lodash.xor = xor;
      lodash.xorBy = xorBy;
      lodash.xorWith = xorWith;
      lodash.zip = zip;
      lodash.zipObject = zipObject;
      lodash.zipObjectDeep = zipObjectDeep;
      lodash.zipWith = zipWith;
      lodash.entries = toPairs;
      lodash.entriesIn = toPairsIn;
      lodash.extend = assignIn;
      lodash.extendWith = assignInWith;
      mixin(lodash, lodash);
      lodash.add = add;
      lodash.attempt = attempt;
      lodash.camelCase = camelCase;
      lodash.capitalize = capitalize;
      lodash.ceil = ceil;
      lodash.clamp = clamp;
      lodash.clone = clone;
      lodash.cloneDeep = cloneDeep;
      lodash.cloneDeepWith = cloneDeepWith;
      lodash.cloneWith = cloneWith;
      lodash.deburr = deburr;
      lodash.divide = divide;
      lodash.endsWith = endsWith;
      lodash.eq = eq;
      lodash.escape = escape;
      lodash.escapeRegExp = escapeRegExp;
      lodash.every = every;
      lodash.find = find;
      lodash.findIndex = findIndex;
      lodash.findKey = findKey;
      lodash.findLast = findLast;
      lodash.findLastIndex = findLastIndex;
      lodash.findLastKey = findLastKey;
      lodash.floor = floor;
      lodash.forEach = forEach;
      lodash.forEachRight = forEachRight;
      lodash.forIn = forIn;
      lodash.forInRight = forInRight;
      lodash.forOwn = forOwn;
      lodash.forOwnRight = forOwnRight;
      lodash.get = get;
      lodash.gt = gt;
      lodash.gte = gte;
      lodash.has = has;
      lodash.hasIn = hasIn;
      lodash.head = head;
      lodash.identity = identity;
      lodash.includes = includes;
      lodash.indexOf = indexOf;
      lodash.inRange = inRange;
      lodash.invoke = invoke;
      lodash.isArguments = isArguments;
      lodash.isArray = isArray;
      lodash.isArrayBuffer = isArrayBuffer;
      lodash.isArrayLike = isArrayLike;
      lodash.isArrayLikeObject = isArrayLikeObject;
      lodash.isBoolean = isBoolean;
      lodash.isBuffer = isBuffer;
      lodash.isDate = isDate;
      lodash.isElement = isElement;
      lodash.isEmpty = isEmpty;
      lodash.isEqual = isEqual;
      lodash.isEqualWith = isEqualWith;
      lodash.isError = isError;
      lodash.isFinite = isFinite;
      lodash.isFunction = isFunction;
      lodash.isInteger = isInteger;
      lodash.isLength = isLength;
      lodash.isMap = isMap;
      lodash.isMatch = isMatch;
      lodash.isMatchWith = isMatchWith;
      lodash.isNaN = isNaN;
      lodash.isNative = isNative;
      lodash.isNil = isNil;
      lodash.isNull = isNull;
      lodash.isNumber = isNumber;
      lodash.isObject = isObject;
      lodash.isObjectLike = isObjectLike;
      lodash.isPlainObject = isPlainObject;
      lodash.isRegExp = isRegExp;
      lodash.isSafeInteger = isSafeInteger;
      lodash.isSet = isSet;
      lodash.isString = isString;
      lodash.isSymbol = isSymbol;
      lodash.isTypedArray = isTypedArray;
      lodash.isUndefined = isUndefined;
      lodash.isWeakMap = isWeakMap;
      lodash.isWeakSet = isWeakSet;
      lodash.join = join;
      lodash.kebabCase = kebabCase;
      lodash.last = last;
      lodash.lastIndexOf = lastIndexOf;
      lodash.lowerCase = lowerCase;
      lodash.lowerFirst = lowerFirst;
      lodash.lt = lt;
      lodash.lte = lte;
      lodash.max = max;
      lodash.maxBy = maxBy;
      lodash.mean = mean;
      lodash.meanBy = meanBy;
      lodash.min = min;
      lodash.minBy = minBy;
      lodash.multiply = multiply;
      lodash.nth = nth;
      lodash.noConflict = noConflict;
      lodash.noop = noop;
      lodash.now = now;
      lodash.pad = pad;
      lodash.padEnd = padEnd;
      lodash.padStart = padStart;
      lodash.parseInt = parseInt;
      lodash.random = random;
      lodash.reduce = reduce;
      lodash.reduceRight = reduceRight;
      lodash.repeat = repeat;
      lodash.replace = replace;
      lodash.result = result;
      lodash.round = round;
      lodash.runInContext = runInContext;
      lodash.sample = sample;
      lodash.size = size;
      lodash.snakeCase = snakeCase;
      lodash.some = some;
      lodash.sortedIndex = sortedIndex;
      lodash.sortedIndexBy = sortedIndexBy;
      lodash.sortedIndexOf = sortedIndexOf;
      lodash.sortedLastIndex = sortedLastIndex;
      lodash.sortedLastIndexBy = sortedLastIndexBy;
      lodash.sortedLastIndexOf = sortedLastIndexOf;
      lodash.startCase = startCase;
      lodash.startsWith = startsWith;
      lodash.subtract = subtract;
      lodash.sum = sum;
      lodash.sumBy = sumBy;
      lodash.template = template;
      lodash.times = times;
      lodash.toInteger = toInteger;
      lodash.toLength = toLength;
      lodash.toLower = toLower;
      lodash.toNumber = toNumber;
      lodash.toSafeInteger = toSafeInteger;
      lodash.toString = toString;
      lodash.toUpper = toUpper;
      lodash.trim = trim;
      lodash.trimEnd = trimEnd;
      lodash.trimStart = trimStart;
      lodash.truncate = truncate;
      lodash.unescape = unescape;
      lodash.uniqueId = uniqueId;
      lodash.upperCase = upperCase;
      lodash.upperFirst = upperFirst;
      lodash.each = forEach;
      lodash.eachRight = forEachRight;
      lodash.first = head;
      mixin(lodash, (function() {
        var source = {};
        baseForOwn(lodash, function(func, methodName) {
          if (!hasOwnProperty.call(lodash.prototype, methodName)) {
            source[methodName] = func;
          }
        });
        return source;
      }()), {'chain': false});
      lodash.VERSION = VERSION;
      arrayEach(['bind', 'bindKey', 'curry', 'curryRight', 'partial', 'partialRight'], function(methodName) {
        lodash[methodName].placeholder = lodash;
      });
      arrayEach(['drop', 'take'], function(methodName, index) {
        LazyWrapper.prototype[methodName] = function(n) {
          var filtered = this.__filtered__;
          if (filtered && !index) {
            return new LazyWrapper(this);
          }
          n = n === undefined ? 1 : nativeMax(toInteger(n), 0);
          var result = this.clone();
          if (filtered) {
            result.__takeCount__ = nativeMin(n, result.__takeCount__);
          } else {
            result.__views__.push({
              'size': nativeMin(n, MAX_ARRAY_LENGTH),
              'type': methodName + (result.__dir__ < 0 ? 'Right' : '')
            });
          }
          return result;
        };
        LazyWrapper.prototype[methodName + 'Right'] = function(n) {
          return this.reverse()[methodName](n).reverse();
        };
      });
      arrayEach(['filter', 'map', 'takeWhile'], function(methodName, index) {
        var type = index + 1,
            isFilter = type == LAZY_FILTER_FLAG || type == LAZY_WHILE_FLAG;
        LazyWrapper.prototype[methodName] = function(iteratee) {
          var result = this.clone();
          result.__iteratees__.push({
            'iteratee': getIteratee(iteratee, 3),
            'type': type
          });
          result.__filtered__ = result.__filtered__ || isFilter;
          return result;
        };
      });
      arrayEach(['head', 'last'], function(methodName, index) {
        var takeName = 'take' + (index ? 'Right' : '');
        LazyWrapper.prototype[methodName] = function() {
          return this[takeName](1).value()[0];
        };
      });
      arrayEach(['initial', 'tail'], function(methodName, index) {
        var dropName = 'drop' + (index ? '' : 'Right');
        LazyWrapper.prototype[methodName] = function() {
          return this.__filtered__ ? new LazyWrapper(this) : this[dropName](1);
        };
      });
      LazyWrapper.prototype.compact = function() {
        return this.filter(identity);
      };
      LazyWrapper.prototype.find = function(predicate) {
        return this.filter(predicate).head();
      };
      LazyWrapper.prototype.findLast = function(predicate) {
        return this.reverse().find(predicate);
      };
      LazyWrapper.prototype.invokeMap = rest(function(path, args) {
        if (typeof path == 'function') {
          return new LazyWrapper(this);
        }
        return this.map(function(value) {
          return baseInvoke(value, path, args);
        });
      });
      LazyWrapper.prototype.reject = function(predicate) {
        predicate = getIteratee(predicate, 3);
        return this.filter(function(value) {
          return !predicate(value);
        });
      };
      LazyWrapper.prototype.slice = function(start, end) {
        start = toInteger(start);
        var result = this;
        if (result.__filtered__ && (start > 0 || end < 0)) {
          return new LazyWrapper(result);
        }
        if (start < 0) {
          result = result.takeRight(-start);
        } else if (start) {
          result = result.drop(start);
        }
        if (end !== undefined) {
          end = toInteger(end);
          result = end < 0 ? result.dropRight(-end) : result.take(end - start);
        }
        return result;
      };
      LazyWrapper.prototype.takeRightWhile = function(predicate) {
        return this.reverse().takeWhile(predicate).reverse();
      };
      LazyWrapper.prototype.toArray = function() {
        return this.take(MAX_ARRAY_LENGTH);
      };
      baseForOwn(LazyWrapper.prototype, function(func, methodName) {
        var checkIteratee = /^(?:filter|find|map|reject)|While$/.test(methodName),
            isTaker = /^(?:head|last)$/.test(methodName),
            lodashFunc = lodash[isTaker ? ('take' + (methodName == 'last' ? 'Right' : '')) : methodName],
            retUnwrapped = isTaker || /^find/.test(methodName);
        if (!lodashFunc) {
          return;
        }
        lodash.prototype[methodName] = function() {
          var value = this.__wrapped__,
              args = isTaker ? [1] : arguments,
              isLazy = value instanceof LazyWrapper,
              iteratee = args[0],
              useLazy = isLazy || isArray(value);
          var interceptor = function(value) {
            var result = lodashFunc.apply(lodash, arrayPush([value], args));
            return (isTaker && chainAll) ? result[0] : result;
          };
          if (useLazy && checkIteratee && typeof iteratee == 'function' && iteratee.length != 1) {
            isLazy = useLazy = false;
          }
          var chainAll = this.__chain__,
              isHybrid = !!this.__actions__.length,
              isUnwrapped = retUnwrapped && !chainAll,
              onlyLazy = isLazy && !isHybrid;
          if (!retUnwrapped && useLazy) {
            value = onlyLazy ? value : new LazyWrapper(this);
            var result = func.apply(value, args);
            result.__actions__.push({
              'func': thru,
              'args': [interceptor],
              'thisArg': undefined
            });
            return new LodashWrapper(result, chainAll);
          }
          if (isUnwrapped && onlyLazy) {
            return func.apply(this, args);
          }
          result = this.thru(interceptor);
          return isUnwrapped ? (isTaker ? result.value()[0] : result.value()) : result;
        };
      });
      arrayEach(['pop', 'push', 'shift', 'sort', 'splice', 'unshift'], function(methodName) {
        var func = arrayProto[methodName],
            chainName = /^(?:push|sort|unshift)$/.test(methodName) ? 'tap' : 'thru',
            retUnwrapped = /^(?:pop|shift)$/.test(methodName);
        lodash.prototype[methodName] = function() {
          var args = arguments;
          if (retUnwrapped && !this.__chain__) {
            var value = this.value();
            return func.apply(isArray(value) ? value : [], args);
          }
          return this[chainName](function(value) {
            return func.apply(isArray(value) ? value : [], args);
          });
        };
      });
      baseForOwn(LazyWrapper.prototype, function(func, methodName) {
        var lodashFunc = lodash[methodName];
        if (lodashFunc) {
          var key = (lodashFunc.name + ''),
              names = realNames[key] || (realNames[key] = []);
          names.push({
            'name': methodName,
            'func': lodashFunc
          });
        }
      });
      realNames[createHybridWrapper(undefined, BIND_KEY_FLAG).name] = [{
        'name': 'wrapper',
        'func': undefined
      }];
      LazyWrapper.prototype.clone = lazyClone;
      LazyWrapper.prototype.reverse = lazyReverse;
      LazyWrapper.prototype.value = lazyValue;
      lodash.prototype.at = wrapperAt;
      lodash.prototype.chain = wrapperChain;
      lodash.prototype.commit = wrapperCommit;
      lodash.prototype.next = wrapperNext;
      lodash.prototype.plant = wrapperPlant;
      lodash.prototype.reverse = wrapperReverse;
      lodash.prototype.toJSON = lodash.prototype.valueOf = lodash.prototype.value = wrapperValue;
      if (iteratorSymbol) {
        lodash.prototype[iteratorSymbol] = wrapperToIterator;
      }
      return lodash;
    }
    var _ = runInContext();
    (freeWindow || freeSelf || {})._ = _;
    if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
      define(function() {
        return _;
      });
    } else if (freeExports && freeModule) {
      if (moduleExports) {
        (freeModule.exports = _)._ = _;
      }
      freeExports._ = _;
    } else {
      root._ = _;
    }
  }.call(this));
  return module.exports;
});

$__System.registerDynamic("3c", [], false, function($__require, $__exports, $__module) {
  var _retrieveGlobal = $__System.get("@@global-helpers").prepareGlobal($__module.id, null, null);
  (function() {
    !function(e) {
      if ("object" == typeof exports && "undefined" != typeof module)
        module.exports = e();
      else if ("function" == typeof define && define.amd)
        define([], e);
      else {
        var f;
        "undefined" != typeof window ? f = window : "undefined" != typeof global ? f = global : "undefined" != typeof self && (f = self), f.Promise = e();
      }
    }(function() {
      var define,
          module,
          exports;
      return (function e(t, n, r) {
        function s(o, u) {
          if (!n[o]) {
            if (!t[o]) {
              var a = typeof _dereq_ == "function" && _dereq_;
              if (!u && a)
                return a(o, !0);
              if (i)
                return i(o, !0);
              var f = new Error("Cannot find module '" + o + "'");
              throw f.code = "MODULE_NOT_FOUND", f;
            }
            var l = n[o] = {exports: {}};
            t[o][0].call(l.exports, function(e) {
              var n = t[o][1][e];
              return s(n ? n : e);
            }, l, l.exports, e, t, n, r);
          }
          return n[o].exports;
        }
        var i = typeof _dereq_ == "function" && _dereq_;
        for (var o = 0; o < r.length; o++)
          s(r[o]);
        return s;
      })({
        1: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise) {
            var SomePromiseArray = Promise._SomePromiseArray;
            function any(promises) {
              var ret = new SomePromiseArray(promises);
              var promise = ret.promise();
              ret.setHowMany(1);
              ret.setUnwrap();
              ret.init();
              return promise;
            }
            Promise.any = function(promises) {
              return any(promises);
            };
            Promise.prototype.any = function() {
              return any(this);
            };
          };
        }, {}],
        2: [function(_dereq_, module, exports) {
          "use strict";
          var firstLineError;
          try {
            throw new Error();
          } catch (e) {
            firstLineError = e;
          }
          var schedule = _dereq_("./schedule");
          var Queue = _dereq_("./queue");
          var util = _dereq_("./util");
          function Async() {
            this._customScheduler = false;
            this._isTickUsed = false;
            this._lateQueue = new Queue(16);
            this._normalQueue = new Queue(16);
            this._haveDrainedQueues = false;
            this._trampolineEnabled = true;
            var self = this;
            this.drainQueues = function() {
              self._drainQueues();
            };
            this._schedule = schedule;
          }
          Async.prototype.setScheduler = function(fn) {
            var prev = this._schedule;
            this._schedule = fn;
            this._customScheduler = true;
            return prev;
          };
          Async.prototype.hasCustomScheduler = function() {
            return this._customScheduler;
          };
          Async.prototype.enableTrampoline = function() {
            this._trampolineEnabled = true;
          };
          Async.prototype.disableTrampolineIfNecessary = function() {
            if (util.hasDevTools) {
              this._trampolineEnabled = false;
            }
          };
          Async.prototype.haveItemsQueued = function() {
            return this._isTickUsed || this._haveDrainedQueues;
          };
          Async.prototype.fatalError = function(e, isNode) {
            if (isNode) {
              process.stderr.write("Fatal " + (e instanceof Error ? e.stack : e) + "\n");
              process.exit(2);
            } else {
              this.throwLater(e);
            }
          };
          Async.prototype.throwLater = function(fn, arg) {
            if (arguments.length === 1) {
              arg = fn;
              fn = function() {
                throw arg;
              };
            }
            if (typeof setTimeout !== "undefined") {
              setTimeout(function() {
                fn(arg);
              }, 0);
            } else
              try {
                this._schedule(function() {
                  fn(arg);
                });
              } catch (e) {
                throw new Error("No async scheduler available\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
          };
          function AsyncInvokeLater(fn, receiver, arg) {
            this._lateQueue.push(fn, receiver, arg);
            this._queueTick();
          }
          function AsyncInvoke(fn, receiver, arg) {
            this._normalQueue.push(fn, receiver, arg);
            this._queueTick();
          }
          function AsyncSettlePromises(promise) {
            this._normalQueue._pushOne(promise);
            this._queueTick();
          }
          if (!util.hasDevTools) {
            Async.prototype.invokeLater = AsyncInvokeLater;
            Async.prototype.invoke = AsyncInvoke;
            Async.prototype.settlePromises = AsyncSettlePromises;
          } else {
            Async.prototype.invokeLater = function(fn, receiver, arg) {
              if (this._trampolineEnabled) {
                AsyncInvokeLater.call(this, fn, receiver, arg);
              } else {
                this._schedule(function() {
                  setTimeout(function() {
                    fn.call(receiver, arg);
                  }, 100);
                });
              }
            };
            Async.prototype.invoke = function(fn, receiver, arg) {
              if (this._trampolineEnabled) {
                AsyncInvoke.call(this, fn, receiver, arg);
              } else {
                this._schedule(function() {
                  fn.call(receiver, arg);
                });
              }
            };
            Async.prototype.settlePromises = function(promise) {
              if (this._trampolineEnabled) {
                AsyncSettlePromises.call(this, promise);
              } else {
                this._schedule(function() {
                  promise._settlePromises();
                });
              }
            };
          }
          Async.prototype.invokeFirst = function(fn, receiver, arg) {
            this._normalQueue.unshift(fn, receiver, arg);
            this._queueTick();
          };
          Async.prototype._drainQueue = function(queue) {
            while (queue.length() > 0) {
              var fn = queue.shift();
              if (typeof fn !== "function") {
                fn._settlePromises();
                continue;
              }
              var receiver = queue.shift();
              var arg = queue.shift();
              fn.call(receiver, arg);
            }
          };
          Async.prototype._drainQueues = function() {
            this._drainQueue(this._normalQueue);
            this._reset();
            this._haveDrainedQueues = true;
            this._drainQueue(this._lateQueue);
          };
          Async.prototype._queueTick = function() {
            if (!this._isTickUsed) {
              this._isTickUsed = true;
              this._schedule(this.drainQueues);
            }
          };
          Async.prototype._reset = function() {
            this._isTickUsed = false;
          };
          module.exports = Async;
          module.exports.firstLineError = firstLineError;
        }, {
          "./queue": 26,
          "./schedule": 29,
          "./util": 36
        }],
        3: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL, tryConvertToPromise, debug) {
            var calledBind = false;
            var rejectThis = function(_, e) {
              this._reject(e);
            };
            var targetRejected = function(e, context) {
              context.promiseRejectionQueued = true;
              context.bindingPromise._then(rejectThis, rejectThis, null, this, e);
            };
            var bindingResolved = function(thisArg, context) {
              if (((this._bitField & 50397184) === 0)) {
                this._resolveCallback(context.target);
              }
            };
            var bindingRejected = function(e, context) {
              if (!context.promiseRejectionQueued)
                this._reject(e);
            };
            Promise.prototype.bind = function(thisArg) {
              if (!calledBind) {
                calledBind = true;
                Promise.prototype._propagateFrom = debug.propagateFromFunction();
                Promise.prototype._boundValue = debug.boundValueFunction();
              }
              var maybePromise = tryConvertToPromise(thisArg);
              var ret = new Promise(INTERNAL);
              ret._propagateFrom(this, 1);
              var target = this._target();
              ret._setBoundTo(maybePromise);
              if (maybePromise instanceof Promise) {
                var context = {
                  promiseRejectionQueued: false,
                  promise: ret,
                  target: target,
                  bindingPromise: maybePromise
                };
                target._then(INTERNAL, targetRejected, undefined, ret, context);
                maybePromise._then(bindingResolved, bindingRejected, undefined, ret, context);
                ret._setOnCancel(maybePromise);
              } else {
                ret._resolveCallback(target);
              }
              return ret;
            };
            Promise.prototype._setBoundTo = function(obj) {
              if (obj !== undefined) {
                this._bitField = this._bitField | 2097152;
                this._boundTo = obj;
              } else {
                this._bitField = this._bitField & (~2097152);
              }
            };
            Promise.prototype._isBound = function() {
              return (this._bitField & 2097152) === 2097152;
            };
            Promise.bind = function(thisArg, value) {
              return Promise.resolve(value).bind(thisArg);
            };
          };
        }, {}],
        4: [function(_dereq_, module, exports) {
          "use strict";
          var old;
          if (typeof Promise !== "undefined")
            old = Promise;
          function noConflict() {
            try {
              if (Promise === bluebird)
                Promise = old;
            } catch (e) {}
            return bluebird;
          }
          var bluebird = _dereq_("./promise")();
          bluebird.noConflict = noConflict;
          module.exports = bluebird;
        }, {"./promise": 22}],
        5: [function(_dereq_, module, exports) {
          "use strict";
          var cr = Object.create;
          if (cr) {
            var callerCache = cr(null);
            var getterCache = cr(null);
            callerCache[" size"] = getterCache[" size"] = 0;
          }
          module.exports = function(Promise) {
            var util = _dereq_("./util");
            var canEvaluate = util.canEvaluate;
            var isIdentifier = util.isIdentifier;
            var getMethodCaller;
            var getGetter;
            if (!true) {
              var makeMethodCaller = function(methodName) {
                return new Function("ensureMethod", "                                    \n\
        return function(obj) {                                               \n\
            'use strict'                                                     \n\
            var len = this.length;                                           \n\
            ensureMethod(obj, 'methodName');                                 \n\
            switch(len) {                                                    \n\
                case 1: return obj.methodName(this[0]);                      \n\
                case 2: return obj.methodName(this[0], this[1]);             \n\
                case 3: return obj.methodName(this[0], this[1], this[2]);    \n\
                case 0: return obj.methodName();                             \n\
                default:                                                     \n\
                    return obj.methodName.apply(obj, this);                  \n\
            }                                                                \n\
        };                                                                   \n\
        ".replace(/methodName/g, methodName))(ensureMethod);
              };
              var makeGetter = function(propertyName) {
                return new Function("obj", "                                             \n\
        'use strict';                                                        \n\
        return obj.propertyName;                                             \n\
        ".replace("propertyName", propertyName));
              };
              var getCompiled = function(name, compiler, cache) {
                var ret = cache[name];
                if (typeof ret !== "function") {
                  if (!isIdentifier(name)) {
                    return null;
                  }
                  ret = compiler(name);
                  cache[name] = ret;
                  cache[" size"]++;
                  if (cache[" size"] > 512) {
                    var keys = Object.keys(cache);
                    for (var i = 0; i < 256; ++i)
                      delete cache[keys[i]];
                    cache[" size"] = keys.length - 256;
                  }
                }
                return ret;
              };
              getMethodCaller = function(name) {
                return getCompiled(name, makeMethodCaller, callerCache);
              };
              getGetter = function(name) {
                return getCompiled(name, makeGetter, getterCache);
              };
            }
            function ensureMethod(obj, methodName) {
              var fn;
              if (obj != null)
                fn = obj[methodName];
              if (typeof fn !== "function") {
                var message = "Object " + util.classString(obj) + " has no method '" + util.toString(methodName) + "'";
                throw new Promise.TypeError(message);
              }
              return fn;
            }
            function caller(obj) {
              var methodName = this.pop();
              var fn = ensureMethod(obj, methodName);
              return fn.apply(obj, this);
            }
            Promise.prototype.call = function(methodName) {
              var args = [].slice.call(arguments, 1);
              ;
              if (!true) {
                if (canEvaluate) {
                  var maybeCaller = getMethodCaller(methodName);
                  if (maybeCaller !== null) {
                    return this._then(maybeCaller, undefined, undefined, args, undefined);
                  }
                }
              }
              args.push(methodName);
              return this._then(caller, undefined, undefined, args, undefined);
            };
            function namedGetter(obj) {
              return obj[this];
            }
            function indexedGetter(obj) {
              var index = +this;
              if (index < 0)
                index = Math.max(0, index + obj.length);
              return obj[index];
            }
            Promise.prototype.get = function(propertyName) {
              var isIndex = (typeof propertyName === "number");
              var getter;
              if (!isIndex) {
                if (canEvaluate) {
                  var maybeGetter = getGetter(propertyName);
                  getter = maybeGetter !== null ? maybeGetter : namedGetter;
                } else {
                  getter = namedGetter;
                }
              } else {
                getter = indexedGetter;
              }
              return this._then(getter, undefined, undefined, propertyName, undefined);
            };
          };
        }, {"./util": 36}],
        6: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, apiRejection, debug) {
            var util = _dereq_("./util");
            var tryCatch = util.tryCatch;
            var errorObj = util.errorObj;
            var async = Promise._async;
            Promise.prototype["break"] = Promise.prototype.cancel = function() {
              if (!debug.cancellation())
                return this._warn("cancellation is disabled");
              var promise = this;
              var child = promise;
              while (promise.isCancellable()) {
                if (!promise._cancelBy(child)) {
                  if (child._isFollowing()) {
                    child._followee().cancel();
                  } else {
                    child._cancelBranched();
                  }
                  break;
                }
                var parent = promise._cancellationParent;
                if (parent == null || !parent.isCancellable()) {
                  if (promise._isFollowing()) {
                    promise._followee().cancel();
                  } else {
                    promise._cancelBranched();
                  }
                  break;
                } else {
                  if (promise._isFollowing())
                    promise._followee().cancel();
                  child = promise;
                  promise = parent;
                }
              }
            };
            Promise.prototype._branchHasCancelled = function() {
              this._branchesRemainingToCancel--;
            };
            Promise.prototype._enoughBranchesHaveCancelled = function() {
              return this._branchesRemainingToCancel === undefined || this._branchesRemainingToCancel <= 0;
            };
            Promise.prototype._cancelBy = function(canceller) {
              if (canceller === this) {
                this._branchesRemainingToCancel = 0;
                this._invokeOnCancel();
                return true;
              } else {
                this._branchHasCancelled();
                if (this._enoughBranchesHaveCancelled()) {
                  this._invokeOnCancel();
                  return true;
                }
              }
              return false;
            };
            Promise.prototype._cancelBranched = function() {
              if (this._enoughBranchesHaveCancelled()) {
                this._cancel();
              }
            };
            Promise.prototype._cancel = function() {
              if (!this.isCancellable())
                return;
              this._setCancelled();
              async.invoke(this._cancelPromises, this, undefined);
            };
            Promise.prototype._cancelPromises = function() {
              if (this._length() > 0)
                this._settlePromises();
            };
            Promise.prototype._unsetOnCancel = function() {
              this._onCancelField = undefined;
            };
            Promise.prototype.isCancellable = function() {
              return this.isPending() && !this.isCancelled();
            };
            Promise.prototype._doInvokeOnCancel = function(onCancelCallback, internalOnly) {
              if (util.isArray(onCancelCallback)) {
                for (var i = 0; i < onCancelCallback.length; ++i) {
                  this._doInvokeOnCancel(onCancelCallback[i], internalOnly);
                }
              } else if (onCancelCallback !== undefined) {
                if (typeof onCancelCallback === "function") {
                  if (!internalOnly) {
                    var e = tryCatch(onCancelCallback).call(this._boundValue());
                    if (e === errorObj) {
                      this._attachExtraTrace(e.e);
                      async.throwLater(e.e);
                    }
                  }
                } else {
                  onCancelCallback._resultCancelled(this);
                }
              }
            };
            Promise.prototype._invokeOnCancel = function() {
              var onCancelCallback = this._onCancel();
              this._unsetOnCancel();
              async.invoke(this._doInvokeOnCancel, this, onCancelCallback);
            };
            Promise.prototype._invokeInternalOnCancel = function() {
              if (this.isCancellable()) {
                this._doInvokeOnCancel(this._onCancel(), true);
                this._unsetOnCancel();
              }
            };
            Promise.prototype._resultCancelled = function() {
              this.cancel();
            };
          };
        }, {"./util": 36}],
        7: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(NEXT_FILTER) {
            var util = _dereq_("./util");
            var getKeys = _dereq_("./es5").keys;
            var tryCatch = util.tryCatch;
            var errorObj = util.errorObj;
            function catchFilter(instances, cb, promise) {
              return function(e) {
                var boundTo = promise._boundValue();
                predicateLoop: for (var i = 0; i < instances.length; ++i) {
                  var item = instances[i];
                  if (item === Error || (item != null && item.prototype instanceof Error)) {
                    if (e instanceof item) {
                      return tryCatch(cb).call(boundTo, e);
                    }
                  } else if (typeof item === "function") {
                    var matchesPredicate = tryCatch(item).call(boundTo, e);
                    if (matchesPredicate === errorObj) {
                      return matchesPredicate;
                    } else if (matchesPredicate) {
                      return tryCatch(cb).call(boundTo, e);
                    }
                  } else if (util.isObject(e)) {
                    var keys = getKeys(item);
                    for (var j = 0; j < keys.length; ++j) {
                      var key = keys[j];
                      if (item[key] != e[key]) {
                        continue predicateLoop;
                      }
                    }
                    return tryCatch(cb).call(boundTo, e);
                  }
                }
                return NEXT_FILTER;
              };
            }
            return catchFilter;
          };
        }, {
          "./es5": 13,
          "./util": 36
        }],
        8: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise) {
            var longStackTraces = false;
            var contextStack = [];
            Promise.prototype._promiseCreated = function() {};
            Promise.prototype._pushContext = function() {};
            Promise.prototype._popContext = function() {
              return null;
            };
            Promise._peekContext = Promise.prototype._peekContext = function() {};
            function Context() {
              this._trace = new Context.CapturedTrace(peekContext());
            }
            Context.prototype._pushContext = function() {
              if (this._trace !== undefined) {
                this._trace._promiseCreated = null;
                contextStack.push(this._trace);
              }
            };
            Context.prototype._popContext = function() {
              if (this._trace !== undefined) {
                var trace = contextStack.pop();
                var ret = trace._promiseCreated;
                trace._promiseCreated = null;
                return ret;
              }
              return null;
            };
            function createContext() {
              if (longStackTraces)
                return new Context();
            }
            function peekContext() {
              var lastIndex = contextStack.length - 1;
              if (lastIndex >= 0) {
                return contextStack[lastIndex];
              }
              return undefined;
            }
            Context.CapturedTrace = null;
            Context.create = createContext;
            Context.deactivateLongStackTraces = function() {};
            Context.activateLongStackTraces = function() {
              var Promise_pushContext = Promise.prototype._pushContext;
              var Promise_popContext = Promise.prototype._popContext;
              var Promise_PeekContext = Promise._peekContext;
              var Promise_peekContext = Promise.prototype._peekContext;
              var Promise_promiseCreated = Promise.prototype._promiseCreated;
              Context.deactivateLongStackTraces = function() {
                Promise.prototype._pushContext = Promise_pushContext;
                Promise.prototype._popContext = Promise_popContext;
                Promise._peekContext = Promise_PeekContext;
                Promise.prototype._peekContext = Promise_peekContext;
                Promise.prototype._promiseCreated = Promise_promiseCreated;
                longStackTraces = false;
              };
              longStackTraces = true;
              Promise.prototype._pushContext = Context.prototype._pushContext;
              Promise.prototype._popContext = Context.prototype._popContext;
              Promise._peekContext = Promise.prototype._peekContext = peekContext;
              Promise.prototype._promiseCreated = function() {
                var ctx = this._peekContext();
                if (ctx && ctx._promiseCreated == null)
                  ctx._promiseCreated = this;
              };
            };
            return Context;
          };
        }, {}],
        9: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, Context) {
            var getDomain = Promise._getDomain;
            var async = Promise._async;
            var Warning = _dereq_("./errors").Warning;
            var util = _dereq_("./util");
            var canAttachTrace = util.canAttachTrace;
            var unhandledRejectionHandled;
            var possiblyUnhandledRejection;
            var bluebirdFramePattern = /[\\\/]bluebird[\\\/]js[\\\/](release|debug|instrumented)/;
            var stackFramePattern = null;
            var formatStack = null;
            var indentStackFrames = false;
            var printWarning;
            var debugging = !!(util.env("BLUEBIRD_DEBUG") != 0 && (true || util.env("BLUEBIRD_DEBUG") || util.env("NODE_ENV") === "development"));
            var warnings = !!(util.env("BLUEBIRD_WARNINGS") != 0 && (debugging || util.env("BLUEBIRD_WARNINGS")));
            var longStackTraces = !!(util.env("BLUEBIRD_LONG_STACK_TRACES") != 0 && (debugging || util.env("BLUEBIRD_LONG_STACK_TRACES")));
            var wForgottenReturn = util.env("BLUEBIRD_W_FORGOTTEN_RETURN") != 0 && (warnings || !!util.env("BLUEBIRD_W_FORGOTTEN_RETURN"));
            Promise.prototype.suppressUnhandledRejections = function() {
              var target = this._target();
              target._bitField = ((target._bitField & (~1048576)) | 524288);
            };
            Promise.prototype._ensurePossibleRejectionHandled = function() {
              if ((this._bitField & 524288) !== 0)
                return;
              this._setRejectionIsUnhandled();
              async.invokeLater(this._notifyUnhandledRejection, this, undefined);
            };
            Promise.prototype._notifyUnhandledRejectionIsHandled = function() {
              fireRejectionEvent("rejectionHandled", unhandledRejectionHandled, undefined, this);
            };
            Promise.prototype._setReturnedNonUndefined = function() {
              this._bitField = this._bitField | 268435456;
            };
            Promise.prototype._returnedNonUndefined = function() {
              return (this._bitField & 268435456) !== 0;
            };
            Promise.prototype._notifyUnhandledRejection = function() {
              if (this._isRejectionUnhandled()) {
                var reason = this._settledValue();
                this._setUnhandledRejectionIsNotified();
                fireRejectionEvent("unhandledRejection", possiblyUnhandledRejection, reason, this);
              }
            };
            Promise.prototype._setUnhandledRejectionIsNotified = function() {
              this._bitField = this._bitField | 262144;
            };
            Promise.prototype._unsetUnhandledRejectionIsNotified = function() {
              this._bitField = this._bitField & (~262144);
            };
            Promise.prototype._isUnhandledRejectionNotified = function() {
              return (this._bitField & 262144) > 0;
            };
            Promise.prototype._setRejectionIsUnhandled = function() {
              this._bitField = this._bitField | 1048576;
            };
            Promise.prototype._unsetRejectionIsUnhandled = function() {
              this._bitField = this._bitField & (~1048576);
              if (this._isUnhandledRejectionNotified()) {
                this._unsetUnhandledRejectionIsNotified();
                this._notifyUnhandledRejectionIsHandled();
              }
            };
            Promise.prototype._isRejectionUnhandled = function() {
              return (this._bitField & 1048576) > 0;
            };
            Promise.prototype._warn = function(message, shouldUseOwnTrace, promise) {
              return warn(message, shouldUseOwnTrace, promise || this);
            };
            Promise.onPossiblyUnhandledRejection = function(fn) {
              var domain = getDomain();
              possiblyUnhandledRejection = typeof fn === "function" ? (domain === null ? fn : domain.bind(fn)) : undefined;
            };
            Promise.onUnhandledRejectionHandled = function(fn) {
              var domain = getDomain();
              unhandledRejectionHandled = typeof fn === "function" ? (domain === null ? fn : domain.bind(fn)) : undefined;
            };
            var disableLongStackTraces = function() {};
            Promise.longStackTraces = function() {
              if (async.haveItemsQueued() && !config.longStackTraces) {
                throw new Error("cannot enable long stack traces after promises have been created\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              if (!config.longStackTraces && longStackTracesIsSupported()) {
                var Promise_captureStackTrace = Promise.prototype._captureStackTrace;
                var Promise_attachExtraTrace = Promise.prototype._attachExtraTrace;
                config.longStackTraces = true;
                disableLongStackTraces = function() {
                  if (async.haveItemsQueued() && !config.longStackTraces) {
                    throw new Error("cannot enable long stack traces after promises have been created\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
                  }
                  Promise.prototype._captureStackTrace = Promise_captureStackTrace;
                  Promise.prototype._attachExtraTrace = Promise_attachExtraTrace;
                  Context.deactivateLongStackTraces();
                  async.enableTrampoline();
                  config.longStackTraces = false;
                };
                Promise.prototype._captureStackTrace = longStackTracesCaptureStackTrace;
                Promise.prototype._attachExtraTrace = longStackTracesAttachExtraTrace;
                Context.activateLongStackTraces();
                async.disableTrampolineIfNecessary();
              }
            };
            Promise.hasLongStackTraces = function() {
              return config.longStackTraces && longStackTracesIsSupported();
            };
            var fireDomEvent = (function() {
              try {
                var event = document.createEvent("CustomEvent");
                event.initCustomEvent("testingtheevent", false, true, {});
                util.global.dispatchEvent(event);
                return function(name, event) {
                  var domEvent = document.createEvent("CustomEvent");
                  domEvent.initCustomEvent(name.toLowerCase(), false, true, event);
                  return !util.global.dispatchEvent(domEvent);
                };
              } catch (e) {}
              return function() {
                return false;
              };
            })();
            var fireGlobalEvent = (function() {
              if (util.isNode) {
                return function() {
                  return process.emit.apply(process, arguments);
                };
              } else {
                if (!util.global) {
                  return function() {
                    return false;
                  };
                }
                return function(name) {
                  var methodName = "on" + name.toLowerCase();
                  var method = util.global[methodName];
                  if (!method)
                    return false;
                  method.apply(util.global, [].slice.call(arguments, 1));
                  return true;
                };
              }
            })();
            function generatePromiseLifecycleEventObject(name, promise) {
              return {promise: promise};
            }
            var eventToObjectGenerator = {
              promiseCreated: generatePromiseLifecycleEventObject,
              promiseFulfilled: generatePromiseLifecycleEventObject,
              promiseRejected: generatePromiseLifecycleEventObject,
              promiseResolved: generatePromiseLifecycleEventObject,
              promiseCancelled: generatePromiseLifecycleEventObject,
              promiseChained: function(name, promise, child) {
                return {
                  promise: promise,
                  child: child
                };
              },
              warning: function(name, warning) {
                return {warning: warning};
              },
              unhandledRejection: function(name, reason, promise) {
                return {
                  reason: reason,
                  promise: promise
                };
              },
              rejectionHandled: generatePromiseLifecycleEventObject
            };
            var activeFireEvent = function(name) {
              var globalEventFired = false;
              try {
                globalEventFired = fireGlobalEvent.apply(null, arguments);
              } catch (e) {
                async.throwLater(e);
                globalEventFired = true;
              }
              var domEventFired = false;
              try {
                domEventFired = fireDomEvent(name, eventToObjectGenerator[name].apply(null, arguments));
              } catch (e) {
                async.throwLater(e);
                domEventFired = true;
              }
              return domEventFired || globalEventFired;
            };
            Promise.config = function(opts) {
              opts = Object(opts);
              if ("longStackTraces" in opts) {
                if (opts.longStackTraces) {
                  Promise.longStackTraces();
                } else if (!opts.longStackTraces && Promise.hasLongStackTraces()) {
                  disableLongStackTraces();
                }
              }
              if ("warnings" in opts) {
                var warningsOption = opts.warnings;
                config.warnings = !!warningsOption;
                wForgottenReturn = config.warnings;
                if (util.isObject(warningsOption)) {
                  if ("wForgottenReturn" in warningsOption) {
                    wForgottenReturn = !!warningsOption.wForgottenReturn;
                  }
                }
              }
              if ("cancellation" in opts && opts.cancellation && !config.cancellation) {
                if (async.haveItemsQueued()) {
                  throw new Error("cannot enable cancellation after promises are in use");
                }
                Promise.prototype._clearCancellationData = cancellationClearCancellationData;
                Promise.prototype._propagateFrom = cancellationPropagateFrom;
                Promise.prototype._onCancel = cancellationOnCancel;
                Promise.prototype._setOnCancel = cancellationSetOnCancel;
                Promise.prototype._attachCancellationCallback = cancellationAttachCancellationCallback;
                Promise.prototype._execute = cancellationExecute;
                propagateFromFunction = cancellationPropagateFrom;
                config.cancellation = true;
              }
              if ("monitoring" in opts) {
                if (opts.monitoring && !config.monitoring) {
                  config.monitoring = true;
                  Promise.prototype._fireEvent = activeFireEvent;
                } else if (!opts.monitoring && config.monitoring) {
                  config.monitoring = false;
                  Promise.prototype._fireEvent = defaultFireEvent;
                }
              }
            };
            function defaultFireEvent() {
              return false;
            }
            Promise.prototype._fireEvent = defaultFireEvent;
            Promise.prototype._execute = function(executor, resolve, reject) {
              try {
                executor(resolve, reject);
              } catch (e) {
                return e;
              }
            };
            Promise.prototype._onCancel = function() {};
            Promise.prototype._setOnCancel = function(handler) {
              ;
            };
            Promise.prototype._attachCancellationCallback = function(onCancel) {
              ;
            };
            Promise.prototype._captureStackTrace = function() {};
            Promise.prototype._attachExtraTrace = function() {};
            Promise.prototype._clearCancellationData = function() {};
            Promise.prototype._propagateFrom = function(parent, flags) {
              ;
              ;
            };
            function cancellationExecute(executor, resolve, reject) {
              var promise = this;
              try {
                executor(resolve, reject, function(onCancel) {
                  if (typeof onCancel !== "function") {
                    throw new TypeError("onCancel must be a function, got: " + util.toString(onCancel));
                  }
                  promise._attachCancellationCallback(onCancel);
                });
              } catch (e) {
                return e;
              }
            }
            function cancellationAttachCancellationCallback(onCancel) {
              if (!this.isCancellable())
                return this;
              var previousOnCancel = this._onCancel();
              if (previousOnCancel !== undefined) {
                if (util.isArray(previousOnCancel)) {
                  previousOnCancel.push(onCancel);
                } else {
                  this._setOnCancel([previousOnCancel, onCancel]);
                }
              } else {
                this._setOnCancel(onCancel);
              }
            }
            function cancellationOnCancel() {
              return this._onCancelField;
            }
            function cancellationSetOnCancel(onCancel) {
              this._onCancelField = onCancel;
            }
            function cancellationClearCancellationData() {
              this._cancellationParent = undefined;
              this._onCancelField = undefined;
            }
            function cancellationPropagateFrom(parent, flags) {
              if ((flags & 1) !== 0) {
                this._cancellationParent = parent;
                var branchesRemainingToCancel = parent._branchesRemainingToCancel;
                if (branchesRemainingToCancel === undefined) {
                  branchesRemainingToCancel = 0;
                }
                parent._branchesRemainingToCancel = branchesRemainingToCancel + 1;
              }
              if ((flags & 2) !== 0 && parent._isBound()) {
                this._setBoundTo(parent._boundTo);
              }
            }
            function bindingPropagateFrom(parent, flags) {
              if ((flags & 2) !== 0 && parent._isBound()) {
                this._setBoundTo(parent._boundTo);
              }
            }
            var propagateFromFunction = bindingPropagateFrom;
            function boundValueFunction() {
              var ret = this._boundTo;
              if (ret !== undefined) {
                if (ret instanceof Promise) {
                  if (ret.isFulfilled()) {
                    return ret.value();
                  } else {
                    return undefined;
                  }
                }
              }
              return ret;
            }
            function longStackTracesCaptureStackTrace() {
              this._trace = new CapturedTrace(this._peekContext());
            }
            function longStackTracesAttachExtraTrace(error, ignoreSelf) {
              if (canAttachTrace(error)) {
                var trace = this._trace;
                if (trace !== undefined) {
                  if (ignoreSelf)
                    trace = trace._parent;
                }
                if (trace !== undefined) {
                  trace.attachExtraTrace(error);
                } else if (!error.__stackCleaned__) {
                  var parsed = parseStackAndMessage(error);
                  util.notEnumerableProp(error, "stack", parsed.message + "\n" + parsed.stack.join("\n"));
                  util.notEnumerableProp(error, "__stackCleaned__", true);
                }
              }
            }
            function checkForgottenReturns(returnValue, promiseCreated, name, promise, parent) {
              if (returnValue === undefined && promiseCreated !== null && wForgottenReturn) {
                if (parent !== undefined && parent._returnedNonUndefined())
                  return;
                if ((promise._bitField & 65535) === 0)
                  return;
                if (name)
                  name = name + " ";
                var msg = "a promise was created in a " + name + "handler but was not returned from it";
                promise._warn(msg, true, promiseCreated);
              }
            }
            function deprecated(name, replacement) {
              var message = name + " is deprecated and will be removed in a future version.";
              if (replacement)
                message += " Use " + replacement + " instead.";
              return warn(message);
            }
            function warn(message, shouldUseOwnTrace, promise) {
              if (!config.warnings)
                return;
              var warning = new Warning(message);
              var ctx;
              if (shouldUseOwnTrace) {
                promise._attachExtraTrace(warning);
              } else if (config.longStackTraces && (ctx = Promise._peekContext())) {
                ctx.attachExtraTrace(warning);
              } else {
                var parsed = parseStackAndMessage(warning);
                warning.stack = parsed.message + "\n" + parsed.stack.join("\n");
              }
              if (!activeFireEvent("warning", warning)) {
                formatAndLogError(warning, "", true);
              }
            }
            function reconstructStack(message, stacks) {
              for (var i = 0; i < stacks.length - 1; ++i) {
                stacks[i].push("From previous event:");
                stacks[i] = stacks[i].join("\n");
              }
              if (i < stacks.length) {
                stacks[i] = stacks[i].join("\n");
              }
              return message + "\n" + stacks.join("\n");
            }
            function removeDuplicateOrEmptyJumps(stacks) {
              for (var i = 0; i < stacks.length; ++i) {
                if (stacks[i].length === 0 || ((i + 1 < stacks.length) && stacks[i][0] === stacks[i + 1][0])) {
                  stacks.splice(i, 1);
                  i--;
                }
              }
            }
            function removeCommonRoots(stacks) {
              var current = stacks[0];
              for (var i = 1; i < stacks.length; ++i) {
                var prev = stacks[i];
                var currentLastIndex = current.length - 1;
                var currentLastLine = current[currentLastIndex];
                var commonRootMeetPoint = -1;
                for (var j = prev.length - 1; j >= 0; --j) {
                  if (prev[j] === currentLastLine) {
                    commonRootMeetPoint = j;
                    break;
                  }
                }
                for (var j = commonRootMeetPoint; j >= 0; --j) {
                  var line = prev[j];
                  if (current[currentLastIndex] === line) {
                    current.pop();
                    currentLastIndex--;
                  } else {
                    break;
                  }
                }
                current = prev;
              }
            }
            function cleanStack(stack) {
              var ret = [];
              for (var i = 0; i < stack.length; ++i) {
                var line = stack[i];
                var isTraceLine = "    (No stack trace)" === line || stackFramePattern.test(line);
                var isInternalFrame = isTraceLine && shouldIgnore(line);
                if (isTraceLine && !isInternalFrame) {
                  if (indentStackFrames && line.charAt(0) !== " ") {
                    line = "    " + line;
                  }
                  ret.push(line);
                }
              }
              return ret;
            }
            function stackFramesAsArray(error) {
              var stack = error.stack.replace(/\s+$/g, "").split("\n");
              for (var i = 0; i < stack.length; ++i) {
                var line = stack[i];
                if ("    (No stack trace)" === line || stackFramePattern.test(line)) {
                  break;
                }
              }
              if (i > 0) {
                stack = stack.slice(i);
              }
              return stack;
            }
            function parseStackAndMessage(error) {
              var stack = error.stack;
              var message = error.toString();
              stack = typeof stack === "string" && stack.length > 0 ? stackFramesAsArray(error) : ["    (No stack trace)"];
              return {
                message: message,
                stack: cleanStack(stack)
              };
            }
            function formatAndLogError(error, title, isSoft) {
              if (typeof console !== "undefined") {
                var message;
                if (util.isObject(error)) {
                  var stack = error.stack;
                  message = title + formatStack(stack, error);
                } else {
                  message = title + String(error);
                }
                if (typeof printWarning === "function") {
                  printWarning(message, isSoft);
                } else if (typeof console.log === "function" || typeof console.log === "object") {
                  console.log(message);
                }
              }
            }
            function fireRejectionEvent(name, localHandler, reason, promise) {
              var localEventFired = false;
              try {
                if (typeof localHandler === "function") {
                  localEventFired = true;
                  if (name === "rejectionHandled") {
                    localHandler(promise);
                  } else {
                    localHandler(reason, promise);
                  }
                }
              } catch (e) {
                async.throwLater(e);
              }
              if (name === "unhandledRejection") {
                if (!activeFireEvent(name, reason, promise) && !localEventFired) {
                  formatAndLogError(reason, "Unhandled rejection ");
                }
              } else {
                activeFireEvent(name, promise);
              }
            }
            function formatNonError(obj) {
              var str;
              if (typeof obj === "function") {
                str = "[function " + (obj.name || "anonymous") + "]";
              } else {
                str = obj && typeof obj.toString === "function" ? obj.toString() : util.toString(obj);
                var ruselessToString = /\[object [a-zA-Z0-9$_]+\]/;
                if (ruselessToString.test(str)) {
                  try {
                    var newStr = JSON.stringify(obj);
                    str = newStr;
                  } catch (e) {}
                }
                if (str.length === 0) {
                  str = "(empty array)";
                }
              }
              return ("(<" + snip(str) + ">, no stack trace)");
            }
            function snip(str) {
              var maxChars = 41;
              if (str.length < maxChars) {
                return str;
              }
              return str.substr(0, maxChars - 3) + "...";
            }
            function longStackTracesIsSupported() {
              return typeof captureStackTrace === "function";
            }
            var shouldIgnore = function() {
              return false;
            };
            var parseLineInfoRegex = /[\/<\(]([^:\/]+):(\d+):(?:\d+)\)?\s*$/;
            function parseLineInfo(line) {
              var matches = line.match(parseLineInfoRegex);
              if (matches) {
                return {
                  fileName: matches[1],
                  line: parseInt(matches[2], 10)
                };
              }
            }
            function setBounds(firstLineError, lastLineError) {
              if (!longStackTracesIsSupported())
                return;
              var firstStackLines = firstLineError.stack.split("\n");
              var lastStackLines = lastLineError.stack.split("\n");
              var firstIndex = -1;
              var lastIndex = -1;
              var firstFileName;
              var lastFileName;
              for (var i = 0; i < firstStackLines.length; ++i) {
                var result = parseLineInfo(firstStackLines[i]);
                if (result) {
                  firstFileName = result.fileName;
                  firstIndex = result.line;
                  break;
                }
              }
              for (var i = 0; i < lastStackLines.length; ++i) {
                var result = parseLineInfo(lastStackLines[i]);
                if (result) {
                  lastFileName = result.fileName;
                  lastIndex = result.line;
                  break;
                }
              }
              if (firstIndex < 0 || lastIndex < 0 || !firstFileName || !lastFileName || firstFileName !== lastFileName || firstIndex >= lastIndex) {
                return;
              }
              shouldIgnore = function(line) {
                if (bluebirdFramePattern.test(line))
                  return true;
                var info = parseLineInfo(line);
                if (info) {
                  if (info.fileName === firstFileName && (firstIndex <= info.line && info.line <= lastIndex)) {
                    return true;
                  }
                }
                return false;
              };
            }
            function CapturedTrace(parent) {
              this._parent = parent;
              this._promisesCreated = 0;
              var length = this._length = 1 + (parent === undefined ? 0 : parent._length);
              captureStackTrace(this, CapturedTrace);
              if (length > 32)
                this.uncycle();
            }
            util.inherits(CapturedTrace, Error);
            Context.CapturedTrace = CapturedTrace;
            CapturedTrace.prototype.uncycle = function() {
              var length = this._length;
              if (length < 2)
                return;
              var nodes = [];
              var stackToIndex = {};
              for (var i = 0,
                  node = this; node !== undefined; ++i) {
                nodes.push(node);
                node = node._parent;
              }
              length = this._length = i;
              for (var i = length - 1; i >= 0; --i) {
                var stack = nodes[i].stack;
                if (stackToIndex[stack] === undefined) {
                  stackToIndex[stack] = i;
                }
              }
              for (var i = 0; i < length; ++i) {
                var currentStack = nodes[i].stack;
                var index = stackToIndex[currentStack];
                if (index !== undefined && index !== i) {
                  if (index > 0) {
                    nodes[index - 1]._parent = undefined;
                    nodes[index - 1]._length = 1;
                  }
                  nodes[i]._parent = undefined;
                  nodes[i]._length = 1;
                  var cycleEdgeNode = i > 0 ? nodes[i - 1] : this;
                  if (index < length - 1) {
                    cycleEdgeNode._parent = nodes[index + 1];
                    cycleEdgeNode._parent.uncycle();
                    cycleEdgeNode._length = cycleEdgeNode._parent._length + 1;
                  } else {
                    cycleEdgeNode._parent = undefined;
                    cycleEdgeNode._length = 1;
                  }
                  var currentChildLength = cycleEdgeNode._length + 1;
                  for (var j = i - 2; j >= 0; --j) {
                    nodes[j]._length = currentChildLength;
                    currentChildLength++;
                  }
                  return;
                }
              }
            };
            CapturedTrace.prototype.attachExtraTrace = function(error) {
              if (error.__stackCleaned__)
                return;
              this.uncycle();
              var parsed = parseStackAndMessage(error);
              var message = parsed.message;
              var stacks = [parsed.stack];
              var trace = this;
              while (trace !== undefined) {
                stacks.push(cleanStack(trace.stack.split("\n")));
                trace = trace._parent;
              }
              removeCommonRoots(stacks);
              removeDuplicateOrEmptyJumps(stacks);
              util.notEnumerableProp(error, "stack", reconstructStack(message, stacks));
              util.notEnumerableProp(error, "__stackCleaned__", true);
            };
            var captureStackTrace = (function stackDetection() {
              var v8stackFramePattern = /^\s*at\s*/;
              var v8stackFormatter = function(stack, error) {
                if (typeof stack === "string")
                  return stack;
                if (error.name !== undefined && error.message !== undefined) {
                  return error.toString();
                }
                return formatNonError(error);
              };
              if (typeof Error.stackTraceLimit === "number" && typeof Error.captureStackTrace === "function") {
                Error.stackTraceLimit += 6;
                stackFramePattern = v8stackFramePattern;
                formatStack = v8stackFormatter;
                var captureStackTrace = Error.captureStackTrace;
                shouldIgnore = function(line) {
                  return bluebirdFramePattern.test(line);
                };
                return function(receiver, ignoreUntil) {
                  Error.stackTraceLimit += 6;
                  captureStackTrace(receiver, ignoreUntil);
                  Error.stackTraceLimit -= 6;
                };
              }
              var err = new Error();
              if (typeof err.stack === "string" && err.stack.split("\n")[0].indexOf("stackDetection@") >= 0) {
                stackFramePattern = /@/;
                formatStack = v8stackFormatter;
                indentStackFrames = true;
                return function captureStackTrace(o) {
                  o.stack = new Error().stack;
                };
              }
              var hasStackAfterThrow;
              try {
                throw new Error();
              } catch (e) {
                hasStackAfterThrow = ("stack" in e);
              }
              if (!("stack" in err) && hasStackAfterThrow && typeof Error.stackTraceLimit === "number") {
                stackFramePattern = v8stackFramePattern;
                formatStack = v8stackFormatter;
                return function captureStackTrace(o) {
                  Error.stackTraceLimit += 6;
                  try {
                    throw new Error();
                  } catch (e) {
                    o.stack = e.stack;
                  }
                  Error.stackTraceLimit -= 6;
                };
              }
              formatStack = function(stack, error) {
                if (typeof stack === "string")
                  return stack;
                if ((typeof error === "object" || typeof error === "function") && error.name !== undefined && error.message !== undefined) {
                  return error.toString();
                }
                return formatNonError(error);
              };
              return null;
            })([]);
            if (typeof console !== "undefined" && typeof console.warn !== "undefined") {
              printWarning = function(message) {
                console.warn(message);
              };
              if (util.isNode && process.stderr.isTTY) {
                printWarning = function(message, isSoft) {
                  var color = isSoft ? "\u001b[33m" : "\u001b[31m";
                  console.warn(color + message + "\u001b[0m\n");
                };
              } else if (!util.isNode && typeof(new Error().stack) === "string") {
                printWarning = function(message, isSoft) {
                  console.warn("%c" + message, isSoft ? "color: darkorange" : "color: red");
                };
              }
            }
            var config = {
              warnings: warnings,
              longStackTraces: false,
              cancellation: false,
              monitoring: false
            };
            if (longStackTraces)
              Promise.longStackTraces();
            return {
              longStackTraces: function() {
                return config.longStackTraces;
              },
              warnings: function() {
                return config.warnings;
              },
              cancellation: function() {
                return config.cancellation;
              },
              monitoring: function() {
                return config.monitoring;
              },
              propagateFromFunction: function() {
                return propagateFromFunction;
              },
              boundValueFunction: function() {
                return boundValueFunction;
              },
              checkForgottenReturns: checkForgottenReturns,
              setBounds: setBounds,
              warn: warn,
              deprecated: deprecated,
              CapturedTrace: CapturedTrace,
              fireDomEvent: fireDomEvent,
              fireGlobalEvent: fireGlobalEvent
            };
          };
        }, {
          "./errors": 12,
          "./util": 36
        }],
        10: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise) {
            function returner() {
              return this.value;
            }
            function thrower() {
              throw this.reason;
            }
            Promise.prototype["return"] = Promise.prototype.thenReturn = function(value) {
              if (value instanceof Promise)
                value.suppressUnhandledRejections();
              return this._then(returner, undefined, undefined, {value: value}, undefined);
            };
            Promise.prototype["throw"] = Promise.prototype.thenThrow = function(reason) {
              return this._then(thrower, undefined, undefined, {reason: reason}, undefined);
            };
            Promise.prototype.catchThrow = function(reason) {
              if (arguments.length <= 1) {
                return this._then(undefined, thrower, undefined, {reason: reason}, undefined);
              } else {
                var _reason = arguments[1];
                var handler = function() {
                  throw _reason;
                };
                return this.caught(reason, handler);
              }
            };
            Promise.prototype.catchReturn = function(value) {
              if (arguments.length <= 1) {
                if (value instanceof Promise)
                  value.suppressUnhandledRejections();
                return this._then(undefined, returner, undefined, {value: value}, undefined);
              } else {
                var _value = arguments[1];
                if (_value instanceof Promise)
                  _value.suppressUnhandledRejections();
                var handler = function() {
                  return _value;
                };
                return this.caught(value, handler);
              }
            };
          };
        }, {}],
        11: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL) {
            var PromiseReduce = Promise.reduce;
            var PromiseAll = Promise.all;
            function promiseAllThis() {
              return PromiseAll(this);
            }
            function PromiseMapSeries(promises, fn) {
              return PromiseReduce(promises, fn, INTERNAL, INTERNAL);
            }
            Promise.prototype.each = function(fn) {
              return this.mapSeries(fn)._then(promiseAllThis, undefined, undefined, this, undefined);
            };
            Promise.prototype.mapSeries = function(fn) {
              return PromiseReduce(this, fn, INTERNAL, INTERNAL);
            };
            Promise.each = function(promises, fn) {
              return PromiseMapSeries(promises, fn)._then(promiseAllThis, undefined, undefined, promises, undefined);
            };
            Promise.mapSeries = PromiseMapSeries;
          };
        }, {}],
        12: [function(_dereq_, module, exports) {
          "use strict";
          var es5 = _dereq_("./es5");
          var Objectfreeze = es5.freeze;
          var util = _dereq_("./util");
          var inherits = util.inherits;
          var notEnumerableProp = util.notEnumerableProp;
          function subError(nameProperty, defaultMessage) {
            function SubError(message) {
              if (!(this instanceof SubError))
                return new SubError(message);
              notEnumerableProp(this, "message", typeof message === "string" ? message : defaultMessage);
              notEnumerableProp(this, "name", nameProperty);
              if (Error.captureStackTrace) {
                Error.captureStackTrace(this, this.constructor);
              } else {
                Error.call(this);
              }
            }
            inherits(SubError, Error);
            return SubError;
          }
          var _TypeError,
              _RangeError;
          var Warning = subError("Warning", "warning");
          var CancellationError = subError("CancellationError", "cancellation error");
          var TimeoutError = subError("TimeoutError", "timeout error");
          var AggregateError = subError("AggregateError", "aggregate error");
          try {
            _TypeError = TypeError;
            _RangeError = RangeError;
          } catch (e) {
            _TypeError = subError("TypeError", "type error");
            _RangeError = subError("RangeError", "range error");
          }
          var methods = ("join pop push shift unshift slice filter forEach some " + "every map indexOf lastIndexOf reduce reduceRight sort reverse").split(" ");
          for (var i = 0; i < methods.length; ++i) {
            if (typeof Array.prototype[methods[i]] === "function") {
              AggregateError.prototype[methods[i]] = Array.prototype[methods[i]];
            }
          }
          es5.defineProperty(AggregateError.prototype, "length", {
            value: 0,
            configurable: false,
            writable: true,
            enumerable: true
          });
          AggregateError.prototype["isOperational"] = true;
          var level = 0;
          AggregateError.prototype.toString = function() {
            var indent = Array(level * 4 + 1).join(" ");
            var ret = "\n" + indent + "AggregateError of:" + "\n";
            level++;
            indent = Array(level * 4 + 1).join(" ");
            for (var i = 0; i < this.length; ++i) {
              var str = this[i] === this ? "[Circular AggregateError]" : this[i] + "";
              var lines = str.split("\n");
              for (var j = 0; j < lines.length; ++j) {
                lines[j] = indent + lines[j];
              }
              str = lines.join("\n");
              ret += str + "\n";
            }
            level--;
            return ret;
          };
          function OperationalError(message) {
            if (!(this instanceof OperationalError))
              return new OperationalError(message);
            notEnumerableProp(this, "name", "OperationalError");
            notEnumerableProp(this, "message", message);
            this.cause = message;
            this["isOperational"] = true;
            if (message instanceof Error) {
              notEnumerableProp(this, "message", message.message);
              notEnumerableProp(this, "stack", message.stack);
            } else if (Error.captureStackTrace) {
              Error.captureStackTrace(this, this.constructor);
            }
          }
          inherits(OperationalError, Error);
          var errorTypes = Error["__BluebirdErrorTypes__"];
          if (!errorTypes) {
            errorTypes = Objectfreeze({
              CancellationError: CancellationError,
              TimeoutError: TimeoutError,
              OperationalError: OperationalError,
              RejectionError: OperationalError,
              AggregateError: AggregateError
            });
            es5.defineProperty(Error, "__BluebirdErrorTypes__", {
              value: errorTypes,
              writable: false,
              enumerable: false,
              configurable: false
            });
          }
          module.exports = {
            Error: Error,
            TypeError: _TypeError,
            RangeError: _RangeError,
            CancellationError: errorTypes.CancellationError,
            OperationalError: errorTypes.OperationalError,
            TimeoutError: errorTypes.TimeoutError,
            AggregateError: errorTypes.AggregateError,
            Warning: Warning
          };
        }, {
          "./es5": 13,
          "./util": 36
        }],
        13: [function(_dereq_, module, exports) {
          var isES5 = (function() {
            "use strict";
            return this === undefined;
          })();
          if (isES5) {
            module.exports = {
              freeze: Object.freeze,
              defineProperty: Object.defineProperty,
              getDescriptor: Object.getOwnPropertyDescriptor,
              keys: Object.keys,
              names: Object.getOwnPropertyNames,
              getPrototypeOf: Object.getPrototypeOf,
              isArray: Array.isArray,
              isES5: isES5,
              propertyIsWritable: function(obj, prop) {
                var descriptor = Object.getOwnPropertyDescriptor(obj, prop);
                return !!(!descriptor || descriptor.writable || descriptor.set);
              }
            };
          } else {
            var has = {}.hasOwnProperty;
            var str = {}.toString;
            var proto = {}.constructor.prototype;
            var ObjectKeys = function(o) {
              var ret = [];
              for (var key in o) {
                if (has.call(o, key)) {
                  ret.push(key);
                }
              }
              return ret;
            };
            var ObjectGetDescriptor = function(o, key) {
              return {value: o[key]};
            };
            var ObjectDefineProperty = function(o, key, desc) {
              o[key] = desc.value;
              return o;
            };
            var ObjectFreeze = function(obj) {
              return obj;
            };
            var ObjectGetPrototypeOf = function(obj) {
              try {
                return Object(obj).constructor.prototype;
              } catch (e) {
                return proto;
              }
            };
            var ArrayIsArray = function(obj) {
              try {
                return str.call(obj) === "[object Array]";
              } catch (e) {
                return false;
              }
            };
            module.exports = {
              isArray: ArrayIsArray,
              keys: ObjectKeys,
              names: ObjectKeys,
              defineProperty: ObjectDefineProperty,
              getDescriptor: ObjectGetDescriptor,
              freeze: ObjectFreeze,
              getPrototypeOf: ObjectGetPrototypeOf,
              isES5: isES5,
              propertyIsWritable: function() {
                return true;
              }
            };
          }
        }, {}],
        14: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL) {
            var PromiseMap = Promise.map;
            Promise.prototype.filter = function(fn, options) {
              return PromiseMap(this, fn, options, INTERNAL);
            };
            Promise.filter = function(promises, fn, options) {
              return PromiseMap(promises, fn, options, INTERNAL);
            };
          };
        }, {}],
        15: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, tryConvertToPromise) {
            var util = _dereq_("./util");
            var CancellationError = Promise.CancellationError;
            var errorObj = util.errorObj;
            function PassThroughHandlerContext(promise, type, handler) {
              this.promise = promise;
              this.type = type;
              this.handler = handler;
              this.called = false;
              this.cancelPromise = null;
            }
            PassThroughHandlerContext.prototype.isFinallyHandler = function() {
              return this.type === 0;
            };
            function FinallyHandlerCancelReaction(finallyHandler) {
              this.finallyHandler = finallyHandler;
            }
            FinallyHandlerCancelReaction.prototype._resultCancelled = function() {
              checkCancel(this.finallyHandler);
            };
            function checkCancel(ctx, reason) {
              if (ctx.cancelPromise != null) {
                if (arguments.length > 1) {
                  ctx.cancelPromise._reject(reason);
                } else {
                  ctx.cancelPromise._cancel();
                }
                ctx.cancelPromise = null;
                return true;
              }
              return false;
            }
            function succeed() {
              return finallyHandler.call(this, this.promise._target()._settledValue());
            }
            function fail(reason) {
              if (checkCancel(this, reason))
                return;
              errorObj.e = reason;
              return errorObj;
            }
            function finallyHandler(reasonOrValue) {
              var promise = this.promise;
              var handler = this.handler;
              if (!this.called) {
                this.called = true;
                var ret = this.isFinallyHandler() ? handler.call(promise._boundValue()) : handler.call(promise._boundValue(), reasonOrValue);
                if (ret !== undefined) {
                  promise._setReturnedNonUndefined();
                  var maybePromise = tryConvertToPromise(ret, promise);
                  if (maybePromise instanceof Promise) {
                    if (this.cancelPromise != null) {
                      if (maybePromise.isCancelled()) {
                        var reason = new CancellationError("late cancellation observer");
                        promise._attachExtraTrace(reason);
                        errorObj.e = reason;
                        return errorObj;
                      } else if (maybePromise.isPending()) {
                        maybePromise._attachCancellationCallback(new FinallyHandlerCancelReaction(this));
                      }
                    }
                    return maybePromise._then(succeed, fail, undefined, this, undefined);
                  }
                }
              }
              if (promise.isRejected()) {
                checkCancel(this);
                errorObj.e = reasonOrValue;
                return errorObj;
              } else {
                checkCancel(this);
                return reasonOrValue;
              }
            }
            Promise.prototype._passThrough = function(handler, type, success, fail) {
              if (typeof handler !== "function")
                return this.then();
              return this._then(success, fail, undefined, new PassThroughHandlerContext(this, type, handler), undefined);
            };
            Promise.prototype.lastly = Promise.prototype["finally"] = function(handler) {
              return this._passThrough(handler, 0, finallyHandler, finallyHandler);
            };
            Promise.prototype.tap = function(handler) {
              return this._passThrough(handler, 1, finallyHandler);
            };
            return PassThroughHandlerContext;
          };
        }, {"./util": 36}],
        16: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, apiRejection, INTERNAL, tryConvertToPromise, Proxyable, debug) {
            var errors = _dereq_("./errors");
            var TypeError = errors.TypeError;
            var util = _dereq_("./util");
            var errorObj = util.errorObj;
            var tryCatch = util.tryCatch;
            var yieldHandlers = [];
            function promiseFromYieldHandler(value, yieldHandlers, traceParent) {
              for (var i = 0; i < yieldHandlers.length; ++i) {
                traceParent._pushContext();
                var result = tryCatch(yieldHandlers[i])(value);
                traceParent._popContext();
                if (result === errorObj) {
                  traceParent._pushContext();
                  var ret = Promise.reject(errorObj.e);
                  traceParent._popContext();
                  return ret;
                }
                var maybePromise = tryConvertToPromise(result, traceParent);
                if (maybePromise instanceof Promise)
                  return maybePromise;
              }
              return null;
            }
            function PromiseSpawn(generatorFunction, receiver, yieldHandler, stack) {
              if (debug.cancellation()) {
                var internal = new Promise(INTERNAL);
                var _finallyPromise = this._finallyPromise = new Promise(INTERNAL);
                this._promise = internal.lastly(function() {
                  return _finallyPromise;
                });
                internal._captureStackTrace();
                internal._setOnCancel(this);
              } else {
                var promise = this._promise = new Promise(INTERNAL);
                promise._captureStackTrace();
              }
              this._stack = stack;
              this._generatorFunction = generatorFunction;
              this._receiver = receiver;
              this._generator = undefined;
              this._yieldHandlers = typeof yieldHandler === "function" ? [yieldHandler].concat(yieldHandlers) : yieldHandlers;
              this._yieldedPromise = null;
              this._cancellationPhase = false;
            }
            util.inherits(PromiseSpawn, Proxyable);
            PromiseSpawn.prototype._isResolved = function() {
              return this._promise === null;
            };
            PromiseSpawn.prototype._cleanup = function() {
              this._promise = this._generator = null;
              if (debug.cancellation() && this._finallyPromise !== null) {
                this._finallyPromise._fulfill();
                this._finallyPromise = null;
              }
            };
            PromiseSpawn.prototype._promiseCancelled = function() {
              if (this._isResolved())
                return;
              var implementsReturn = typeof this._generator["return"] !== "undefined";
              var result;
              if (!implementsReturn) {
                var reason = new Promise.CancellationError("generator .return() sentinel");
                Promise.coroutine.returnSentinel = reason;
                this._promise._attachExtraTrace(reason);
                this._promise._pushContext();
                result = tryCatch(this._generator["throw"]).call(this._generator, reason);
                this._promise._popContext();
              } else {
                this._promise._pushContext();
                result = tryCatch(this._generator["return"]).call(this._generator, undefined);
                this._promise._popContext();
              }
              this._cancellationPhase = true;
              this._yieldedPromise = null;
              this._continue(result);
            };
            PromiseSpawn.prototype._promiseFulfilled = function(value) {
              this._yieldedPromise = null;
              this._promise._pushContext();
              var result = tryCatch(this._generator.next).call(this._generator, value);
              this._promise._popContext();
              this._continue(result);
            };
            PromiseSpawn.prototype._promiseRejected = function(reason) {
              this._yieldedPromise = null;
              this._promise._attachExtraTrace(reason);
              this._promise._pushContext();
              var result = tryCatch(this._generator["throw"]).call(this._generator, reason);
              this._promise._popContext();
              this._continue(result);
            };
            PromiseSpawn.prototype._resultCancelled = function() {
              if (this._yieldedPromise instanceof Promise) {
                var promise = this._yieldedPromise;
                this._yieldedPromise = null;
                promise.cancel();
              }
            };
            PromiseSpawn.prototype.promise = function() {
              return this._promise;
            };
            PromiseSpawn.prototype._run = function() {
              this._generator = this._generatorFunction.call(this._receiver);
              this._receiver = this._generatorFunction = undefined;
              this._promiseFulfilled(undefined);
            };
            PromiseSpawn.prototype._continue = function(result) {
              var promise = this._promise;
              if (result === errorObj) {
                this._cleanup();
                if (this._cancellationPhase) {
                  return promise.cancel();
                } else {
                  return promise._rejectCallback(result.e, false);
                }
              }
              var value = result.value;
              if (result.done === true) {
                this._cleanup();
                if (this._cancellationPhase) {
                  return promise.cancel();
                } else {
                  return promise._resolveCallback(value);
                }
              } else {
                var maybePromise = tryConvertToPromise(value, this._promise);
                if (!(maybePromise instanceof Promise)) {
                  maybePromise = promiseFromYieldHandler(maybePromise, this._yieldHandlers, this._promise);
                  if (maybePromise === null) {
                    this._promiseRejected(new TypeError("A value %s was yielded that could not be treated as a promise\u000a\u000a    See http://goo.gl/MqrFmX\u000a\u000a".replace("%s", value) + "From coroutine:\u000a" + this._stack.split("\n").slice(1, -7).join("\n")));
                    return;
                  }
                }
                maybePromise = maybePromise._target();
                var bitField = maybePromise._bitField;
                ;
                if (((bitField & 50397184) === 0)) {
                  this._yieldedPromise = maybePromise;
                  maybePromise._proxy(this, null);
                } else if (((bitField & 33554432) !== 0)) {
                  this._promiseFulfilled(maybePromise._value());
                } else if (((bitField & 16777216) !== 0)) {
                  this._promiseRejected(maybePromise._reason());
                } else {
                  this._promiseCancelled();
                }
              }
            };
            Promise.coroutine = function(generatorFunction, options) {
              if (typeof generatorFunction !== "function") {
                throw new TypeError("generatorFunction must be a function\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              var yieldHandler = Object(options).yieldHandler;
              var PromiseSpawn$ = PromiseSpawn;
              var stack = new Error().stack;
              return function() {
                var generator = generatorFunction.apply(this, arguments);
                var spawn = new PromiseSpawn$(undefined, undefined, yieldHandler, stack);
                var ret = spawn.promise();
                spawn._generator = generator;
                spawn._promiseFulfilled(undefined);
                return ret;
              };
            };
            Promise.coroutine.addYieldHandler = function(fn) {
              if (typeof fn !== "function") {
                throw new TypeError("expecting a function but got " + util.classString(fn));
              }
              yieldHandlers.push(fn);
            };
            Promise.spawn = function(generatorFunction) {
              debug.deprecated("Promise.spawn()", "Promise.coroutine()");
              if (typeof generatorFunction !== "function") {
                return apiRejection("generatorFunction must be a function\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              var spawn = new PromiseSpawn(generatorFunction, this);
              var ret = spawn.promise();
              spawn._run(Promise.spawn);
              return ret;
            };
          };
        }, {
          "./errors": 12,
          "./util": 36
        }],
        17: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, tryConvertToPromise, INTERNAL) {
            var util = _dereq_("./util");
            var canEvaluate = util.canEvaluate;
            var tryCatch = util.tryCatch;
            var errorObj = util.errorObj;
            var reject;
            if (!true) {
              if (canEvaluate) {
                var thenCallback = function(i) {
                  return new Function("value", "holder", "                             \n\
            'use strict';                                                    \n\
            holder.pIndex = value;                                           \n\
            holder.checkFulfillment(this);                                   \n\
            ".replace(/Index/g, i));
                };
                var promiseSetter = function(i) {
                  return new Function("promise", "holder", "                           \n\
            'use strict';                                                    \n\
            holder.pIndex = promise;                                         \n\
            ".replace(/Index/g, i));
                };
                var generateHolderClass = function(total) {
                  var props = new Array(total);
                  for (var i = 0; i < props.length; ++i) {
                    props[i] = "this.p" + (i + 1);
                  }
                  var assignment = props.join(" = ") + " = null;";
                  var cancellationCode = "var promise;\n" + props.map(function(prop) {
                    return "                                                         \n\
                promise = " + prop + ";                                      \n\
                if (promise instanceof Promise) {                            \n\
                    promise.cancel();                                        \n\
                }                                                            \n\
            ";
                  }).join("\n");
                  var passedArguments = props.join(", ");
                  var name = "Holder$" + total;
                  var code = "return function(tryCatch, errorObj, Promise) {           \n\
            'use strict';                                                    \n\
            function [TheName](fn) {                                         \n\
                [TheProperties]                                              \n\
                this.fn = fn;                                                \n\
                this.now = 0;                                                \n\
            }                                                                \n\
            [TheName].prototype.checkFulfillment = function(promise) {       \n\
                var now = ++this.now;                                        \n\
                if (now === [TheTotal]) {                                    \n\
                    promise._pushContext();                                  \n\
                    var callback = this.fn;                                  \n\
                    var ret = tryCatch(callback)([ThePassedArguments]);      \n\
                    promise._popContext();                                   \n\
                    if (ret === errorObj) {                                  \n\
                        promise._rejectCallback(ret.e, false);               \n\
                    } else {                                                 \n\
                        promise._resolveCallback(ret);                       \n\
                    }                                                        \n\
                }                                                            \n\
            };                                                               \n\
                                                                             \n\
            [TheName].prototype._resultCancelled = function() {              \n\
                [CancellationCode]                                           \n\
            };                                                               \n\
                                                                             \n\
            return [TheName];                                                \n\
        }(tryCatch, errorObj, Promise);                                      \n\
        ";
                  code = code.replace(/\[TheName\]/g, name).replace(/\[TheTotal\]/g, total).replace(/\[ThePassedArguments\]/g, passedArguments).replace(/\[TheProperties\]/g, assignment).replace(/\[CancellationCode\]/g, cancellationCode);
                  return new Function("tryCatch", "errorObj", "Promise", code)(tryCatch, errorObj, Promise);
                };
                var holderClasses = [];
                var thenCallbacks = [];
                var promiseSetters = [];
                for (var i = 0; i < 8; ++i) {
                  holderClasses.push(generateHolderClass(i + 1));
                  thenCallbacks.push(thenCallback(i + 1));
                  promiseSetters.push(promiseSetter(i + 1));
                }
                reject = function(reason) {
                  this._reject(reason);
                };
              }
            }
            Promise.join = function() {
              var last = arguments.length - 1;
              var fn;
              if (last > 0 && typeof arguments[last] === "function") {
                fn = arguments[last];
                if (!true) {
                  if (last <= 8 && canEvaluate) {
                    var ret = new Promise(INTERNAL);
                    ret._captureStackTrace();
                    var HolderClass = holderClasses[last - 1];
                    var holder = new HolderClass(fn);
                    var callbacks = thenCallbacks;
                    for (var i = 0; i < last; ++i) {
                      var maybePromise = tryConvertToPromise(arguments[i], ret);
                      if (maybePromise instanceof Promise) {
                        maybePromise = maybePromise._target();
                        var bitField = maybePromise._bitField;
                        ;
                        if (((bitField & 50397184) === 0)) {
                          maybePromise._then(callbacks[i], reject, undefined, ret, holder);
                          promiseSetters[i](maybePromise, holder);
                        } else if (((bitField & 33554432) !== 0)) {
                          callbacks[i].call(ret, maybePromise._value(), holder);
                        } else if (((bitField & 16777216) !== 0)) {
                          ret._reject(maybePromise._reason());
                        } else {
                          ret._cancel();
                        }
                      } else {
                        callbacks[i].call(ret, maybePromise, holder);
                      }
                    }
                    if (!ret._isFateSealed()) {
                      ret._setAsyncGuaranteed();
                      ret._setOnCancel(holder);
                    }
                    return ret;
                  }
                }
              }
              var args = [].slice.call(arguments);
              ;
              if (fn)
                args.pop();
              var ret = new PromiseArray(args).promise();
              return fn !== undefined ? ret.spread(fn) : ret;
            };
          };
        }, {"./util": 36}],
        18: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, apiRejection, tryConvertToPromise, INTERNAL, debug) {
            var getDomain = Promise._getDomain;
            var util = _dereq_("./util");
            var tryCatch = util.tryCatch;
            var errorObj = util.errorObj;
            var EMPTY_ARRAY = [];
            function MappingPromiseArray(promises, fn, limit, _filter) {
              this.constructor$(promises);
              this._promise._captureStackTrace();
              var domain = getDomain();
              this._callback = domain === null ? fn : domain.bind(fn);
              this._preservedValues = _filter === INTERNAL ? new Array(this.length()) : null;
              this._limit = limit;
              this._inFlight = 0;
              this._queue = limit >= 1 ? [] : EMPTY_ARRAY;
              this._init$(undefined, -2);
            }
            util.inherits(MappingPromiseArray, PromiseArray);
            MappingPromiseArray.prototype._init = function() {};
            MappingPromiseArray.prototype._promiseFulfilled = function(value, index) {
              var values = this._values;
              var length = this.length();
              var preservedValues = this._preservedValues;
              var limit = this._limit;
              if (index < 0) {
                index = (index * -1) - 1;
                values[index] = value;
                if (limit >= 1) {
                  this._inFlight--;
                  this._drainQueue();
                  if (this._isResolved())
                    return true;
                }
              } else {
                if (limit >= 1 && this._inFlight >= limit) {
                  values[index] = value;
                  this._queue.push(index);
                  return false;
                }
                if (preservedValues !== null)
                  preservedValues[index] = value;
                var promise = this._promise;
                var callback = this._callback;
                var receiver = promise._boundValue();
                promise._pushContext();
                var ret = tryCatch(callback).call(receiver, value, index, length);
                var promiseCreated = promise._popContext();
                debug.checkForgottenReturns(ret, promiseCreated, preservedValues !== null ? "Promise.filter" : "Promise.map", promise);
                if (ret === errorObj) {
                  this._reject(ret.e);
                  return true;
                }
                var maybePromise = tryConvertToPromise(ret, this._promise);
                if (maybePromise instanceof Promise) {
                  maybePromise = maybePromise._target();
                  var bitField = maybePromise._bitField;
                  ;
                  if (((bitField & 50397184) === 0)) {
                    if (limit >= 1)
                      this._inFlight++;
                    values[index] = maybePromise;
                    maybePromise._proxy(this, (index + 1) * -1);
                    return false;
                  } else if (((bitField & 33554432) !== 0)) {
                    ret = maybePromise._value();
                  } else if (((bitField & 16777216) !== 0)) {
                    this._reject(maybePromise._reason());
                    return true;
                  } else {
                    this._cancel();
                    return true;
                  }
                }
                values[index] = ret;
              }
              var totalResolved = ++this._totalResolved;
              if (totalResolved >= length) {
                if (preservedValues !== null) {
                  this._filter(values, preservedValues);
                } else {
                  this._resolve(values);
                }
                return true;
              }
              return false;
            };
            MappingPromiseArray.prototype._drainQueue = function() {
              var queue = this._queue;
              var limit = this._limit;
              var values = this._values;
              while (queue.length > 0 && this._inFlight < limit) {
                if (this._isResolved())
                  return;
                var index = queue.pop();
                this._promiseFulfilled(values[index], index);
              }
            };
            MappingPromiseArray.prototype._filter = function(booleans, values) {
              var len = values.length;
              var ret = new Array(len);
              var j = 0;
              for (var i = 0; i < len; ++i) {
                if (booleans[i])
                  ret[j++] = values[i];
              }
              ret.length = j;
              this._resolve(ret);
            };
            MappingPromiseArray.prototype.preservedValues = function() {
              return this._preservedValues;
            };
            function map(promises, fn, options, _filter) {
              if (typeof fn !== "function") {
                return apiRejection("expecting a function but got " + util.classString(fn));
              }
              var limit = typeof options === "object" && options !== null ? options.concurrency : 0;
              limit = typeof limit === "number" && isFinite(limit) && limit >= 1 ? limit : 0;
              return new MappingPromiseArray(promises, fn, limit, _filter).promise();
            }
            Promise.prototype.map = function(fn, options) {
              return map(this, fn, options, null);
            };
            Promise.map = function(promises, fn, options, _filter) {
              return map(promises, fn, options, _filter);
            };
          };
        }, {"./util": 36}],
        19: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL, tryConvertToPromise, apiRejection, debug) {
            var util = _dereq_("./util");
            var tryCatch = util.tryCatch;
            Promise.method = function(fn) {
              if (typeof fn !== "function") {
                throw new Promise.TypeError("expecting a function but got " + util.classString(fn));
              }
              return function() {
                var ret = new Promise(INTERNAL);
                ret._captureStackTrace();
                ret._pushContext();
                var value = tryCatch(fn).apply(this, arguments);
                var promiseCreated = ret._popContext();
                debug.checkForgottenReturns(value, promiseCreated, "Promise.method", ret);
                ret._resolveFromSyncValue(value);
                return ret;
              };
            };
            Promise.attempt = Promise["try"] = function(fn) {
              if (typeof fn !== "function") {
                return apiRejection("expecting a function but got " + util.classString(fn));
              }
              var ret = new Promise(INTERNAL);
              ret._captureStackTrace();
              ret._pushContext();
              var value;
              if (arguments.length > 1) {
                debug.deprecated("calling Promise.try with more than 1 argument");
                var arg = arguments[1];
                var ctx = arguments[2];
                value = util.isArray(arg) ? tryCatch(fn).apply(ctx, arg) : tryCatch(fn).call(ctx, arg);
              } else {
                value = tryCatch(fn)();
              }
              var promiseCreated = ret._popContext();
              debug.checkForgottenReturns(value, promiseCreated, "Promise.try", ret);
              ret._resolveFromSyncValue(value);
              return ret;
            };
            Promise.prototype._resolveFromSyncValue = function(value) {
              if (value === util.errorObj) {
                this._rejectCallback(value.e, false);
              } else {
                this._resolveCallback(value, true);
              }
            };
          };
        }, {"./util": 36}],
        20: [function(_dereq_, module, exports) {
          "use strict";
          var util = _dereq_("./util");
          var maybeWrapAsError = util.maybeWrapAsError;
          var errors = _dereq_("./errors");
          var OperationalError = errors.OperationalError;
          var es5 = _dereq_("./es5");
          function isUntypedError(obj) {
            return obj instanceof Error && es5.getPrototypeOf(obj) === Error.prototype;
          }
          var rErrorKey = /^(?:name|message|stack|cause)$/;
          function wrapAsOperationalError(obj) {
            var ret;
            if (isUntypedError(obj)) {
              ret = new OperationalError(obj);
              ret.name = obj.name;
              ret.message = obj.message;
              ret.stack = obj.stack;
              var keys = es5.keys(obj);
              for (var i = 0; i < keys.length; ++i) {
                var key = keys[i];
                if (!rErrorKey.test(key)) {
                  ret[key] = obj[key];
                }
              }
              return ret;
            }
            util.markAsOriginatingFromRejection(obj);
            return obj;
          }
          function nodebackForPromise(promise, multiArgs) {
            return function(err, value) {
              if (promise === null)
                return;
              if (err) {
                var wrapped = wrapAsOperationalError(maybeWrapAsError(err));
                promise._attachExtraTrace(wrapped);
                promise._reject(wrapped);
              } else if (!multiArgs) {
                promise._fulfill(value);
              } else {
                var args = [].slice.call(arguments, 1);
                ;
                promise._fulfill(args);
              }
              promise = null;
            };
          }
          module.exports = nodebackForPromise;
        }, {
          "./errors": 12,
          "./es5": 13,
          "./util": 36
        }],
        21: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise) {
            var util = _dereq_("./util");
            var async = Promise._async;
            var tryCatch = util.tryCatch;
            var errorObj = util.errorObj;
            function spreadAdapter(val, nodeback) {
              var promise = this;
              if (!util.isArray(val))
                return successAdapter.call(promise, val, nodeback);
              var ret = tryCatch(nodeback).apply(promise._boundValue(), [null].concat(val));
              if (ret === errorObj) {
                async.throwLater(ret.e);
              }
            }
            function successAdapter(val, nodeback) {
              var promise = this;
              var receiver = promise._boundValue();
              var ret = val === undefined ? tryCatch(nodeback).call(receiver, null) : tryCatch(nodeback).call(receiver, null, val);
              if (ret === errorObj) {
                async.throwLater(ret.e);
              }
            }
            function errorAdapter(reason, nodeback) {
              var promise = this;
              if (!reason) {
                var newReason = new Error(reason + "");
                newReason.cause = reason;
                reason = newReason;
              }
              var ret = tryCatch(nodeback).call(promise._boundValue(), reason);
              if (ret === errorObj) {
                async.throwLater(ret.e);
              }
            }
            Promise.prototype.asCallback = Promise.prototype.nodeify = function(nodeback, options) {
              if (typeof nodeback == "function") {
                var adapter = successAdapter;
                if (options !== undefined && Object(options).spread) {
                  adapter = spreadAdapter;
                }
                this._then(adapter, errorAdapter, undefined, this, nodeback);
              }
              return this;
            };
          };
        }, {"./util": 36}],
        22: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function() {
            var makeSelfResolutionError = function() {
              return new TypeError("circular promise resolution chain\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
            };
            var reflectHandler = function() {
              return new Promise.PromiseInspection(this._target());
            };
            var apiRejection = function(msg) {
              return Promise.reject(new TypeError(msg));
            };
            function Proxyable() {}
            var UNDEFINED_BINDING = {};
            var util = _dereq_("./util");
            var getDomain;
            if (util.isNode) {
              getDomain = function() {
                var ret = process.domain;
                if (ret === undefined)
                  ret = null;
                return ret;
              };
            } else {
              getDomain = function() {
                return null;
              };
            }
            util.notEnumerableProp(Promise, "_getDomain", getDomain);
            var es5 = _dereq_("./es5");
            var Async = _dereq_("./async");
            var async = new Async();
            es5.defineProperty(Promise, "_async", {value: async});
            var errors = _dereq_("./errors");
            var TypeError = Promise.TypeError = errors.TypeError;
            Promise.RangeError = errors.RangeError;
            var CancellationError = Promise.CancellationError = errors.CancellationError;
            Promise.TimeoutError = errors.TimeoutError;
            Promise.OperationalError = errors.OperationalError;
            Promise.RejectionError = errors.OperationalError;
            Promise.AggregateError = errors.AggregateError;
            var INTERNAL = function() {};
            var APPLY = {};
            var NEXT_FILTER = {};
            var tryConvertToPromise = _dereq_("./thenables")(Promise, INTERNAL);
            var PromiseArray = _dereq_("./promise_array")(Promise, INTERNAL, tryConvertToPromise, apiRejection, Proxyable);
            var Context = _dereq_("./context")(Promise);
            var createContext = Context.create;
            var debug = _dereq_("./debuggability")(Promise, Context);
            var CapturedTrace = debug.CapturedTrace;
            var PassThroughHandlerContext = _dereq_("./finally")(Promise, tryConvertToPromise);
            var catchFilter = _dereq_("./catch_filter")(NEXT_FILTER);
            var nodebackForPromise = _dereq_("./nodeback");
            var errorObj = util.errorObj;
            var tryCatch = util.tryCatch;
            function check(self, executor) {
              if (typeof executor !== "function") {
                throw new TypeError("expecting a function but got " + util.classString(executor));
              }
              if (self.constructor !== Promise) {
                throw new TypeError("the promise constructor cannot be invoked directly\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
            }
            function Promise(executor) {
              this._bitField = 0;
              this._fulfillmentHandler0 = undefined;
              this._rejectionHandler0 = undefined;
              this._promise0 = undefined;
              this._receiver0 = undefined;
              if (executor !== INTERNAL) {
                check(this, executor);
                this._resolveFromExecutor(executor);
              }
              this._promiseCreated();
              this._fireEvent("promiseCreated", this);
            }
            Promise.prototype.toString = function() {
              return "[object Promise]";
            };
            Promise.prototype.caught = Promise.prototype["catch"] = function(fn) {
              var len = arguments.length;
              if (len > 1) {
                var catchInstances = new Array(len - 1),
                    j = 0,
                    i;
                for (i = 0; i < len - 1; ++i) {
                  var item = arguments[i];
                  if (util.isObject(item)) {
                    catchInstances[j++] = item;
                  } else {
                    return apiRejection("expecting an object but got " + util.classString(item));
                  }
                }
                catchInstances.length = j;
                fn = arguments[i];
                return this.then(undefined, catchFilter(catchInstances, fn, this));
              }
              return this.then(undefined, fn);
            };
            Promise.prototype.reflect = function() {
              return this._then(reflectHandler, reflectHandler, undefined, this, undefined);
            };
            Promise.prototype.then = function(didFulfill, didReject) {
              if (debug.warnings() && arguments.length > 0 && typeof didFulfill !== "function" && typeof didReject !== "function") {
                var msg = ".then() only accepts functions but was passed: " + util.classString(didFulfill);
                if (arguments.length > 1) {
                  msg += ", " + util.classString(didReject);
                }
                this._warn(msg);
              }
              return this._then(didFulfill, didReject, undefined, undefined, undefined);
            };
            Promise.prototype.done = function(didFulfill, didReject) {
              var promise = this._then(didFulfill, didReject, undefined, undefined, undefined);
              promise._setIsFinal();
            };
            Promise.prototype.spread = function(fn) {
              if (typeof fn !== "function") {
                return apiRejection("expecting a function but got " + util.classString(fn));
              }
              return this.all()._then(fn, undefined, undefined, APPLY, undefined);
            };
            Promise.prototype.toJSON = function() {
              var ret = {
                isFulfilled: false,
                isRejected: false,
                fulfillmentValue: undefined,
                rejectionReason: undefined
              };
              if (this.isFulfilled()) {
                ret.fulfillmentValue = this.value();
                ret.isFulfilled = true;
              } else if (this.isRejected()) {
                ret.rejectionReason = this.reason();
                ret.isRejected = true;
              }
              return ret;
            };
            Promise.prototype.all = function() {
              if (arguments.length > 0) {
                this._warn(".all() was passed arguments but it does not take any");
              }
              return new PromiseArray(this).promise();
            };
            Promise.prototype.error = function(fn) {
              return this.caught(util.originatesFromRejection, fn);
            };
            Promise.is = function(val) {
              return val instanceof Promise;
            };
            Promise.fromNode = Promise.fromCallback = function(fn) {
              var ret = new Promise(INTERNAL);
              ret._captureStackTrace();
              var multiArgs = arguments.length > 1 ? !!Object(arguments[1]).multiArgs : false;
              var result = tryCatch(fn)(nodebackForPromise(ret, multiArgs));
              if (result === errorObj) {
                ret._rejectCallback(result.e, true);
              }
              if (!ret._isFateSealed())
                ret._setAsyncGuaranteed();
              return ret;
            };
            Promise.all = function(promises) {
              return new PromiseArray(promises).promise();
            };
            Promise.cast = function(obj) {
              var ret = tryConvertToPromise(obj);
              if (!(ret instanceof Promise)) {
                ret = new Promise(INTERNAL);
                ret._captureStackTrace();
                ret._setFulfilled();
                ret._rejectionHandler0 = obj;
              }
              return ret;
            };
            Promise.resolve = Promise.fulfilled = Promise.cast;
            Promise.reject = Promise.rejected = function(reason) {
              var ret = new Promise(INTERNAL);
              ret._captureStackTrace();
              ret._rejectCallback(reason, true);
              return ret;
            };
            Promise.setScheduler = function(fn) {
              if (typeof fn !== "function") {
                throw new TypeError("expecting a function but got " + util.classString(fn));
              }
              return async.setScheduler(fn);
            };
            Promise.prototype._then = function(didFulfill, didReject, _, receiver, internalData) {
              var haveInternalData = internalData !== undefined;
              var promise = haveInternalData ? internalData : new Promise(INTERNAL);
              var target = this._target();
              var bitField = target._bitField;
              if (!haveInternalData) {
                promise._propagateFrom(this, 3);
                promise._captureStackTrace();
                if (receiver === undefined && ((this._bitField & 2097152) !== 0)) {
                  if (!((bitField & 50397184) === 0)) {
                    receiver = this._boundValue();
                  } else {
                    receiver = target === this ? undefined : this._boundTo;
                  }
                }
                this._fireEvent("promiseChained", this, promise);
              }
              var domain = getDomain();
              if (!((bitField & 50397184) === 0)) {
                var handler,
                    value,
                    settler = target._settlePromiseCtx;
                if (((bitField & 33554432) !== 0)) {
                  value = target._rejectionHandler0;
                  handler = didFulfill;
                } else if (((bitField & 16777216) !== 0)) {
                  value = target._fulfillmentHandler0;
                  handler = didReject;
                  target._unsetRejectionIsUnhandled();
                } else {
                  settler = target._settlePromiseLateCancellationObserver;
                  value = new CancellationError("late cancellation observer");
                  target._attachExtraTrace(value);
                  handler = didReject;
                }
                async.invoke(settler, target, {
                  handler: domain === null ? handler : (typeof handler === "function" && domain.bind(handler)),
                  promise: promise,
                  receiver: receiver,
                  value: value
                });
              } else {
                target._addCallbacks(didFulfill, didReject, promise, receiver, domain);
              }
              return promise;
            };
            Promise.prototype._length = function() {
              return this._bitField & 65535;
            };
            Promise.prototype._isFateSealed = function() {
              return (this._bitField & 117506048) !== 0;
            };
            Promise.prototype._isFollowing = function() {
              return (this._bitField & 67108864) === 67108864;
            };
            Promise.prototype._setLength = function(len) {
              this._bitField = (this._bitField & -65536) | (len & 65535);
            };
            Promise.prototype._setFulfilled = function() {
              this._bitField = this._bitField | 33554432;
              this._fireEvent("promiseFulfilled", this);
            };
            Promise.prototype._setRejected = function() {
              this._bitField = this._bitField | 16777216;
              this._fireEvent("promiseRejected", this);
            };
            Promise.prototype._setFollowing = function() {
              this._bitField = this._bitField | 67108864;
              this._fireEvent("promiseResolved", this);
            };
            Promise.prototype._setIsFinal = function() {
              this._bitField = this._bitField | 4194304;
            };
            Promise.prototype._isFinal = function() {
              return (this._bitField & 4194304) > 0;
            };
            Promise.prototype._unsetCancelled = function() {
              this._bitField = this._bitField & (~65536);
            };
            Promise.prototype._setCancelled = function() {
              this._bitField = this._bitField | 65536;
              this._fireEvent("promiseCancelled", this);
            };
            Promise.prototype._setAsyncGuaranteed = function() {
              if (async.hasCustomScheduler())
                return;
              this._bitField = this._bitField | 134217728;
            };
            Promise.prototype._receiverAt = function(index) {
              var ret = index === 0 ? this._receiver0 : this[index * 4 - 4 + 3];
              if (ret === UNDEFINED_BINDING) {
                return undefined;
              } else if (ret === undefined && this._isBound()) {
                return this._boundValue();
              }
              return ret;
            };
            Promise.prototype._promiseAt = function(index) {
              return this[index * 4 - 4 + 2];
            };
            Promise.prototype._fulfillmentHandlerAt = function(index) {
              return this[index * 4 - 4 + 0];
            };
            Promise.prototype._rejectionHandlerAt = function(index) {
              return this[index * 4 - 4 + 1];
            };
            Promise.prototype._boundValue = function() {};
            Promise.prototype._migrateCallback0 = function(follower) {
              var bitField = follower._bitField;
              var fulfill = follower._fulfillmentHandler0;
              var reject = follower._rejectionHandler0;
              var promise = follower._promise0;
              var receiver = follower._receiverAt(0);
              if (receiver === undefined)
                receiver = UNDEFINED_BINDING;
              this._addCallbacks(fulfill, reject, promise, receiver, null);
            };
            Promise.prototype._migrateCallbackAt = function(follower, index) {
              var fulfill = follower._fulfillmentHandlerAt(index);
              var reject = follower._rejectionHandlerAt(index);
              var promise = follower._promiseAt(index);
              var receiver = follower._receiverAt(index);
              if (receiver === undefined)
                receiver = UNDEFINED_BINDING;
              this._addCallbacks(fulfill, reject, promise, receiver, null);
            };
            Promise.prototype._addCallbacks = function(fulfill, reject, promise, receiver, domain) {
              var index = this._length();
              if (index >= 65535 - 4) {
                index = 0;
                this._setLength(0);
              }
              if (index === 0) {
                this._promise0 = promise;
                this._receiver0 = receiver;
                if (typeof fulfill === "function") {
                  this._fulfillmentHandler0 = domain === null ? fulfill : domain.bind(fulfill);
                }
                if (typeof reject === "function") {
                  this._rejectionHandler0 = domain === null ? reject : domain.bind(reject);
                }
              } else {
                var base = index * 4 - 4;
                this[base + 2] = promise;
                this[base + 3] = receiver;
                if (typeof fulfill === "function") {
                  this[base + 0] = domain === null ? fulfill : domain.bind(fulfill);
                }
                if (typeof reject === "function") {
                  this[base + 1] = domain === null ? reject : domain.bind(reject);
                }
              }
              this._setLength(index + 1);
              return index;
            };
            Promise.prototype._proxy = function(proxyable, arg) {
              this._addCallbacks(undefined, undefined, arg, proxyable, null);
            };
            Promise.prototype._resolveCallback = function(value, shouldBind) {
              if (((this._bitField & 117506048) !== 0))
                return;
              if (value === this)
                return this._rejectCallback(makeSelfResolutionError(), false);
              var maybePromise = tryConvertToPromise(value, this);
              if (!(maybePromise instanceof Promise))
                return this._fulfill(value);
              if (shouldBind)
                this._propagateFrom(maybePromise, 2);
              var promise = maybePromise._target();
              if (promise === this) {
                this._reject(makeSelfResolutionError());
                return;
              }
              var bitField = promise._bitField;
              if (((bitField & 50397184) === 0)) {
                var len = this._length();
                if (len > 0)
                  promise._migrateCallback0(this);
                for (var i = 1; i < len; ++i) {
                  promise._migrateCallbackAt(this, i);
                }
                this._setFollowing();
                this._setLength(0);
                this._setFollowee(promise);
              } else if (((bitField & 33554432) !== 0)) {
                this._fulfill(promise._value());
              } else if (((bitField & 16777216) !== 0)) {
                this._reject(promise._reason());
              } else {
                var reason = new CancellationError("late cancellation observer");
                promise._attachExtraTrace(reason);
                this._reject(reason);
              }
            };
            Promise.prototype._rejectCallback = function(reason, synchronous, ignoreNonErrorWarnings) {
              var trace = util.ensureErrorObject(reason);
              var hasStack = trace === reason;
              if (!hasStack && !ignoreNonErrorWarnings && debug.warnings()) {
                var message = "a promise was rejected with a non-error: " + util.classString(reason);
                this._warn(message, true);
              }
              this._attachExtraTrace(trace, synchronous ? hasStack : false);
              this._reject(reason);
            };
            Promise.prototype._resolveFromExecutor = function(executor) {
              var promise = this;
              this._captureStackTrace();
              this._pushContext();
              var synchronous = true;
              var r = this._execute(executor, function(value) {
                promise._resolveCallback(value);
              }, function(reason) {
                promise._rejectCallback(reason, synchronous);
              });
              synchronous = false;
              this._popContext();
              if (r !== undefined) {
                promise._rejectCallback(r, true);
              }
            };
            Promise.prototype._settlePromiseFromHandler = function(handler, receiver, value, promise) {
              var bitField = promise._bitField;
              if (((bitField & 65536) !== 0))
                return;
              promise._pushContext();
              var x;
              if (receiver === APPLY) {
                if (!value || typeof value.length !== "number") {
                  x = errorObj;
                  x.e = new TypeError("cannot .spread() a non-array: " + util.classString(value));
                } else {
                  x = tryCatch(handler).apply(this._boundValue(), value);
                }
              } else {
                x = tryCatch(handler).call(receiver, value);
              }
              var promiseCreated = promise._popContext();
              bitField = promise._bitField;
              if (((bitField & 65536) !== 0))
                return;
              if (x === NEXT_FILTER) {
                promise._reject(value);
              } else if (x === errorObj) {
                promise._rejectCallback(x.e, false);
              } else {
                debug.checkForgottenReturns(x, promiseCreated, "", promise, this);
                promise._resolveCallback(x);
              }
            };
            Promise.prototype._target = function() {
              var ret = this;
              while (ret._isFollowing())
                ret = ret._followee();
              return ret;
            };
            Promise.prototype._followee = function() {
              return this._rejectionHandler0;
            };
            Promise.prototype._setFollowee = function(promise) {
              this._rejectionHandler0 = promise;
            };
            Promise.prototype._settlePromise = function(promise, handler, receiver, value) {
              var isPromise = promise instanceof Promise;
              var bitField = this._bitField;
              var asyncGuaranteed = ((bitField & 134217728) !== 0);
              if (((bitField & 65536) !== 0)) {
                if (isPromise)
                  promise._invokeInternalOnCancel();
                if (receiver instanceof PassThroughHandlerContext && receiver.isFinallyHandler()) {
                  receiver.cancelPromise = promise;
                  if (tryCatch(handler).call(receiver, value) === errorObj) {
                    promise._reject(errorObj.e);
                  }
                } else if (handler === reflectHandler) {
                  promise._fulfill(reflectHandler.call(receiver));
                } else if (receiver instanceof Proxyable) {
                  receiver._promiseCancelled(promise);
                } else if (isPromise || promise instanceof PromiseArray) {
                  promise._cancel();
                } else {
                  receiver.cancel();
                }
              } else if (typeof handler === "function") {
                if (!isPromise) {
                  handler.call(receiver, value, promise);
                } else {
                  if (asyncGuaranteed)
                    promise._setAsyncGuaranteed();
                  this._settlePromiseFromHandler(handler, receiver, value, promise);
                }
              } else if (receiver instanceof Proxyable) {
                if (!receiver._isResolved()) {
                  if (((bitField & 33554432) !== 0)) {
                    receiver._promiseFulfilled(value, promise);
                  } else {
                    receiver._promiseRejected(value, promise);
                  }
                }
              } else if (isPromise) {
                if (asyncGuaranteed)
                  promise._setAsyncGuaranteed();
                if (((bitField & 33554432) !== 0)) {
                  promise._fulfill(value);
                } else {
                  promise._reject(value);
                }
              }
            };
            Promise.prototype._settlePromiseLateCancellationObserver = function(ctx) {
              var handler = ctx.handler;
              var promise = ctx.promise;
              var receiver = ctx.receiver;
              var value = ctx.value;
              if (typeof handler === "function") {
                if (!(promise instanceof Promise)) {
                  handler.call(receiver, value, promise);
                } else {
                  this._settlePromiseFromHandler(handler, receiver, value, promise);
                }
              } else if (promise instanceof Promise) {
                promise._reject(value);
              }
            };
            Promise.prototype._settlePromiseCtx = function(ctx) {
              this._settlePromise(ctx.promise, ctx.handler, ctx.receiver, ctx.value);
            };
            Promise.prototype._settlePromise0 = function(handler, value, bitField) {
              var promise = this._promise0;
              var receiver = this._receiverAt(0);
              this._promise0 = undefined;
              this._receiver0 = undefined;
              this._settlePromise(promise, handler, receiver, value);
            };
            Promise.prototype._clearCallbackDataAtIndex = function(index) {
              var base = index * 4 - 4;
              this[base + 2] = this[base + 3] = this[base + 0] = this[base + 1] = undefined;
            };
            Promise.prototype._fulfill = function(value) {
              var bitField = this._bitField;
              if (((bitField & 117506048) >>> 16))
                return;
              if (value === this) {
                var err = makeSelfResolutionError();
                this._attachExtraTrace(err);
                return this._reject(err);
              }
              this._setFulfilled();
              this._rejectionHandler0 = value;
              if ((bitField & 65535) > 0) {
                if (((bitField & 134217728) !== 0)) {
                  this._settlePromises();
                } else {
                  async.settlePromises(this);
                }
              }
            };
            Promise.prototype._reject = function(reason) {
              var bitField = this._bitField;
              if (((bitField & 117506048) >>> 16))
                return;
              this._setRejected();
              this._fulfillmentHandler0 = reason;
              if (this._isFinal()) {
                return async.fatalError(reason, util.isNode);
              }
              if ((bitField & 65535) > 0) {
                async.settlePromises(this);
              } else {
                this._ensurePossibleRejectionHandled();
              }
            };
            Promise.prototype._fulfillPromises = function(len, value) {
              for (var i = 1; i < len; i++) {
                var handler = this._fulfillmentHandlerAt(i);
                var promise = this._promiseAt(i);
                var receiver = this._receiverAt(i);
                this._clearCallbackDataAtIndex(i);
                this._settlePromise(promise, handler, receiver, value);
              }
            };
            Promise.prototype._rejectPromises = function(len, reason) {
              for (var i = 1; i < len; i++) {
                var handler = this._rejectionHandlerAt(i);
                var promise = this._promiseAt(i);
                var receiver = this._receiverAt(i);
                this._clearCallbackDataAtIndex(i);
                this._settlePromise(promise, handler, receiver, reason);
              }
            };
            Promise.prototype._settlePromises = function() {
              var bitField = this._bitField;
              var len = (bitField & 65535);
              if (len > 0) {
                if (((bitField & 16842752) !== 0)) {
                  var reason = this._fulfillmentHandler0;
                  this._settlePromise0(this._rejectionHandler0, reason, bitField);
                  this._rejectPromises(len, reason);
                } else {
                  var value = this._rejectionHandler0;
                  this._settlePromise0(this._fulfillmentHandler0, value, bitField);
                  this._fulfillPromises(len, value);
                }
                this._setLength(0);
              }
              this._clearCancellationData();
            };
            Promise.prototype._settledValue = function() {
              var bitField = this._bitField;
              if (((bitField & 33554432) !== 0)) {
                return this._rejectionHandler0;
              } else if (((bitField & 16777216) !== 0)) {
                return this._fulfillmentHandler0;
              }
            };
            function deferResolve(v) {
              this.promise._resolveCallback(v);
            }
            function deferReject(v) {
              this.promise._rejectCallback(v, false);
            }
            Promise.defer = Promise.pending = function() {
              debug.deprecated("Promise.defer", "new Promise");
              var promise = new Promise(INTERNAL);
              return {
                promise: promise,
                resolve: deferResolve,
                reject: deferReject
              };
            };
            util.notEnumerableProp(Promise, "_makeSelfResolutionError", makeSelfResolutionError);
            _dereq_("./method")(Promise, INTERNAL, tryConvertToPromise, apiRejection, debug);
            _dereq_("./bind")(Promise, INTERNAL, tryConvertToPromise, debug);
            _dereq_("./cancel")(Promise, PromiseArray, apiRejection, debug);
            _dereq_("./direct_resolve")(Promise);
            _dereq_("./synchronous_inspection")(Promise);
            _dereq_("./join")(Promise, PromiseArray, tryConvertToPromise, INTERNAL, debug);
            Promise.Promise = Promise;
            _dereq_('./map.js')(Promise, PromiseArray, apiRejection, tryConvertToPromise, INTERNAL, debug);
            _dereq_('./call_get.js')(Promise);
            _dereq_('./using.js')(Promise, apiRejection, tryConvertToPromise, createContext, INTERNAL, debug);
            _dereq_('./timers.js')(Promise, INTERNAL, debug);
            _dereq_('./generators.js')(Promise, apiRejection, INTERNAL, tryConvertToPromise, Proxyable, debug);
            _dereq_('./nodeify.js')(Promise);
            _dereq_('./promisify.js')(Promise, INTERNAL);
            _dereq_('./props.js')(Promise, PromiseArray, tryConvertToPromise, apiRejection);
            _dereq_('./race.js')(Promise, INTERNAL, tryConvertToPromise, apiRejection);
            _dereq_('./reduce.js')(Promise, PromiseArray, apiRejection, tryConvertToPromise, INTERNAL, debug);
            _dereq_('./settle.js')(Promise, PromiseArray, debug);
            _dereq_('./some.js')(Promise, PromiseArray, apiRejection);
            _dereq_('./filter.js')(Promise, INTERNAL);
            _dereq_('./each.js')(Promise, INTERNAL);
            _dereq_('./any.js')(Promise);
            util.toFastProperties(Promise);
            util.toFastProperties(Promise.prototype);
            function fillTypes(value) {
              var p = new Promise(INTERNAL);
              p._fulfillmentHandler0 = value;
              p._rejectionHandler0 = value;
              p._promise0 = value;
              p._receiver0 = value;
            }
            fillTypes({a: 1});
            fillTypes({b: 2});
            fillTypes({c: 3});
            fillTypes(1);
            fillTypes(function() {});
            fillTypes(undefined);
            fillTypes(false);
            fillTypes(new Promise(INTERNAL));
            debug.setBounds(Async.firstLineError, util.lastLineError);
            return Promise;
          };
        }, {
          "./any.js": 1,
          "./async": 2,
          "./bind": 3,
          "./call_get.js": 5,
          "./cancel": 6,
          "./catch_filter": 7,
          "./context": 8,
          "./debuggability": 9,
          "./direct_resolve": 10,
          "./each.js": 11,
          "./errors": 12,
          "./es5": 13,
          "./filter.js": 14,
          "./finally": 15,
          "./generators.js": 16,
          "./join": 17,
          "./map.js": 18,
          "./method": 19,
          "./nodeback": 20,
          "./nodeify.js": 21,
          "./promise_array": 23,
          "./promisify.js": 24,
          "./props.js": 25,
          "./race.js": 27,
          "./reduce.js": 28,
          "./settle.js": 30,
          "./some.js": 31,
          "./synchronous_inspection": 32,
          "./thenables": 33,
          "./timers.js": 34,
          "./using.js": 35,
          "./util": 36
        }],
        23: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL, tryConvertToPromise, apiRejection, Proxyable) {
            var util = _dereq_("./util");
            var isArray = util.isArray;
            function toResolutionValue(val) {
              switch (val) {
                case -2:
                  return [];
                case -3:
                  return {};
              }
            }
            function PromiseArray(values) {
              var promise = this._promise = new Promise(INTERNAL);
              if (values instanceof Promise) {
                promise._propagateFrom(values, 3);
              }
              promise._setOnCancel(this);
              this._values = values;
              this._length = 0;
              this._totalResolved = 0;
              this._init(undefined, -2);
            }
            util.inherits(PromiseArray, Proxyable);
            PromiseArray.prototype.length = function() {
              return this._length;
            };
            PromiseArray.prototype.promise = function() {
              return this._promise;
            };
            PromiseArray.prototype._init = function init(_, resolveValueIfEmpty) {
              var values = tryConvertToPromise(this._values, this._promise);
              if (values instanceof Promise) {
                values = values._target();
                var bitField = values._bitField;
                ;
                this._values = values;
                if (((bitField & 50397184) === 0)) {
                  this._promise._setAsyncGuaranteed();
                  return values._then(init, this._reject, undefined, this, resolveValueIfEmpty);
                } else if (((bitField & 33554432) !== 0)) {
                  values = values._value();
                } else if (((bitField & 16777216) !== 0)) {
                  return this._reject(values._reason());
                } else {
                  return this._cancel();
                }
              }
              values = util.asArray(values);
              if (values === null) {
                var err = apiRejection("expecting an array or an iterable object but got " + util.classString(values)).reason();
                this._promise._rejectCallback(err, false);
                return;
              }
              if (values.length === 0) {
                if (resolveValueIfEmpty === -5) {
                  this._resolveEmptyArray();
                } else {
                  this._resolve(toResolutionValue(resolveValueIfEmpty));
                }
                return;
              }
              this._iterate(values);
            };
            PromiseArray.prototype._iterate = function(values) {
              var len = this.getActualLength(values.length);
              this._length = len;
              this._values = this.shouldCopyValues() ? new Array(len) : this._values;
              var result = this._promise;
              var isResolved = false;
              var bitField = null;
              for (var i = 0; i < len; ++i) {
                var maybePromise = tryConvertToPromise(values[i], result);
                if (maybePromise instanceof Promise) {
                  maybePromise = maybePromise._target();
                  bitField = maybePromise._bitField;
                } else {
                  bitField = null;
                }
                if (isResolved) {
                  if (bitField !== null) {
                    maybePromise.suppressUnhandledRejections();
                  }
                } else if (bitField !== null) {
                  if (((bitField & 50397184) === 0)) {
                    maybePromise._proxy(this, i);
                    this._values[i] = maybePromise;
                  } else if (((bitField & 33554432) !== 0)) {
                    isResolved = this._promiseFulfilled(maybePromise._value(), i);
                  } else if (((bitField & 16777216) !== 0)) {
                    isResolved = this._promiseRejected(maybePromise._reason(), i);
                  } else {
                    isResolved = this._promiseCancelled(i);
                  }
                } else {
                  isResolved = this._promiseFulfilled(maybePromise, i);
                }
              }
              if (!isResolved)
                result._setAsyncGuaranteed();
            };
            PromiseArray.prototype._isResolved = function() {
              return this._values === null;
            };
            PromiseArray.prototype._resolve = function(value) {
              this._values = null;
              this._promise._fulfill(value);
            };
            PromiseArray.prototype._cancel = function() {
              if (this._isResolved() || !this._promise.isCancellable())
                return;
              this._values = null;
              this._promise._cancel();
            };
            PromiseArray.prototype._reject = function(reason) {
              this._values = null;
              this._promise._rejectCallback(reason, false);
            };
            PromiseArray.prototype._promiseFulfilled = function(value, index) {
              this._values[index] = value;
              var totalResolved = ++this._totalResolved;
              if (totalResolved >= this._length) {
                this._resolve(this._values);
                return true;
              }
              return false;
            };
            PromiseArray.prototype._promiseCancelled = function() {
              this._cancel();
              return true;
            };
            PromiseArray.prototype._promiseRejected = function(reason) {
              this._totalResolved++;
              this._reject(reason);
              return true;
            };
            PromiseArray.prototype._resultCancelled = function() {
              if (this._isResolved())
                return;
              var values = this._values;
              this._cancel();
              if (values instanceof Promise) {
                values.cancel();
              } else {
                for (var i = 0; i < values.length; ++i) {
                  if (values[i] instanceof Promise) {
                    values[i].cancel();
                  }
                }
              }
            };
            PromiseArray.prototype.shouldCopyValues = function() {
              return true;
            };
            PromiseArray.prototype.getActualLength = function(len) {
              return len;
            };
            return PromiseArray;
          };
        }, {"./util": 36}],
        24: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL) {
            var THIS = {};
            var util = _dereq_("./util");
            var nodebackForPromise = _dereq_("./nodeback");
            var withAppended = util.withAppended;
            var maybeWrapAsError = util.maybeWrapAsError;
            var canEvaluate = util.canEvaluate;
            var TypeError = _dereq_("./errors").TypeError;
            var defaultSuffix = "Async";
            var defaultPromisified = {__isPromisified__: true};
            var noCopyProps = ["arity", "length", "name", "arguments", "caller", "callee", "prototype", "__isPromisified__"];
            var noCopyPropsPattern = new RegExp("^(?:" + noCopyProps.join("|") + ")$");
            var defaultFilter = function(name) {
              return util.isIdentifier(name) && name.charAt(0) !== "_" && name !== "constructor";
            };
            function propsFilter(key) {
              return !noCopyPropsPattern.test(key);
            }
            function isPromisified(fn) {
              try {
                return fn.__isPromisified__ === true;
              } catch (e) {
                return false;
              }
            }
            function hasPromisified(obj, key, suffix) {
              var val = util.getDataPropertyOrDefault(obj, key + suffix, defaultPromisified);
              return val ? isPromisified(val) : false;
            }
            function checkValid(ret, suffix, suffixRegexp) {
              for (var i = 0; i < ret.length; i += 2) {
                var key = ret[i];
                if (suffixRegexp.test(key)) {
                  var keyWithoutAsyncSuffix = key.replace(suffixRegexp, "");
                  for (var j = 0; j < ret.length; j += 2) {
                    if (ret[j] === keyWithoutAsyncSuffix) {
                      throw new TypeError("Cannot promisify an API that has normal methods with '%s'-suffix\u000a\u000a    See http://goo.gl/MqrFmX\u000a".replace("%s", suffix));
                    }
                  }
                }
              }
            }
            function promisifiableMethods(obj, suffix, suffixRegexp, filter) {
              var keys = util.inheritedDataKeys(obj);
              var ret = [];
              for (var i = 0; i < keys.length; ++i) {
                var key = keys[i];
                var value = obj[key];
                var passesDefaultFilter = filter === defaultFilter ? true : defaultFilter(key, value, obj);
                if (typeof value === "function" && !isPromisified(value) && !hasPromisified(obj, key, suffix) && filter(key, value, obj, passesDefaultFilter)) {
                  ret.push(key, value);
                }
              }
              checkValid(ret, suffix, suffixRegexp);
              return ret;
            }
            var escapeIdentRegex = function(str) {
              return str.replace(/([$])/, "\\$");
            };
            var makeNodePromisifiedEval;
            if (!true) {
              var switchCaseArgumentOrder = function(likelyArgumentCount) {
                var ret = [likelyArgumentCount];
                var min = Math.max(0, likelyArgumentCount - 1 - 3);
                for (var i = likelyArgumentCount - 1; i >= min; --i) {
                  ret.push(i);
                }
                for (var i = likelyArgumentCount + 1; i <= 3; ++i) {
                  ret.push(i);
                }
                return ret;
              };
              var argumentSequence = function(argumentCount) {
                return util.filledRange(argumentCount, "_arg", "");
              };
              var parameterDeclaration = function(parameterCount) {
                return util.filledRange(Math.max(parameterCount, 3), "_arg", "");
              };
              var parameterCount = function(fn) {
                if (typeof fn.length === "number") {
                  return Math.max(Math.min(fn.length, 1023 + 1), 0);
                }
                return 0;
              };
              makeNodePromisifiedEval = function(callback, receiver, originalName, fn, _, multiArgs) {
                var newParameterCount = Math.max(0, parameterCount(fn) - 1);
                var argumentOrder = switchCaseArgumentOrder(newParameterCount);
                var shouldProxyThis = typeof callback === "string" || receiver === THIS;
                function generateCallForArgumentCount(count) {
                  var args = argumentSequence(count).join(", ");
                  var comma = count > 0 ? ", " : "";
                  var ret;
                  if (shouldProxyThis) {
                    ret = "ret = callback.call(this, {{args}}, nodeback); break;\n";
                  } else {
                    ret = receiver === undefined ? "ret = callback({{args}}, nodeback); break;\n" : "ret = callback.call(receiver, {{args}}, nodeback); break;\n";
                  }
                  return ret.replace("{{args}}", args).replace(", ", comma);
                }
                function generateArgumentSwitchCase() {
                  var ret = "";
                  for (var i = 0; i < argumentOrder.length; ++i) {
                    ret += "case " + argumentOrder[i] + ":" + generateCallForArgumentCount(argumentOrder[i]);
                  }
                  ret += "                                                             \n\
        default:                                                             \n\
            var args = new Array(len + 1);                                   \n\
            var i = 0;                                                       \n\
            for (var i = 0; i < len; ++i) {                                  \n\
               args[i] = arguments[i];                                       \n\
            }                                                                \n\
            args[i] = nodeback;                                              \n\
            [CodeForCall]                                                    \n\
            break;                                                           \n\
        ".replace("[CodeForCall]", (shouldProxyThis ? "ret = callback.apply(this, args);\n" : "ret = callback.apply(receiver, args);\n"));
                  return ret;
                }
                var getFunctionCode = typeof callback === "string" ? ("this != null ? this['" + callback + "'] : fn") : "fn";
                var body = "'use strict';                                                \n\
        var ret = function (Parameters) {                                    \n\
            'use strict';                                                    \n\
            var len = arguments.length;                                      \n\
            var promise = new Promise(INTERNAL);                             \n\
            promise._captureStackTrace();                                    \n\
            var nodeback = nodebackForPromise(promise, " + multiArgs + ");   \n\
            var ret;                                                         \n\
            var callback = tryCatch([GetFunctionCode]);                      \n\
            switch(len) {                                                    \n\
                [CodeForSwitchCase]                                          \n\
            }                                                                \n\
            if (ret === errorObj) {                                          \n\
                promise._rejectCallback(maybeWrapAsError(ret.e), true, true);\n\
            }                                                                \n\
            if (!promise._isFateSealed()) promise._setAsyncGuaranteed();     \n\
            return promise;                                                  \n\
        };                                                                   \n\
        notEnumerableProp(ret, '__isPromisified__', true);                   \n\
        return ret;                                                          \n\
    ".replace("[CodeForSwitchCase]", generateArgumentSwitchCase()).replace("[GetFunctionCode]", getFunctionCode);
                body = body.replace("Parameters", parameterDeclaration(newParameterCount));
                return new Function("Promise", "fn", "receiver", "withAppended", "maybeWrapAsError", "nodebackForPromise", "tryCatch", "errorObj", "notEnumerableProp", "INTERNAL", body)(Promise, fn, receiver, withAppended, maybeWrapAsError, nodebackForPromise, util.tryCatch, util.errorObj, util.notEnumerableProp, INTERNAL);
              };
            }
            function makeNodePromisifiedClosure(callback, receiver, _, fn, __, multiArgs) {
              var defaultThis = (function() {
                return this;
              })();
              var method = callback;
              if (typeof method === "string") {
                callback = fn;
              }
              function promisified() {
                var _receiver = receiver;
                if (receiver === THIS)
                  _receiver = this;
                var promise = new Promise(INTERNAL);
                promise._captureStackTrace();
                var cb = typeof method === "string" && this !== defaultThis ? this[method] : callback;
                var fn = nodebackForPromise(promise, multiArgs);
                try {
                  cb.apply(_receiver, withAppended(arguments, fn));
                } catch (e) {
                  promise._rejectCallback(maybeWrapAsError(e), true, true);
                }
                if (!promise._isFateSealed())
                  promise._setAsyncGuaranteed();
                return promise;
              }
              util.notEnumerableProp(promisified, "__isPromisified__", true);
              return promisified;
            }
            var makeNodePromisified = canEvaluate ? makeNodePromisifiedEval : makeNodePromisifiedClosure;
            function promisifyAll(obj, suffix, filter, promisifier, multiArgs) {
              var suffixRegexp = new RegExp(escapeIdentRegex(suffix) + "$");
              var methods = promisifiableMethods(obj, suffix, suffixRegexp, filter);
              for (var i = 0,
                  len = methods.length; i < len; i += 2) {
                var key = methods[i];
                var fn = methods[i + 1];
                var promisifiedKey = key + suffix;
                if (promisifier === makeNodePromisified) {
                  obj[promisifiedKey] = makeNodePromisified(key, THIS, key, fn, suffix, multiArgs);
                } else {
                  var promisified = promisifier(fn, function() {
                    return makeNodePromisified(key, THIS, key, fn, suffix, multiArgs);
                  });
                  util.notEnumerableProp(promisified, "__isPromisified__", true);
                  obj[promisifiedKey] = promisified;
                }
              }
              util.toFastProperties(obj);
              return obj;
            }
            function promisify(callback, receiver, multiArgs) {
              return makeNodePromisified(callback, receiver, undefined, callback, null, multiArgs);
            }
            Promise.promisify = function(fn, options) {
              if (typeof fn !== "function") {
                throw new TypeError("expecting a function but got " + util.classString(fn));
              }
              if (isPromisified(fn)) {
                return fn;
              }
              options = Object(options);
              var receiver = options.context === undefined ? THIS : options.context;
              var multiArgs = !!options.multiArgs;
              var ret = promisify(fn, receiver, multiArgs);
              util.copyDescriptors(fn, ret, propsFilter);
              return ret;
            };
            Promise.promisifyAll = function(target, options) {
              if (typeof target !== "function" && typeof target !== "object") {
                throw new TypeError("the target of promisifyAll must be an object or a function\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              options = Object(options);
              var multiArgs = !!options.multiArgs;
              var suffix = options.suffix;
              if (typeof suffix !== "string")
                suffix = defaultSuffix;
              var filter = options.filter;
              if (typeof filter !== "function")
                filter = defaultFilter;
              var promisifier = options.promisifier;
              if (typeof promisifier !== "function")
                promisifier = makeNodePromisified;
              if (!util.isIdentifier(suffix)) {
                throw new RangeError("suffix must be a valid identifier\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              var keys = util.inheritedDataKeys(target);
              for (var i = 0; i < keys.length; ++i) {
                var value = target[keys[i]];
                if (keys[i] !== "constructor" && util.isClass(value)) {
                  promisifyAll(value.prototype, suffix, filter, promisifier, multiArgs);
                  promisifyAll(value, suffix, filter, promisifier, multiArgs);
                }
              }
              return promisifyAll(target, suffix, filter, promisifier, multiArgs);
            };
          };
        }, {
          "./errors": 12,
          "./nodeback": 20,
          "./util": 36
        }],
        25: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, tryConvertToPromise, apiRejection) {
            var util = _dereq_("./util");
            var isObject = util.isObject;
            var es5 = _dereq_("./es5");
            var Es6Map;
            if (typeof Map === "function")
              Es6Map = Map;
            var mapToEntries = (function() {
              var index = 0;
              var size = 0;
              function extractEntry(value, key) {
                this[index] = value;
                this[index + size] = key;
                index++;
              }
              return function mapToEntries(map) {
                size = map.size;
                index = 0;
                var ret = new Array(map.size * 2);
                map.forEach(extractEntry, ret);
                return ret;
              };
            })();
            var entriesToMap = function(entries) {
              var ret = new Es6Map();
              var length = entries.length / 2 | 0;
              for (var i = 0; i < length; ++i) {
                var key = entries[length + i];
                var value = entries[i];
                ret.set(key, value);
              }
              return ret;
            };
            function PropertiesPromiseArray(obj) {
              var isMap = false;
              var entries;
              if (Es6Map !== undefined && obj instanceof Es6Map) {
                entries = mapToEntries(obj);
                isMap = true;
              } else {
                var keys = es5.keys(obj);
                var len = keys.length;
                entries = new Array(len * 2);
                for (var i = 0; i < len; ++i) {
                  var key = keys[i];
                  entries[i] = obj[key];
                  entries[i + len] = key;
                }
              }
              this.constructor$(entries);
              this._isMap = isMap;
              this._init$(undefined, -3);
            }
            util.inherits(PropertiesPromiseArray, PromiseArray);
            PropertiesPromiseArray.prototype._init = function() {};
            PropertiesPromiseArray.prototype._promiseFulfilled = function(value, index) {
              this._values[index] = value;
              var totalResolved = ++this._totalResolved;
              if (totalResolved >= this._length) {
                var val;
                if (this._isMap) {
                  val = entriesToMap(this._values);
                } else {
                  val = {};
                  var keyOffset = this.length();
                  for (var i = 0,
                      len = this.length(); i < len; ++i) {
                    val[this._values[i + keyOffset]] = this._values[i];
                  }
                }
                this._resolve(val);
                return true;
              }
              return false;
            };
            PropertiesPromiseArray.prototype.shouldCopyValues = function() {
              return false;
            };
            PropertiesPromiseArray.prototype.getActualLength = function(len) {
              return len >> 1;
            };
            function props(promises) {
              var ret;
              var castValue = tryConvertToPromise(promises);
              if (!isObject(castValue)) {
                return apiRejection("cannot await properties of a non-object\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              } else if (castValue instanceof Promise) {
                ret = castValue._then(Promise.props, undefined, undefined, undefined, undefined);
              } else {
                ret = new PropertiesPromiseArray(castValue).promise();
              }
              if (castValue instanceof Promise) {
                ret._propagateFrom(castValue, 2);
              }
              return ret;
            }
            Promise.prototype.props = function() {
              return props(this);
            };
            Promise.props = function(promises) {
              return props(promises);
            };
          };
        }, {
          "./es5": 13,
          "./util": 36
        }],
        26: [function(_dereq_, module, exports) {
          "use strict";
          function arrayMove(src, srcIndex, dst, dstIndex, len) {
            for (var j = 0; j < len; ++j) {
              dst[j + dstIndex] = src[j + srcIndex];
              src[j + srcIndex] = void 0;
            }
          }
          function Queue(capacity) {
            this._capacity = capacity;
            this._length = 0;
            this._front = 0;
          }
          Queue.prototype._willBeOverCapacity = function(size) {
            return this._capacity < size;
          };
          Queue.prototype._pushOne = function(arg) {
            var length = this.length();
            this._checkCapacity(length + 1);
            var i = (this._front + length) & (this._capacity - 1);
            this[i] = arg;
            this._length = length + 1;
          };
          Queue.prototype._unshiftOne = function(value) {
            var capacity = this._capacity;
            this._checkCapacity(this.length() + 1);
            var front = this._front;
            var i = ((((front - 1) & (capacity - 1)) ^ capacity) - capacity);
            this[i] = value;
            this._front = i;
            this._length = this.length() + 1;
          };
          Queue.prototype.unshift = function(fn, receiver, arg) {
            this._unshiftOne(arg);
            this._unshiftOne(receiver);
            this._unshiftOne(fn);
          };
          Queue.prototype.push = function(fn, receiver, arg) {
            var length = this.length() + 3;
            if (this._willBeOverCapacity(length)) {
              this._pushOne(fn);
              this._pushOne(receiver);
              this._pushOne(arg);
              return;
            }
            var j = this._front + length - 3;
            this._checkCapacity(length);
            var wrapMask = this._capacity - 1;
            this[(j + 0) & wrapMask] = fn;
            this[(j + 1) & wrapMask] = receiver;
            this[(j + 2) & wrapMask] = arg;
            this._length = length;
          };
          Queue.prototype.shift = function() {
            var front = this._front,
                ret = this[front];
            this[front] = undefined;
            this._front = (front + 1) & (this._capacity - 1);
            this._length--;
            return ret;
          };
          Queue.prototype.length = function() {
            return this._length;
          };
          Queue.prototype._checkCapacity = function(size) {
            if (this._capacity < size) {
              this._resizeTo(this._capacity << 1);
            }
          };
          Queue.prototype._resizeTo = function(capacity) {
            var oldCapacity = this._capacity;
            this._capacity = capacity;
            var front = this._front;
            var length = this._length;
            var moveItemsCount = (front + length) & (oldCapacity - 1);
            arrayMove(this, 0, this, oldCapacity, moveItemsCount);
          };
          module.exports = Queue;
        }, {}],
        27: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL, tryConvertToPromise, apiRejection) {
            var util = _dereq_("./util");
            var raceLater = function(promise) {
              return promise.then(function(array) {
                return race(array, promise);
              });
            };
            function race(promises, parent) {
              var maybePromise = tryConvertToPromise(promises);
              if (maybePromise instanceof Promise) {
                return raceLater(maybePromise);
              } else {
                promises = util.asArray(promises);
                if (promises === null)
                  return apiRejection("expecting an array or an iterable object but got " + util.classString(promises));
              }
              var ret = new Promise(INTERNAL);
              if (parent !== undefined) {
                ret._propagateFrom(parent, 3);
              }
              var fulfill = ret._fulfill;
              var reject = ret._reject;
              for (var i = 0,
                  len = promises.length; i < len; ++i) {
                var val = promises[i];
                if (val === undefined && !(i in promises)) {
                  continue;
                }
                Promise.cast(val)._then(fulfill, reject, undefined, ret, null);
              }
              return ret;
            }
            Promise.race = function(promises) {
              return race(promises, undefined);
            };
            Promise.prototype.race = function() {
              return race(this, undefined);
            };
          };
        }, {"./util": 36}],
        28: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, apiRejection, tryConvertToPromise, INTERNAL, debug) {
            var getDomain = Promise._getDomain;
            var util = _dereq_("./util");
            var tryCatch = util.tryCatch;
            function ReductionPromiseArray(promises, fn, initialValue, _each) {
              this.constructor$(promises);
              var domain = getDomain();
              this._fn = domain === null ? fn : domain.bind(fn);
              if (initialValue !== undefined) {
                initialValue = Promise.resolve(initialValue);
                initialValue._attachCancellationCallback(this);
              }
              this._initialValue = initialValue;
              this._currentCancellable = null;
              this._eachValues = _each === INTERNAL ? [] : undefined;
              this._promise._captureStackTrace();
              this._init$(undefined, -5);
            }
            util.inherits(ReductionPromiseArray, PromiseArray);
            ReductionPromiseArray.prototype._gotAccum = function(accum) {
              if (this._eachValues !== undefined && accum !== INTERNAL) {
                this._eachValues.push(accum);
              }
            };
            ReductionPromiseArray.prototype._eachComplete = function(value) {
              this._eachValues.push(value);
              return this._eachValues;
            };
            ReductionPromiseArray.prototype._init = function() {};
            ReductionPromiseArray.prototype._resolveEmptyArray = function() {
              this._resolve(this._eachValues !== undefined ? this._eachValues : this._initialValue);
            };
            ReductionPromiseArray.prototype.shouldCopyValues = function() {
              return false;
            };
            ReductionPromiseArray.prototype._resolve = function(value) {
              this._promise._resolveCallback(value);
              this._values = null;
            };
            ReductionPromiseArray.prototype._resultCancelled = function(sender) {
              if (sender === this._initialValue)
                return this._cancel();
              if (this._isResolved())
                return;
              this._resultCancelled$();
              if (this._currentCancellable instanceof Promise) {
                this._currentCancellable.cancel();
              }
              if (this._initialValue instanceof Promise) {
                this._initialValue.cancel();
              }
            };
            ReductionPromiseArray.prototype._iterate = function(values) {
              this._values = values;
              var value;
              var i;
              var length = values.length;
              if (this._initialValue !== undefined) {
                value = this._initialValue;
                i = 0;
              } else {
                value = Promise.resolve(values[0]);
                i = 1;
              }
              this._currentCancellable = value;
              if (!value.isRejected()) {
                for (; i < length; ++i) {
                  var ctx = {
                    accum: null,
                    value: values[i],
                    index: i,
                    length: length,
                    array: this
                  };
                  value = value._then(gotAccum, undefined, undefined, ctx, undefined);
                }
              }
              if (this._eachValues !== undefined) {
                value = value._then(this._eachComplete, undefined, undefined, this, undefined);
              }
              value._then(completed, completed, undefined, value, this);
            };
            Promise.prototype.reduce = function(fn, initialValue) {
              return reduce(this, fn, initialValue, null);
            };
            Promise.reduce = function(promises, fn, initialValue, _each) {
              return reduce(promises, fn, initialValue, _each);
            };
            function completed(valueOrReason, array) {
              if (this.isFulfilled()) {
                array._resolve(valueOrReason);
              } else {
                array._reject(valueOrReason);
              }
            }
            function reduce(promises, fn, initialValue, _each) {
              if (typeof fn !== "function") {
                return apiRejection("expecting a function but got " + util.classString(fn));
              }
              var array = new ReductionPromiseArray(promises, fn, initialValue, _each);
              return array.promise();
            }
            function gotAccum(accum) {
              this.accum = accum;
              this.array._gotAccum(accum);
              var value = tryConvertToPromise(this.value, this.array._promise);
              if (value instanceof Promise) {
                this.array._currentCancellable = value;
                return value._then(gotValue, undefined, undefined, this, undefined);
              } else {
                return gotValue.call(this, value);
              }
            }
            function gotValue(value) {
              var array = this.array;
              var promise = array._promise;
              var fn = tryCatch(array._fn);
              promise._pushContext();
              var ret;
              if (array._eachValues !== undefined) {
                ret = fn.call(promise._boundValue(), value, this.index, this.length);
              } else {
                ret = fn.call(promise._boundValue(), this.accum, value, this.index, this.length);
              }
              if (ret instanceof Promise) {
                array._currentCancellable = ret;
              }
              var promiseCreated = promise._popContext();
              debug.checkForgottenReturns(ret, promiseCreated, array._eachValues !== undefined ? "Promise.each" : "Promise.reduce", promise);
              return ret;
            }
          };
        }, {"./util": 36}],
        29: [function(_dereq_, module, exports) {
          "use strict";
          var util = _dereq_("./util");
          var schedule;
          var noAsyncScheduler = function() {
            throw new Error("No async scheduler available\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
          };
          var NativePromise = util.getNativePromise();
          if (util.isNode && typeof MutationObserver === "undefined") {
            var GlobalSetImmediate = global.setImmediate;
            var ProcessNextTick = process.nextTick;
            schedule = util.isRecentNode ? function(fn) {
              GlobalSetImmediate.call(global, fn);
            } : function(fn) {
              ProcessNextTick.call(process, fn);
            };
          } else if (typeof NativePromise === "function") {
            var nativePromise = NativePromise.resolve();
            schedule = function(fn) {
              nativePromise.then(fn);
            };
          } else if ((typeof MutationObserver !== "undefined") && !(typeof window !== "undefined" && window.navigator && window.navigator.standalone)) {
            schedule = (function() {
              var div = document.createElement("div");
              var opts = {attributes: true};
              var toggleScheduled = false;
              var div2 = document.createElement("div");
              var o2 = new MutationObserver(function() {
                div.classList.toggle("foo");
                toggleScheduled = false;
              });
              o2.observe(div2, opts);
              var scheduleToggle = function() {
                if (toggleScheduled)
                  return;
                toggleScheduled = true;
                div2.classList.toggle("foo");
              };
              return function schedule(fn) {
                var o = new MutationObserver(function() {
                  o.disconnect();
                  fn();
                });
                o.observe(div, opts);
                scheduleToggle();
              };
            })();
          } else if (typeof setImmediate !== "undefined") {
            schedule = function(fn) {
              setImmediate(fn);
            };
          } else if (typeof setTimeout !== "undefined") {
            schedule = function(fn) {
              setTimeout(fn, 0);
            };
          } else {
            schedule = noAsyncScheduler;
          }
          module.exports = schedule;
        }, {"./util": 36}],
        30: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, debug) {
            var PromiseInspection = Promise.PromiseInspection;
            var util = _dereq_("./util");
            function SettledPromiseArray(values) {
              this.constructor$(values);
            }
            util.inherits(SettledPromiseArray, PromiseArray);
            SettledPromiseArray.prototype._promiseResolved = function(index, inspection) {
              this._values[index] = inspection;
              var totalResolved = ++this._totalResolved;
              if (totalResolved >= this._length) {
                this._resolve(this._values);
                return true;
              }
              return false;
            };
            SettledPromiseArray.prototype._promiseFulfilled = function(value, index) {
              var ret = new PromiseInspection();
              ret._bitField = 33554432;
              ret._settledValueField = value;
              return this._promiseResolved(index, ret);
            };
            SettledPromiseArray.prototype._promiseRejected = function(reason, index) {
              var ret = new PromiseInspection();
              ret._bitField = 16777216;
              ret._settledValueField = reason;
              return this._promiseResolved(index, ret);
            };
            Promise.settle = function(promises) {
              debug.deprecated(".settle()", ".reflect()");
              return new SettledPromiseArray(promises).promise();
            };
            Promise.prototype.settle = function() {
              return Promise.settle(this);
            };
          };
        }, {"./util": 36}],
        31: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, PromiseArray, apiRejection) {
            var util = _dereq_("./util");
            var RangeError = _dereq_("./errors").RangeError;
            var AggregateError = _dereq_("./errors").AggregateError;
            var isArray = util.isArray;
            var CANCELLATION = {};
            function SomePromiseArray(values) {
              this.constructor$(values);
              this._howMany = 0;
              this._unwrap = false;
              this._initialized = false;
            }
            util.inherits(SomePromiseArray, PromiseArray);
            SomePromiseArray.prototype._init = function() {
              if (!this._initialized) {
                return;
              }
              if (this._howMany === 0) {
                this._resolve([]);
                return;
              }
              this._init$(undefined, -5);
              var isArrayResolved = isArray(this._values);
              if (!this._isResolved() && isArrayResolved && this._howMany > this._canPossiblyFulfill()) {
                this._reject(this._getRangeError(this.length()));
              }
            };
            SomePromiseArray.prototype.init = function() {
              this._initialized = true;
              this._init();
            };
            SomePromiseArray.prototype.setUnwrap = function() {
              this._unwrap = true;
            };
            SomePromiseArray.prototype.howMany = function() {
              return this._howMany;
            };
            SomePromiseArray.prototype.setHowMany = function(count) {
              this._howMany = count;
            };
            SomePromiseArray.prototype._promiseFulfilled = function(value) {
              this._addFulfilled(value);
              if (this._fulfilled() === this.howMany()) {
                this._values.length = this.howMany();
                if (this.howMany() === 1 && this._unwrap) {
                  this._resolve(this._values[0]);
                } else {
                  this._resolve(this._values);
                }
                return true;
              }
              return false;
            };
            SomePromiseArray.prototype._promiseRejected = function(reason) {
              this._addRejected(reason);
              return this._checkOutcome();
            };
            SomePromiseArray.prototype._promiseCancelled = function() {
              if (this._values instanceof Promise || this._values == null) {
                return this._cancel();
              }
              this._addRejected(CANCELLATION);
              return this._checkOutcome();
            };
            SomePromiseArray.prototype._checkOutcome = function() {
              if (this.howMany() > this._canPossiblyFulfill()) {
                var e = new AggregateError();
                for (var i = this.length(); i < this._values.length; ++i) {
                  if (this._values[i] !== CANCELLATION) {
                    e.push(this._values[i]);
                  }
                }
                if (e.length > 0) {
                  this._reject(e);
                } else {
                  this._cancel();
                }
                return true;
              }
              return false;
            };
            SomePromiseArray.prototype._fulfilled = function() {
              return this._totalResolved;
            };
            SomePromiseArray.prototype._rejected = function() {
              return this._values.length - this.length();
            };
            SomePromiseArray.prototype._addRejected = function(reason) {
              this._values.push(reason);
            };
            SomePromiseArray.prototype._addFulfilled = function(value) {
              this._values[this._totalResolved++] = value;
            };
            SomePromiseArray.prototype._canPossiblyFulfill = function() {
              return this.length() - this._rejected();
            };
            SomePromiseArray.prototype._getRangeError = function(count) {
              var message = "Input array must contain at least " + this._howMany + " items but contains only " + count + " items";
              return new RangeError(message);
            };
            SomePromiseArray.prototype._resolveEmptyArray = function() {
              this._reject(this._getRangeError(0));
            };
            function some(promises, howMany) {
              if ((howMany | 0) !== howMany || howMany < 0) {
                return apiRejection("expecting a positive integer\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              var ret = new SomePromiseArray(promises);
              var promise = ret.promise();
              ret.setHowMany(howMany);
              ret.init();
              return promise;
            }
            Promise.some = function(promises, howMany) {
              return some(promises, howMany);
            };
            Promise.prototype.some = function(howMany) {
              return some(this, howMany);
            };
            Promise._SomePromiseArray = SomePromiseArray;
          };
        }, {
          "./errors": 12,
          "./util": 36
        }],
        32: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise) {
            function PromiseInspection(promise) {
              if (promise !== undefined) {
                promise = promise._target();
                this._bitField = promise._bitField;
                this._settledValueField = promise._isFateSealed() ? promise._settledValue() : undefined;
              } else {
                this._bitField = 0;
                this._settledValueField = undefined;
              }
            }
            PromiseInspection.prototype._settledValue = function() {
              return this._settledValueField;
            };
            var value = PromiseInspection.prototype.value = function() {
              if (!this.isFulfilled()) {
                throw new TypeError("cannot get fulfillment value of a non-fulfilled promise\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              return this._settledValue();
            };
            var reason = PromiseInspection.prototype.error = PromiseInspection.prototype.reason = function() {
              if (!this.isRejected()) {
                throw new TypeError("cannot get rejection reason of a non-rejected promise\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
              }
              return this._settledValue();
            };
            var isFulfilled = PromiseInspection.prototype.isFulfilled = function() {
              return (this._bitField & 33554432) !== 0;
            };
            var isRejected = PromiseInspection.prototype.isRejected = function() {
              return (this._bitField & 16777216) !== 0;
            };
            var isPending = PromiseInspection.prototype.isPending = function() {
              return (this._bitField & 50397184) === 0;
            };
            var isResolved = PromiseInspection.prototype.isResolved = function() {
              return (this._bitField & 50331648) !== 0;
            };
            PromiseInspection.prototype.isCancelled = Promise.prototype._isCancelled = function() {
              return (this._bitField & 65536) === 65536;
            };
            Promise.prototype.isCancelled = function() {
              return this._target()._isCancelled();
            };
            Promise.prototype.isPending = function() {
              return isPending.call(this._target());
            };
            Promise.prototype.isRejected = function() {
              return isRejected.call(this._target());
            };
            Promise.prototype.isFulfilled = function() {
              return isFulfilled.call(this._target());
            };
            Promise.prototype.isResolved = function() {
              return isResolved.call(this._target());
            };
            Promise.prototype.value = function() {
              return value.call(this._target());
            };
            Promise.prototype.reason = function() {
              var target = this._target();
              target._unsetRejectionIsUnhandled();
              return reason.call(target);
            };
            Promise.prototype._value = function() {
              return this._settledValue();
            };
            Promise.prototype._reason = function() {
              this._unsetRejectionIsUnhandled();
              return this._settledValue();
            };
            Promise.PromiseInspection = PromiseInspection;
          };
        }, {}],
        33: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL) {
            var util = _dereq_("./util");
            var errorObj = util.errorObj;
            var isObject = util.isObject;
            function tryConvertToPromise(obj, context) {
              if (isObject(obj)) {
                if (obj instanceof Promise)
                  return obj;
                var then = getThen(obj);
                if (then === errorObj) {
                  if (context)
                    context._pushContext();
                  var ret = Promise.reject(then.e);
                  if (context)
                    context._popContext();
                  return ret;
                } else if (typeof then === "function") {
                  if (isAnyBluebirdPromise(obj)) {
                    var ret = new Promise(INTERNAL);
                    obj._then(ret._fulfill, ret._reject, undefined, ret, null);
                    return ret;
                  }
                  return doThenable(obj, then, context);
                }
              }
              return obj;
            }
            function doGetThen(obj) {
              return obj.then;
            }
            function getThen(obj) {
              try {
                return doGetThen(obj);
              } catch (e) {
                errorObj.e = e;
                return errorObj;
              }
            }
            var hasProp = {}.hasOwnProperty;
            function isAnyBluebirdPromise(obj) {
              return hasProp.call(obj, "_promise0");
            }
            function doThenable(x, then, context) {
              var promise = new Promise(INTERNAL);
              var ret = promise;
              if (context)
                context._pushContext();
              promise._captureStackTrace();
              if (context)
                context._popContext();
              var synchronous = true;
              var result = util.tryCatch(then).call(x, resolve, reject);
              synchronous = false;
              if (promise && result === errorObj) {
                promise._rejectCallback(result.e, true, true);
                promise = null;
              }
              function resolve(value) {
                if (!promise)
                  return;
                promise._resolveCallback(value);
                promise = null;
              }
              function reject(reason) {
                if (!promise)
                  return;
                promise._rejectCallback(reason, synchronous, true);
                promise = null;
              }
              return ret;
            }
            return tryConvertToPromise;
          };
        }, {"./util": 36}],
        34: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, INTERNAL, debug) {
            var util = _dereq_("./util");
            var TimeoutError = Promise.TimeoutError;
            function HandleWrapper(handle) {
              this.handle = handle;
            }
            HandleWrapper.prototype._resultCancelled = function() {
              clearTimeout(this.handle);
            };
            var afterValue = function(value) {
              return delay(+this).thenReturn(value);
            };
            var delay = Promise.delay = function(ms, value) {
              var ret;
              var handle;
              if (value !== undefined) {
                ret = Promise.resolve(value)._then(afterValue, null, null, ms, undefined);
                if (debug.cancellation() && value instanceof Promise) {
                  ret._setOnCancel(value);
                }
              } else {
                ret = new Promise(INTERNAL);
                handle = setTimeout(function() {
                  ret._fulfill();
                }, +ms);
                if (debug.cancellation()) {
                  ret._setOnCancel(new HandleWrapper(handle));
                }
              }
              ret._setAsyncGuaranteed();
              return ret;
            };
            Promise.prototype.delay = function(ms) {
              return delay(ms, this);
            };
            var afterTimeout = function(promise, message, parent) {
              var err;
              if (typeof message !== "string") {
                if (message instanceof Error) {
                  err = message;
                } else {
                  err = new TimeoutError("operation timed out");
                }
              } else {
                err = new TimeoutError(message);
              }
              util.markAsOriginatingFromRejection(err);
              promise._attachExtraTrace(err);
              promise._reject(err);
              if (parent != null) {
                parent.cancel();
              }
            };
            function successClear(value) {
              clearTimeout(this.handle);
              return value;
            }
            function failureClear(reason) {
              clearTimeout(this.handle);
              throw reason;
            }
            Promise.prototype.timeout = function(ms, message) {
              ms = +ms;
              var ret,
                  parent;
              var handleWrapper = new HandleWrapper(setTimeout(function timeoutTimeout() {
                if (ret.isPending()) {
                  afterTimeout(ret, message, parent);
                }
              }, ms));
              if (debug.cancellation()) {
                parent = this.then();
                ret = parent._then(successClear, failureClear, undefined, handleWrapper, undefined);
                ret._setOnCancel(handleWrapper);
              } else {
                ret = this._then(successClear, failureClear, undefined, handleWrapper, undefined);
              }
              return ret;
            };
          };
        }, {"./util": 36}],
        35: [function(_dereq_, module, exports) {
          "use strict";
          module.exports = function(Promise, apiRejection, tryConvertToPromise, createContext, INTERNAL, debug) {
            var util = _dereq_("./util");
            var TypeError = _dereq_("./errors").TypeError;
            var inherits = _dereq_("./util").inherits;
            var errorObj = util.errorObj;
            var tryCatch = util.tryCatch;
            function thrower(e) {
              setTimeout(function() {
                throw e;
              }, 0);
            }
            function castPreservingDisposable(thenable) {
              var maybePromise = tryConvertToPromise(thenable);
              if (maybePromise !== thenable && typeof thenable._isDisposable === "function" && typeof thenable._getDisposer === "function" && thenable._isDisposable()) {
                maybePromise._setDisposable(thenable._getDisposer());
              }
              return maybePromise;
            }
            function dispose(resources, inspection) {
              var i = 0;
              var len = resources.length;
              var ret = new Promise(INTERNAL);
              function iterator() {
                if (i >= len)
                  return ret._fulfill();
                var maybePromise = castPreservingDisposable(resources[i++]);
                if (maybePromise instanceof Promise && maybePromise._isDisposable()) {
                  try {
                    maybePromise = tryConvertToPromise(maybePromise._getDisposer().tryDispose(inspection), resources.promise);
                  } catch (e) {
                    return thrower(e);
                  }
                  if (maybePromise instanceof Promise) {
                    return maybePromise._then(iterator, thrower, null, null, null);
                  }
                }
                iterator();
              }
              iterator();
              return ret;
            }
            function Disposer(data, promise, context) {
              this._data = data;
              this._promise = promise;
              this._context = context;
            }
            Disposer.prototype.data = function() {
              return this._data;
            };
            Disposer.prototype.promise = function() {
              return this._promise;
            };
            Disposer.prototype.resource = function() {
              if (this.promise().isFulfilled()) {
                return this.promise().value();
              }
              return null;
            };
            Disposer.prototype.tryDispose = function(inspection) {
              var resource = this.resource();
              var context = this._context;
              if (context !== undefined)
                context._pushContext();
              var ret = resource !== null ? this.doDispose(resource, inspection) : null;
              if (context !== undefined)
                context._popContext();
              this._promise._unsetDisposable();
              this._data = null;
              return ret;
            };
            Disposer.isDisposer = function(d) {
              return (d != null && typeof d.resource === "function" && typeof d.tryDispose === "function");
            };
            function FunctionDisposer(fn, promise, context) {
              this.constructor$(fn, promise, context);
            }
            inherits(FunctionDisposer, Disposer);
            FunctionDisposer.prototype.doDispose = function(resource, inspection) {
              var fn = this.data();
              return fn.call(resource, resource, inspection);
            };
            function maybeUnwrapDisposer(value) {
              if (Disposer.isDisposer(value)) {
                this.resources[this.index]._setDisposable(value);
                return value.promise();
              }
              return value;
            }
            function ResourceList(length) {
              this.length = length;
              this.promise = null;
              this[length - 1] = null;
            }
            ResourceList.prototype._resultCancelled = function() {
              var len = this.length;
              for (var i = 0; i < len; ++i) {
                var item = this[i];
                if (item instanceof Promise) {
                  item.cancel();
                }
              }
            };
            Promise.using = function() {
              var len = arguments.length;
              if (len < 2)
                return apiRejection("you must pass at least 2 arguments to Promise.using");
              var fn = arguments[len - 1];
              if (typeof fn !== "function") {
                return apiRejection("expecting a function but got " + util.classString(fn));
              }
              var input;
              var spreadArgs = true;
              if (len === 2 && Array.isArray(arguments[0])) {
                input = arguments[0];
                len = input.length;
                spreadArgs = false;
              } else {
                input = arguments;
                len--;
              }
              var resources = new ResourceList(len);
              for (var i = 0; i < len; ++i) {
                var resource = input[i];
                if (Disposer.isDisposer(resource)) {
                  var disposer = resource;
                  resource = resource.promise();
                  resource._setDisposable(disposer);
                } else {
                  var maybePromise = tryConvertToPromise(resource);
                  if (maybePromise instanceof Promise) {
                    resource = maybePromise._then(maybeUnwrapDisposer, null, null, {
                      resources: resources,
                      index: i
                    }, undefined);
                  }
                }
                resources[i] = resource;
              }
              var reflectedResources = new Array(resources.length);
              for (var i = 0; i < reflectedResources.length; ++i) {
                reflectedResources[i] = Promise.resolve(resources[i]).reflect();
              }
              var resultPromise = Promise.all(reflectedResources).then(function(inspections) {
                for (var i = 0; i < inspections.length; ++i) {
                  var inspection = inspections[i];
                  if (inspection.isRejected()) {
                    errorObj.e = inspection.error();
                    return errorObj;
                  } else if (!inspection.isFulfilled()) {
                    resultPromise.cancel();
                    return;
                  }
                  inspections[i] = inspection.value();
                }
                promise._pushContext();
                fn = tryCatch(fn);
                var ret = spreadArgs ? fn.apply(undefined, inspections) : fn(inspections);
                var promiseCreated = promise._popContext();
                debug.checkForgottenReturns(ret, promiseCreated, "Promise.using", promise);
                return ret;
              });
              var promise = resultPromise.lastly(function() {
                var inspection = new Promise.PromiseInspection(resultPromise);
                return dispose(resources, inspection);
              });
              resources.promise = promise;
              promise._setOnCancel(resources);
              return promise;
            };
            Promise.prototype._setDisposable = function(disposer) {
              this._bitField = this._bitField | 131072;
              this._disposer = disposer;
            };
            Promise.prototype._isDisposable = function() {
              return (this._bitField & 131072) > 0;
            };
            Promise.prototype._getDisposer = function() {
              return this._disposer;
            };
            Promise.prototype._unsetDisposable = function() {
              this._bitField = this._bitField & (~131072);
              this._disposer = undefined;
            };
            Promise.prototype.disposer = function(fn) {
              if (typeof fn === "function") {
                return new FunctionDisposer(fn, this, createContext());
              }
              throw new TypeError();
            };
          };
        }, {
          "./errors": 12,
          "./util": 36
        }],
        36: [function(_dereq_, module, exports) {
          "use strict";
          var es5 = _dereq_("./es5");
          var canEvaluate = typeof navigator == "undefined";
          var errorObj = {e: {}};
          var tryCatchTarget;
          var globalObject = typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : this !== undefined ? this : null;
          function tryCatcher() {
            try {
              var target = tryCatchTarget;
              tryCatchTarget = null;
              return target.apply(this, arguments);
            } catch (e) {
              errorObj.e = e;
              return errorObj;
            }
          }
          function tryCatch(fn) {
            tryCatchTarget = fn;
            return tryCatcher;
          }
          var inherits = function(Child, Parent) {
            var hasProp = {}.hasOwnProperty;
            function T() {
              this.constructor = Child;
              this.constructor$ = Parent;
              for (var propertyName in Parent.prototype) {
                if (hasProp.call(Parent.prototype, propertyName) && propertyName.charAt(propertyName.length - 1) !== "$") {
                  this[propertyName + "$"] = Parent.prototype[propertyName];
                }
              }
            }
            T.prototype = Parent.prototype;
            Child.prototype = new T();
            return Child.prototype;
          };
          function isPrimitive(val) {
            return val == null || val === true || val === false || typeof val === "string" || typeof val === "number";
          }
          function isObject(value) {
            return typeof value === "function" || typeof value === "object" && value !== null;
          }
          function maybeWrapAsError(maybeError) {
            if (!isPrimitive(maybeError))
              return maybeError;
            return new Error(safeToString(maybeError));
          }
          function withAppended(target, appendee) {
            var len = target.length;
            var ret = new Array(len + 1);
            var i;
            for (i = 0; i < len; ++i) {
              ret[i] = target[i];
            }
            ret[i] = appendee;
            return ret;
          }
          function getDataPropertyOrDefault(obj, key, defaultValue) {
            if (es5.isES5) {
              var desc = Object.getOwnPropertyDescriptor(obj, key);
              if (desc != null) {
                return desc.get == null && desc.set == null ? desc.value : defaultValue;
              }
            } else {
              return {}.hasOwnProperty.call(obj, key) ? obj[key] : undefined;
            }
          }
          function notEnumerableProp(obj, name, value) {
            if (isPrimitive(obj))
              return obj;
            var descriptor = {
              value: value,
              configurable: true,
              enumerable: false,
              writable: true
            };
            es5.defineProperty(obj, name, descriptor);
            return obj;
          }
          function thrower(r) {
            throw r;
          }
          var inheritedDataKeys = (function() {
            var excludedPrototypes = [Array.prototype, Object.prototype, Function.prototype];
            var isExcludedProto = function(val) {
              for (var i = 0; i < excludedPrototypes.length; ++i) {
                if (excludedPrototypes[i] === val) {
                  return true;
                }
              }
              return false;
            };
            if (es5.isES5) {
              var getKeys = Object.getOwnPropertyNames;
              return function(obj) {
                var ret = [];
                var visitedKeys = Object.create(null);
                while (obj != null && !isExcludedProto(obj)) {
                  var keys;
                  try {
                    keys = getKeys(obj);
                  } catch (e) {
                    return ret;
                  }
                  for (var i = 0; i < keys.length; ++i) {
                    var key = keys[i];
                    if (visitedKeys[key])
                      continue;
                    visitedKeys[key] = true;
                    var desc = Object.getOwnPropertyDescriptor(obj, key);
                    if (desc != null && desc.get == null && desc.set == null) {
                      ret.push(key);
                    }
                  }
                  obj = es5.getPrototypeOf(obj);
                }
                return ret;
              };
            } else {
              var hasProp = {}.hasOwnProperty;
              return function(obj) {
                if (isExcludedProto(obj))
                  return [];
                var ret = [];
                enumeration: for (var key in obj) {
                  if (hasProp.call(obj, key)) {
                    ret.push(key);
                  } else {
                    for (var i = 0; i < excludedPrototypes.length; ++i) {
                      if (hasProp.call(excludedPrototypes[i], key)) {
                        continue enumeration;
                      }
                    }
                    ret.push(key);
                  }
                }
                return ret;
              };
            }
          })();
          var thisAssignmentPattern = /this\s*\.\s*\S+\s*=/;
          function isClass(fn) {
            try {
              if (typeof fn === "function") {
                var keys = es5.names(fn.prototype);
                var hasMethods = es5.isES5 && keys.length > 1;
                var hasMethodsOtherThanConstructor = keys.length > 0 && !(keys.length === 1 && keys[0] === "constructor");
                var hasThisAssignmentAndStaticMethods = thisAssignmentPattern.test(fn + "") && es5.names(fn).length > 0;
                if (hasMethods || hasMethodsOtherThanConstructor || hasThisAssignmentAndStaticMethods) {
                  return true;
                }
              }
              return false;
            } catch (e) {
              return false;
            }
          }
          function toFastProperties(obj) {
            function FakeConstructor() {}
            FakeConstructor.prototype = obj;
            var l = 8;
            while (l--)
              new FakeConstructor();
            return obj;
            eval(obj);
          }
          var rident = /^[a-z$_][a-z$_0-9]*$/i;
          function isIdentifier(str) {
            return rident.test(str);
          }
          function filledRange(count, prefix, suffix) {
            var ret = new Array(count);
            for (var i = 0; i < count; ++i) {
              ret[i] = prefix + i + suffix;
            }
            return ret;
          }
          function safeToString(obj) {
            try {
              return obj + "";
            } catch (e) {
              return "[no string representation]";
            }
          }
          function isError(obj) {
            return obj !== null && typeof obj === "object" && typeof obj.message === "string" && typeof obj.name === "string";
          }
          function markAsOriginatingFromRejection(e) {
            try {
              notEnumerableProp(e, "isOperational", true);
            } catch (ignore) {}
          }
          function originatesFromRejection(e) {
            if (e == null)
              return false;
            return ((e instanceof Error["__BluebirdErrorTypes__"].OperationalError) || e["isOperational"] === true);
          }
          function canAttachTrace(obj) {
            return isError(obj) && es5.propertyIsWritable(obj, "stack");
          }
          var ensureErrorObject = (function() {
            if (!("stack" in new Error())) {
              return function(value) {
                if (canAttachTrace(value))
                  return value;
                try {
                  throw new Error(safeToString(value));
                } catch (err) {
                  return err;
                }
              };
            } else {
              return function(value) {
                if (canAttachTrace(value))
                  return value;
                return new Error(safeToString(value));
              };
            }
          })();
          function classString(obj) {
            return {}.toString.call(obj);
          }
          function copyDescriptors(from, to, filter) {
            var keys = es5.names(from);
            for (var i = 0; i < keys.length; ++i) {
              var key = keys[i];
              if (filter(key)) {
                try {
                  es5.defineProperty(to, key, es5.getDescriptor(from, key));
                } catch (ignore) {}
              }
            }
          }
          var asArray = function(v) {
            if (es5.isArray(v)) {
              return v;
            }
            return null;
          };
          if (typeof Symbol !== "undefined" && Symbol.iterator) {
            var ArrayFrom = typeof Array.from === "function" ? function(v) {
              return Array.from(v);
            } : function(v) {
              var ret = [];
              var it = v[Symbol.iterator]();
              var itResult;
              while (!((itResult = it.next()).done)) {
                ret.push(itResult.value);
              }
              return ret;
            };
            asArray = function(v) {
              if (es5.isArray(v)) {
                return v;
              } else if (v != null && typeof v[Symbol.iterator] === "function") {
                return ArrayFrom(v);
              }
              return null;
            };
          }
          var isNode = typeof process !== "undefined" && classString(process).toLowerCase() === "[object process]";
          function env(key, def) {
            return isNode ? process.env[key] : def;
          }
          function getNativePromise() {
            if (typeof Promise === "function") {
              try {
                var promise = new Promise(function() {});
                if ({}.toString.call(promise) === "[object Promise]") {
                  return Promise;
                }
              } catch (e) {}
            }
          }
          var ret = {
            isClass: isClass,
            isIdentifier: isIdentifier,
            inheritedDataKeys: inheritedDataKeys,
            getDataPropertyOrDefault: getDataPropertyOrDefault,
            thrower: thrower,
            isArray: es5.isArray,
            asArray: asArray,
            notEnumerableProp: notEnumerableProp,
            isPrimitive: isPrimitive,
            isObject: isObject,
            isError: isError,
            canEvaluate: canEvaluate,
            errorObj: errorObj,
            tryCatch: tryCatch,
            inherits: inherits,
            withAppended: withAppended,
            maybeWrapAsError: maybeWrapAsError,
            toFastProperties: toFastProperties,
            filledRange: filledRange,
            toString: safeToString,
            canAttachTrace: canAttachTrace,
            ensureErrorObject: ensureErrorObject,
            originatesFromRejection: originatesFromRejection,
            markAsOriginatingFromRejection: markAsOriginatingFromRejection,
            classString: classString,
            copyDescriptors: copyDescriptors,
            hasDevTools: typeof chrome !== "undefined" && chrome && typeof chrome.loadTimes === "function",
            isNode: isNode,
            env: env,
            global: globalObject,
            getNativePromise: getNativePromise
          };
          ret.isRecentNode = ret.isNode && (function() {
            var version = process.versions.node.split(".").map(Number);
            return (version[0] === 0 && version[1] > 10) || (version[0] > 0);
          })();
          if (ret.isNode)
            ret.toFastProperties(process);
          try {
            throw new Error();
          } catch (e) {
            ret.lastLineError = e;
          }
          module.exports = ret;
        }, {"./es5": 13}]
      }, {}, [4])(4);
    });
    ;
    if (typeof window !== 'undefined' && window !== null) {
      window.P = window.Promise;
    } else if (typeof self !== 'undefined' && self !== null) {
      self.P = self.Promise;
    }
  })();
  return _retrieveGlobal();
});

$__System.register("3d", [], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var PREFIX;
  return {
    setters: [],
    execute: function() {
      exports_1("PREFIX", PREFIX = "/licensing-assistant/api");
    }
  };
});

$__System.register("3e", [], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var Error;
  return {
    setters: [],
    execute: function() {
      Error = (function() {
        function Error(code, message, success) {
          this.code = code;
          this.message = message;
          this.success = success;
        }
        return Error;
      }());
      exports_1("Error", Error);
    }
  };
});

$__System.register("23", ["22", "3c", "3d", "3e"], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var lodash_1,
      bluebird_1,
      Config,
      model_ts_1;
  var API;
  return {
    setters: [function(lodash_1_1) {
      lodash_1 = lodash_1_1;
    }, function(bluebird_1_1) {
      bluebird_1 = bluebird_1_1;
    }, function(Config_1) {
      Config = Config_1;
    }, function(model_ts_1_1) {
      model_ts_1 = model_ts_1_1;
    }],
    execute: function() {
      (function(API) {
        var _licenses = null;
        var _licenseTerms = null;
        function path(method) {
          return Config.PREFIX + "/" + method;
        }
        function get(method) {
          return bluebird_1["default"].resolve($.getJSON(path(method))).then(function(data) {
            if (data.code)
              return bluebird_1["default"].reject(data);
            else
              return data;
          });
        }
        function licenses() {
          if (_licenses)
            return bluebird_1["default"].resolve(_licenses);
          else {
            return get("licenses").then(function(lic) {
              _licenses = lic;
              return lic;
            });
          }
        }
        API.licenses = licenses;
        function terms() {
          if (_licenseTerms)
            return bluebird_1["default"].resolve(_licenseTerms);
          else
            return licenses().then(function(lic) {
              var terms = lodash_1["default"].flatMap(lic, "terms");
              _licenseTerms = lodash_1["default"].uniqBy(lodash_1["default"].flatten(terms), "id");
              return _licenseTerms;
            });
        }
        API.terms = terms;
        function license(shortName) {
          if (_licenses) {
            var found = lodash_1["default"].find(_licenses, function(val) {
              return val.shortName === shortName;
            });
            if (found)
              return bluebird_1["default"].resolve(found);
            else
              return bluebird_1["default"].reject(new model_ts_1.Error(42, "license not found"));
          } else
            return get("licenses/" + shortName);
        }
        API.license = license;
      })(API = API || (API = {}));
      exports_1("API", API);
    }
  };
});

$__System.register("3f", ["2a", "2c", "22", "1c", "23"], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var _tplTemplate,
      lodash_1,
      i18next_1,
      api_ts_1;
  var ShowLicense;
  return {
    setters: [function(_1) {}, function(_tplTemplate_1) {
      _tplTemplate = _tplTemplate_1;
    }, function(lodash_1_1) {
      lodash_1 = lodash_1_1;
    }, function(i18next_1_1) {
      i18next_1 = i18next_1_1;
    }, function(api_ts_1_1) {
      api_ts_1 = api_ts_1_1;
    }],
    execute: function() {
      ShowLicense = (function() {
        function ShowLicense(el, shortName) {
          var _this = this;
          this.el = el;
          this.shortName = shortName;
          this.tplOpts = {'imports': {
              'jq': jQuery,
              'i18n': function(p) {
                return i18next_1["default"].t(p);
              }
            }};
          this.template = lodash_1["default"].template(_tplTemplate.default(), this.tplOpts);
          api_ts_1.API.license(shortName).then(function(lic) {
            _this.license = lic;
            el = el.html(_this.template({
              'name': lic.name,
              'url': lic.url,
              'description': lic.description,
              'permissions': lodash_1["default"].filter(lic.terms, function(t) {
                return t.type == "Permission";
              }),
              'obligations': lodash_1["default"].filter(lic.terms, function(t) {
                return t.type == "Obligation";
              }),
              'prohibitions': lodash_1["default"].filter(lic.terms, function(t) {
                return t.type == "Prohibition";
              }),
              'compatibleLicenses': lic.compatibleLicenses
            }));
          }).catch(function(err) {
            console.dir(err);
            throw err;
          });
        }
        return ShowLicense;
      }());
      exports_1("ShowLicense", ShowLicense);
    }
  };
});

$__System.register("40", ["1b", "21", "29", "3f"], function(exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var i18n_ts_1,
      filter_licenses_ts_1,
      licensing_wizard_ts_1,
      show_license_ts_1;
  return {
    setters: [function(i18n_ts_1_1) {
      i18n_ts_1 = i18n_ts_1_1;
    }, function(filter_licenses_ts_1_1) {
      filter_licenses_ts_1 = filter_licenses_ts_1_1;
    }, function(licensing_wizard_ts_1_1) {
      licensing_wizard_ts_1 = licensing_wizard_ts_1_1;
    }, function(show_license_ts_1_1) {
      show_license_ts_1 = show_license_ts_1_1;
    }],
    execute: function() {
      console.log("LA loaded. Translations loaded. " + i18n_ts_1["default"]);
      window.LicenseAssistant = {
        FilterLicenses: filter_licenses_ts_1.FilterLicenses,
        LicensingWizard: licensing_wizard_ts_1.LicensingWizard,
        ShowLicense: show_license_ts_1.ShowLicense
      };
    }
  };
});

$__System.register('1', ['40'], function (_export) {
  'use strict';

  return {
    setters: [function (_) {}],
    execute: function () {}
  };
});
$__System.register('lib/filter_licenses/filter_licenses.scss!github:mobilexag/plugin-sass@0.4.3/index.js', [], false, function() {});
$__System.register('lib/licensing_wizard/licensing_wizard.scss!github:mobilexag/plugin-sass@0.4.3/index.js', [], false, function() {});
$__System.register('lib/show_license/show_license.scss!github:mobilexag/plugin-sass@0.4.3/index.js', [], false, function() {});
(function(c){if (typeof document == 'undefined') return; var d=document,a='appendChild',i='styleSheet',s=d.createElement('style');s.type='text/css';d.getElementsByTagName('head')[0][a](s);s[i]?s[i].cssText=c:s[a](d.createTextNode(c));})
(".compa-heading{font-size:16px;font-weight:bold}\n.stepwizard-step p{margin-top:10px}.stepwizard-row{display:table-row}.stepwizard{display:table;width:100%;position:relative}.stepwizard-step button[disabled]{opacity:1 !important;filter:alpha(opacity=100) !important}.stepwizard-row:before{top:14px;bottom:0;position:absolute;content:\" \";width:100%;height:1px;background-color:#ccc;z-order:0}.stepwizard-step{display:table-cell;text-align:center;position:relative}.btn-circle{width:30px;height:30px;text-align:center !important;padding:6px 0 !important;font-size:12px !important;line-height:1.428571429 !important;border-radius:15px !important}\n");
})
(function(factory) {
  if (typeof define == 'function' && define.amd)
    define([], factory);
  else if (typeof module == 'object' && module.exports && typeof require == 'function')
    module.exports = factory();
  else
    factory();
});
//# sourceMappingURL=lib.js.map