$(function() {
    window.showLicense = function(a) {
        new LicenseAssistant.ShowLicense($("#target"), a)
    };
    $("#btn_show_license").unbind("click").click(function(a) {
        a.preventDefault();
        var b = "DL-DE-BY1.0";
        new LicenseAssistant.ShowLicense($("#target"), b), console.log("Showing license: " + b)
    });
    $("#btn_list_licenses").unbind("click").click(function(a) {
        a.preventDefault(), new LicenseAssistant.FilterLicenses($("#target"))
    });
    $("#btn_licensing_wizard").unbind("click").click(function(a) {
        a.preventDefault(), new LicenseAssistant.LicensingWizard($("#target"))
    });
});
