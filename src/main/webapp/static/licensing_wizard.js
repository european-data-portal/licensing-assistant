$(function() {
    window.showLicense = function(name) {
        new LicenseAssistant.ShowLicense($("#la_target"), name);
    };

    new LicenseAssistant.LicensingWizard($("#la_target"));
});
