$(function() {
    window.showLicense = function(name) {
        document.location.href = "show-license?license_id=" + encodeURIComponent(name);
    };
    new LicenseAssistant.FilterLicenses($("#la_target"));
});
